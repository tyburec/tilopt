#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <memory>

class tile
{
public:
	typedef short int tcode;
	enum svgPlotMode
	{
		COLOR,
		TEXT,
		SVGDATA,
		SVGCOLOR
	};

protected:
	tcode code1; // north OR northwest
	tcode code2; // south OR southwest
	tcode code3; // east OR southeast
	tcode code4; // west OR northeast
	std::vector<bool> constNorth;
	std::vector<bool> constSouth;
	std::vector<bool> constEast;
	std::vector<bool> constWest;
	std::vector<bool> constNorthEast;
	std::vector<bool> constNorthWest;
	std::vector<bool> constSouthEast;
	std::vector<bool> constSouthWest;
	int numEdgeColors;
	int numVertexColors;
	std::string svgData;
	svgPlotMode plotMode;

public:
	void setValues(tcode code1, tcode code2, tcode code3, tcode code4);
	std::string printColor(int val) const;
	std::string printColorRGB(int val) const;
	void setPrintMode(tile::svgPlotMode mode);
	tile::svgPlotMode getPrintMode();

	void setConstNorth(std::vector<bool> constNorth) { tile::constNorth = constNorth; };
	void setConstSouth(std::vector<bool> constSouth) { tile::constSouth = constSouth; };
	void setConstEast(std::vector<bool> constEast) { tile::constEast = constEast; };
	void setConstWest(std::vector<bool> constWest) { tile::constWest = constWest; };
	void setConstNorthEast(std::vector<bool> constNorthEast) { tile::constNorthEast = constNorthEast; };
	void setConstNorthWest(std::vector<bool> constNorthWest) { tile::constNorthWest = constNorthWest; };
	void setConstSouthEast(std::vector<bool> constSouthEast) { tile::constSouthEast = constSouthEast; };
	void setConstSouthWest(std::vector<bool> constSouthWest) { tile::constSouthWest = constSouthWest; };

	const std::vector<bool> &getConstNorth() const { return constNorth; };
	const std::vector<bool> &getConstSouth() const { return constSouth; };
	const std::vector<bool> &getConstEast() const { return constEast; };
	const std::vector<bool> &getConstWest() const { return constWest; };
	const std::vector<bool> &getConstNorthEast() const { return constNorthEast; };
	const std::vector<bool> &getConstNorthWest() const { return constNorthWest; };
	const std::vector<bool> &getConstSouthEast() const { return constSouthEast; };
	const std::vector<bool> &getConstSouthWest() const { return constSouthWest; };

	virtual tcode getNorth() const = 0;
	virtual tcode getSouth() const = 0;
	virtual tcode getEast() const = 0;
	virtual tcode getWest() const = 0;
	virtual tcode getNorthEast() const = 0;
	virtual tcode getNorthWest() const = 0;
	virtual tcode getSouthEast() const = 0;
	virtual tcode getSouthWest() const = 0;

	void setSvgData(std::string d) { svgData = d; };
	std::string getSvgData(int id);
	virtual std::string getSvgData(int id, svgPlotMode spm) const = 0;

	void printTile() const;
	virtual void printTile(int line) const = 0;

	virtual std::shared_ptr<tile> clone() const = 0;

	tile() { setPrintMode(tile::svgPlotMode::COLOR); };
	tile(tcode code1, tcode code2, tcode code3, tcode code4);
	virtual ~tile(){};
};

void tile::setPrintMode(tile::svgPlotMode mode)
{
	plotMode = mode;
}

tile::svgPlotMode tile::getPrintMode()
{
	return plotMode;
}

std::string tile::getSvgData(int id)
{
	std::string out = getSvgData(id, getPrintMode());
	return out;
}

void tile::setValues(tcode code1, tcode code2, tcode code3, tcode code4)
{
	tile::code1 = code1;
	tile::code2 = code2;
	tile::code3 = code3;
	tile::code4 = code4;
}

tile::tile(tcode code1, tcode code2, tcode code3, tcode code4)
{
	setPrintMode(tile::svgPlotMode::COLOR);
	tile::setValues(code1, code2, code3, code4);
}

void tile::printTile() const
{
	for (int i = 0; i < 4; i++)
	{
		printTile(i);
		std::cout << std::endl;
	}
}

std::string tile::printColorRGB(int val) const
{
	if (val > 255)
	{
		std::cerr << "ERROR in tile::printColorRGB(): The program can currently plot only 256 colors." << std::endl;
		exit(2);
	}
	double goldenRatio = 0.618033988749895;
	std::string rgb;
	double r, g, b;
	double h = goldenRatio * val * 360 + 30;
	h = fmod(h, 360);
	double s = 0.60 - fmod(val, 11) / 22 + 10 / 44;
	double v = 0.95 - fmod(val, 4) / 10;

	(h == 360.0) ? (h = 0.0) : (h /= 60.0);
	int fract = h - floor(h);

	double p = v * (1.0 - s);
	double q = v * (1.0 - s * fract);
	double t = v * (1.0 - s * (1.0 - fract));

	if (0.0 <= h && h < 1.0)
	{
		r = v;
		g = t;
		b = p;
	}
	else if (1.0 <= h && h < 2.0)
	{
		r = q;
		g = v;
		b = p;
	}
	else if (2.0 <= h && h < 3.0)
	{
		r = p;
		g = v;
		b = t;
	}
	else if (3.0 <= h && h < 4.0)
	{
		r = p;
		g = q;
		b = v;
	}
	else if (4.0 <= h && h < 5.0)
	{
		r = t;
		g = p;
		b = v;
	}
	else if (5.0 <= h && h < 6.0)
	{
		r = v;
		g = p;
		b = q;
	}
	else
	{
		r = 0.0;
		g = 0.0;
		b = 0.0;
	}

	rgb = "rgb(" + std::to_string(r * 255) + "," + std::to_string(g * 255) + "," + std::to_string(b * 255) + ")";
	return rgb;
}

std::string tile::printColor(int val) const
{
	if (val == -1)
	{
		std::string reset = "\033[0m";
		return reset;
	}
	else
	{
		if (val < 256)
		{
			std::string color = "\033[38;5;" + std::to_string(val) + "m";
			return color;
		}
		else
		{
			std::cerr << "ERROR in tile::printColor(): The program can currently plot only 256 colors." << std::endl;
			exit(2);
		}
	}
}
