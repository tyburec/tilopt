# Usage

- `tiling_optimizer tile TILESET_FILENAME WIDTH HEIGHT (COUNT)` will find and plot the `HEIGHT`x`WIDTH` tiling. If the `COUNT` option is specified, the program will find `COUNT` valid tilings if they exist.
- `tiling_optimizer reduce TILESET_FILENAME DEPTH` will inspect redundant tiles in (`DEPTH`*2+1)x(`DEPTH`*2+1) tilings, gradually placing individual tiles to the center, and produce `TILESET_FILENAME`\_reduced.tset tileset. If `DEPTH` is set to `inf`, the program will remove tiles that can not appear in infinite rows/columns/diagonals (diagonals only for vertex-based tile sets).
- `tiling_optimizer plotset TILESET_FILENAME` will plot the tile set.
- `tiling_optimizer periodic TILESET_FILENAME WIDTH HEIGHT` will search the smallest periodic domain upper-bounded by the `WIDTH`x`HEIGHT` size.
- `tiling_optimizer maxconarea TILESET_FILENAME WIDTH HEIGHT` will search the largest tileable simply-connected area within the `WIDTH`x`HEIGHT` domain.
- `tiling_optimizer maxconsquare TILESET_FILENAME WIDTH HEIGHT` will search the largest tileable simply-connected square within the `WIDTH`x`HEIGHT` domain.
- `tiling_optimizer maxcover TILESET_FILENAME WIDTH HEIGHT` will try to cover the maximum area of the `WIDTH`x`HEIGHT` domain. The covering may be disconnected.
- `tiling_optimizer maxcsp TILESET_FILENAME WIDTH HEIGHT` will try to create tiling `WIDTH`x`HEIGHT` such that the maximum number of color matches is satisfied.
- `tiling_optimizer convert TILESET_FILENAME METHOD` will convert vertex-based Wang tiles to edge-based Wang tiles using the `equivalent` method; or edge-based Wang tiles to vertex-based Wang tiles using one of the `diagonal_translation`, `horizontal_translation`, `vertical_translation`, `rotation`, `subdivision`, or `vertex_labeling` methods. Both the edge-based and vertex-based Wang tiles can be grouped into larger blocks (of edge-based Wang tiles) using the `grouping` method.
- `tiling_optimizer packing TILESET_FILENAME WIDTH HEIGHT (periodic)` finds and plots a (`periodic`) tile packing.
- `tiling_optimizer necessary TILESET_FILENAME WIDTH HEIGHT (tile)` inspectes whether individual tiles, or a tile `TILE`, in the tile set are required in all `WIDTH`x`HEIGHT` tilings
- `tiling_optimizer distmat TILESET_FILENAME` prints the graph distance matrices of given tile set.
- `tiling_optimizer heuristics TILESET_FILENAME WIDTH HEIGHT` generates the maximum-cover-based tiling using the DAG-shortest-path heuristics.

# List of (aperiodic) sets of Wang tiles

## 104 tiles (E, TODO)

- BERGER, Robert. The Undecidability of the Domino Problem. PhD thesis, Harvard University, 1964.
- _The original tile set is written wrongly. Ollinger's version of 103 tiles seems to tile at most 17x17 squares. Working version should be found!_

## 20,426 tiles (E)

- BERGER, Robert. The Undecidability of the Domino Problem. American Mathematical Soc., 1966.
- _Most of the tiles are claimed to be redundant._

## 92 tiles over 26 colors (E, ADDED)

- KNUTH, Donald E. Fundamental Algorithms, volume 1 of The Art of Computer Programming, section 2.3. 4.3 The infinity lemma. 1968.
- _The tileset can be reduced to 86 tiles, to be published in an answer to an exercise in Section 7.2.2.1._
- _One tile reduction is done by running `tiling_optimizer reduce 92_26_knuth_aperiodic_edge 1`_
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/92_26_knuth_aperiodic_edge.svg" width=100%>

## 40 tiles over 16 colors (E, ADDED)

- _Obtained by H. Läuchli in 1966, but published later._
- WANG, Hao. Notes on a class of tiling problems. Fundamenta Mathematicae, 1975, 82.4: 295-305.
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/40_16_lauchli_aperiodic_edge.svg" width=100%>

## 52 tiles over 8 colors (E, ADDED)

- ROBINSON, R. M. Seven polygons which permit only nonperiodic tilings of the plane. Notices Amer. Math. Soc, 1967, 14: 835.
- POIZAT, Bruno. Une théorie finiement axiomatisable et superstable. Groupe d’étude des théories stables, 1980, 3.1: 1-9.
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/52_8_robinson_aperiodic_edge.svg" width=100%>

## 56 tiles over 12 colors (E, ADDED)

- ROBINSON, Raphael M. Undecidability and nonperiodicity for tilings of the plane. Inventiones mathematicae, 1971, 12.3: 177-209.
- _The tile set was also published in Tilings and Patterns, with an error of missing arrow._
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/56_12_robinson_aperiodic_edge.svg" width=100%>

## 35 tiles (E)

- ROBINSON, Raphael M. Undecidability and nonperiodicity for tilings of the plane. Inventiones mathematicae, 1971, 12.3: 177-209.
- _Only mentioned existance._

## 34 tiles (E)

- _By Roger Penrose in 1973._

## 32 tiles over 16 colors (E, ADDED)

- GRÜNBAUM, Branko; SHEPHARD, Geoffrey Colin. Tilings and patterns. Freeman, 1987, [8, pp. 593].
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/32_16_robinson_aperiodic_edge.svg" width=100%>
- _By R. M. Robinson in 1973._

## 24 tiles over 24 colors (E, ADDED)

- GRÜNBAUM, Branko; SHEPHARD, Geoffrey Colin. Tilings and patterns. Freeman, 1987, [8, pp. 593].
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/24_24_robinson_aperiodic_edge.svg" width=100%>
- _By R. M. Robinson in 1977._

## 16 tiles over 6 colors (E, ADDED)

- GRÜNBAUM, Branko; SHEPHARD, Geoffrey Colin. Tilings and patterns. Freeman, 1987, [8, pp. 595];
- ROBINSON, Raphael M. Undecidable tiling problems in the hyperbolic plane. Inventiones mathematicae, 1978, 44.3: 259-264.
- _By R. Ammann in 1978._
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/16_6_ammann_aperiodic_edge.svg" width=100%>

## 24 tiles over 9 colors (E, ADDED)

- GRÜNBAUM, Branko; SHEPHARD, Geoffrey Colin. Tilings and patterns. Freeman, 1987, [8, pp. 593].
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/24_9_grunbaum_aperiodic_edge.svg" width=100%>
- Original contribution of the book.

## 64 tiles (E)

- SOCOLAR, SENECHAL, M. Quasicrystals and Geometry. Cambridge Univ. 1995.

## 14 tiles over 6 colors (E, ADDED)

- KARI J., KARI, Jarkko. A small aperiodic set of Wang tiles. Discrete Mathematics, 1996, 160.1-3: 259-264.
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/14_6_kari_aperiodic_edge.svg" width=100%>

## 13 tiles over 5 colors (E, ADDED)

- ČULÍK Karel. An aperiodic set of 13 Wang tiles. Discrete Mathematics, 1996, 160.1-3: 245-251.
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/13_5_culik_aperiodic_edge.svg" width=100%>

## 696 tiles (E)

- KARI, Jarkko; PAPASOGLU, Panagiotis. Deterministic aperiodic tile sets. Geometric and functional analysis, 1999, 9.2: 353-369.

## 44 tiles over 6 colors (V, ADDED)

- LAGAE, Ares; KARI, Jarkko; DUTRÉ, Philip. Aperiodic sets of square tiles with colored corners. Report CW, 2006, 460.
- _The tile set can be reduced to the Nurmi's set of 30 tiles, by running `tiling_optimizer reduce 44_6_lagae_aperiodic_vertex 1`._
- _The tile set is not aperidic! Prove by running `tiling_optimizer periodic 44_6_lagae_aperiodic_vertex 6`._
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/44_6_lagae_aperiodic_vertex.svg" width=100%>

## 104 tiles (E, TODO)

- OLLINGER, Nicolas. Two-by-two substitution systems and the undecidability of the domino problem. Logic and Theory of Algorithms, 2008, 476-485.
- _There is probably an error in the manuscript._

## 11 tiles over 5 colors (E, ADDED)

- JEANDEL, Emmanuel; RAO, Michael. An aperiodic set of 11 Wang tiles. arXiv preprint arXiv:1506.06492, 2015.
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/11_5_jeandel_aperiodic_edge.svg" width=100%>

## 11 tiles over 4 colors (E, ADDED)

- JEANDEL, Emmanuel; RAO, Michael. An aperiodic set of 11 Wang tiles. arXiv preprint arXiv:1506.06492, 2015.
- _The smallest possible aperiodic set of Wang tiles._
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/11_4_jeandel_aperiodic_edge.svg" width=100%>

## 30 tiles over 6 colors (V, ADDED)

- NURMI, Tuomas. From Checkerboard to Cloverfield: Using Wang Tiles in Seamless Non-Periodic Patterns. Bridges Finland Conference Proceedings, 2016.
- _Reduced Lagae's tile set._
- _The tile set is not aperidic! Prove by running `tiling_optimizer periodic 30_6_nurmi_aperiodic_vertex 6`_
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/30_6_nurmi_aperiodic_vertex.svg" width=100%>

## 19 tiles over 16 colors (E, ADDED)

- LABBÉ, Sébastien. A self-similar aperiodic set of 19 Wang tiles. In Geometriae Dedicata (Vol. 201, Issue 1, pp. 81–109). Springer Science and Business Media LLC. 2019. https://doi.org/10.1007/s10711-018-0384-8.
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/19_16_labbe_aperiodic_edge.svg" width=100%>

## 16 tiles over 8 colors (E, ADDED)

- LABBÉ, Sébastien; LEPŠOVÁ, Jana.. A Numeration System for Fibonacci-Like Wang Shifts. In Lecture Notes in Computer Science (pp. 104–116). Springer International Publishing. 2021. https://doi.org/10.1007/978-3-030-85088-3_9
  <img src="http://mech.fsv.cvut.cz/~tyburec/img/tilesets/16_8_labbe_aperiodic_edge.svg" width=100%>

# Conversions

## Aperiodic edge-based Wang tiles to aperiodic vertex-based Wang tiles

- The tile sets are reduced with `DEPTH` set to 10, except for tile sets marked with (\*) - they were reduced with `DEPTH` set to 5
- Tiles that can not be used in tilings of an infinite rows/columns/diagonals were also filtered out (valid mainly for Robinson's tile set of 56 tiles over 12 colors)
- Horizontal and vertical translation does not lead to bijective tilings, hence was not considered here

| Original set         | Diagonal translation |    Rotation |  Subdivision | Vertex labeling |
| :------------------- | -------------------: | ----------: | -----------: | --------------: |
| Jeandel, 11 over 4   |           57 over 11 |   36 over 8 |   44 over 16 |      95 over 25 |
| Jeandel, 11 over 5   |           55 over 11 |   34 over 9 |   44 over 17 |      81 over 23 |
| Čulík, 13 over 5     |           74 over 13 |   53 over 9 |   52 over 19 |     235 over 40 |
| Kari, 14 over 6      |          180 over 14 |   82 over 9 |   56 over 21 | (\*)825 over 68 |
| Ammann, 16 over 6    |           51 over 16 |  47 over 12 |   46 over 23 |      72 over 31 |
| Grünbaum, 24 over 9  |           72 over 24 |  66 over 18 |   96 over 34 |      98 over 42 |
| Robinson, 24 over 24 |           71 over 24 |  57 over 24 |   96 over 49 |      88 over 33 |
| Robinson, 32 over 16 |           86 over 32 |  84 over 28 |  128 over 49 |     112 over 52 |
| Läuchli, 40 over 16  |          128 over 40 | 104 over 32 |  160 over 57 |     172 over 64 |
| Robinson, 56 over 12 |          410 over 56 | 424 over 24 |  224 over 69 |   1156 over 352 |
| Knuth, 92 over 26    |          364 over 91 | 432 over 46 | 364 over 118 |    868 over 327 |

## Blockify aperiodic edge-based Wang tiles

- The tile sets are reduced with `DEPTH` set to 10
- `WIDTH`x`HEIGHT`

| Original set         |        1x2 |        2x1 |        1x3 | 3x1 | 2x2 | 1x4 | 4x1 |
| :------------------- | ---------: | ---------: | ---------: | --: | --: | --: | --: |
| Jeandel, 11 over 4   | 31 over 15 | 27 over 12 | 69 over 41 |     |     |     |     |
| Jeandel, 11 over 5   | 29 over 13 | 27 over 13 | 55 over 31 |     |     |     |     |
| Čulík, 13 over 5     |            |            |            |     |     |     |     |
| Kari, 14 over 6      |            |            |            |     |     |     |     |
| Ammann, 16 over 6    |            |            |            |     |     |     |     |
| Grünbaum, 24 over 9  |            |            |            |     |     |     |     |
| Robinson, 24 over 24 |            |            |            |     |     |     |     |
| Robinson, 32 over 16 |            |            |            |     |     |     |     |
| Läuchli, 40 over 16  |            |            |            |     |     |     |     |
| Robinson, 56 over 12 |            |            |            |     |     |     |     |
| Knuth, 92 over 26    |            |            |            |     |     |     |     |

## Notes

- In 1D, the domino problem is decidable. In other words, an infinite line can be tiled (using a finite count of dominoes) if and only if there exists a periodic solution. Additionaly, 2D tilings impose stronger constraints on tile placements; but still, each tile set needs to allow tiling of infinite rows/columns in a periodic fashion. Consequently, another simple and linear reduction method is stated as follows: 1) Create a directed graphs of both the horizontal and vertical connectivity information among tiles. 2) 1D periodical solution corresponds to a cycle in the graphs. We are interested in finding all vertices of the graphs (tiles) that do not lie in a cycle. This can be done by Tarjan's algorithm to find strongly connected components (set of vertices that are mutually reachable -- i.e., are connected though some cycles). 3) Find strongly connected components of size 1 and check if such cycle allows periodic row/column solution, i.e. the tile has equal horizontal, or vertical edges. 4) Mark the rest of 1-sized SCCs as redundant tiles.
