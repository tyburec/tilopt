#include <iostream>
#include "tiling.hpp"
#include <sstream>
#include <filesystem>
#include <string>

#ifdef DGUROBI
optimizerInterface::solverModel::solvers solver = optimizerInterface::solverModel::solvers::GUROBI;
#else
#ifdef DMOSEK
optimizerInterface::solverModel::solvers solver = optimizerInterface::solverModel::solvers::MOSEK;
#else
optimizerInterface::solverModel::solvers solver = optimizerInterface::solverModel::solvers::NONE;
#endif
#endif

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        std::cerr << "ERROR in main(): Too few input arguments!" << std::endl;
        exit(2);
    }
    // read input arguments
    std::string operation = argv[1];

    if (!operation.compare("reduce"))
    {
        if (argc < 4)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " reduce tileset_path depth\"" << std::endl;
            exit(2);
        }
        std::istringstream issD(argv[3]);
        std::string fname = argv[2];
        int depth = 0;
        if (((issD >> depth).fail()) || (depth < 1))
        {
            std::string isInf = argv[3];
            if (!isInf.compare("inf"))
            {
                depth = -1;
            }
            else
            {
                std::cerr << "ERROR in main(): depth has to be a positive integer, or inf." << std::endl;
                exit(2);
            }
        }

        if (depth > 0)
        {
            tiling tiling1(fname + ".tset", depth * 2 + 1, depth * 2 + 1);
            tiling1.setAllSolutions(true);
            tiling1.setObjType(tiling::objTypes::REDUNDANCY);
            tiling1.optimize(tiling::formulationTypes::LP_TILES, solver);
            const tileset &tset = tiling1.getTileSet();
            std::string newFName = fname + "_reduced_d" + std::to_string(depth);
            tset.writeTsetFile("./output/" + newFName);
            std::cout << "File " << newFName << ".tset written successfully." << std::endl;
        }
        else
        {
            tiling tiling1(fname + ".tset", 1, 1);
            tiling1.infiniteRowColumnReduce();
            const tileset &tset = tiling1.getTileSet();
            std::string newFName = fname + "_reduced_dINF";
            tset.writeTsetFile("./output/" + newFName);
            std::cout << "File " << newFName << ".tset written successfully." << std::endl;
        }
    }
    else if (!operation.compare("tile"))
    {
        if (argc < 5)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " tile tileset_path width height (count)\"" << std::endl;
            exit(2);
        }
        std::string fname = argv[2];
        int tileNum = 1;
        if (argc == 6)
        {
            std::istringstream issA(argv[5]);
            if (((issA >> tileNum).fail()) || (tileNum < 1))
            {
                std::cerr << "ERROR in main(): count has to be a positive integer." << std::endl;
                exit(2);
            }
        }
        std::istringstream issW(argv[3]), issH(argv[4]);
        int width = 0, height = 0;
        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }
        tiling tiling1(fname + ".tset", width, height);
        tiling1.setSolutionsCount(tileNum);
        tiling1.setObjType(tiling::objTypes::NONE);
        tiling1.optimize(tiling::formulationTypes::LP_TILES, solver);
        if (tiling1.getFeasibility())
        {
            tiling1.plotTilings();
            tiling1.plotTilingsSVG("./output/" + fname + std::to_string(width) + "x" + std::to_string(height));
        }
        else
        {
            std::cerr << "ERROR in main(): No solution exists!" << std::endl;
            exit(2);
        }
    }
    else if (!operation.compare("convert"))
    {
        if (argc < 4)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " convert tileset_path method\"" << std::endl;
            exit(2);
        }
        std::string fname = argv[2];
        tileset tset(fname + ".tset");
        std::string method = argv[3];

        if (!method.compare("grouping"))
        {
            if (argc < 6)
            {
                std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " convert tileset_path grouping width height\"" << std::endl;
                exit(2);
            }
            std::istringstream issW(argv[4]), issH(argv[5]);
            int width = 0, height = 0;
            if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
            {
                std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
                exit(2);
            }

            tiling tiling1(tset, width, height);
            tiling1.setAllSolutions(true);
            tiling1.optimize(tiling::formulationTypes::LP_TILES, solver);
            tileset groupedSet;
            std::vector<std::vector<std::vector<tile::tcode>>> assMat;
            tiling1.getAssemblyMatrix(assMat);
            tset.groupSet(groupedSet, assMat);
            groupedSet.writeTsetFile("./output/" + fname + "_grouping");
            std::cout << "File " << fname << "_grouping.tset written successfully." << std::endl;
        }
        else
        {
            tileset nSet;
            switch (tset.getSetType())
            {
            case tileset::setTypes::EDGE_BASED:
            {
                if (!method.compare("diagonal_translation"))
                {
                    tset.edgeToVertex(tileset::conversionMethod::DIAGONAL_TRANSLATION, nSet);
                    fname += "_diagonaltranslation";
                }
                else if (!method.compare("horizontal_translation"))
                {
                    tset.edgeToVertex(tileset::conversionMethod::HORIZONTAL_TRANSLATION, nSet);
                    fname += "_horizontaltranslation";
                }
                else if (!method.compare("vertical_translation"))
                {
                    tset.edgeToVertex(tileset::conversionMethod::VERTICAL_TRANSLATION, nSet);
                    fname += "_verticaltranslation";
                }
                else if (!method.compare("rotation"))
                {
                    tset.edgeToVertex(tileset::conversionMethod::ROTATION, nSet);
                    fname += "_rotation";
                }
                else if (!method.compare("subdivision"))
                {
                    tset.edgeToVertex(tileset::conversionMethod::SUBDIVISION, nSet);
                    fname += "_subdivision";
                }
                else if (!method.compare("vertex_labeling"))
                {
                    tset.edgeToVertex(tileset::conversionMethod::VERTEX_LABELING, nSet);
                    fname += "_vertexlabeling";
                }
                else
                {
                    std::cerr << "ERROR in main(): Unknown conversion method!" << std::endl;
                    exit(2);
                }
                break;
            }

            case tileset::setTypes::VERTEX_BASED:
            {
                if (!method.compare("equivalent"))
                {
                    tset.vertexToEdge(nSet);
                    fname += "_equivalent";
                }
                else
                {
                    std::cerr << "ERROR in main(): Unknown conversion method!" << std::endl;
                    exit(2);
                }
                break;
            }

            default:
            {
                std::cerr << "ERROR in main(): Unknown set type!" << std::endl;
                exit(2);
                break;
            }
            }
            nSet.writeTsetFile("./output/" + fname);
            std::cout << "File " << fname << ".tset written successfully." << std::endl;
        }
    }
    else if (!operation.compare("plotset"))
    {
        if (argc < 4)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " plotset tileset_path numcolumns\"" << std::endl;
            exit(2);
        }
        std::string fname = argv[2];
        std::istringstream issC(argv[3]);
        int columns;
        if (((issC >> columns).fail()) || (columns < 1))
        {
            std::cerr << "ERROR in main(): number of columns has to be a positive integer." << std::endl;
            exit(2);
        }
        tileset tset(fname + ".tset");
        tset.plotTileSet(columns);
        tset.plotTileSetSVG(columns, "./output/" + fname);
    }
    else if (!operation.compare("plottiling"))
    {
        if (argc < 2)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " plottiling tiling_path \"" << std::endl;
            exit(2);
        }
        std::string fname = argv[2];
        tiling newTiling(fname);
        // newTiling.plotTiling(0);
        std::filesystem::create_directory("output");
        newTiling.plotTilingSVG("./output/" + fname, 0);
    }
    else if (!operation.compare("periodic"))
    {
        if (argc < 5)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " periodic tileset_path width height\"" << std::endl;
            exit(2);
        }

        std::string fname = argv[2];
        std::istringstream issW(argv[3]), issH(argv[4]);
        int width = 0, height = 0;

        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }

        tiling tiling1(fname + ".tset", width, height);

        tiling1.setObjType(tiling::objTypes::MINPERIODIC);
        tiling1.optimize(tiling::formulationTypes::LP_TILES, solver);

        if (tiling1.getFeasibility())
        {
            tiling1.plotTilings();
            tiling1.plotTilingsSVG("./output/" + fname + std::to_string(width) + "x" + std::to_string(height));
        }
        else
        {
            std::cerr << "ERROR in main(): No solution exists!" << std::endl;
            exit(2);
        }
    }
    else if (!operation.compare("packing"))
    {
        if (argc < 5)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " packing tileset_path width height (periodic)\"" << std::endl;
            exit(2);
        }

        std::string fname = argv[2];
        std::istringstream issW(argv[3]), issH(argv[4]);
        bool isPeriodic = 0;

        if (argc == 6)
        {
            std::string par = argv[5];
            if (!par.compare("periodic"))
            {
                isPeriodic = true;
            }
        }

        int width = 0, height = 0;

        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }
        tiling tiling1(fname + ".tset", width, height);

        if (isPeriodic)
        {
            tiling1.addPeriodicBoundaryCondition();
        }
        tiling1.addPackingConstraint();
        tiling1.optimize(tiling::formulationTypes::LP_TILES, solver);

        if (tiling1.getFeasibility())
        {
            tiling1.plotTilings();
            tiling1.plotTilingsSVG("./output/" + fname + std::to_string(width) + "x" + std::to_string(height));
        }
        else
        {
            std::cerr << "ERROR in main(): No solution exists!" << std::endl;
            exit(2);
        }
    }
    else if (!operation.compare("maxconarea"))
    {
        if (argc < 5)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " maxconarea tileset_path width height\"" << std::endl;
            exit(2);
        }

        std::istringstream issW(argv[3]), issH(argv[4]);

        int width = 0, height = 0;

        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }
        std::string fname = argv[2];
        tiling tiling1(fname + ".tset", width, height);
        tiling1.setObjType(tiling::objTypes::MAXCONAREA);
        tiling1.optimize(tiling::formulationTypes::LP_TILES, solver);

        if (tiling1.getFeasibility())
        {
            tiling1.plotTilings();
            tiling1.plotTilingsSVG("./output/" + fname + std::to_string(width) + "x" + std::to_string(height));
        }
        else
        {
            std::cerr << "ERROR in main(): No solution exists!" << std::endl;
            exit(2);
        }
    }
    else if (!operation.compare("maxconsquare"))
    {
        if (argc < 5)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " maxconsquare tileset_path width height\"" << std::endl;
            exit(2);
        }

        std::string fname = argv[2];
        std::istringstream issW(argv[3]), issH(argv[4]);

        int width = 0, height = 0;

        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }
        tiling tiling1(fname + ".tset", width, height);

        tiling1.setObjType(tiling::objTypes::MAXCONSQUARE);
        tiling1.optimize(tiling::formulationTypes::LP_TILES, solver);

        if (tiling1.getFeasibility())
        {
            tiling1.plotTilings();
            tiling1.plotTilingsSVG("./output/" + fname + std::to_string(width) + "x" + std::to_string(height));
        }
        else
        {
            std::cerr << "ERROR in main(): No solution exists!" << std::endl;
            exit(2);
        }
    }
    else if (!operation.compare("maxcover"))
    {
        if (argc < 5)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " maxcover tileset_path width height\"" << std::endl;
            exit(2);
        }

        std::istringstream issW(argv[3]), issH(argv[4]);

        int width = 0, height = 0;

        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }
        std::string fname = argv[2];
        tiling tiling1(fname + ".tset", width, height);
        tiling1.setObjType(tiling::objTypes::MAXCOVER);
        tiling1.optimize(tiling::formulationTypes::LP_TILES, solver);

        if (tiling1.getFeasibility())
        {
            tiling1.plotTilings();
            tiling1.plotTilingsSVG("./output/" + fname + std::to_string(width) + "x" + std::to_string(height));
        }
        else
        {
            std::cerr << "ERROR in main(): No solution exists!" << std::endl;
            exit(2);
        }
    }
    else if (!operation.compare("maxcsp"))
    {
        if (argc < 5)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " maxcsp tileset_path width height\"" << std::endl;
            exit(2);
        }

        std::string fname = argv[2];
        std::istringstream issW(argv[3]), issH(argv[4]);

        int width = 0, height = 0;

        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }
        tiling tiling1(fname + ".tset", width, height);
        tiling1.setObjType(tiling::objTypes::MAXCSP);
        tiling1.optimize(tiling::formulationTypes::LP_TILES, solver);

        if (tiling1.getFeasibility())
        {
            tiling1.plotTilings();
            tiling1.plotTilingsSVG("./output/" + fname + std::to_string(width) + "x" + std::to_string(height));
        }
        else
        {
            std::cerr << "ERROR in main(): No solution exists!" << std::endl;
            exit(2);
        }
    }
    else if (!operation.compare("necessary"))
    {
        if (argc < 5)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " necessary tileset_path width heigth (tile)\"" << std::endl;
            exit(2);
        }

        std::istringstream issW(argv[3]), issH(argv[4]);

        int width = 0, height = 0;

        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }

        std::string fname = argv[2];
        tiling tiling1(fname + ".tset", width, height);

        if (argc == 6)
        {
            std::istringstream issT(argv[5]);
            int tile;
            if (((issT >> tile).fail()) || (tile < 0))
            {
                std::cerr << "ERROR in main(): tile number has to be a nonnegative integer." << std::endl;
                exit(2);
            }
            tiling1.findNecessaryTile(width, height, solver, tile);
        }
        else
        {
            tiling1.findNecessaryTiles(width, height, solver);
        }
    }
    else if (!operation.compare("distmat"))
    {
        std::string fname = argv[2];
        tileset tset(fname + ".tset");
        tset.printMinDistancesMatrix();
        exit(2);
    }
    else if (!operation.compare("transducers"))
    {
        std::string fname = argv[2];
        tileset tset(fname + ".tset");
        graphInterface::graph transducerGraph(0), dualTransducerGraph(0);
        tset.getTransducerGraph(transducerGraph);
        tset.getDualTransducerGraph(dualTransducerGraph);
        transducerGraph.writeDotFile("./output/" + fname + "_transducer");
        dualTransducerGraph.writeDotFile("./output/" + fname + "_dualtransducer");
        exit(2);
    }
    else if (!operation.compare("heuristics"))
    {
        if (argc < 6)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0] << " heuristics tileset_path width height algorithm\"" << std::endl;
            exit(2);
        }

        std::string fname = argv[2];
        std::istringstream issW(argv[3]), issH(argv[4]), issAlg(argv[5]);

        int width = 0, height = 0, alg = -1;

        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }
        if ((issAlg >> alg).fail())
        {
            std::cerr << "ERROR in main(): algorithm must be an integer." << std::endl;
            exit(2);
        }
        tiling tiling1(fname + ".tset", width, height);
        tiling1.setObjType(tiling::objTypes::MAXCOVER);
        tiling1.heuristics(true, true, alg);

        if (tiling1.getFeasibility())
        {
            // tiling1.plotTilings();
            tiling1.plotTilingsSVG("./output/" + fname + std::to_string(width) + "x" + std::to_string(height));
        }
        else
        {
            std::cerr << "ERROR in main(): No solution exists!" << std::endl;
            exit(2);
        }
    }

    else if (!operation.compare("test_heuristics"))
    {
        if (argc < 6)
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0]
                      << " test_heuristics width height numtiles numcolors launchcount treshold\""
                      << " or \"test_heuristics tileset width height treshold\"" << std::endl;
            exit(2);
        }

        std::istringstream issW, issH, issT, issC, issL, issTr;
        std::string fname = "";
        int width = 0, height = 0, numTiles = 0, numColors = 0, numIter = 1;
        double treshold = 0.0;
        bool isRandomized = false;

        if (argc == 6)
        {
            fname += argv[2];
            issW = std::istringstream(argv[3]);
            issH = std::istringstream(argv[4]);
            issTr = std::istringstream(argv[5]);
        }
        else if (argc == 8)
        {
            issW = std::istringstream(argv[2]);
            issH = std::istringstream(argv[3]);
            issT = std::istringstream(argv[4]);
            issC = std::istringstream(argv[5]);
            issL = std::istringstream(argv[6]);
            issTr = std::istringstream(argv[7]);
            isRandomized = true;
        }
        else
        {
            std::cerr << "ERROR in main(): The program shall be called with \"" << argv[0]
                      << " test_heuristics width height numtiles numcolors launchcount treshold\""
                      << " or \"test_heuristics tileset width height treshold\"" << std::endl;
            exit(2);
        }

        if (((issW >> width).fail()) || ((issH >> height).fail()) || (width < 1) || (height < 1))
        {
            std::cerr << "ERROR in main(): width and height have to be positive integers." << std::endl;
            exit(2);
        }

        if ((issTr >> treshold).fail())
        {
            std::cerr << "ERROR in main(): treshold has to be a double." << std::endl;
            exit(2);
        }

        if (isRandomized)
        {
            if (((issT >> numTiles).fail()) || ((issC >> numColors).fail()) || (numTiles < 1) || (numColors < 1))
            {
                std::cerr << "ERROR in main(): number of tiles and colors have to be positive integers." << std::endl;
                exit(2);
            }

            if (((issL >> numIter).fail()) || (numIter < 1))
            {
                std::cerr << "ERROR in main(): number of iterations has to be a positive integer." << std::endl;
                exit(2);
            }
        }

        double sumRatios = 0.0;
        double minRatio = 1.0;
        double maxRatio = 0.0;
        int exceedCount = 0;

        for (int i = 0; i < numIter; ++i)
        {
            tileset tset;
            if (isRandomized)
            {
                tset.generateRandomTileSet(tileset::setTypes::EDGE_BASED, numTiles, numColors);
            }
            else
            {
                tset.readTsetFile(fname + ".tset");
            }

            // solve the heuristics
            tiling tiling1(tset, width, height);
            tiling1.setObjType(tiling::objTypes::MAXCOVER);
            int nTiles = tiling1.heuristics(false, false, 1);

            // find optimal solution
            tiling tiling2(tset, width, height);
            tiling2.setObjType(tiling::objTypes::MAXCOVER);
            tiling2.setFormulationType(tiling::formulationTypes::LP_TILES);
            tiling2.callSolver(solver);
            int nTilesOpt = tiling2.getNumberOfPlacedTiles(0);

            double ratio = (double)nTiles / (double)nTilesOpt;
            sumRatios += ratio;
            maxRatio = std::max(ratio, maxRatio);
            minRatio = std::min(ratio, minRatio);

            std::string tsetName = "./output/heuristics_failed/";
            std::string usedLabel = "";
            std::string ccode = "";
            std::string ecode = "";

            if (ratio < treshold)
            {
                exceedCount += 1;

                if (isRandomized)
                {
                    int lastNumber = 0;
                    int nZeros = 6;
                    while (true)
                    {
                        std::string strLast = std::to_string(lastNumber);
                        usedLabel = tsetName + std::string(nZeros - strLast.length(), '0') + strLast + "_failed";
                        std::ifstream infile(usedLabel + "_tileset.tset");
                        if (infile.good())
                        {
                            lastNumber += 1;
                        }
                        else
                        {
                            break;
                        }
                    }

                    int tWidth = std::min(8, numTiles);
                    tset.plotTileSetSVG(tWidth, usedLabel + "_tileset");
                    tset.writeTsetFile(usedLabel + "_tileset");
                    tiling1.plotTilingsSVG(usedLabel + "_tiling");
                }
                else
                {
                    tset.plotTileSet(8);
                    tiling1.plotTilings();
                }

                ccode = "\e[31m";
                ecode = "\e[39m";
            }

            std::cout << ccode << "It. " << std::setw(6) << i + 1 << ",   heur.: " << std::setw(5) << nTiles << ",   opt.: " << std::setw(5) << nTilesOpt << ",   ratio: " << std::setw(8) << std::fixed << ratio << ecode << std::endl;
        }

        std::cout << std::endl;
        std::cout << "Minimum ratio: " << std::setw(8) << std::fixed << minRatio << std::endl;
        std::cout << "Maximum ratio: " << std::setw(8) << std::fixed << maxRatio << std::endl;
        std::cout << "Average ratio: " << std::setw(8) << std::fixed << (sumRatios / (double)numIter) << std::endl;
        std::cout << "Exceed count:  " << std::setw(8) << exceedCount << std::endl;
    }
    else
    {
        std::cerr << "ERROR in main(): unknown operation!" << std::endl;
        exit(2);
    }

    return 0;
}