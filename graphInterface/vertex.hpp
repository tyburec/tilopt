/*
Header-only graph class.
Copyright (C) 2018 Marek Tyburec, marek.tyburec@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "edge.hpp"
#include <string>
#include <iostream>
#include <memory>
#include <vector>

namespace graphInterface
{
    class vertex
    {
    private:
        std::vector<edge_t> inEdges;
        std::vector<edge_t> outEdges;
        std::string label;
        int color;

    public:
        int getIndegree() const { return (int)inEdges.size(); };
        int getOutdegree() const { return (int)outEdges.size(); };
        int getNumberOfAdjacentVertices() const;
        int getDegree() const { return getOutdegree() - getIndegree(); };
        int getFlowBalance() const;
        const std::string &getLabel() const { return label; };
        const int &getColor() const { return color; };
        const std::vector<edge_t> &getInEdges() const { return inEdges; };
        const std::vector<edge_t> &getOutEdges() const { return outEdges; };

        void setLabel(std::string l) { label = l; };
        void setColor(int c) { color = c; };

        void addOutwardEdge(edge_t oe) { outEdges.push_back(oe); };
        void addInwardEdge(edge_t ie) { inEdges.push_back(ie); };
        void removeOutwardEdge(int number) { outEdges.erase(outEdges.begin() + number); };
        void removeInwardEdge(int number) { inEdges.erase(inEdges.begin() + number); };

        void permuteOutwardEdgeOrder(std::vector<int> order);
        void randomizeEdgeOrder();

        vertex();
        vertex(int num);
        vertex(std::string l, int c);
        vertex(std::string l, int c, int num);
    };

    typedef graphInterface::vertex *vertex_t;

    int vertex::getNumberOfAdjacentVertices() const
    {
        std::set<int> adjVertices;
        for (const auto &it : getInEdges())
        {
            adjVertices.insert(it->getTailVertex());
        }
        for (const auto &it : getOutEdges())
        {
            adjVertices.insert(it->getHeadVertex());
        }
        return (int)adjVertices.size();
    }

    void vertex::randomizeEdgeOrder()
    {
        std::random_device rd{};
        std::mt19937 g{rd()};
        std::shuffle(outEdges.begin(), outEdges.end(), g);
    }

    void vertex::permuteOutwardEdgeOrder(std::vector<int> order)
    {
        auto edgeOrderTemp = getOutEdges();

        for (unsigned int i = 0; i < order.size(); ++i)
        {
            outEdges[i] = edgeOrderTemp[order[i]];
        }
    }

    int vertex::getFlowBalance() const
    {
        int out = 0;

        for (int i = 0; i < getIndegree(); ++i)
        {
            auto oldOut = out;
            out -= inEdges[i]->getFlow();
            if (oldOut < out)
            {
                std::cerr << "ERROR in vertex::getFlowBalance(): integer oveflow." << std::endl;
                exit(2);
            }
        }

        for (int i = 0; i < getOutdegree(); ++i)
        {
            auto oldOut = out;
            out += outEdges[i]->getFlow();
            if (oldOut > out)
            {
                std::cerr << "ERROR in vertex::getFlowBalance(): integer oveflow." << std::endl;
                exit(2);
            }
        }

        return out;
    }

    vertex::vertex()
    {
        setColor(-1);
    }

    vertex::vertex(int num)
    {
        setColor(-1);
        inEdges.reserve(num);
        outEdges.reserve(num);
    }

    vertex::vertex(std::string l, int c)
    {
        setLabel(l);
        setColor(c);
    }

    vertex::vertex(std::string l, int c, int num)
    {
        setLabel(l);
        setColor(c);
        inEdges.reserve(num);
        outEdges.reserve(num);
    }
}