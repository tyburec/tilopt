/*
Header-only graph class.
Copyright (C) 2018 Marek Tyburec, marek.tyburec@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* TODO:    - maximum matching in bipartite graphs (network flows, Hungarian algorithm)
            - maximum matching in non-bipartite graphs (Edmond's blossom algorithm)
            - graph coloring based on ordering (maximumCardinality for chordal, minDegree, ...)
            - graph coloring using backtracking
            - distinguish planar graphs (efficient algorithm for at most 4-coloring)
            - bissection using 2nd eigenvector of Laplacian matrix
 */

#pragma once

#include <list>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <stack>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <limits>
#include <iterator>
#include <queue>
#include <utility>
#ifdef USE_OMP
#include <omp.h>
#endif
#include <random>
#include <chrono>
#include <fstream>
#include <utility>
#include <sstream>
#include "vertex.hpp"
#include "edge.hpp"

namespace graphInterface
{
    class graph
    {
    private:
        int numVertices;
        int numEdges;
        int sourceVertex;
        int terminalVertex;

        std::vector<vertex_t> vertices;
        std::vector<edge_t> edges;

        bool dotFile_flowLabel;
        bool dotFile_weightLabel;

        int augmentFlowEdmondsKarp(std::vector<int> &path);
        int augmentFlowDinic(std::vector<int> &path);
        int augmentFlowFordFulkerson(std::vector<int> &path);
        int augmentFlowCostNegativeCycle();
        void bellmanFordResidual(std::vector<double> &len, std::vector<edge_t> &prevNode) const;

    public:
        graph(int nV);
        graph(const graph &inGraph);
        graph &operator=(const graph &other);
        ~graph();

        enum maxFlowAlgorithms
        {
            FordFulkerson,
            EdmondsKarp,
            Dinic
        };
        enum allPairsShortestPathsAlgorithms
        {
            FloydWarshall,
            Johnson,
            BellmanFord,
            Dijkstra,
            DirectedAcyclicGraph,
            BreadthFirstSearch
        };
        enum eliminationOrderingAlgorithms
        {
            MaximumCardinality,
            MinimumDegree,
            MinimumFillIn
        };

        const edge_t &addEdge(int tail, int head);
        void addUndirectedEdge(int vertexA, int vertexB);
        const edge_t &addEdge(int tail, int head, double cost);
        void addUndirectedEdge(int vertexA, int vertexB, double cost);
        const edge_t &addEdge(int tail, int head, std::string label);
        const edge_t &addEdge(int tail, int head, std::string label, double cost);
        void addUndirectedEdge(int vertexA, int vertexB, std::string label, double cost);
        const edge_t &addEdge(int tail, int head, int flowLB, int flow, int flowUB);
        void addUndirectedEdge(int vertexA, int vertexB, int flowLB, int flow, int flowUB);
        const edge_t &addEdge(int tail, int head, double cost, int flowLB, int flow, int flowUB);
        const edge_t &addEdge(int tail, int head, std::string label, double cost, int flowLB, int flow, int flowUB);
        void addUndirectedEdge(int vertexA, int vertexB, std::string label, double cost, int flowLB, int flow, int flowUB);
        const vertex_t &addVertex(std::string label, int color);
        void setSourceVertex(int vertex);
        void setTerminalVertex(int vertex);
        void setEdgesThicknesses(std::vector<edge_t> &pEdge, bool value);
        void setDotFiledotFileFlowLabels(bool val) { dotFile_flowLabel = val; };
        void setDotFiledotFileWeightLabels(bool val) { dotFile_weightLabel = val; };

        int getNumVertices() const { return numVertices; };
        int getNumEdges() const { return numEdges; };
        int getSourceVertex() const { return sourceVertex; };
        int getTerminalVertex() const { return terminalVertex; };
        void getCutFromSet(const std::vector<int> &set, std::vector<edge_t> &e) const;
        void getFlowsThroughVertices(std::vector<int> &inFlow, std::vector<int> &outFlow) const;
        void getVertex(int number, vertex_t &v) const;
        const vertex_t &getVertex(int number) const { return vertices[number]; };
        void getVertices(std::vector<vertex_t> &v) const;
        std::vector<vertex_t> &getVertices() { return vertices; };
        void getEdge(std::vector<edge_t> &fEdge, int tail, int head) const;
        void getEdges(std::vector<edge_t> &e) const;
        std::vector<edge_t> &getEdges() { return edges; };
        void getEdgeTailVertices(const std::vector<edge_t> &pEdges, std::vector<int> &prevNodes);
        bool getDotFiledotFileFlowLabels() const { return dotFile_flowLabel; };
        bool getDotFiledotFileWeightLabels() const { return dotFile_weightLabel; };

        void randomizeEdgeOrder();
        void erdosRenyi(int nVertices, double probability, bool directed, bool selfLoops);
        void maximumFlow(maxFlowAlgorithms algorithm, std::vector<int> &minCutVertices, int &maxFlow);
        void fordFulkerson(std::vector<int> &minCutVertices, int &maxFlow);
        void edmondsKarp(std::vector<int> &minCutVertices, int &maxFlow);
        void dinic(std::vector<int> &minCutVertices, int &maxFlow);
        double cycleCanceling();
        void residualGraph(graph &resGraph) const;
        bool checkKirchhoffLaw() const;
        bool checkFeasibleFlow() const;
        bool admitsFeasibleFlow() const;
        bool findFeasibleFlow(maxFlowAlgorithms algorithm);
        void transposeGraph(graph &newGraph) const;
        void reverseGraph(graph &newGraph) const;
        void topologicalSort(std::vector<int> &topOrder) const;
        void maximumCardinalitySearch(std::vector<int> &order) const;
        void minimumDegreeOrder(std::vector<int> &order) const;
        void maximumDegreeOrder(std::vector<int> &order) const;
        void minimumFillInOrder(std::vector<int> &order) const;
        void connectedComponents(std::vector<std::vector<int>> &components) const;
        void stronglyConnectedComponents(std::vector<std::vector<int>> &scc) const;
        void johnson(std::vector<std::vector<double>> &distMat, std::vector<std::vector<edge_t>> &pEdge);
        void johnson(std::vector<std::vector<double>> &distMat);
        void floydWarshall(std::vector<std::vector<double>> &, std::vector<std::vector<edge_t>> &pEdge) const;
        void floydWarshall(std::vector<std::vector<double>> &distMat) const;
        void allPairsShortestPaths(std::vector<std::vector<double>> &distMat, std::vector<std::vector<edge_t>> &pEdge, allPairsShortestPathsAlgorithms algorithm);
        void allPairsShortestPaths(std::vector<std::vector<double>> &distMat, allPairsShortestPathsAlgorithms algorithm);
        void bellmanFord(int source, std::vector<double> &len, std::vector<edge_t> &pEdge) const;
        void dijkstra(int source, std::vector<double> &len, std::vector<edge_t> &pEdge) const;
        void dijkstra(int source, int terminal, std::vector<double> &len, std::vector<edge_t> &pEdge) const;
        void primJarnik(double &cost, std::vector<edge_t> &pEdge) const;
        void DAGShortestPath(int source, std::vector<double> &len, std::vector<edge_t> &pEdge, bool isTopOrdered) const;
        void DAGShortestPath(int source, std::vector<double> &len, std::vector<edge_t> &pEdge) const;
        void DAGLongestPath(int source, std::vector<double> &len, std::vector<edge_t> &pEdge, bool isTopOrdered);
        void DAGLongestPath(int source, std::vector<double> &len, std::vector<edge_t> &pEdge);
        void breadthFirstSearch(int source, std::vector<int> &level, std::vector<edge_t> &pEdge) const;
        void depthFirstSearch(int source, std::vector<int> &level, std::vector<edge_t> &pEdge) const;
        void retrieveShortestPath(int terminal, std::vector<edge_t> &pEdge, std::vector<edge_t> &path) const;
        void writeDotFile(std::string fname) const;
        void writeDotFile(std::string fname, std::vector<std::vector<int>> subGraphs) const;
        void writeGraphIntoTextFile(const std::string &fname) const;
        void loadGraphFromTextFile(const std::string &fname);
        void printGraphInfo() const;
        void subgraph(graphInterface::graph &g, std::set<int> set) const;
        void kernighanLin(double &obj, std::vector<int> &partitionA, std::vector<int> &partitionB) const;
        void eliminationGame(const std::vector<int> &sigma, std::vector<std::vector<int>> &cliques, int &fillInEdges) const;
        void eliminationGame(std::vector<std::vector<int>> &cliques, int &fillInEdges, eliminationOrderingAlgorithms method) const;
        void eliminationGame(std::vector<std::vector<int>> &cliques, int &fillInEdges) const;
        void cliqueTree(const std::vector<std::vector<int>> &cliques, graph &cliqueT) const;
        void cliqueTree(graph &cliqueT, eliminationOrderingAlgorithms algorithm) const;
        void cliqueTree(graph &cliqueT) const;
        void componentTree(std::vector<std::vector<int>> components, graph &componentG) const;
        auto fillInEdgesCount(std::vector<std::vector<int>> &cliques) const -> size_t;
        auto fillInEdges(std::vector<std::vector<int>> &cliques) const -> std::set<std::pair<int, int>>;
        void greedyColoring(int &numColors, const std::vector<int> &order) const;
        void greedyColoring(int &numColors) const;
        void welshPowell(int &numColors) const;

        bool isBipartite() const;
        bool isMultiGraph() const;
        bool isWeightedNegatively() const;
        bool isWeightedEqually() const;
        bool isChordal() const;
        bool isUndirected() const;
        bool isAcyclic() const;
        bool isConnected() const;
        bool isStronglyConnected() const;
        bool isTree() const;
        bool isComplete() const;
        bool isEulerian() const;
        bool isRegular(int &k) const;
        bool isPerfectEliminationOrder(const std::vector<int> &sigma) const;
        bool isMaximumFlow(const std::vector<int> &minCutVertices) const;
        bool isValidTriangulation(std::vector<std::vector<int>> &cliques) const;
    };

    void graph::welshPowell(int &numColors) const
    {
        numColors = 0;
        int numColoredVertices = 0;
        std::vector<int> maxDegreeOrder;
        maximumDegreeOrder(maxDegreeOrder);

        while (numColoredVertices < getNumVertices())
        {
            std::vector<bool> canBeColored(getNumVertices(), true);
            for (const auto &it : maxDegreeOrder)
            {
                if ((vertices[it]->getColor() == -1) && (canBeColored[it]))
                {
                    vertices[it]->setColor(numColors + 1);
                    ++numColoredVertices;
                    for (const auto &e : vertices[it]->getOutEdges())
                    {
                        canBeColored[e->getHeadVertex()] = false;
                    }
                    for (const auto &e : vertices[it]->getInEdges())
                    {
                        canBeColored[e->getTailVertex()] = false;
                    }
                }
            }
            ++numColors;
        }
    }

    void graph::greedyColoring(int &numColors) const
    {
        std::vector<int> order(getNumVertices());
        for (int i = 0; i < getNumVertices(); ++i)
        {
            order[i] = i;
        }
        std::random_shuffle(order.begin(), order.end());
        greedyColoring(numColors, order);
    }

    void graph::greedyColoring(int &numColors, const std::vector<int> &order) const
    {
        numColors = 0;

        for (const auto &it : vertices)
        {
            it->setColor(-1);
        }

        for (const auto &it : order)
        {
            std::vector<bool> neighborColors(getNumVertices(), false);
            for (const auto &e : vertices[it]->getOutEdges())
            {
                auto h = e->getHeadVertex();
                if (vertices[h]->getColor() != -1)
                {
                    neighborColors[vertices[h]->getColor()] = true;
                }
            }
            for (const auto &e : vertices[it]->getInEdges())
            {
                auto t = e->getTailVertex();
                if (vertices[t]->getColor() != -1)
                {
                    neighborColors[vertices[t]->getColor()] = true;
                }
            }
            int firstAvailableColor = -1;
            for (unsigned int i = 0; i < neighborColors.size(); ++i)
            {
                if (neighborColors[i] == false)
                {
                    firstAvailableColor = i;
                    break;
                }
            }
            vertices[it]->setColor(firstAvailableColor);

            if (firstAvailableColor >= numColors)
            {
                numColors = firstAvailableColor + 1;
            }
        }
    }

    bool graph::isBipartite() const
    {
        /* Assigns 2-coloring if the graph is bipartite */
        std::queue<int> queue;
        std::vector<bool> visited(getNumVertices(), false);
        std::vector<bool> coloring(getNumVertices(), false);

        for (int i = 0; i < getNumVertices(); ++i)
        {
            if (visited[i])
            {
                continue;
            }

            queue.push(i);
            coloring[i] = false;
            visited[i] = true;

            while (!queue.empty())
            {
                // get the first element
                auto topVertex = queue.front();

                // remove it from the queue
                queue.pop();

                // iterate through outcoming edges
                for (const auto &it : vertices[topVertex]->getOutEdges())
                {
                    auto nextVertex = it->getHeadVertex();
                    if (!visited[nextVertex])
                    {
                        visited[nextVertex] = true;
                        coloring[nextVertex] = !coloring[topVertex];
                        queue.push(nextVertex);
                    }
                    else if ((visited[nextVertex]) & (coloring[nextVertex] == coloring[topVertex]))
                    {
                        return false;
                    }
                }
                // iterate through incoming edges
                for (const auto &it : vertices[topVertex]->getInEdges())
                {
                    auto nextVertex = it->getTailVertex();
                    if (!visited[nextVertex])
                    {
                        visited[nextVertex] = true;
                        coloring[nextVertex] = !coloring[topVertex];
                        queue.push(nextVertex);
                    }
                    else if ((visited[nextVertex]) & (coloring[nextVertex] == coloring[topVertex]))
                    {
                        return false;
                    }
                }
            }
        }

        // assign two-coloring to the graph (as it exists)
        for (int i = 0; i < getNumVertices(); ++i)
        {
            vertices[i]->setColor((int)coloring[i]);
        }

        return true;
    }

    graph::~graph()
    {
        for (int i = 0; i < getNumEdges(); ++i)
        {
            delete edges[i];
        }

        for (int i = 0; i < getNumVertices(); ++i)
        {
            delete vertices[i];
        }
    }

    graph &graph::operator=(const graph &other)
    {
        for (int i = 0; i < getNumEdges(); ++i)
        {
            delete edges[i];
        }
        edges.resize(0);

        for (int i = 0; i < getNumVertices(); ++i)
        {
            delete vertices[i];
        }
        vertices.resize(0);

        numVertices = other.getNumVertices();
        sourceVertex = other.getSourceVertex();
        terminalVertex = other.getTerminalVertex();
        dotFile_flowLabel = other.getDotFiledotFileFlowLabels();
        dotFile_weightLabel = other.getDotFiledotFileWeightLabels();

        vertices.reserve(numVertices);
        for (int iV = 0; iV < numVertices; ++iV)
        {
            auto reserveSize = std::max(other.vertices[iV]->getOutdegree(), other.vertices[iV]->getIndegree());
            vertices.emplace_back(new vertex(other.vertices[iV]->getLabel(), other.vertices[iV]->getColor(), reserveSize));
        }

        edges.reserve(other.getNumEdges());
        for (int iE = 0; iE < other.getNumEdges(); ++iE)
        {
            addEdge(other.edges[iE]->getTailVertex(), other.edges[iE]->getHeadVertex(), other.edges[iE]->getLabel(), other.edges[iE]->getWeight(),
                    other.edges[iE]->getFlowLowerBound(), other.edges[iE]->getFlow(), other.edges[iE]->getFlowUpperBound());
        }

        return *this;
    }

    void graph::erdosRenyi(int nVertices, double probability, bool directed, bool selfLoops)
    {
        if ((probability < 0.0) | (probability > 1.0))
        {
            std::cerr << "ERROR in graph::erdosRenyi(): probability should be in the range [0; 1]." << std::endl;
            exit(2);
        }

        std::mt19937_64 rng;
        uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
        std::seed_seq ss{uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed >> 32)};
        rng.seed(ss);
        std::uniform_real_distribution<double> unif(0, 1);
        std::uniform_real_distribution<double> uniDoub(0, 1e5);
        std::uniform_real_distribution<double> uniw(-uniDoub(rng), uniDoub(rng));
        std::uniform_int_distribution<int> unii(0, nVertices - 1);
        std::uniform_int_distribution<int> uniInt(0, std::numeric_limits<int>::max() / nVertices);
        std::uniform_int_distribution<int> unilb(0, uniInt(rng) / nVertices);
        std::uniform_int_distribution<int> uniub(0, uniInt(rng));

        for (const auto &it : edges)
        {
            delete it;
        }
        edges.resize(0);
        numEdges = 0;

        for (const auto &it : vertices)
        {
            delete it;
        }
        vertices.resize(0);
        numVertices = 0;
        for (int i = 0; i < nVertices; ++i)
        {
            addVertex("", -1);
        }
        sourceVertex = unii(rng);
        terminalVertex = unii(rng);
        if (nVertices != 1)
        {
            while (sourceVertex == terminalVertex)
            {
                terminalVertex = unii(rng);
            }
        }

        if (directed)
        {
            for (int i = 0; i < nVertices; ++i)
            {
                for (int j = 0; j < nVertices; ++j)
                {
                    if ((i != j) | ((i == j) & selfLoops))
                    {
                        if (unif(rng) <= probability)
                        {
                            int ub = uniub(rng);
                            int lb = unilb(rng);
                            while (lb > ub)
                            {
                                ub = uniub(rng);
                                lb = unilb(rng);
                            }
                            addEdge(i, j, "", uniw(rng), lb, 0, ub);
                        }
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < nVertices; ++i)
            {
                for (int j = i; j < nVertices; ++j)
                {
                    if (i != j)
                    {
                        if (unif(rng) <= probability)
                        {
                            int ub = uniub(rng);
                            int lb = unilb(rng);
                            while (lb > ub)
                            {
                                ub = uniub(rng);
                                lb = unilb(rng);
                            }
                            addUndirectedEdge(i, j, "", uniw(rng), lb, 0, ub);
                        }
                    }
                    else if (selfLoops)
                    {
                        if (unif(rng) <= probability)
                        {
                            int ub = uniub(rng);
                            int lb = unilb(rng);
                            while (lb > ub)
                            {
                                ub = uniub(rng);
                                lb = unilb(rng);
                            }
                            addEdge(i, j, "", uniw(rng), lb, 0, ub);
                        }
                    }
                }
            }
        }
    }

    void graph::setEdgesThicknesses(std::vector<edge_t> &pEdge, bool value)
    {
        for (const auto &it : pEdge)
        {
            if (it)
            {
                it->setThickness(value);
            }
        }
    }

    void graph::subgraph(graphInterface::graph &g, std::set<int> set) const
    {
        g = graphInterface::graph(set.size());
        std::vector<int> order(getNumVertices(), -1);
        int i = 0;
        for (const auto &it : set)
        {
            order[it] = i;
            ++i;
        }

        for (const auto &it : set)
        {
            for (const auto &e : vertices[it]->getOutEdges())
            {
                auto head = e->getHeadVertex();
                auto tail = e->getTailVertex();

                if ((order[tail] != -1) & (order[head] != -1))
                {
                    g.addEdge(order[tail], order[head], e->getLabel(), e->getWeight(), e->getFlowLowerBound(), e->getFlow(), e->getFlowUpperBound());
                }
            }
        }
    }

    void graph::kernighanLin(double &obj, std::vector<int> &partitionA, std::vector<int> &partitionB) const
    {
        if (!isUndirected())
        {
            std::cerr << "ERROR in graph::kernighanLin(): graph is not undirected." << std::endl;
            exit(2);
        }

        if (isMultiGraph())
        {
            std::cerr << "ERROR in graph::kernighanLin(): input graph is multigraph." << std::endl;
            exit(2);
        }

        // initially, make the (balanced) partitioning random
        std::random_device rd;
        std::mt19937 rng(rd());
        std::uniform_int_distribution<int> uni(0, getNumVertices() - 1);
        std::vector<bool> inPartitionA(getNumVertices(), false), isUsed(getNumVertices(), false);
        int inPartA = 0;
        while (inPartA < (getNumVertices() / 2))
        {
            auto randI = uni(rng);
            if (!inPartitionA[randI])
            {
                inPartitionA[randI] = true;
                inPartA += 1;
            }
        }

        double improvement = std::numeric_limits<double>::max();

        /* internal costs -- A-A and B-B
           external costs -- A-B and B-A
           edge costs c_ab*/
        std::vector<double> internal(getNumVertices(), 0),
            external(getNumVertices(), 0);
        std::map<std::pair<int, int>, double> eCosts;
        double inPartitionObj = 0;
        for (const auto &it : edges)
        {
            auto tail = it->getTailVertex();
            auto head = it->getHeadVertex();

            if (tail < head)
            {
                if (inPartitionA[tail] == inPartitionA[head])
                {
                    internal[tail] += it->getWeight();
                    internal[head] += it->getWeight();
                }
                else
                {
                    external[tail] += it->getWeight();
                    external[head] += it->getWeight();
                    inPartitionObj += it->getWeight();
                }
                eCosts.emplace(std::make_pair(tail, head), it->getWeight());
            }
        }
        std::vector<bool> bestPartition = inPartitionA;
        double bestObj = inPartitionObj;

        for (int k = 0; k < getNumVertices() / 2; ++k)
        {
            // best improvement for partition A and B vertices
            int a(-1), b(-1);
            improvement = std::numeric_limits<double>::min();

            for (int i = 0; i < getNumVertices(); ++i)
            {
                if (!isUsed[i])
                {
                    for (int j = i + 1; j < getNumVertices(); ++j)
                    {
                        if (!isUsed[j])
                        {
                            if (inPartitionA[i] != inPartitionA[j])
                            {
                                auto eFind = eCosts.find(std::make_pair(i, j));
                                double cab = 0.0;
                                if (eFind != eCosts.end())
                                {
                                    cab = 2.0 * eFind->second;
                                }
                                double possibleImprovement = external[i] + external[j] - internal[i] - internal[j] - cab;

                                if (possibleImprovement > improvement)
                                {
                                    improvement = possibleImprovement;
                                    a = i;
                                    b = j;
                                }
                            }
                        }
                    }
                }
            }

            if ((a == -1) | (b == -1))
            {
                break;
            }

            // interchange the vertices
            inPartitionA[a] = (!inPartitionA[a]);
            inPartitionA[b] = (!inPartitionA[b]);
            inPartitionObj -= improvement;
            isUsed[a] = isUsed[b] = true;

            if (bestObj > inPartitionObj)
            {
                bestObj = inPartitionObj;
                bestPartition = inPartitionA;
            }

            // update internal and external costs
            for (const auto &it : vertices[a]->getOutEdges())
            {
                auto head = it->getHeadVertex();
                auto w = it->getWeight();
                if (head != b)
                {
                    if (inPartitionA[a] == inPartitionA[head])
                    {
                        external[head] -= w;
                        external[a] -= w;
                        internal[a] += w;
                        internal[head] += w;
                    }
                    else
                    {
                        external[head] += w;
                        external[a] += w;
                        internal[head] -= w;
                        internal[a] -= w;
                    }
                }
            }

            for (const auto &it : vertices[b]->getOutEdges())
            {
                auto head = it->getHeadVertex();
                auto w = it->getWeight();
                if (head != a)
                {
                    if (inPartitionA[b] == inPartitionA[head])
                    {
                        external[head] -= w;
                        external[b] -= w;
                        internal[head] += w;
                        internal[b] += w;
                    }
                    else
                    {
                        external[head] += w;
                        external[b] += w;
                        internal[head] -= w;
                        internal[b] -= w;
                    }
                }
            }
        }

        partitionA.clear();
        partitionA.reserve(getNumVertices() / 2 + 1);
        partitionB.clear();
        partitionB.reserve(getNumVertices() / 2 + 1);
        obj = bestObj;
        for (int i = 0; i < getNumVertices(); ++i)
        {
            if (bestPartition[i])
            {
                partitionA.push_back(i);
            }
            else
            {
                partitionB.push_back(i);
            }
        }
    }

    void graph::printGraphInfo() const
    {
        int w = 60;
        int l = 6;
        std::cout << "GRAPH INFO:" << std::endl;
        std::cout.width(w);
        std::cout << std::left << "   Number of vertices:" << std::right << std::setw(l) << getNumVertices() << std::endl;
        std::cout.width(w);
        std::cout << std::left << "   Number of edges:" << std::right << std::setw(l) << getNumEdges() << std::endl;

        std::vector<std::vector<int>> components;
        connectedComponents(components);
        if (components.size() == 1)
        {
            std::cout.width(w);
            std::cout << std::left << "   Connected:" << std::right << std::setw(l) << std::boolalpha << true << std::endl;
        }
        else
        {
            std::cout.width(w);
            std::cout << std::left << "   Connected:" << std::right << std::setw(l) << std::boolalpha << false << std::endl;
        }
        std::cout.width(w);
        std::cout << std::left << "   Number of connected components:" << std::right << std::setw(l) << components.size() << std::endl;

        if (components.size() == 1)
        {
            std::vector<std::vector<int>> scc;
            stronglyConnectedComponents(scc);
            if (scc.size() == 1)
            {
                std::cout.width(w);
                std::cout << std::left << "   Strongly connected:" << std::right << std::setw(l) << std::boolalpha << true << std::endl;
            }
            else
            {
                std::cout.width(w);
                std::cout << std::left << "   Strongly connected:" << std::right << std::setw(l) << std::boolalpha << false << std::endl;
            }
            std::cout.width(w);
            std::cout << std::left << "   Number of strongly connected components:" << std::right << std::setw(l) << scc.size() << std::endl;

            std::cout.width(w);
            std::cout << std::left << "   Undirected:" << std::right << std::setw(l) << std::boolalpha << isUndirected() << std::endl;
            std::cout.width(w);
            std::cout << std::left << "   Acyclic:" << std::right << std::setw(l) << std::boolalpha << isAcyclic() << std::endl;
            std::cout.width(w);
            std::cout << std::left << "   Tree:" << std::right << std::setw(l) << std::boolalpha << isTree() << std::endl;
            std::cout.width(w);
            std::cout << std::left << "   Multigraph:" << std::right << std::setw(l) << std::boolalpha << isMultiGraph() << std::endl;
            std::cout.width(w);
            std::cout << std::left << "   Chordal:" << std::right << std::setw(l) << std::boolalpha << isChordal() << std::endl;
            std::cout.width(w);
            std::cout << std::left << "   Eulerian:" << std::right << std::setw(l) << std::boolalpha << isEulerian() << std::endl;
            // std::cout.width(w); std::cout << std::left << "   Planar:" << std::right << "TODO" << std::endl;
            std::cout.width(w);
            std::cout << std::left << "   Bipartite:" << std::right << std::setw(l) << std::boolalpha << isBipartite() << std::endl;
            std::cout.width(w);
            std::cout << std::left << "   Complete:" << std::right << std::setw(l) << std::boolalpha << isComplete() << std::endl;

            int k = -1;
            bool reg = isRegular(k);
            if (reg)
            {
                std::cout.width(w);
                std::cout << std::left << "   Regular:" << std::right << std::setw(l) << k << "-regular" << std::endl;
            }
            else
            {
                std::cout.width(w);
                std::cout << std::left << "   Regular:" << std::right << std::setw(l) << std::boolalpha << reg << std::endl;
            }
            std::cout.width(w);
            std::cout << std::left << "   Negative costs:" << std::right << std::setw(l) << std::boolalpha << isWeightedNegatively() << std::endl;
        }
        else
        {
            for (unsigned int i = 0; i < components.size(); ++i)
            {
                graphInterface::graph g(1);
                std::set<int> set(components[i].begin(), components[i].end());
                subgraph(g, set);

                std::cout << std::left << "   Connected component #" << i << std::endl;
                std::cout.width(w);
                std::cout << std::left << "     Number of vertices:" << std::right << std::setw(l) << g.getNumVertices() << std::endl;
                std::cout.width(w);
                std::cout << std::left << "     Number of edges:" << std::right << std::setw(l) << g.getNumEdges() << std::endl;
                std::vector<std::vector<int>> scc;
                g.stronglyConnectedComponents(scc);
                if (scc.size() == 1)
                {
                    std::cout.width(w);
                    std::cout << std::left << "     Strongly connected:" << std::right << std::setw(l) << std::boolalpha << true << std::endl;
                }
                else
                {
                    std::cout.width(w);
                    std::cout << std::left << "     Strongly connected:" << std::right << std::setw(l) << std::boolalpha << false << std::endl;
                }
                std::cout.width(w);
                std::cout << std::left << "     Number of strongly connected components:" << std::right << std::setw(l) << scc.size() << std::endl;

                std::cout.width(w);
                std::cout << std::left << "     Undirected:" << std::right << std::setw(l) << std::boolalpha << g.isUndirected() << std::endl;

                std::cout.width(w);
                std::cout << std::left << "     Acyclic:" << std::right << std::setw(l) << std::boolalpha << g.isAcyclic() << std::endl;
                std::cout.width(w);
                std::cout << std::left << "     Tree:" << std::right << std::setw(l) << std::boolalpha << g.isTree() << std::endl;
                std::cout.width(w);
                std::cout << std::left << "     Multigraph:" << std::right << std::setw(l) << std::boolalpha << g.isMultiGraph() << std::endl;
                std::cout.width(w);
                std::cout << std::left << "     Chordal:" << std::right << std::setw(l) << std::boolalpha << g.isChordal() << std::endl;
                std::cout.width(w);
                std::cout << std::left << "     Eulerian:" << std::right << std::setw(l) << std::boolalpha << g.isEulerian() << std::endl;
                // std::cout.width(w); std::cout << std::left << "     Planar:" << std::right << "TODO" << std::endl;
                std::cout.width(w);
                std::cout << std::left << "     Bipartite:" << std::right << std::setw(l) << std::boolalpha << isBipartite() << std::endl;
                std::cout.width(w);
                std::cout << std::left << "     Complete:" << std::right << std::setw(l) << std::boolalpha << g.isComplete() << std::endl;

                int k = -1;
                bool reg = g.isRegular(k);
                if (reg)
                {
                    std::cout.width(w);
                    std::cout << std::left << "     Regular:" << std::right << std::setw(l) << k << "-regular" << std::endl;
                }
                else
                {
                    std::cout.width(w);
                    std::cout << std::left << "     Regular:" << std::right << std::setw(l) << std::boolalpha << reg << std::endl;
                }
                std::cout.width(w);
                std::cout << std::left << "     Negative costs:" << std::right << std::setw(l) << std::boolalpha << g.isWeightedNegatively() << std::endl;
            }
        }
    }

    bool graph::isRegular(int &k) const
    {
        bool regular = true;

        // all vertices need to have equal in- and out- degrees
        for (int i = 0; i < getNumVertices(); ++i)
        {
            if (i == 0)
            {
                k = vertices[i]->getIndegree();
            }
            else
            {
                if (k != vertices[i]->getIndegree())
                {
                    regular = false;
                    break;
                }
            }

            if (k != vertices[i]->getOutdegree())
            {
                regular = false;
                break;
            }
        }

        return regular;
    }

    bool graph::isEulerian() const
    {
        bool eulerian = true;

        // every vertex needs to have equal in- and out- degrees
        std::vector<int> inDegree(getNumVertices());
        std::vector<int> outDegree(getNumVertices());
        for (int i = 0; i < getNumVertices(); ++i)
        {
            inDegree[i] = vertices[i]->getIndegree();
            outDegree[i] = vertices[i]->getOutdegree();
            if (inDegree[i] != outDegree[i])
            {
                eulerian = false;
                break;
            }
        }

        // non-zero degree vertices have to be within a single strongly connected component
        if (eulerian == true)
        {
            if (isStronglyConnected() == false)
            {
                eulerian = false;
            }
        }

        return eulerian;
    }

    bool graph::isComplete() const
    {
        bool complete = false;
        if ((isMultiGraph() == false) & (getNumEdges() == (getNumVertices() * getNumVertices())))
            complete = true;
        return complete;
    }

    bool graph::isTree() const
    {
        bool tree = false;

        if (isUndirected())
            // undirected tree
            if (isConnected() & (getNumEdges() == (getNumVertices() * 2 - 2)))
                tree = true;
            else
                // Another option would be to check connectedness and no cycles
                if (isConnected() & (getNumEdges() == (getNumVertices() - 1)))
                    tree = true;

        return tree;
    }

    void graph::writeDotFile(std::string fname) const
    {
        std::vector<std::vector<int>> subGraphs(0);
        writeDotFile(fname, subGraphs);
    }

    void graph::writeDotFile(std::string fname, std::vector<std::vector<int>> subGraphs) const
    {
        fname = fname + ".dot";
        std::ofstream file;
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        try
        {
            file.open(fname);
        }
        catch (std::ios_base::failure &inErr)
        {
            std::runtime_error{std::string{"ERROR in graph::writeDotFile(): The requested output file cannot be created. System error: "}.append(inErr.what())};
        }

        file << "// (c) 2018 Marek Tyburec" << std::endl;

        auto isU = isUndirected();
        if (isU)
        {
            file << "graph dGraph {" << std::endl;
        }
        else
        {
            file << "digraph dGraph {" << std::endl;
        }

        // graph style
        file << "\tnode [shape=circle colorscheme=\"paired12\" style=filled];" << std::endl;

        // clusters / subgraphs
        for (unsigned int i = 0; i < subGraphs.size(); ++i)
        {
            file << "\tsubgraph cluster_" << std::to_string(i) << "{" << std::endl;
            file << "\t\tlabel=\"\";" << std::endl;
            file << "\t\t";
            for (unsigned int j = 0; j < subGraphs[i].size(); ++j)
            {
                file << std::to_string(subGraphs[i][j]) << "; ";
            }
            file << std::endl;
            file << "\t\tstyle = rounded;" << std::endl;
            file << "\t}" << std::endl;
        }

        // vertices information
        bool maxColorWarning = false;
        for (int i = 0; i < getNumVertices(); ++i)
        {
            auto label = vertices[i]->getLabel();
            if (!label.empty())
            {
                label = "\t" + std::to_string(i) + " [label=\"" + label + "\"";
            }
            else
            {
                label = "\t" + std::to_string(i) + " [";
            }
            if (i == getSourceVertex())
            {
                label += "shape=doublecircle";
            }
            else if (i == getTerminalVertex())
            {
                label += "shape=Mcircle";
            }
            int col = vertices[i]->getColor();
            if (col > 11)
            {
                maxColorWarning = true;
            }
            if (col != -1)
            {
                label += " fillcolor=" + std::to_string(col % 12 + 1);
            }
            else
            {
                label += " style=\"\"";
            }
            label += "];";
            file << label << std::endl;
        }
        if (maxColorWarning)
        {
            std::cerr << "Warning in graph::writeDotFile(): color index larger than 11." << std::endl;
        }

        for (const auto &it : edges)
        {
            auto label = it->getLabel();
            if (label.empty())
            {
                std::string w = std::to_string(it->getWeight());
                w.erase(w.find_last_not_of('0') + 1, std::string::npos);
                if (w.back() == '.')
                {
                    w += "0";
                }

                if (getDotFiledotFileFlowLabels())
                {
                    auto lFlow = it->getFlowLowerBound();
                    if (lFlow == std::numeric_limits<int>::min())
                    {
                        label += "-∞";
                    }
                    else
                    {
                        label += std::to_string(lFlow);
                    }
                    label += "|" + std::to_string(it->getFlow()) + "|";
                    auto uFlow = it->getFlowUpperBound();
                    if (uFlow == std::numeric_limits<int>::max())
                    {
                        label += "∞";
                    }
                    else
                    {
                        label += std::to_string(uFlow);
                    }
                    if (getDotFiledotFileWeightLabels())
                    {
                        label += ":";
                    }
                }
                if (getDotFiledotFileWeightLabels())
                {
                    label += w;
                }
            }
            if (isU)
            {
                if (it->getTailVertex() < it->getHeadVertex())
                {
                    file << "\t" << it->getTailVertex() << " -- " << it->getHeadVertex() << " [label=\"" << label << "\"";
                    if (it->isThick())
                    {
                        file << " penwidth=3.0 fontname=\"times bold\"";
                    }
                    file << "];" << std::endl;
                }
            }
            else
            {
                file << "\t" << it->getTailVertex() << " -> " << it->getHeadVertex() << " [label=\"" << label << "\"";
                if (it->isThick())
                {
                    file << " penwidth=3.0 fontname=\"times bold\"";
                }
                file << "];" << std::endl;
            }
        }
        file << "}";

        file.close();

        std::cout << "The file " << fname << " was written successfully." << std::endl;
    }

    void graph::writeGraphIntoTextFile(const std::string &fname) const
    {

        // Open file for writing
        std::ofstream outFile;
        outFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        try
        {
            outFile.open(fname);
        }
        catch (std::ios_base::failure &inErr)
        {
            std::runtime_error{std::string{"ERROR in graph::writeGraphIntoTextFile(): The requested output file cannot be created. System error: "}.append(inErr.what())};
        }

        // Write data
        outFile << this->getNumVertices() << '\n';
        outFile << this->getNumEdges() << '\n';
        outFile << sourceVertex << '\n';
        outFile << terminalVertex << '\n';
        for (const auto &e : edges)
        {
            outFile << e->getTailVertex() << " " << e->getHeadVertex() << " " << e->getWeight() << " "
                    << e->getFlowLowerBound() << " " << e->getFlow() << " " << e->getFlowUpperBound() << '\n';
        }
    }

    void graph::loadGraphFromTextFile(const std::string &fname)
    {

        // Open file for loading
        std::ifstream inFile;
        try
        {
            inFile.open(fname);
        }
        catch (std::ios_base::failure &inErr)
        {
            throw std::runtime_error{inErr.what()};
        }

        // Erase graph
        for (const auto &it : edges)
        {
            delete it;
        }
        edges.clear();
        numEdges = 0;

        for (const auto &it : vertices)
        {
            delete it;
        }
        vertices.clear();
        numVertices = 0;
        sourceVertex = -1;
        terminalVertex = -1;

        // Load data
        auto nEdges = 0;

        inFile >> numVertices;
        inFile >> nEdges;
        inFile >> sourceVertex;
        inFile >> terminalVertex;

        vertices.reserve(numVertices);
        for (int i = 0; i < numVertices; ++i)
        {
            vertices.emplace_back(new vertex("", -1, std::round(std::sqrt(nEdges))));
        }

        edges.reserve(nEdges);
        for (int iE = 0; iE < nEdges; ++iE)
        {
            auto vA = 0;
            auto vB = 0;
            auto w = 0.0;
            auto flowLB = 0;
            auto flow = 0;
            auto flowUB = 0;

            inFile >> vA >> vB >> w >> flowLB >> flow >> flowUB;
            addEdge(vA, vB, "", w, flowLB, flow, flowUB);
        }
    }

    void graph::getEdges(std::vector<edge_t> &e) const
    {
        e = edges;
    }

    void graph::getVertex(int number, vertex_t &v) const
    {
        if ((number < getNumVertices()) && (number >= 0))
        {
            v = vertices[number];
        }
        else
        {
            std::cerr << "ERROR in graph::getVertex(): vertex number out of bounds." << std::endl;
            exit(2);
        }
    }

    void graph::getVertices(std::vector<vertex_t> &v) const
    {
        v = vertices;
    }

    void graph::setSourceVertex(int vertex)
    {
        if ((vertex >= getNumVertices()) | (vertex < 0))
        {
            std::cerr << "ERROR in graph::setSourceVertex(): source vertex out of bounds <0, " << getNumVertices() - 1 << ">." << std::endl;
            exit(2);
        }
        sourceVertex = vertex;
    }

    void graph::setTerminalVertex(int vertex)
    {
        if ((vertex >= getNumVertices()) | (vertex < 0))
        {
            std::cerr << "ERROR in graph::setTerminalVertex(): terminal vertex out of bounds <0, " << getNumVertices() - 1 << ">." << std::endl;
            exit(2);
        }
        terminalVertex = vertex;
    }

    void graph::getFlowsThroughVertices(std::vector<int> &inFlow, std::vector<int> &outFlow) const
    {
        inFlow.resize(getNumVertices(), 0);
        outFlow.resize(getNumVertices(), 0);

        for (const auto &it : edges)
        {
            auto head = it->getHeadVertex();
            auto tail = it->getTailVertex();
            outFlow[tail] += it->getFlow();
            inFlow[head] += it->getFlow();
        }
    }

    bool graph::checkKirchhoffLaw() const
    {
        bool check = true;

        std::vector<int> flowBalances(getNumVertices());

        for (int i = 0; i < getNumVertices(); ++i)
        {
            flowBalances[i] = vertices[i]->getFlowBalance();
        }

        for (int i = 0; i < getNumVertices(); ++i)
        {
            if ((flowBalances[i] != 0) & (i != getSourceVertex()) & (i != getTerminalVertex()))
            {
                check = false;
                break;
            }
        }

        return check;
    }

    bool graph::checkFeasibleFlow() const
    {
        bool isFeasible = true;

        for (const auto &it : edges)
        {
            if ((it->getFlow() > it->getFlowUpperBound()) | (it->getFlow() < it->getFlowLowerBound()))
            {
                isFeasible = false;
                break;
            }
        }

        if (isFeasible == true)
        {
            if (checkKirchhoffLaw() == false)
            {
                isFeasible = false;
            }
        }

        return isFeasible;
    }

    bool graph::findFeasibleFlow(maxFlowAlgorithms algorithm)
    {
        // check situation when the flow bounds contradict
        for (const auto &it : edges)
        {
            if (it->getFlowUpperBound() < it->getFlowLowerBound())
            {
                return false;
            }
        }

        graph newGraph(getNumVertices() + 2);
        for (const auto &it : edges)
        {
            newGraph.addEdge(it->getTailVertex(), it->getHeadVertex(), it->getLabel(), it->getWeight(), 0, 0, it->getFlowUpperBound() - it->getFlowLowerBound());
        }

        // arc from terminal to source with infinite capacity
        newGraph.addEdge(getTerminalVertex(), getSourceVertex(), "", 1.0, 0, 0, std::numeric_limits<int>::max());

        // compute balances
        std::vector<int> balances(getNumVertices(), 0);
        for (const auto &it : edges)
        {
            auto tail = it->getTailVertex();
            auto head = it->getHeadVertex();
            balances[tail] -= it->getFlowLowerBound();
            balances[head] += it->getFlowLowerBound();
        }

        // mark new source and terminal
        newGraph.setSourceVertex(getNumVertices());
        newGraph.setTerminalVertex(getNumVertices() + 1);

        // add edges according to balances
        for (unsigned int i = 0; i < balances.size(); ++i)
        {
            if (balances[i] > 0)
            {
                newGraph.addEdge(getNumVertices(), (int)i, "", 1.0, 0, 0, balances[i]);
            }
            else if (balances[i] < 0)
            {
                newGraph.addEdge((int)i, getNumVertices() + 1, "", 1.0, 0, 0, -balances[i]);
            }
        }

        // solve the Feasible flow decision problem
        int maxFlow;
        std::vector<int> minCutVertices;
        newGraph.maximumFlow(algorithm, minCutVertices, maxFlow);

        // check if added edges are saturated
        bool isFeasible = true;
        for (int i = getNumEdges() + 1; i < newGraph.getNumEdges(); ++i)
        {
            if (newGraph.edges[i]->getFlow() < newGraph.edges[i]->getFlowUpperBound())
            {
                isFeasible = false;
                break;
            }
        }

        // copy the feasible flow to the original graph
        if (isFeasible)
        {
            for (int i = 0; i < getNumEdges(); ++i)
            {
                edges[i]->setFlow(edges[i]->getFlowLowerBound() + newGraph.edges[i]->getFlow());
            }
        }

        return isFeasible;
    }

    void graph::maximumFlow(maxFlowAlgorithms algorithm, std::vector<int> &minCutVertices, int &maxFlow)
    {
        setDotFiledotFileFlowLabels(true);
        bool isFeasible = true;

        if (checkFeasibleFlow() == false)
        {
            isFeasible = findFeasibleFlow(algorithm);
        }

        if (isFeasible == true)
        {
            int capacity = 1;

            while (capacity > 0)
            {
                switch (algorithm)
                {
                case maxFlowAlgorithms::FordFulkerson:
                {
                    capacity = augmentFlowFordFulkerson(minCutVertices);
                    break;
                }

                case maxFlowAlgorithms::EdmondsKarp:
                {
                    capacity = augmentFlowEdmondsKarp(minCutVertices);
                    break;
                }

                case maxFlowAlgorithms::Dinic:
                {
                    capacity = augmentFlowDinic(minCutVertices);
                    break;
                }

                default:
                {
                    std::cerr << "ERROR in graph::maximumFlow(): unknown maximum flow algorithm." << std::endl;
                    exit(2);
                    break;
                }
                }
            }

            maxFlow = vertices[getSourceVertex()]->getFlowBalance();
        }
        else
        {
            minCutVertices = {};
            maxFlow = -1;
        }
    }

    void graph::fordFulkerson(std::vector<int> &minCutVertices, int &maxFlow)
    {
        maximumFlow(maxFlowAlgorithms::FordFulkerson, minCutVertices, maxFlow);
    }

    void graph::edmondsKarp(std::vector<int> &minCutVertices, int &maxFlow)
    {
        maximumFlow(maxFlowAlgorithms::EdmondsKarp, minCutVertices, maxFlow);
    }

    void graph::dinic(std::vector<int> &minCutVertices, int &maxFlow)
    {
        maximumFlow(maxFlowAlgorithms::Dinic, minCutVertices, maxFlow);
    }

    bool graph::isMaximumFlow(const std::vector<int> &set) const
    {
        auto notYetVisited = std::vector<bool>(vertices.size(), true);

        auto belongsToSink = std::vector<bool>(vertices.size(), true);
        for (auto vertexA : set)
        {
            belongsToSink.at(vertexA) = false;
        }

        if (belongsToSink[getSourceVertex()])
        {
            return false;
        }

        if (!belongsToSink[getTerminalVertex()])
        {
            return false;
        }

        if (!checkFeasibleFlow())
        {
            return false;
        }

        int outFlow = 0, inFlow = 0, outBound = 0, inBound = 0;
        for (int i = 0; i < getNumVertices(); ++i)
        {
            for (const auto &it : vertices[i]->getOutEdges())
            {
                if ((belongsToSink[it->getTailVertex()] == false) & (belongsToSink[it->getHeadVertex()]))
                {
                    outFlow += it->getFlow();
                    outBound += it->getFlowUpperBound();
                }

                if ((belongsToSink[it->getTailVertex()]) & (belongsToSink[it->getHeadVertex()] == false))
                {
                    inFlow += it->getFlow();
                    inBound += it->getFlowLowerBound();
                }
            }
        }

        if ((outFlow - inFlow) == (outBound - inBound))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    int graph::augmentFlowDinic(std::vector<int> &path)
    {
        if (getSourceVertex() == getTerminalVertex())
        {
            std::cerr << "ERROR in graph::augmentFlowDinic(): the source and terminal verices need to be different!" << std::endl;
            exit(2);
        }

        if (getSourceVertex() == -1)
        {
            std::cerr << "ERROR in graph::augmentFlowDinic(): the source vertex was not set!" << std::endl;
            exit(2);
        }

        if (getTerminalVertex() == -1)
        {
            std::cerr << "ERROR in graph::augmentFlowDinic(): the terminal vertex was not set!" << std::endl;
            exit(2);
        }

        path.clear();
        path.reserve(getNumVertices());
        int originalFlow = vertices[getSourceVertex()]->getFlowBalance();

        // BFS to create the level graph
        std::vector<int> levels(getNumVertices(), std::numeric_limits<int>::max());
        std::queue<int> queue;

        // mark the source
        queue.push(getSourceVertex());
        levels[getSourceVertex()] = 0;
        levels[getTerminalVertex()] = std::numeric_limits<int>::max();
        int level = 1;

        while ((!queue.empty()) && (level <= levels[getTerminalVertex()]))
        {
            // get the first element
            int topVertex = queue.front();

            // remove it from the queue
            queue.pop();

            // increase level
            level += 1;

            // forward edges
            for (const auto &it : vertices[topVertex]->getOutEdges())
            {
                auto nextVertex = it->getHeadVertex();
                if ((levels[nextVertex] == std::numeric_limits<int>::max()) & (it->getFlow() < it->getFlowUpperBound()))
                {
                    levels[nextVertex] = level;
                    queue.push(nextVertex);
                }
            }

            // backward edges
            for (const auto &it : vertices[topVertex]->getInEdges())
            {
                auto nextVertex = it->getTailVertex();
                if ((levels[nextVertex] == std::numeric_limits<int>::max()) & (it->getFlow() > it->getFlowLowerBound()))
                {
                    levels[nextVertex] = level;
                    queue.push(nextVertex);
                }
            }
        }

        if (levels[getTerminalVertex()] != std::numeric_limits<int>::max())
        {
            // DFS to augment flow in the layer graph
            while (true)
            {
                std::stack<int> dfsStack;
                dfsStack.push(getTerminalVertex());
                std::vector<edge_t> usedEdges(getNumVertices());
                std::vector<int> capacities(getNumVertices(), 0);

                // get augmenting path
                while ((!dfsStack.empty()) & (!usedEdges[getSourceVertex()]))
                {
                    auto vertex = dfsStack.top();
                    dfsStack.pop();
                    auto curLevel = levels[vertex];

                    for (const auto &it : vertices[vertex]->getOutEdges())
                    {
                        auto head = it->getHeadVertex();
                        auto cap = it->getFlow() - it->getFlowLowerBound();
                        if ((levels[head] == (curLevel - 1)) & (cap > 0))
                        {
                            dfsStack.push(head);
                            usedEdges[head] = it;
                            capacities[head] = -cap;
                        }
                    }

                    for (const auto &it : vertices[vertex]->getInEdges())
                    {
                        auto tail = it->getTailVertex();
                        auto cap = it->getFlowUpperBound() - it->getFlow();
                        if ((levels[tail] == (curLevel - 1)) & (cap > 0))
                        {
                            dfsStack.push(tail);
                            usedEdges[tail] = it;
                            capacities[tail] = cap;
                        }
                    }
                }

                // compute capacity
                int augCapacity = std::numeric_limits<int>::max();
                int curVertex = getSourceVertex();
                if (!usedEdges[getSourceVertex()])
                {
                    augCapacity = 0;
                }
                else
                {
                    while (curVertex != getTerminalVertex())
                    {
                        augCapacity = std::min(augCapacity, std::abs(capacities[curVertex]));

                        if (capacities[curVertex] > 0)
                        {
                            curVertex = usedEdges[curVertex]->getHeadVertex();
                        }
                        else
                        {
                            curVertex = usedEdges[curVertex]->getTailVertex();
                        }
                    }
                }

                if (augCapacity > 0)
                {
                    // augment flow
                    int curVertex = getSourceVertex();
                    while (curVertex != getTerminalVertex())
                    {
                        if (capacities[curVertex] > 0)
                        {
                            usedEdges[curVertex]->setFlow(usedEdges[curVertex]->getFlow() + augCapacity);
                            curVertex = usedEdges[curVertex]->getHeadVertex();
                        }
                        else
                        {
                            usedEdges[curVertex]->setFlow(usedEdges[curVertex]->getFlow() - augCapacity);
                            curVertex = usedEdges[curVertex]->getTailVertex();
                        }
                    }
                }
                else
                {
                    break;
                }
            }
        }
        else
        {
            // get vertices defining minimum cut
            for (int i = 0; i < getNumVertices(); ++i)
            {
                if (levels[i] < std::numeric_limits<int>::max())
                {
                    path.push_back(i);
                }
            }
        }

        return vertices[getSourceVertex()]->getFlowBalance() - originalFlow;
    }

    void graph::getCutFromSet(const std::vector<int> &set, std::vector<edge_t> &e) const
    {
        e.resize(0);

        auto notYetVisited = std::vector<bool>(vertices.size(), true);

        // Label graph nodes belonging to sink
        auto belongsToSink = std::vector<bool>(vertices.size(), true);
        for (const auto &vertexA : set)
        {
            belongsToSink.at(vertexA) = false;
        }

        for (const auto &vertexA : set)
        {
            if (notYetVisited.at(vertexA))
            {
                notYetVisited.at(vertexA) = false;
                for (auto &outEdge : vertices.at(vertexA)->getOutEdges())
                {
                    if (belongsToSink.at(outEdge->getHeadVertex()))
                    {
                        e.push_back(outEdge);
                    }
                }
            }
        }
    }

    int graph::augmentFlowEdmondsKarp(std::vector<int> &path)
    {
        if (getSourceVertex() == getTerminalVertex())
        {
            std::cerr << "ERROR in graph::augmentFlowEdmondsKarp(): the source and terminal verices need to be different!" << std::endl;
            exit(2);
        }

        if (getSourceVertex() == -1)
        {
            std::cerr << "ERROR in graph::augmentFlowEdmondsKarp(): the source vertex was not set!" << std::endl;
            exit(2);
        }

        if (getTerminalVertex() == -1)
        {
            std::cerr << "ERROR in graph::augmentFlowEdmondsKarp(): the terminal vertex was not set!" << std::endl;
            exit(2);
        }

        int capacity = 0;
        path.clear();
        path.reserve(getNumVertices());
        std::vector<bool> isMarked(getNumVertices(), false);
        std::vector<bool> forwardDir(getNumVertices(), false);
        std::vector<edge_t> usedEdges(getNumVertices());
        std::queue<int> queue;

        // mark the source
        queue.push(getSourceVertex());
        isMarked[getSourceVertex()] = true;

        while ((!queue.empty()) && (isMarked[getTerminalVertex()] == false))
        {
            // get the first element
            int topVertex = queue.front();

            // remove it from the queue
            queue.pop();

            // forward edges
            for (const auto &it : vertices[topVertex]->getOutEdges())
            {
                auto nextVertex = it->getHeadVertex();
                if ((isMarked[nextVertex] == false) & (it->getFlow() < it->getFlowUpperBound()))
                {
                    isMarked[nextVertex] = true;
                    forwardDir[nextVertex] = true;
                    queue.push(nextVertex);
                    usedEdges[nextVertex] = it;
                }
            }

            // backward edges
            for (const auto &it : vertices[topVertex]->getInEdges())
            {
                auto nextVertex = it->getTailVertex();
                if ((isMarked[nextVertex] == false) & (it->getFlow() > it->getFlowLowerBound()))
                {
                    isMarked[nextVertex] = true;
                    forwardDir[nextVertex] = false;
                    queue.push(nextVertex);
                    usedEdges[nextVertex] = it;
                }
            }
        }

        // capacity to sufficiently large number
        capacity = std::numeric_limits<int>::max();

        // reconstruct the path and compute capacity
        if (isMarked[getTerminalVertex()])
        {
            int curVertex = getTerminalVertex();
            while (true)
            {
                path.push_back(curVertex);

                if (!usedEdges[curVertex])
                {
                    break;
                }

                int curEdgeCapacity;

                if (forwardDir[curVertex] == true)
                {
                    curEdgeCapacity = usedEdges[curVertex]->getFlowUpperBound() - usedEdges[curVertex]->getFlow();
                    curVertex = usedEdges[curVertex]->getTailVertex();
                }
                else
                {
                    curEdgeCapacity = usedEdges[curVertex]->getFlow() - usedEdges[curVertex]->getFlowLowerBound();
                    curVertex = usedEdges[curVertex]->getHeadVertex();
                }

                if (capacity > curEdgeCapacity)
                {
                    capacity = curEdgeCapacity;
                }
            }

            // augment the flow
            curVertex = getTerminalVertex();
            while (usedEdges[curVertex])
            {
                if (forwardDir[curVertex] == true)
                {
                    usedEdges[curVertex]->setFlow(usedEdges[curVertex]->getFlow() + capacity);
                    curVertex = usedEdges[curVertex]->getTailVertex();
                }
                else
                {
                    usedEdges[curVertex]->setFlow(usedEdges[curVertex]->getFlow() - capacity);
                    curVertex = usedEdges[curVertex]->getHeadVertex();
                }
            }
        }
        else
        {
            // get vertices defining minimum cut
            for (unsigned int i = 0; i < isMarked.size(); ++i)
            {
                if (isMarked[i])
                {
                    path.push_back(i);
                }
                capacity = 0;
            }
        }

        return capacity;
    }

    int graph::augmentFlowFordFulkerson(std::vector<int> &path)
    {
        if (getSourceVertex() == getTerminalVertex())
        {
            std::cerr << "ERROR in graph::augmentFlowEdmondsKarp(): the source and terminal verices need to be different!" << std::endl;
            exit(2);
        }

        if (getSourceVertex() == -1)
        {
            std::cerr << "ERROR in graph::augmentFlowEdmondsKarp(): the source vertex was not set!" << std::endl;
            exit(2);
        }

        if (getTerminalVertex() == -1)
        {
            std::cerr << "ERROR in graph::augmentFlowEdmondsKarp(): the terminal vertex was not set!" << std::endl;
            exit(2);
        }

        int capacity = 0;
        path.clear();
        path.reserve(getNumVertices());
        std::vector<bool> isMarked(getNumVertices(), false);
        std::vector<bool> forwardDir(getNumVertices(), false);
        std::vector<edge_t> usedEdges(getNumVertices());
        std::stack<int> stack;

        // mark the source
        stack.push(getSourceVertex());
        isMarked[getSourceVertex()] = true;

        while ((!stack.empty()) && (isMarked[getTerminalVertex()] == false))
        {
            // get the first element
            int topVertex = stack.top();

            // remove it from the queue
            stack.pop();

            // forward edges
            for (const auto &it : vertices[topVertex]->getOutEdges())
            {
                auto nextVertex = it->getHeadVertex();
                if ((isMarked[nextVertex] == false) & (it->getFlow() < it->getFlowUpperBound()))
                {
                    isMarked[nextVertex] = true;
                    forwardDir[nextVertex] = true;
                    stack.push(nextVertex);
                    usedEdges[nextVertex] = it;
                }
            }

            // backward edges
            for (const auto &it : vertices[topVertex]->getInEdges())
            {
                auto nextVertex = it->getTailVertex();
                if ((isMarked[nextVertex] == false) & (it->getFlow() > it->getFlowLowerBound()))
                {
                    isMarked[nextVertex] = true;
                    forwardDir[nextVertex] = false;
                    stack.push(nextVertex);
                    usedEdges[nextVertex] = it;
                }
            }
        }

        // capacity to sufficiently large number
        capacity = std::numeric_limits<int>::max();

        // reconstruct the path and compute capacity
        if (isMarked[getTerminalVertex()])
        {
            int curVertex = getTerminalVertex();
            while (true)
            {
                path.push_back(curVertex);

                if (!usedEdges[curVertex])
                {
                    break;
                }

                int curEdgeCapacity;

                if (forwardDir[curVertex] == true)
                {
                    curEdgeCapacity = usedEdges[curVertex]->getFlowUpperBound() - usedEdges[curVertex]->getFlow();
                    curVertex = usedEdges[curVertex]->getTailVertex();
                }
                else
                {
                    curEdgeCapacity = usedEdges[curVertex]->getFlow() - usedEdges[curVertex]->getFlowLowerBound();
                    curVertex = usedEdges[curVertex]->getHeadVertex();
                }

                if (capacity > curEdgeCapacity)
                {
                    capacity = curEdgeCapacity;
                }
            }

            // augment the flow
            curVertex = getTerminalVertex();
            while (usedEdges[curVertex])
            {
                if (forwardDir[curVertex] == true)
                {
                    usedEdges[curVertex]->setFlow(usedEdges[curVertex]->getFlow() + capacity);
                    curVertex = usedEdges[curVertex]->getTailVertex();
                }
                else
                {
                    usedEdges[curVertex]->setFlow(usedEdges[curVertex]->getFlow() - capacity);
                    curVertex = usedEdges[curVertex]->getHeadVertex();
                }
            }
        }
        else
        {
            // get vertices defining minimum cut
            for (unsigned int i = 0; i < isMarked.size(); ++i)
            {
                if (isMarked[i])
                {
                    path.push_back(i);
                }
                capacity = 0;
            }
        }

        return capacity;
    }

    void graph::residualGraph(graph &resGraph) const
    {
        if (resGraph.getNumVertices() != getNumVertices())
        {
            resGraph = graph(getNumVertices());
        }

        for (int i = 0; i < getNumVertices(); ++i)
        {
            for (const auto &it : vertices[i]->getOutEdges())
            {
                auto flowDifferenceU = it->getFlowUpperBound() - it->getFlow();
                auto flowDifferenceL = it->getFlow() - it->getFlowLowerBound();

                if (flowDifferenceU > 0)
                {
                    resGraph.addEdge(it->getTailVertex(), it->getHeadVertex(), "", it->getWeight(), 0, 0, flowDifferenceU);
                }

                if (flowDifferenceL > 0)
                {
                    resGraph.addEdge(it->getHeadVertex(), it->getTailVertex(), "", -1 * it->getWeight(), 0, 0, flowDifferenceL);
                }
            }
        }
    }

    double graph::cycleCanceling()
    {
        // check if source and terminal are marked!
        if (getSourceVertex() == getTerminalVertex())
        {
            std::cerr << "ERROR in graph::cycleCanceling(): The source and terminal verices need to be different!" << std::endl;
            exit(2);
        }

        if (getSourceVertex() == -1)
        {
            std::cerr << "ERROR in graph::cycleCanceling(): The source vertex was not set!" << std::endl;
            exit(2);
        }

        if (getTerminalVertex() == -1)
        {
            std::cerr << "ERROR in graph::cycleCanceling(): The terminal vertex was not set!" << std::endl;
            exit(2);
        }

        // feasible flow
        bool isFeasible = true;
        if (checkFeasibleFlow() == false)
        {
            isFeasible = findFeasibleFlow(maxFlowAlgorithms::EdmondsKarp);
        }

        if (isFeasible == true)
        {
            int capacity = 1;

            while (capacity > 0)
            {
                // find cost negative cycle
                capacity = augmentFlowCostNegativeCycle();
            }
        }
        else
        {
            std::cerr << "Warning in graph::cycleCanceling(): feasible flow does not exist!" << std::endl;
        }

        // compute the cost
        double totalCost = 0.0;
        for (const auto &it : edges)
        {
            totalCost += it->getWeight() * it->getFlow();
        }

        return totalCost;
    }

    bool graph::isMultiGraph() const
    {
        bool hasParallelArcs = false;
        for (int i = 0; i < getNumVertices(); ++i)
        {
            std::vector<bool> heads(getNumVertices(), false);
            for (const auto &it : vertices[i]->getOutEdges())
            {
                auto head = it->getHeadVertex();
                if (heads[head])
                {
                    hasParallelArcs = true;
                    break;
                }
                else
                {
                    heads[head] = true;
                }
            }
        }
        return hasParallelArcs;
    }

    bool graph::isWeightedEqually() const
    {
        if (getNumEdges() == 0)
        {
            return true;
        }
        else
        {
            auto w = edges[0]->getWeight();
            for (const auto &it : edges)
            {
                if (it->getWeight() != w)
                {
                    return false;
                }
            }
        }

        return true;
    }

    bool graph::isWeightedNegatively() const
    {
        return std::any_of(edges.cbegin(), edges.cend(), [](const edge_t e)
                           { return e->getWeight() < 0.0; });
    }

    int graph::augmentFlowCostNegativeCycle()
    {
        std::vector<int> cycle;
        cycle.reserve(getNumVertices());
        int capacity = -1;

        std::vector<double> len;
        std::vector<edge_t> prevNode;

        bellmanFordResidual(len, prevNode);

        // find vertex that has a negative cycle
        int hasCycle = -1;
        for (const auto &it : edges)
        {
            auto flowDifferenceL = it->getFlow() - it->getFlowLowerBound();
            auto flowDifferenceU = it->getFlowUpperBound() - it->getFlow();

            if (flowDifferenceU > 0)
            {
                auto v = it->getTailVertex();
                auto w = it->getHeadVertex();

                double curVal = len[v] + it->getWeight();

                if (len[w] > curVal)
                {
                    hasCycle = v;
                    break;
                }
            }

            if (flowDifferenceL > 0)
            {
                auto v = it->getHeadVertex();
                auto w = it->getTailVertex();

                double curVal = len[v] - it->getWeight();

                if (len[w] > curVal)
                {
                    hasCycle = v;
                    break;
                }
            }
        }

        // find the cycle
        if (hasCycle > -1)
        {
            std::vector<bool> pathSet(getNumVertices(), false);
            auto next = hasCycle;

            while (pathSet[next] == false)
            {
                pathSet[next] = true;
                cycle.push_back(next);

                if (!prevNode[next])
                {
                    next = -1;
                }
                else if (next == prevNode[next]->getTailVertex())
                {
                    next = prevNode[next]->getHeadVertex();
                }
                else
                {
                    next = prevNode[next]->getTailVertex();
                }

                if (next == -1)
                {
                    std::cerr << "ERROR in graph::augmentFlowCostNegativeCycle(): the path forms a tree, not a cycle! However, Bellman-Ford reports that there is a negative cycle." << std::endl;
                    exit(2);
                }
            }
            cycle.push_back(next);

            // we know that next is in the cycle, so we search its first occurance
            auto cycleBeg = std::find(cycle.begin(), cycle.end(), next);
            if (cycleBeg != cycle.begin())
            {
                cycle.erase(cycle.begin(), cycleBeg);
            }
            std::reverse(cycle.begin(), cycle.end());

            // determine capacity
            capacity = std::numeric_limits<int>::max();
            for (unsigned int i = 1; i < cycle.size(); ++i)
            {
                auto tail = prevNode[cycle[i]]->getTailVertex();
                if (tail == cycle[i - 1])
                {
                    auto flowDifferenceU = prevNode[cycle[i]]->getFlowUpperBound() - prevNode[cycle[i]]->getFlow();
                    capacity = std::min(capacity, flowDifferenceU);
                }
                else
                {
                    auto flowDifferenceL = prevNode[cycle[i]]->getFlow() - prevNode[cycle[i]]->getFlowLowerBound();
                    capacity = std::min(capacity, flowDifferenceL);
                }
            }

            // increase capacity if positive
            if (capacity > 0)
            {
                for (unsigned int i = 1; i < cycle.size(); ++i)
                {
                    auto tail = prevNode[cycle[i]]->getTailVertex();
                    if (tail == cycle[i - 1])
                    {
                        prevNode[cycle[i]]->setFlow(prevNode[cycle[i]]->getFlow() + capacity);
                    }
                    else
                    {
                        prevNode[cycle[i]]->setFlow(prevNode[cycle[i]]->getFlow() - capacity);
                    }
                }
            }
            else
            {
                std::cerr << "ERROR in graph::augmentFlowCostNegativeCycle(): Capacity should be a positive number!" << std::endl;
                exit(2);
            }
        }

        return capacity;
    }

    void graph::retrieveShortestPath(int terminal, std::vector<edge_t> &pEdge, std::vector<edge_t> &path) const
    {
        std::vector<bool> visited(getNumVertices(), false);

        path.clear();
        int vert = terminal;

        while (vert != -1)
        {
            if (visited[vert] == true)
            {
                break;
            }

            visited[vert] = true;
            if (pEdge[vert] == nullptr)
            {
                vert = -1;
            }
            else
            {
                path.push_back(pEdge[vert]);
                vert = pEdge[vert]->getTailVertex();
            }
        }

        std::reverse(path.begin(), path.end());
    }

    bool graph::isAcyclic() const
    {
        bool acyclic = true;

        std::vector<int> topOrder;
        topologicalSort(topOrder);
        if (topOrder.back() == -1)
        {
            acyclic = false;
        }

        return acyclic;
    }

    void graph::DAGLongestPath(int source, std::vector<double> &len, std::vector<edge_t> &pEdge)
    {
        DAGLongestPath(source, len, pEdge, false);
    }

    void graph::DAGLongestPath(int source, std::vector<double> &len, std::vector<edge_t> &pEdge, bool isTopOrdered)
    {
        // reverse edge weights
        for (const auto &e : edges)
        {
            e->setWeight(-e->getWeight());
        }

        // solve DAGShortestPaths
        DAGShortestPath(source, len, pEdge, isTopOrdered);

        // restore original weights
        for (const auto &e : edges)
        {
            e->setWeight(-e->getWeight());
        }

        // reverse distances
        for (unsigned int i = 0; i < len.size(); ++i)
        {
            len[i] *= -1;
        }
    }

    void graph::DAGShortestPath(int source, std::vector<double> &len, std::vector<edge_t> &pEdge) const
    {
        DAGShortestPath(source, len, pEdge, false);
    }

    void graph::DAGShortestPath(int source, std::vector<double> &len, std::vector<edge_t> &pEdge, bool isTopOrdered) const
    {
        len.resize(getNumVertices(), std::numeric_limits<double>::infinity());
        len[source] = 0;
        pEdge.resize(getNumVertices(), nullptr);

        // topological order
        std::vector<int> topOrder;
        if (isTopOrdered == false)
        {
            topologicalSort(topOrder);
            if (topOrder.back() == -1)
            {
                std::cerr << "ERROR in graph::DAGShortestPath(): the graph is not acyclic. DAG shortest path can not work." << std::endl;
                exit(2);
            }
        }
        else
        {
            topOrder.resize(getNumVertices());
            std::iota(topOrder.begin(), topOrder.end(), 0);
        }

        // main loop
        for (int i = 0; i < getNumVertices(); ++i)
        {
            auto tail = topOrder[i];

            for (const auto &it : vertices[tail]->getOutEdges())
            {
                auto head = it->getHeadVertex();

                double curVal = len[tail] + it->getWeight();

                if (len[head] > curVal)
                {
                    len[head] = curVal;
                    pEdge[head] = it;
                }
            }
        }
    }

    void graph::breadthFirstSearch(int source, std::vector<int> &level, std::vector<edge_t> &pEdge) const
    {
        level.resize(getNumVertices(), -1);
        pEdge.resize(getNumVertices(), nullptr);

        std::queue<int> queue;

        // mark the source
        queue.push(source);
        level[source] = 0;

        while (!queue.empty())
        {
            // get the first element
            auto topVertex = queue.front();

            // remove it from the queue
            queue.pop();

            // iterate through outcoming edges
            for (const auto &it : vertices[topVertex]->getOutEdges())
            {
                auto nextVertex = it->getHeadVertex();
                if (level[nextVertex] == -1)
                {
                    level[nextVertex] = level[topVertex] + 1;
                    pEdge[nextVertex] = it;
                    queue.push(nextVertex);
                }
            }
        }
    }

    void graph::depthFirstSearch(int source, std::vector<int> &level, std::vector<edge_t> &pEdge) const
    {
        level.resize(getNumVertices(), -1);
        pEdge.resize(getNumVertices(), nullptr);

        std::stack<int> stack;

        // mark the source
        stack.push(source);
        level[source] = 0;

        while (!stack.empty())
        {
            // get the first element
            auto topVertex = stack.top();

            // remove it from the stack
            stack.pop();

            // iterate through outcoming edges
            for (const auto &it : vertices[topVertex]->getOutEdges())
            {
                auto nextVertex = it->getHeadVertex();
                if (level[nextVertex] == -1)
                {
                    level[nextVertex] = level[topVertex] + 1;
                    pEdge[nextVertex] = it;
                    stack.push(nextVertex);
                }
            }
        }
    }

    void graph::dijkstra(int source, std::vector<double> &len, std::vector<edge_t> &pEdge) const
    {
        dijkstra(source, -1, len, pEdge);
    }

    void graph::dijkstra(int source, int terminal, std::vector<double> &len, std::vector<edge_t> &pEdge) const
    {
        if (isWeightedNegatively())
        {
            std::cerr << "ERROR in graph::dijkstra(): negative costs are not supported in Dijkstra algorithm!" << std::endl;
            exit(2);
        }

        // initialize distances
        len.resize(getNumVertices(), std::numeric_limits<double>::infinity());
        len[source] = 0;
        std::vector<bool> inSet(getNumVertices(), false);

        // initialize predecedors
        pEdge.resize(getNumVertices(), nullptr);

        // initialize queue with the source vertex <distance,vertex>
        std::set<std::pair<double, int>> queue;
        queue.insert(std::make_pair(0.0, source));

        while (!queue.empty())
        {
            auto top = queue.begin();
            int vertexA = top->second;
            queue.erase(top);
            inSet[vertexA] = true;

            // if vertexA is permanent
            if (vertexA == terminal)
            {
                break;
            }

            for (const auto &e : vertices[vertexA]->getOutEdges())
            {
                auto vertexB = e->getHeadVertex();
                auto cost = e->getWeight();
                auto rhs = len[vertexA] + cost;

                if ((len[vertexB] > rhs) && (!inSet[vertexB]))
                {
                    if (len[vertexB] != std::numeric_limits<double>::infinity())
                    {
                        auto it = queue.find(std::make_pair(len[vertexB], vertexB));
                        if (it != queue.end())
                        {
                            queue.erase(it);
                        }
                    }

                    len[vertexB] = rhs;
                    pEdge[vertexB] = e;
                    queue.insert(std::make_pair(len[vertexB], vertexB));
                }
            }
        }
    }

    void graph::getEdgeTailVertices(const std::vector<edge_t> &pEdges, std::vector<int> &prevNode)
    {
        prevNode.resize(pEdges.size(), -1);
        for (unsigned int i = 0; i < pEdges.size(); ++i)
        {
            if (pEdges[i] != nullptr)
            {
                prevNode[i] = pEdges[i]->getTailVertex();
            }
        }
    }

    void graph::primJarnik(double &cost, std::vector<edge_t> &pEdge) const
    {
        if (!isUndirected())
        {
            std::cerr << "ERROR in primJarnik(): input graph is not undirected." << std::endl;
            exit(2);
        }

        // initialize distances
        std::vector<double> len(getNumVertices(), std::numeric_limits<double>::infinity());
        std::vector<bool> inSet(getNumVertices(), false);
        cost = 0.0;

        // the first vertex may be arbitrary
        int vertex = 0;
        len[vertex] = 0;

        // initialize predecedors
        pEdge.resize(getNumVertices(), nullptr);

        // initialize queue with the source vertex <distance,vertex>
        std::set<std::pair<double, int>> queue;
        queue.insert(std::make_pair(0.0, vertex));

        while (!queue.empty())
        {
            auto top = queue.begin();
            int vertexA = top->second;
            queue.erase(top);
            inSet[vertexA] = true;

            for (const auto &e : vertices[vertexA]->getOutEdges())
            {
                auto vertexB = e->getHeadVertex();
                auto c = e->getWeight();

                if ((len[vertexB] > c) && (!inSet[vertexB]))
                {
                    if (len[vertexB] != std::numeric_limits<double>::infinity())
                    {
                        auto it = queue.find(std::make_pair(len[vertexB], vertexB));
                        if (it != queue.end())
                        {
                            queue.erase(it);
                        }
                    }

                    len[vertexB] = c;
                    pEdge[vertexB] = e;
                    queue.insert(std::make_pair(len[vertexB], vertexB));
                }
            }

            // handling disconnected graphs
            if (queue.empty())
            {
                for (int i = 0; i < getNumVertices(); i++)
                {
                    if (!inSet[i])
                    {
                        queue.insert(std::make_pair(0.0, i));
                        break;
                    }
                }
            }
        }

        for (const auto &it : pEdge)
        {
            if (it)
            {
                cost += it->getWeight();
            }
        }
    }

    void graph::bellmanFord(int source, std::vector<double> &len, std::vector<edge_t> &pEdge) const
    {
        len.resize(getNumVertices(), std::numeric_limits<double>::infinity());
        len[source] = 0;

        pEdge.resize(getNumVertices(), nullptr);

        for (int i = 0; i < getNumVertices() - 1; ++i)
        {
            bool isChanged = false;

            for (const auto &it : edges)
            {
                auto v = it->getTailVertex();
                auto w = it->getHeadVertex();
                double curVal = len[v] + it->getWeight();

                if (len[w] > curVal)
                {
                    len[w] = curVal;
                    pEdge[w] = it;
                    isChanged = true;
                }
            }

            // if no change in previous iteration, we do not continue
            if (isChanged == false)
            {
                break;
            }
        }
    }

    void graph::bellmanFordResidual(std::vector<double> &len, std::vector<edge_t> &prevNode) const
    {
        /** Bellman-Ford algorithm on implicit residual graph **/

        std::vector<edge_t> addedEdges(getNumVertices());
        len.resize(getNumVertices(), 0);

        prevNode.resize(getNumVertices());

        for (int i = 0; i < getNumVertices() - 1; ++i)
        {
            bool isChanged = false;

            for (const auto &it : edges)
            {
                auto flowDifferenceL = it->getFlow() - it->getFlowLowerBound();
                auto flowDifferenceU = it->getFlowUpperBound() - it->getFlow();

                if (flowDifferenceU > 0)
                {
                    auto v = it->getTailVertex();
                    auto w = it->getHeadVertex();

                    double curVal = len[v] + it->getWeight();

                    if (len[w] > curVal)
                    {
                        len[w] = curVal;
                        prevNode[w] = it;
                        isChanged = true;
                    }
                }

                if (flowDifferenceL > 0)
                {
                    auto v = it->getHeadVertex();
                    auto w = it->getTailVertex();

                    double curVal = len[v] - it->getWeight();

                    if (len[w] > curVal)
                    {
                        len[w] = curVal;
                        prevNode[w] = it;
                        isChanged = true;
                    }
                }
            }

            // if no change in previous iteration, we do not continue
            if (isChanged == false)
            {
                break;
            }
        }
    }

    graph::graph(int nV)
    {
        numVertices = nV;
        numEdges = 0;
        sourceVertex = -1;
        terminalVertex = -1;
        dotFile_flowLabel = false;
        dotFile_weightLabel = true;

        // Resize vector of edges and try to allocate memory for a complete graph
        int reserveSize = 0;
        edges.resize(0);
        try
        {
            edges.reserve(nV * nV);
            reserveSize = nV;
        }
        catch (const std::bad_alloc &e)
        {
            std::cerr << "Warning: Complete graph cannot be allocated (std::bad_alloc exception returned).\n";
            edges.reserve(4 * nV);
            reserveSize = 4;
        }

        vertices.reserve(nV);
        for (int i = 0; i < nV; ++i)
        {
            vertices.emplace_back(new vertex("", -1, reserveSize));
        }
    }

    graph::graph(const graph &inGraph)
    {
        numVertices = inGraph.getNumVertices();
        numEdges = 0;
        sourceVertex = inGraph.getSourceVertex();
        terminalVertex = inGraph.getTerminalVertex();
        dotFile_flowLabel = inGraph.getDotFiledotFileFlowLabels();
        dotFile_weightLabel = inGraph.getDotFiledotFileWeightLabels();

        vertices.reserve(numVertices);
        for (const auto &v : inGraph.vertices)
        {
            auto reserveSize = std::max(v->getIndegree(), v->getOutdegree());
            vertices.emplace_back(new vertex(v->getLabel(), v->getColor(), reserveSize));
        }

        edges.reserve(inGraph.getNumEdges());
        for (const auto &e : inGraph.edges)
        {
            addEdge(e->getTailVertex(), e->getHeadVertex(), e->getLabel(), e->getWeight(), e->getFlowLowerBound(), e->getFlow(), e->getFlowUpperBound());
        }
    }

    const edge_t &graph::addEdge(int tail, int head)
    {
        return addEdge(tail, head, "", 1.0, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
    }

    void graph::addUndirectedEdge(int vertexA, int vertexB)
    {
        addEdge(vertexA, vertexB, "", 1.0, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
        addEdge(vertexB, vertexA, "", 1.0, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
    }

    const edge_t &graph::addEdge(int tail, int head, std::string label)
    {
        return addEdge(tail, head, label, 1.0, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
    }

    const edge_t &graph::addEdge(int tail, int head, double cost)
    {
        return addEdge(tail, head, "", cost, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
    }

    void graph::addUndirectedEdge(int vertexA, int vertexB, double cost)
    {
        addEdge(vertexA, vertexB, "", cost, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
        addEdge(vertexB, vertexA, "", cost, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
    }

    const edge_t &graph::addEdge(int tail, int head, std::string label, double cost)
    {
        return addEdge(tail, head, label, cost, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
    }

    void graph::addUndirectedEdge(int vertexA, int vertexB, std::string label, double cost)
    {
        addEdge(vertexA, vertexB, label, cost, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
        addEdge(vertexB, vertexA, label, cost, std::numeric_limits<int>::min(), 0, std::numeric_limits<int>::max());
    }

    const edge_t &graph::addEdge(int tail, int head, int flowLB, int flow, int flowUB)
    {
        return addEdge(tail, head, "", 1.0, flowLB, flow, flowUB);
    }

    void graph::addUndirectedEdge(int vertexA, int vertexB, int flowLB, int flow, int flowUB)
    {
        addEdge(vertexA, vertexB, "", 1.0, flowLB, flow, flowUB);
        addEdge(vertexB, vertexA, "", 1.0, flowLB, flow, flowUB);
    }

    const edge_t &graph::addEdge(int tail, int head, double cost, int flowLB, int flow, int flowUB)
    {
        return addEdge(tail, head, "", cost, flowLB, flow, flowUB);
    }

    void graph::addUndirectedEdge(int vertexA, int vertexB, std::string label, double cost, int flowLB, int flow, int flowUB)
    {
        addEdge(vertexA, vertexB, label, cost, flowLB, flow, flowUB);
        addEdge(vertexB, vertexA, label, cost, flowLB, flow, flowUB);
    }

    const edge_t &graph::addEdge(int tail, int head, std::string label, double cost, int flowLB, int flow, int flowUB)
    {
        if ((tail < 0) | (head < 0))
        {
            std::cerr << "ERROR in graph::addEdge(): vertices have to be non-negative! Input edge " << tail << "->" << head << "." << std::endl;
            exit(2);
        }

        if ((tail > getNumVertices() - 1) | (head > getNumVertices() - 1))
        {
            std::cerr << "ERROR in graph::addEdge(): vertices have to be lower than " << getNumVertices() << "! Input edge " << tail << "->" << head << "." << std::endl;
            exit(2);
        }

        edges.emplace_back(new edge(tail, head, cost, flowLB, flow, flowUB, label));
        vertices[tail]->addOutwardEdge(edges.back());
        vertices[head]->addInwardEdge(edges.back());

        numEdges += 1;

        return edges.back();
    }

    const vertex_t &graph::addVertex(std::string label, int color)
    {
        vertices.emplace_back(new vertex(label, color));
        numVertices += 1;

        return vertices.back();
    }

    void graph::randomizeEdgeOrder()
    {
#ifdef USE_OMP
#pragma omp parallel for
#endif
        for (int i = 0; i < getNumVertices(); ++i)
        {
            vertices[i]->randomizeEdgeOrder();
        }
    }

    void graph::getEdge(std::vector<edge_t> &fEdge, int tail, int head) const
    {
        if ((tail < 0) | (tail > getNumVertices() - 1))
        {
            std::cerr << "ERROR in graph::getEdge(): tail vertex out of bounds in graphInterface::graph::getEdge()." << std::endl;
            exit(2);
        }

        if ((head < 0) | (head > getNumVertices() - 1))
        {
            std::cerr << "ERROR in graph::getEdge(): head vertex out of bounds in graphInterface::graph::getEdge()." << std::endl;
            exit(2);
        }

        fEdge.clear();

        for (const auto &it : vertices[tail]->getOutEdges())
        {
            if (it->getHeadVertex() == head)
            {
                fEdge.push_back(it);
            }
        }
    }

    void graph::transposeGraph(graph &newGraph) const
    {
        reverseGraph(newGraph);
    }

    void graph::reverseGraph(graph &newGraph) const
    {
        newGraph = graph(getNumVertices());
        for (const auto &it : edges)
        {
            newGraph.addEdge(it->getHeadVertex(), it->getTailVertex(), it->getLabel(), it->getWeight(), it->getFlowLowerBound(), it->getFlow(), it->getFlowUpperBound());
        }
    }

    bool graph::isUndirected() const
    {
        bool isUndirected = true;
        std::vector<std::multiset<size_t>> lhVals(getNumVertices());
        std::vector<std::multiset<size_t>> rhVals(getNumVertices());

        for (const auto &it : edges)
        {
            auto tail = it->getTailVertex();
            auto head = it->getHeadVertex();
            auto weight = it->getWeight();
            auto hWeight = std::hash<double>()(weight);

            if (tail < head)
            {
                auto hHead = std::hash<double>()(head);
                size_t seed = 0;
                seed ^= hHead + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                seed ^= hWeight + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                lhVals[tail].insert(seed);
            }
            else if (head < tail)
            {
                auto hTail = std::hash<double>()(tail);
                size_t seed = 0;
                seed ^= hTail + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                seed ^= hWeight + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                rhVals[head].insert(seed);
            }
            // self loops (head == tail) are assumed to be undirected
        }

        // if the number of edges in multisets does not equal
        for (int i = 0; i < getNumVertices(); ++i)
        {
            if (lhVals[i].size() != rhVals[i].size())
            {
                isUndirected = false;
                return isUndirected;
            }
        }

        // compare the edges endpoints + weight
        for (int i = 0; i < getNumVertices(); ++i)
        {
            if (lhVals[i] != rhVals[i])
            {
                isUndirected = false;
            }
        }

        return isUndirected;
    }

    void graph::connectedComponents(std::vector<std::vector<int>> &components) const
    {
        components.resize(1);
        components.reserve(getNumVertices());
        components[0].reserve(getNumVertices());

        std::vector<bool> visited(getNumVertices(), false);
        int numVisits = 0;
        int queueInsert = 0;
        int componentNumber = 0;
        std::set<int> queue;

        while (true)
        {
            queue.insert(queueInsert);

            while (queue.size() > 0)
            {
                auto top = *(queue.begin());
                queue.erase(queue.begin());
                visited[top] = true;
                numVisits += 1;
                components[componentNumber].push_back(top);

                for (const auto &it : vertices[top]->getInEdges())
                {
                    auto vert = it->getTailVertex();
                    if (visited[vert] == false)
                    {
                        queue.insert(vert);
                    }
                }

                for (const auto &it : vertices[top]->getOutEdges())
                {
                    auto vert = it->getHeadVertex();
                    if (visited[vert] == false)
                    {
                        queue.insert(vert);
                    }
                }
            }

            auto visitedIterator = std::find(visited.begin(), visited.end(), false);
            if (visitedIterator == visited.end())
            {
                break;
            }
            else
            {
                queueInsert = std::distance(visited.begin(), visitedIterator);
                componentNumber += 1;
                components.resize(componentNumber + 1);
                components[componentNumber].reserve(getNumVertices() - numVisits);
            }
        }
    }

    bool graph::isConnected() const
    {
        std::vector<std::vector<int>> components;
        connectedComponents(components);

        if (components.size() > 1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool graph::isStronglyConnected() const
    {
        std::vector<std::vector<int>> scc;
        stronglyConnectedComponents(scc);

        if (scc.size() > 1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void graph::minimumFillInOrder(std::vector<int> &order) const
    {
        // check if the graph is undirected
        if (!isUndirected())
        {
            std::cerr << "ERROR in graph::minimumFillInOrder(): not an undirected graph." << std::endl;
            exit(2);
        }

        order.clear();
        order.reserve(getNumVertices());

        // create and populate set/queue structure
        std::vector<int> fillIn(getNumVertices());
        std::set<std::pair<int, int>> queue;

        // create and populate bucket structure
        for (int i = 0; i < getNumVertices(); ++i)
        {
            std::vector<std::vector<int>> clique(1);
            clique[0].push_back(i);
            for (const auto &e : vertices[i]->getOutEdges())
                clique[0].push_back(e->getHeadVertex());
            auto fillInNum = fillInEdgesCount(clique);
            queue.insert(std::make_pair(fillInNum, i));
            fillIn[i] = fillInNum;
        }

        // visited vector
        std::vector<bool> visited(getNumVertices(), false);

        for (int i = 0; i < getNumVertices(); ++i)
        {
            // take the minimum fill-in vertex and ``remove'' it from the graph
            auto top = queue.begin();
            auto pickedVertex = top->second;
            queue.erase(top);
            visited[pickedVertex] = true;

            // put it in the order
            order.push_back(pickedVertex);

            // update fill-in of adjacent vertices
            for (const auto &it : vertices[pickedVertex]->getOutEdges())
            {
                auto adjacentVertex = it->getHeadVertex();
                if (!visited[adjacentVertex])
                {
                    std::vector<std::vector<int>> clique(1);
                    clique[0].reserve(vertices[adjacentVertex]->getOutdegree() + 1);
                    clique[0].push_back(adjacentVertex);
                    for (const auto &e : vertices[adjacentVertex]->getOutEdges())
                    {
                        auto head = e->getHeadVertex();
                        if (!visited[head])
                        {
                            clique[0].push_back(head);
                        }
                    }
                    auto fillInNum = fillInEdgesCount(clique);

                    auto it = queue.find(std::make_pair(fillIn[adjacentVertex], adjacentVertex));
                    if (it != queue.end())
                    {
                        queue.erase(it);
                        queue.insert(std::make_pair(fillInNum, adjacentVertex));
                        fillIn[adjacentVertex] = fillInNum;
                    }
                    else
                    {
                        std::cerr << "ERROR in graph::minimumFillInOrder(): edge not found." << std::endl;
                        exit(2);
                    }
                }
            }
        }
    }

    void graph::maximumDegreeOrder(std::vector<int> &order) const
    {
        minimumDegreeOrder(order);
        std::reverse(order.begin(), order.end());
    }

    void graph::minimumDegreeOrder(std::vector<int> &order) const
    {
        // check if the graph is undirected
        if (!isUndirected())
        {
            std::cerr << "ERROR in graph::minimumDegreeOrder(): not an undirected graph." << std::endl;
            exit(2);
        }

        order.clear();
        order.reserve(getNumVertices());

        // create and populate bucket structure
        std::vector<std::list<int>> bucket(getNumVertices() + 1);
        std::vector<std::list<int>::iterator> position(getNumVertices());
        std::vector<int> degrees(getNumVertices());
        int maxDegree = std::numeric_limits<int>::min();
        int minDegree = std::numeric_limits<int>::max();
        for (int i = 0; i < getNumVertices(); ++i)
        {
            auto degree = vertices[i]->getOutdegree();
            bucket[degree].push_front(i);
            position[i] = bucket[degree].begin();
            degrees[i] = degree;
            if (maxDegree < degree)
            {
                maxDegree = degree;
            }
            if (minDegree > degree)
            {
                minDegree = degree;
            }
        }

        // visited vector
        std::vector<bool> visited(getNumVertices(), false);

        for (int i = 0; i < getNumVertices(); ++i)
        {
            // take the minimum degree vertex and ``remove'' it from the graph
            auto pickedVertex = bucket[minDegree].front();
            bucket[minDegree].pop_front();
            visited[pickedVertex] = true;

            // put it in the order
            order.push_back(pickedVertex);

            // update degrees of adjacent vertices
            for (const auto &it : vertices[pickedVertex]->getOutEdges())
            {
                auto adjacentVertex = it->getHeadVertex();
                if (!visited[adjacentVertex])
                {
                    bucket[degrees[adjacentVertex]].erase(position[adjacentVertex]);
                    degrees[adjacentVertex] -= 1;
                    bucket[degrees[adjacentVertex]].push_front(adjacentVertex);
                    position[adjacentVertex] = bucket[degrees[adjacentVertex]].begin();

                    if (degrees[adjacentVertex] < minDegree)
                    {
                        minDegree = degrees[adjacentVertex];
                    }
                }
            }

            // find minimum degree
            for (; minDegree <= maxDegree; minDegree++)
            {
                if (!bucket[minDegree].empty())
                {
                    break;
                }
            }
        }
    }

    bool graph::isChordal() const
    {
        bool isChordal = true;

        if (isUndirected())
        {
            // elimination ordering
            std::vector<int> sigma;
            maximumCardinalitySearch(sigma);

            // test zero fill in
            isChordal = isPerfectEliminationOrder(sigma);
        }
        else
        {
            isChordal = false;
        }

        return isChordal;
    }

    bool graph::isPerfectEliminationOrder(const std::vector<int> &sigma) const
    {
        std::vector<int> sigmaInv(getNumVertices(), -1);
        for (int i = 0; i < getNumVertices(); ++i)
            sigmaInv[sigma[i]] = i;

        std::vector<std::set<int>> A(getNumVertices());

        for (int i = 0; i < getNumVertices() - 2; ++i)
        {
            auto v = sigma[i];

            std::vector<int> X;
            for (const auto &it : vertices[v]->getOutEdges())
            {
                auto x = it->getHeadVertex();
                if (sigmaInv[v] < sigmaInv[x])
                    X.push_back(x);
            }

            if (X.size() > 0)
            {
                auto sigmaInvMin = std::numeric_limits<int>::max();
                for (unsigned long int i = 0; i < X.size(); ++i)
                    sigmaInvMin = std::min(sigmaInvMin, sigmaInv[X[i]]);
                auto u = sigma[sigmaInvMin];

                // concatenate X \ {u} to A(u)
                for (unsigned long int i = 0; i < X.size(); ++i)
                    if (X[i] != u)
                        A[u].insert(X[i]);
            }

            // get adjacent vertices
            std::set<int> neighbors;
            for (const auto &it : vertices[v]->getOutEdges())
                neighbors.insert(it->getHeadVertex());

            // if |A(v) \ Adj(v)| > 0 then return false
            std::vector<int> difference;
            std::set_difference(A[v].begin(), A[v].end(), neighbors.begin(), neighbors.end(), std::inserter(difference, difference.begin()));

            if (difference.size() > 0)
                return false;
        }

        return true;
    }

    void graph::maximumCardinalitySearch(std::vector<int> &order) const
    {
        // presumes undirected and connected graph
        if (isUndirected() == false)
        {
            std::cerr << "ERROR in graph::maximumCardinalitySearch(): the graph is not undirected." << std::endl;
            exit(2);
        }

        // initialize all vertices as not visited (unnumbered)
        std::vector<bool> visited(getNumVertices(), false);
        order.resize(getNumVertices(), -1);

        // initialize bucket structure
        std::vector<std::list<int>> bucket(getNumVertices());
        std::vector<std::list<int>::iterator> position(getNumVertices());
        std::vector<int> numVisitedNeighbors(getNumVertices(), 0);
        int maxVisited = 0;
        for (int i = 0; i < getNumVertices(); ++i)
        {
            bucket[0].push_front(i);
            position[i] = bucket[0].begin();
        }

        for (int i = getNumVertices() - 1; i >= 0; --i)
        {
            // pick vertex with the largest number of marked neighbors
            auto vertex = bucket[maxVisited].front();
            bucket[maxVisited].erase(position[vertex]);

            // set elimination order of vertex to i
            order[i] = vertex;
            visited[vertex] = true;

            // increase the number of marked neighbors for adjacent vertices
            for (const auto &e : vertices[vertex]->getOutEdges())
            {
                auto head = e->getHeadVertex();

                // if not processed yet
                if (!visited[head])
                {
                    bucket[numVisitedNeighbors[head]].erase(position[head]);
                    numVisitedNeighbors[head] += 1;
                    bucket[numVisitedNeighbors[head]].push_front(head);
                    position[head] = bucket[numVisitedNeighbors[head]].begin();

                    if (numVisitedNeighbors[head] > maxVisited)
                    {
                        maxVisited = numVisitedNeighbors[head];
                    }
                }
            }

            // find maximum cardinality bucket
            for (; maxVisited > -1; --maxVisited)
            {
                if (!bucket[maxVisited].empty())
                {
                    break;
                }
            }
        }
    }

    void graph::componentTree(std::vector<std::vector<int>> components, graph &componentG) const
    {
        std::vector<int> inComponent(getNumVertices(), -1);
        componentG = graph(components.size());

        for (unsigned int i = 0; i < components.size(); ++i)
        {
            for (const auto &it : components[i])
            {
                inComponent[it] = i;
            }
        }

        for (const auto &it : inComponent)
        {
            if (it == -1)
            {
                std::cerr << "ERROR in graph::componentTree(): some vertex not present in a component.";
                exit(2);
            }
        }

        for (const auto &e : edges)
        {
            if (inComponent[e->getTailVertex()] != inComponent[e->getHeadVertex()])
            {
                componentG.addEdge(inComponent[e->getTailVertex()], inComponent[e->getHeadVertex()]);
            }
        }

        for (unsigned int i = 0; i < components.size(); ++i)
        {
            std::string label;
            for (unsigned int j = 0; j < components[i].size(); ++j)
            {
                label += std::to_string(components[i][j]);
                if (j < components[i].size() - 1)
                {
                    label += ", ";
                }
            }
            vertices[i]->setLabel(label);
        }
    }

    void graph::cliqueTree(graph &cliqueT) const
    {
        std::vector<std::vector<int>> cliques;
        int fillInEdges;
        eliminationGame(cliques, fillInEdges);
        cliqueTree(cliques, cliqueT);
    }

    void graph::cliqueTree(graph &cliqueT, eliminationOrderingAlgorithms algorithm) const
    {
        std::vector<std::vector<int>> cliques;
        int fillInEdges;
        eliminationGame(cliques, fillInEdges, algorithm);
        cliqueTree(cliques, cliqueT);
    }

    void graph::cliqueTree(const std::vector<std::vector<int>> &cliques, graph &cliqueT) const
    {
        // create the clique graph
        int numCliques = cliques.size();
        std::vector<std::set<int>> cliquesSorted(numCliques);
        for (int i = 0; i < numCliques; ++i)
        {
            std::copy(cliques[i].begin(), cliques[i].end(), std::inserter(cliquesSorted[i], cliquesSorted[i].end()));
        }
        graph cliqueGraph(numCliques);
        for (int i = 0; i < numCliques; ++i)
        {
            for (int j = i + 1; j < numCliques; ++j)
            {
                std::vector<int> inCommon;
                std::set_intersection(cliquesSorted[i].begin(), cliquesSorted[i].end(),
                                      cliquesSorted[j].begin(), cliquesSorted[j].end(), std::back_inserter(inCommon));

                if (inCommon.size() > 0)
                {
                    cliqueGraph.addUndirectedEdge(i, j, "", -(double)inCommon.size());
                }
            }
        }

        // the clique tree is the maximum weight spanning tree
        double cost;
        std::vector<edge_t> pEdge;
        cliqueGraph.primJarnik(cost, pEdge);

        // construct MST and compute topological order
        graph MSTgraph(numCliques);
        for (int i = 0; i < numCliques; ++i)
        {
            if (pEdge[i])
            {
                MSTgraph.addEdge(pEdge[i]->getTailVertex(), i);
            }
        }
        std::vector<int> topOrder;
        MSTgraph.topologicalSort(topOrder);
        if (*(topOrder.rbegin()) == -1)
        {
            std::cerr << "ERROR in graph::cliqueTree(): MST is not a tree!" << std::endl;
            exit(2);
        }
        std::vector<int> topLabels(numCliques);
        for (int i = 0; i < numCliques; ++i)
        {
            topLabels[topOrder[i]] = i;
        }

        // create the resulting clique tree graph with edges corresponding to minimal vertex separators (inefficient way)
        // order the graph vertices in topological order
        cliqueT = graph(numCliques);
        for (int i = 0; i < numCliques; i++)
        {
            auto &curCliqueNum = topOrder[i];
            auto &curClique = cliquesSorted[curCliqueNum];
            std::ostringstream labelVertex;
            std::copy(curClique.begin(), curClique.end(), std::ostream_iterator<int>(labelVertex, ","));
            std::string label = labelVertex.str();
            if (label.size() > 0)
            {
                label.pop_back();
            }
            cliqueT.vertices[i]->setLabel(label);

            if (pEdge[curCliqueNum])
            {
                auto &prevCliqueNum = pEdge[curCliqueNum]->getTailVertex();
                auto &prevClique = cliquesSorted[prevCliqueNum];
                std::vector<int> inCommon;
                std::set_intersection(prevClique.begin(), prevClique.end(),
                                      curClique.begin(), curClique.end(), std::back_inserter(inCommon));

                std::ostringstream labelEdge;
                std::copy(inCommon.begin(), inCommon.end(), std::ostream_iterator<int>(labelEdge, ","));
                std::string label = labelEdge.str();
                if (label.size() > 0)
                {
                    label.pop_back();
                }

                cliqueT.addEdge(topLabels[prevCliqueNum], topLabels[curCliqueNum], label, (double)inCommon.size());
            }
        }
    }

    bool graph::isValidTriangulation(std::vector<std::vector<int>> &cliques) const
    {
        bool out = true;

        std::vector<std::set<int>> vertexInCliques(getNumVertices());
        for (unsigned int i = 0; i < cliques.size(); ++i)
        {
            for (unsigned int j = 0; j < cliques[i].size(); j++)
            {
                auto v = cliques[i][j];
                vertexInCliques[v].insert(i);
            }
        }

        for (const auto &it : edges)
        {
            auto tail = it->getTailVertex();
            auto head = it->getHeadVertex();
            if (tail != head)
            {
                std::vector<int> inCommon;
                std::set_intersection(vertexInCliques[tail].begin(), vertexInCliques[tail].end(),
                                      vertexInCliques[head].begin(), vertexInCliques[head].end(), std::back_inserter(inCommon));
                if (inCommon.size() == 0)
                    return false;
            }
        }
        return out;
    }

    void graph::eliminationGame(std::vector<std::vector<int>> &cliques, int &fillInEdges) const
    {
        eliminationGame(cliques, fillInEdges, eliminationOrderingAlgorithms::MaximumCardinality);
    }

    void graph::eliminationGame(std::vector<std::vector<int>> &cliques, int &fillInEdges, eliminationOrderingAlgorithms method) const
    {
        cliques.clear();

        std::vector<int> order;

        switch (method)
        {
        case MaximumCardinality:
        {
            maximumCardinalitySearch(order);
            break;
        }

        case MinimumDegree:
        {
            minimumDegreeOrder(order);
            break;
        }

        case MinimumFillIn:
        {
            minimumFillInOrder(order);
            break;
        }

        default:
        {
            std::cerr << "ERROR in graph::eliminationGame(): unknown eliminationOrderingAlgorithms method." << std::endl;
            exit(2);
            break;
        }
        }

        eliminationGame(order, cliques, fillInEdges);
    }

    void graph::eliminationGame(const std::vector<int> &sigma, std::vector<std::vector<int>> &cliques, int &fillInEdges) const
    {
        /**
         * Elimination game to compute (maximal) cliques.
         * @param sigma denotes input ordering
         * @param cliques is a 2d array of cliques resulting from the elimination game
         * @param fillInEdges returns the number of added (undirected) edges during triangulation, 0 iff the graph is chordal
         *
         * Shamir, R. (1994). Advanced topics in graph algorithms. Technical report. Tel-Aviv University.
         **/

        // copy graph in which we add edges
        std::vector<std::set<int>> addedEdges(getNumVertices());

        cliques.clear();
        cliques.resize(1);
        fillInEdges = 0;
        std::vector<int> sigmaInv(getNumVertices());
        for (int i = 0; i < getNumVertices(); ++i)
            sigmaInv[sigma[i]] = i;

        // initialize set to zeros
        std::vector<int> s(getNumVertices(), 0);

        int clique = 0;
        for (int i = 0; i < getNumVertices(); ++i)
        {
            int v = sigma[i];

            // add to X adjacent vertices with higher order
            std::vector<int> X;
            for (const auto &it : vertices[v]->getOutEdges())
            {
                int x = it->getHeadVertex();
                if (sigmaInv[v] < sigmaInv[x])
                    X.push_back(x);
            }
            for (const auto &x : addedEdges[v])
                if (sigmaInv[v] < sigmaInv[x])
                    X.push_back(x);

            // if the vertex does not have any adjacent, it is already the maximal clique
            if (vertices[v]->getOutdegree() == 0)
            {
                cliques[clique] = {v};
                cliques.resize(clique + 2);
                clique += 1;
            }

            // skip to another vertex if no adjacent vertex can be processed
            if (X.size() == 0)
                continue;

            // find minimal sigmaInv of vertices in X
            int sigmaInvMin = std::numeric_limits<int>::max();
            for (unsigned int i = 0; i < X.size(); ++i)
                sigmaInvMin = std::min(sigmaInvMin, sigmaInv[X[i]]);
            int u = sigma[sigmaInvMin];
            s[u] = std::max(s[u], (int)X.size() - 1);

            if (s[v] < (int)X.size())
            {
                cliques[clique].push_back(v);
                cliques[clique].insert(cliques[clique].end(), X.begin(), X.end());
                cliques.resize(clique + 2);
                clique += 1;

                // add chords to the graph
                for (unsigned int e1 = 0; e1 < X.size(); ++e1)
                {
                    for (unsigned int e2 = e1 + 1; e2 < X.size(); ++e2)
                    {
                        std::vector<edge_t> fEdge;
                        getEdge(fEdge, X[e1], X[e2]);
                        if (fEdge.size() == 0)
                        {
                            if (addedEdges[X[e1]].find(X[e2]) == addedEdges[X[e1]].end())
                            {
                                addedEdges[X[e1]].insert(X[e2]);
                                addedEdges[X[e2]].insert(X[e1]);
                                fillInEdges += 1; // because we count undirected only
                            }
                        }
                    }
                }
            }
        }

        cliques.resize(cliques.size() - 1);

        if (!isValidTriangulation(cliques))
        {
            std::cerr << "ERROR in graph::eliminationGame(): not a valid triangulation." << std::endl;
            exit(2);
        }
    }

    auto graph::fillInEdgesCount(std::vector<std::vector<int>> &cliques) const -> size_t
    {
        auto fillInEdges = size_t{0};

        for (unsigned int i = 0; i < cliques.size(); ++i)
        {
            int expectedNumEdges = cliques[i].size() * cliques[i].size();
            int realNumEdges = 0;

            for (const auto &it : cliques[i])
            {
                std::vector<bool> connection(getNumVertices(), false);
                connection[it] = true;
                std::vector<bool> cliqueBool(getNumVertices(), false);
                for (const auto &v : cliques[i])
                    cliqueBool[v] = true;

                for (const auto &e : vertices[it]->getOutEdges())
                {
                    auto head = e->getHeadVertex();
                    if (cliqueBool[head])
                        connection[head] = true;
                }
                for (const auto &j : cliques[i])
                    realNumEdges += (int)connection[j];
            }
            fillInEdges += (expectedNumEdges - realNumEdges);
        }

        // undirected edge type
        fillInEdges /= 2;
        return fillInEdges;
    }

    auto graph::fillInEdges(std::vector<std::vector<int>> &cliques) const -> std::set<std::pair<int, int>>
    {
        auto addedEdges = std::set<std::pair<int, int>>();

        for (const auto &clique : cliques)
        {
            for (const auto &tailVertex : clique)
            {
                auto headVertices = std::vector<bool>(getNumVertices(), false);
                headVertices[tailVertex] = true; // self-loop
                // Mark all connected edges
                for (const auto &e : vertices[tailVertex]->getOutEdges())
                    headVertices[e->getHeadVertex()] = true;
                // Add clique-edges not present in headVertices to addedEdges
                for (const auto &headVertex : clique)
                    if (!headVertices[headVertex])
                        if (tailVertex < headVertex)
                            addedEdges.insert({tailVertex, headVertex});
            }
        }
        return addedEdges;
    }

    void graph::topologicalSort(std::vector<int> &topOrder) const
    {
        // topOrder = -1 iff the graph contains a cycle

        topOrder.resize(getNumVertices(), -1);

        int countNodes = 0;
        std::queue<int> tsortQueue;
        std::vector<int> inDegree(getNumVertices());

        // omp_lock_t writelock;
        // omp_init_lock(&writelock);

        // #pragma omp parallel for
        for (auto i = 0; i < (int)vertices.size(); ++i)
        {
            inDegree[i] = vertices[i]->getIndegree();
            if (inDegree[i] == 0)
            {
                // omp_set_lock(&writelock);
                tsortQueue.push(i);
                // omp_unset_lock(&writelock);
            }
        }

        // main loop
        while (!tsortQueue.empty())
        {
            // pick vertex and remove it from queue
            int vert = tsortQueue.front();
            tsortQueue.pop();

            // insert into topological order
            topOrder[countNodes] = vert;

            // number of visited nodes
            countNodes += 1;

            // decrease in-degree of all connected nodes
            // #pragma omp parallel
            {
                for (const auto &it : vertices[vert]->getOutEdges())
                {
                    // #pragma omp single nowait
                    {
                        int head = it->getHeadVertex();
                        inDegree[head] -= 1;

                        if (inDegree[head] == 0)
                        {
                            // omp_set_lock(&writelock);
                            tsortQueue.push(head);
                            // omp_unset_lock(&writelock);
                        }
                    }
                }
            }
        }
    }

    void graph::allPairsShortestPaths(std::vector<std::vector<double>> &distMat, allPairsShortestPathsAlgorithms algorithm)
    {
        std::vector<std::vector<edge_t>> pEdge;
        allPairsShortestPaths(distMat, pEdge, algorithm);
    }

    void graph::allPairsShortestPaths(std::vector<std::vector<double>> &distMat, std::vector<std::vector<edge_t>> &pEdge, allPairsShortestPathsAlgorithms algorithm)
    {
        switch (algorithm)
        {
        case FloydWarshall:
        {
            floydWarshall(distMat, pEdge);
            break;
        }

        case Johnson:
        {
            johnson(distMat, pEdge);
            break;
        }

        case BellmanFord:
        {
            distMat.resize(getNumVertices());
            pEdge.resize(getNumVertices());
#ifdef USE_OMP
#pragma omp parallel for
#endif
            for (int i = 0; i < getNumVertices(); ++i)
            {
                bellmanFord(i, distMat[i], pEdge[i]);
            }
            break;
        }

        case Dijkstra:
        {
            distMat.resize(getNumVertices());
            pEdge.resize(getNumVertices());
#ifdef USE_OMP
#pragma omp parallel for
#endif
            for (int i = 0; i < getNumVertices(); ++i)
            {
                dijkstra(i, distMat[i], pEdge[i]);
            }
            break;
        }

        case DirectedAcyclicGraph:
        {
            distMat.resize(getNumVertices());
            pEdge.resize(getNumVertices());
#ifdef USE_OMP
#pragma omp parallel for
#endif
            for (int i = 0; i < getNumVertices(); ++i)
            {
                DAGShortestPath(i, distMat[i], pEdge[i], false);
            }
            break;
        }

        case BreadthFirstSearch:
        {
            if (!isWeightedEqually())
            {
                std::cerr << "ERROR in allPairsShortestPaths(): BreadthFirstSearch requires equal edge weights." << std::endl;
                exit(2);
            }
            distMat.resize(getNumVertices(), std::vector<double>(getNumVertices(), std::numeric_limits<double>::infinity()));
            pEdge.resize(getNumVertices());
#ifdef USE_OMP
#pragma omp parallel for
#endif
            for (int i = 0; i < getNumVertices(); ++i)
            {
                std::vector<int> dMat;
                breadthFirstSearch(i, dMat, pEdge[i]);
                for (int j = 0; j < getNumVertices(); ++j)
                {
                    if (dMat[j] != -1)
                    {
                        distMat[i][j] = (double)dMat[j];
                    }
                }
            }
            break;
        }

        default:
        {
            std::cerr << "ERROR in graph::allPairsShortestPaths(): unknown algorithm" << std::endl;
            exit(2);
        }
        }

        switch (algorithm)
        {
        case BellmanFord:
        case Dijkstra:
        case BreadthFirstSearch:
        {
// account for self-loops
#ifdef USE_OMP
#pragma omp parallel for
#endif
            for (int i = 0; i < getNumVertices(); ++i)
            {
                // self-loops in source
                distMat[i][i] = std::numeric_limits<double>::infinity();
                for (const auto &it : vertices[i]->getOutEdges())
                {
                    auto nextVertex = it->getHeadVertex();
                    if (nextVertex == i)
                    {
                        auto w = it->getWeight();
                        if (w < distMat[i][i])
                        {
                            distMat[i][i] = w;
                            pEdge[i][i] = it;
                        }
                    }
                }
            }

// shortest cycles
#ifdef USE_OMP
#pragma omp parallel for
#endif
            for (auto i = 0; i < getNumVertices(); ++i)
            {
                for (auto j = 0; j < getNumVertices(); ++j)
                {
                    if (i != j)
                    {
                        auto cyc = distMat[i][j] + distMat[j][i];

                        if (!std::isinf(cyc))
                        {
                            if ((cyc < distMat[i][i]) | (pEdge[i][i] == nullptr))
                            {
                                distMat[i][i] = cyc;
                                pEdge[i][i] = pEdge[j][i];
                            }
                        }
                    }
                }

                if (pEdge[i][i] == nullptr)
                {
                    distMat[i][i] = std::numeric_limits<double>::infinity();
                }
            }
            break;
        }

        case FloydWarshall:
        case Johnson:
        case DirectedAcyclicGraph:
        default:
        {
            break;
        };
        }
    }

    void graph::johnson(std::vector<std::vector<double>> &distMat)
    {
        std::vector<std::vector<edge_t>> pEdge;
        johnson(distMat, pEdge);
    }

    void graph::johnson(std::vector<std::vector<double>> &distMat, std::vector<std::vector<edge_t>> &pEdge)
    {
        // create edges distance matrix
        distMat.resize(getNumVertices());
        pEdge.resize(getNumVertices());
#ifdef USE_OMP
#pragma omp parallel for
#endif
        for (int i = 0; i < getNumVertices(); ++i)
        {
            distMat[i].resize(getNumVertices(), std::numeric_limits<double>::infinity());
            pEdge[i].resize(getNumVertices(), nullptr);
        }

        // add new vertex, connect all vertices to it
        edges.reserve(getNumVertices() + getNumEdges());
        addVertex("", -1);
        for (int i = 0; i < getNumVertices(); ++i)
        {
            addEdge(getNumVertices() - 1, i, 0.0);
        }

        // run Bellman-Ford
        std::vector<double> lenG(0);
        std::vector<edge_t> pEdgeG(0);
        bellmanFord(getNumVertices() - 1, lenG, pEdgeG);

        // check if negative cycle exists
        for (const auto &it : edges)
        {
            auto vertexA = it->getTailVertex();
            auto vertexB = it->getHeadVertex();

            if (lenG[vertexB] > (lenG[vertexA] + it->getWeight()))
            {
                std::cerr << "ERROR in graph::johnson(): the graph contains a negative cycle." << std::endl;
                exit(2);
            }
        }

        // remove added vertex and edges
        numVertices -= 1;
        delete vertices.back();
        vertices.pop_back();
        for (int i = 0; i < getNumVertices(); ++i)
        {
            vertices[i]->removeInwardEdge(vertices[i]->getIndegree() - 1);
            delete edges.back();
            edges.pop_back();
            numEdges -= 1;
        }

        // reweight original graph to get non-negative weights
        std::vector<int> originalWeights(getNumEdges(), -1);
#ifdef USE_OMP
#pragma omp parallel for
#endif
        for (int i = 0; i < getNumEdges(); ++i)
        {
            originalWeights[i] = edges[i]->getWeight();
            auto tail = edges[i]->getTailVertex();
            auto head = edges[i]->getHeadVertex();
            edges[i]->setWeight(originalWeights[i] + lenG[tail] - lenG[head]);
        }

// run dijkstra on reweighted original graph
#ifdef USE_OMP
#pragma omp parallel for
#endif
        for (int i = 0; i < getNumVertices(); ++i)
        {
            dijkstra(i, -1, distMat[i], pEdge[i]);

            // correct distances due to reweighting
            for (int j = 0; j < getNumVertices(); ++j)
            {
                distMat[i][j] += lenG[j] - lenG[i];
            }
        }

// set back original weights
#ifdef USE_OMP
#pragma omp parallel for
#endif
        for (int i = 0; i < getNumEdges(); ++i)
        {
            edges[i]->setWeight(originalWeights[i]);
        }

// account for self-loops
#ifdef USE_OMP
#pragma omp parallel for
#endif
        for (int i = 0; i < getNumVertices(); ++i)
        {
            // self-loops in source
            distMat[i][i] = std::numeric_limits<double>::infinity();
            for (const auto &it : vertices[i]->getOutEdges())
            {
                auto nextVertex = it->getHeadVertex();
                if (nextVertex == i)
                {
                    auto w = it->getWeight();
                    if (w < distMat[i][i])
                    {
                        distMat[i][i] = w;
                        pEdge[i][i] = it;
                    }
                }
            }
        }

// cycles
#ifdef USE_OMP
#pragma omp parallel for
#endif
        for (auto i = 0; i < getNumVertices(); ++i)
        {
            for (auto j = 0; j < getNumVertices(); ++j)
            {
                if (i != j)
                {
                    auto cyc = distMat[i][j] + distMat[j][i];

                    if (!std::isinf(cyc))
                    {
                        if ((cyc < distMat[i][i]) | (pEdge[i][i] == nullptr))
                        {
                            distMat[i][i] = cyc;
                            pEdge[i][i] = pEdge[j][i];
                        }
                    }
                }
            }

            if (pEdge[i][i] == nullptr)
            {
                distMat[i][i] = std::numeric_limits<double>::infinity();
            }
        }
    }

    void graph::floydWarshall(std::vector<std::vector<double>> &distMat) const
    {
        std::vector<std::vector<edge_t>> pEdge;
        floydWarshall(distMat, pEdge);
    }

    void graph::floydWarshall(std::vector<std::vector<double>> &distMat, std::vector<std::vector<edge_t>> &pEdge) const
    {
        // create edges distance matrix
        distMat.resize(getNumVertices());
        pEdge.resize(getNumVertices());
        for (int i = 0; i < getNumVertices(); ++i)
        {
            distMat[i].resize(getNumVertices(), std::numeric_limits<double>::infinity());
            pEdge[i].resize(getNumVertices(), nullptr);
        }

#ifdef USE_OMP
        omp_lock_t writelock;
        omp_init_lock(&writelock);
#endif

// initialize distance matrix
#ifdef USE_OMP
#pragma omp parallel for
#endif
        for (int i = 0; i < getNumVertices(); ++i)
        {
            for (const auto &it : vertices[i]->getOutEdges())
            {
                auto head = it->getHeadVertex();
                if (distMat[i][head] > it->getWeight())
                {
                    distMat[i][head] = it->getWeight();
                    pEdge[i][head] = it;
                }
            }
        }

        // main loop
        for (int k = 0; k < getNumVertices(); ++k)
        {
#ifdef USE_OMP
#pragma omp parallel for
#endif
            for (int i = 0; i < getNumVertices(); ++i)
            {
                if ((i != k) & (distMat[i][k] != std::numeric_limits<double>::infinity()))
                {
                    for (int j = 0; j < getNumVertices(); ++j)
                    {
                        if ((j != k) & (distMat[k][j] != std::numeric_limits<double>::infinity()))
                        {
                            double curVal = (distMat[i][k] + distMat[k][j]);
                            if (distMat[i][j] > curVal)
                            {
#ifdef USE_OMP
                                omp_set_lock(&writelock);
#endif
                                distMat[i][j] = curVal;
                                pEdge[i][j] = pEdge[k][j];
#ifdef USE_OMP
                                omp_unset_lock(&writelock);
#endif
                            }
                        }
                    }
                }
            }
        }
    }

    void graph::stronglyConnectedComponents(std::vector<std::vector<int>> &scc) const
    {
        // Kosaraju's algorithm
        scc.clear();
        scc.reserve(getNumVertices());

        // empty stack
        std::stack<int> sccStack, dfsStack;

        // post-order DFS transversal to occupate sccStack
        std::vector<bool> visited(getNumVertices(), false);
        std::vector<int> blocked(getNumVertices(), 0);
        std::vector<int> predecedor(getNumVertices(), -1);

        for (int vertex = 0; vertex < getNumVertices(); ++vertex)
        {
            if (visited[vertex] == false)
            {
                dfsStack.push(vertex);

                while (!dfsStack.empty())
                {
                    vertex = dfsStack.top();
                    dfsStack.pop();

                    if (visited[vertex] == false)
                    {
                        visited[vertex] = true;
                    }

                    int stackCount = 0;
                    for (const auto &it : vertices[vertex]->getOutEdges())
                    {
                        auto head = it->getHeadVertex();
                        if (visited[head] == false)
                        {
                            visited[head] = true;
                            blocked[vertex] += 1;
                            predecedor[head] = vertex;
                            dfsStack.push(head);
                            stackCount += 1;
                        }
                    }

                    if (stackCount == 0)
                    {
                        while (blocked[vertex] == 0)
                        {
                            sccStack.push(vertex);
                            if (predecedor[vertex] != -1)
                            {
                                vertex = predecedor[vertex];
                            }
                            blocked[vertex] -= 1;
                        }
                    }
                }
            }
        }

        // process vertices in the sccStack order
        visited.assign(getNumVertices(), false);

        while (!sccStack.empty())
        {
            auto vertex = sccStack.top();
            sccStack.pop();

            if (visited[vertex] == false)
            {
                // second DFS on reverse graph
                dfsStack.push(vertex);
                std::vector<int> currentSCC;
                currentSCC.reserve(getNumVertices());

                while (!dfsStack.empty())
                {
                    vertex = dfsStack.top();
                    dfsStack.pop();

                    if (visited[vertex] == false)
                    {
                        visited[vertex] = true;
                        currentSCC.push_back(vertex);

                        for (const auto &it : vertices[vertex]->getInEdges())
                        {
                            auto tail = it->getTailVertex();
                            if (visited[tail] == false)
                            {
                                dfsStack.push(tail);
                            }
                        }
                    }
                }
                scc.push_back(currentSCC);
            }
        }
    }
}