#include "graph.hpp"
#include "edge.hpp"
#include <vector>
#include <sstream>
#include <fstream>

int main(int argc, char **argv)
{
    const std::string passed = "\033[1;32mPASSED\033[0m";
    const std::string failed = "\033[1;31mFAILED\033[0m";
    const std::string ok = "\033[1;32mOK\033[0m";
    const std::string ko = "\033[1;31mKO\033[0m";

    std::cout << "STRONGLY CONNECTED COMPONENTS TESTS:" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/scc" + std::to_string(iter) + ".g";
        std::string solName = "tests/scc" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            std::set<std::set<int>> expectedSCC, SCC;
            std::vector<std::vector<int>> sccs;
            graphInterface::graph g(0);
            g.loadGraphFromTextFile(name);
            g.stronglyConnectedComponents(sccs);
            for (unsigned int i = 0; i < sccs.size(); i++)
            {
                std::set<int> scc;
                for (unsigned int j = 0; j < sccs[i].size(); j++)
                {
                    scc.insert(sccs[i][j]);
                }
                SCC.insert(scc);
            }
            std::string line;
            while (std::getline(sol, line))
            {
                std::istringstream iss(line);
                std::set<int> scc;
                int num;
                while (iss >> num)
                {
                    scc.insert(num);
                }
                expectedSCC.insert(scc);
            }
            std::string status = passed;
            if (expectedSCC != SCC)
            {
                status = failed;
            }
            std::cout << std::setw(50) << std::left << name;
            std::cout << status << std::endl;
        }
        else
        {
            break;
        }
    }
    std::cout << std::endl;

    std::cout << "CONNECTED COMPONENTS TESTS:" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/cc" + std::to_string(iter) + ".g";
        std::string solName = "tests/cc" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            std::set<std::set<int>> expectedCC, CC;
            std::vector<std::vector<int>> ccs;
            graphInterface::graph g(0);
            g.loadGraphFromTextFile(name);
            g.connectedComponents(ccs);
            for (unsigned int i = 0; i < ccs.size(); i++)
            {
                std::set<int> cc;
                for (unsigned int j = 0; j < ccs[i].size(); j++)
                {
                    cc.insert(ccs[i][j]);
                }
                CC.insert(cc);
            }
            std::string line;
            while (std::getline(sol, line))
            {
                std::istringstream iss(line);
                std::set<int> cc;
                int num;
                while (iss >> num)
                {
                    cc.insert(num);
                }
                expectedCC.insert(cc);
            }
            std::string status = passed;
            if (expectedCC != CC)
            {
                status = failed;
            }
            std::cout << std::setw(50) << std::left << name;
            std::cout << status << std::endl;
        }
        else
        {
            break;
        }
    }
    std::cout << std::endl;

    std::cout << "BIPARTITE GRAPHS TESTS:" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/bipartite" + std::to_string(iter) + ".g";
        std::string solName = "tests/bipartite" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            graphInterface::graph g(0);
            g.loadGraphFromTextFile(name);
            bool expectedValue;
            sol >> expectedValue;
            std::string status = passed;
            if (g.isBipartite() != expectedValue)
            {
                status = failed;
            }
            std::cout << std::setw(50) << std::left << name;
            std::cout << status << std::endl;
        }
        else
        {
            break;
        }
    }
    std::cout << std::endl;

    std::cout << "DAG LONGEST PATH TESTS:" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/daglong" + std::to_string(iter) + ".g";
        std::string solName = "tests/daglong" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            graphInterface::graph g(0);
            g.loadGraphFromTextFile(name);

            std::vector<double> expDist;
            expDist.reserve(g.getNumVertices());
            std::string line;
            std::getline(sol, line);
            std::istringstream iss(line);
            std::string num;
            double dNum;
            while (iss >> num)
            {
                if (num == "Inf")
                {
                    dNum = std::numeric_limits<double>::infinity();
                }
                else if (num == "-Inf")
                {
                    dNum = -std::numeric_limits<double>::infinity();
                }
                else
                {
                    dNum = std::atof(num.c_str());
                }
                expDist.push_back(dNum);
            }

            std::vector<graphInterface::edge_t> pEdge;
            std::vector<double> len;
            g.DAGLongestPath(g.getSourceVertex(), len, pEdge);

            std::string status = passed;
            if (len != expDist)
            {
                status = failed;
            }
            std::cout << std::setw(50) << std::left << name;
            std::cout << status << std::endl;
        }
        else
        {
            break;
        }
    }
    std::cout << std::endl;

    std::cout << "ALL PAIRS SHORTEST PATHS TESTS:" << std::endl;
    std::cout << "(Floyd-Warhsall / Johnson / Bellman-Ford)" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/spathsall" + std::to_string(iter) + ".g";
        std::string solName = "tests/spathsall" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            std::vector<graphInterface::graph::allPairsShortestPathsAlgorithms> alg =
                {graphInterface::graph::allPairsShortestPathsAlgorithms::FloydWarshall,
                 graphInterface::graph::allPairsShortestPathsAlgorithms::Johnson,
                 graphInterface::graph::allPairsShortestPathsAlgorithms::BellmanFord};

            std::vector<std::vector<double>> expDistMat;
            std::string line;
            while (std::getline(sol, line))
            {
                std::istringstream iss(line);
                std::vector<double> distVec;
                std::string num;
                double dNum;
                while (iss >> num)
                {
                    if (num == "Inf")
                    {
                        dNum = std::numeric_limits<double>::infinity();
                    }
                    else if (num == "-Inf")
                    {
                        dNum = -std::numeric_limits<double>::infinity();
                    }
                    else
                    {
                        dNum = std::atof(num.c_str());
                    }
                    distVec.push_back(dNum);
                }
                expDistMat.push_back(distVec);
            }

            std::cout << std::setw(50) << std::left << name;
            for (unsigned int i = 0; i < alg.size(); i++)
            {
                std::vector<std::vector<double>> distMat;
                graphInterface::graph g(0);
                g.loadGraphFromTextFile(name);
                g.allPairsShortestPaths(distMat, alg[i]);
                std::string status = ok;

                if (distMat != expDistMat)
                {
                    status = ko;
                }
                if (i < alg.size() - 1)
                {
                    status += "/";
                }
                std::cout << status;
            }

            std::cout << std::endl;
        }
        else
        {
            break;
        }
    }
    std::cout << std::endl;

    /*
    std::cout << "BELLMAN-FORD ALGORITHM: Negative cycles." << std::endl;
    int numVertices = 7;
    graphInterface::graph test(numVertices);
    test.addEdge(1, 2, 3.0);
    test.addEdge(2, 3, -1.0);
    test.addEdge(3, 5, -2.0);
    test.addEdge(5, 4, 1.0);
    test.addEdge(4, 2, 4.0);
    test.addEdge(5, 6, -2.0);
    test.addEdge(4, 6, -4.0);
    for (int i=0; i<numVertices-1; i++)
    {
        test.addEdge(0, i+1, 0.0);
    }
    test.setSourceVertex(0);
    std::vector<int> path;
    int capacity = 0;
    test.costNegativeCycle(path, capacity);
    for (unsigned int i=0; i<path.size(); i++)
    {
        std::cout << path[i] << " ";
    }
    std::cout << std::endl << "Capacity: " << capacity << std::endl << std::endl;

    std::cout  << "TOPOLOGICAL SORT" << std::endl;
    graphInterface::graph tSort(6);
    tSort.addEdge(5, 2);
    tSort.addEdge(5, 0);
    tSort.addEdge(4, 0);
    tSort.addEdge(4, 1);
    tSort.addEdge(2, 3);
    tSort.addEdge(3, 1);
    std::vector<int> topOrder;
    tSort.topologicalSort(topOrder);
    for (unsigned int i=0; i<topOrder.size(); i++)
    {
        std::cout << topOrder[i] << '\t';
    }
    std::cout <<std::endl << std::endl;

    std::cout << "DAG SHORTEST PATHS" << std::endl;
    graphInterface::graph dag(6);
    dag.addEdge(0, 1, 5.0);
    dag.addEdge(0, 2, 3.0);
    dag.addEdge(1, 3, 6.0);
    dag.addEdge(1, 2, 2.0);
    dag.addEdge(2, 4, 4.0);
    dag.addEdge(2, 5, 2.0);
    dag.addEdge(2, 3, 7.0);
    dag.addEdge(3, 4, -1.0);
    dag.addEdge(4, 5, -2.0);
    len.resize(0);
    prevNode.resize(0);
    dag.DAGShortestPath(1, len, prevNode);
    for (unsigned int i=0; i<len.size(); i++)
    {
        std::cout << len[i] << "\t";
    }
    std::cout << std::endl;
    for (unsigned int i=0; i<prevNode.size(); i++)
    {
        std::cout << prevNode[i] << "\t";
    }
    std::cout << std::endl;
    std::vector<int> shortestPath;
    dag.retrieveShortestPath(4, prevNode, shortestPath);
    for (unsigned int i=0; i<shortestPath.size(); i++)
    {
        std::cout << shortestPath[i] << "\t";
    }
    std::cout << std::endl << std::endl;

    std::cout << "DIJKSTRA ALGORITHM: Shortest path from source" << std::endl;
    graphInterface::graph dijk(9);
    dijk.addEdge(0, 1, 4);
    dijk.addEdge(1, 0, 4);
    dijk.addEdge(0, 7, 8);
    dijk.addEdge(7, 0, 8);
    dijk.addEdge(1, 2, 8);
    dijk.addEdge(2, 1, 8);
    dijk.addEdge(1, 7, 11);
    dijk.addEdge(7, 1, 11);
    dijk.addEdge(2, 3, 7);
    dijk.addEdge(3, 2, 7);
    dijk.addEdge(2, 8, 2);
    dijk.addEdge(8, 2, 2);
    dijk.addEdge(2, 5, 4);
    dijk.addEdge(5, 2, 4);
    dijk.addEdge(3, 4, 9);
    dijk.addEdge(4, 3, 9);
    dijk.addEdge(3, 5, 14);
    dijk.addEdge(5, 3, 14);
    dijk.addEdge(4, 5, 10);
    dijk.addEdge(5, 4, 10);
    dijk.addEdge(5, 6, 2);
    dijk.addEdge(6, 5, 2);
    dijk.addEdge(6, 7, 1);
    dijk.addEdge(7, 6, 1);
    dijk.addEdge(6, 8, 6);
    dijk.addEdge(8, 6, 6);
    dijk.addEdge(7, 8, 7);
    dijk.addEdge(8, 7, 7);
    len.resize(0);
    prevNode.resize(0);
    dijk.dijkstra(0, len, prevNode);
    for (unsigned int i=0; i<len.size(); i++)
    {
        std::cout << len[i] << "\t";
    }
    std::cout << std::endl;
    for (unsigned int i=0; i<prevNode.size(); i++)
    {
        std::cout << prevNode[i] << "\t";
    }
    std::cout << std::endl;
    shortestPath.resize(0);
    dijk.retrieveShortestPath(4, prevNode, shortestPath);
    for (unsigned int i=0; i<shortestPath.size(); i++)
    {
        std::cout << shortestPath[i] << "\t";
    }
    std::cout << std::endl << std::endl;

    std::cout << "DIJKSTRA ALGORITHM: Shortest path from source" << std::endl;
    dijk = graphInterface::graph(9);
    dijk.addEdge(0, 1, 4);
    dijk.addEdge(1, 0, 4);
    dijk.addEdge(0, 7, 8);
    dijk.addEdge(7, 0, 8);
    dijk.addEdge(1, 2, 8);
    dijk.addEdge(2, 1, 8);
    dijk.addEdge(1, 7, 11);
    dijk.addEdge(7, 1, 11);
    dijk.addEdge(2, 3, 7);
    dijk.addEdge(3, 2, 7);
    dijk.addEdge(2, 8, 2);
    dijk.addEdge(8, 2, 2);
    dijk.addEdge(2, 5, 4);
    dijk.addEdge(5, 2, 4);
    dijk.addEdge(3, 4, 9);
    dijk.addEdge(4, 3, 9);
    dijk.addEdge(3, 5, 14);
    dijk.addEdge(5, 3, 14);
    dijk.addEdge(4, 5, 10);
    dijk.addEdge(5, 4, 10);
    dijk.addEdge(5, 6, 2);
    dijk.addEdge(6, 5, 2);
    dijk.addEdge(6, 7, 1);
    dijk.addEdge(7, 6, 1);
    dijk.addEdge(6, 8, 6);
    dijk.addEdge(8, 6, 6);
    dijk.addEdge(7, 8, 7);
    dijk.addEdge(8, 7, 7);
    len.resize(0);
    prevNode.resize(0);
    dijk.dijkstra(0, 5, len, prevNode);
    for (unsigned int i=0; i<len.size(); i++)
    {
        std::cout << len[i] << "\t";
    }
    std::cout << std::endl;
    for (unsigned int i=0; i<prevNode.size(); i++)
    {
        std::cout << prevNode[i] << "\t";
    }
    std::cout << std::endl;
    shortestPath.resize(0);
    dijk.retrieveShortestPath(5, prevNode, shortestPath);
    for (unsigned int i=0; i<shortestPath.size(); i++)
    {
        std::cout << shortestPath[i] << "\t";
    }
    std::cout << std::endl << std::endl;
*/

    std::cout << "MINIMUM SPANNING TREE TESTS:" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/mst" + std::to_string(iter) + ".g";
        std::string solName = "tests/mst" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            graphInterface::graph g(0);
            g.loadGraphFromTextFile(name);
            double expectedValue;
            sol >> expectedValue;
            std::vector<graphInterface::edge_t> pEdge;
            double value;
            g.primJarnik(value, pEdge);
            std::string status = passed;
            if (value != expectedValue)
            {
                status = failed;
            }
            std::cout << std::setw(50) << std::left << name;
            std::cout << status << std::endl;
        }
        else
        {
            break;
        }
    }
    std::cout << std::endl;

    std::cout << "MAXIMUM FLOW TESTS:" << std::endl;
    std::cout << "(Ford-Fulkerson / Edmonds-Karp / Dinic)" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/maxflow" + std::to_string(iter) + ".g";
        std::string solName = "tests/maxflow" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            std::cout << std::setw(50) << std::left << name;

            int expectedValue;
            sol >> expectedValue;

            graphInterface::graph g(0);
            g.loadGraphFromTextFile(name);
            std::vector<graphInterface::graph::maxFlowAlgorithms> alg =
                {graphInterface::graph::maxFlowAlgorithms::FordFulkerson,
                 graphInterface::graph::maxFlowAlgorithms::EdmondsKarp,
                 graphInterface::graph::maxFlowAlgorithms::Dinic};

            for (unsigned int i = 0; i < alg.size(); i++)
            {
                std::vector<int> minCut;
                int maxFlow;
                g.maximumFlow(alg[i], minCut, maxFlow);
                std::vector<graphInterface::edge_t> cutEdges;
                g.getCutFromSet(minCut, cutEdges);
                std::string status = ok;
                if (expectedValue != maxFlow)
                {
                    status = ko;
                }
                else if (((!g.isMaximumFlow(minCut)) && (maxFlow != -1)))
                {
                    status = ko;
                }
                if (i < alg.size() - 1)
                {
                    status += "/";
                }
                std::cout << status;
            }
            std::cout << std::endl;
        }
        else
        {
            break;
        }
    }
    std::cout << std::endl;

    std::cout << "MINIMUM COST FLOW TESTS:" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/mincostflow" + std::to_string(iter) + ".g";
        std::string solName = "tests/mincostflow" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            graphInterface::graph g(0);
            g.loadGraphFromTextFile(name);
            double expectedValue;
            sol >> expectedValue;
            std::string status = passed;
            double value = g.cycleCanceling();
            if ((value != expectedValue) || (!g.checkFeasibleFlow()))
            {
                status = failed;
            }
            std::cout << std::setw(50) << std::left << name;
            std::cout << status << std::endl;
        }
        else
        {
            break;
        }
        iter += 1;
    }
    std::cout << std::endl;

    std::cout << "CHORDALITY TESTS:" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/chordal" + std::to_string(iter) + ".g";
        std::string solName = "tests/chordal" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            graphInterface::graph g(0);
            g.loadGraphFromTextFile(name);
            bool expectedValue;
            sol >> expectedValue;
            std::string status = passed;
            if (g.isChordal() != expectedValue)
            {
                status = failed;
            }
            std::cout << std::setw(50) << std::left << name;
            std::cout << status << std::endl;
        }
        else
        {
            break;
        }
    }
    std::cout << std::endl;

    std::cout << "MAXIMAL CLIQUES TESTS:" << std::endl;
    for (int iter = 0;; ++iter)
    {
        std::string name = "tests/maxcliques" + std::to_string(iter) + ".g";
        std::string solName = "tests/maxcliques" + std::to_string(iter) + ".sol";
        std::ifstream f(name.c_str());
        std::ifstream sol(solName.c_str());
        if (f.good() && sol.good())
        {
            std::set<std::set<int>> expectedCliques, cliques;
            std::vector<std::vector<int>> clqs;
            int fillInEdges;
            graphInterface::graph g(0);
            g.loadGraphFromTextFile(name);
            g.eliminationGame(clqs, fillInEdges, graphInterface::graph::eliminationOrderingAlgorithms::MaximumCardinality);
            for (unsigned int i = 0; i < clqs.size(); i++)
            {
                std::set<int> cliq;
                for (unsigned int j = 0; j < clqs[i].size(); j++)
                {
                    cliq.insert(clqs[i][j]);
                }
                cliques.insert(cliq);
            }
            std::string line;
            while (std::getline(sol, line))
            {
                std::istringstream iss(line);
                std::set<int> cliq;
                int num;
                while (iss >> num)
                {
                    cliq.insert(num);
                }
                expectedCliques.insert(cliq);
            }
            std::string status = passed;
            if (expectedCliques != cliques)
            {
                status = failed;
            }
            std::cout << std::setw(50) << std::left << name;
            std::cout << status << std::endl;
        }
        else
        {
            break;
        }
    }
    std::cout << std::endl;

    return 0;
}
