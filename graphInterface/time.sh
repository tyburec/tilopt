#!/bin/bash

minTime=1000000
maxTime=0
sumTime=0
TIMEFORMAT=%R
numIter=0

calc() { awk "BEGIN{print $*}"; }
decimalmax() { echo $1 $2 | awk '{if ($1 > $2) {print $1} else {print $2}}'; }
decimalmin() { echo $1 $2 | awk '{if ($1 > $2) {print $2} else {print $1}}'; }

for k in {1..100}; do
	{ elapsedTime=$( { time ./$1 > /dev/null 2>&4-; } 2>&1 ); } 3>&1 4>&2
	minTime=$(decimalmin $minTime $elapsedTime)
	maxTime=$(decimalmax $maxTime $elapsedTime)
    sumTime=$(calc $sumTime+$elapsedTime)
    numIter=$(calc $numIter+1)
done

meanTime=$(calc $sumTime/$numIter)
echo "minTime: $minTime"
echo "maxTime: $maxTime"
echo "meanTime: $meanTime"
unset TIMEFORMAT