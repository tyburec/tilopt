/*
Header-only graph class.
Copyright (C) 2018 Marek Tyburec, marek.tyburec@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "vertex.hpp"
#include <string>
#include <iostream>
#include <memory>

namespace graphInterface
{
    class edge
    {
    private:
        int flow;
        int flowLB;
        int flowUB;
        double weight;
        std::string label;
        int tail;
        int head;
        bool thick;

    public:
        const int &getFlow() const { return flow; };
        const int &getFlowLowerBound() const { return flowLB; };
        const int &getFlowUpperBound() const { return flowUB; };
        const double &getWeight() const { return weight; };
        const int &getTailVertex() const { return tail; };
        const int &getHeadVertex() const { return head; };
        const std::string &getLabel() const { return label; };
        const bool isThick() const { return thick; };

        void setFlow(int f) { flow = f; };
        void setFLowLowerBound(int flb) { flowLB = flb; };
        void setFlowUpperBound(int fub) { flowUB = fub; };
        void setWeight(double w) { weight = w; };
        void setTailVertex(int tv) { tail = tv; };
        void setHeadVertex(int hv) { head = hv; };
        void setLabel(std::string l) { label = l; };
        void setThickness(bool it) { thick = it; };

        edge(){};
        edge(int tv, int hv, double w, int fl, int f, int fu, std::string l);
    };

    typedef graphInterface::edge *edge_t;

    edge::edge(int tv, int hv, double w, int fl, int f, int fu, std::string l)
    {
        setTailVertex(tv);
        setHeadVertex(hv);
        setWeight(w);
        setFLowLowerBound(fl);
        setFlowUpperBound(fu);
        setFlow(f);
        setLabel(l);
        setThickness(0);
    }
}