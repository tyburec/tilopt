/*
Header-only graph class.
Copyright (C) 2018 Marek Tyburec, marek.tyburec@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * ciquetreemex.cpp - create clique tree for a given chordal graph
 *
 * Input:   Nx2 matrix of undirected graph edges    (edgeMat)
 *          String choosing ordering algorithm      (orderingAlgorithm)
 *          Optional file name for dot export       (fname)
 *
 * Output:  Mx1 cell array of vertex labels         (cliqueTreeVertexLabels)
 *          Lx2 matrix of edges                     (cliqueTreeEdges)
 *          Lx1 cell array of edge labels           (cliqueTreeEdgeLables)
 *          Scalar, fill-in: number of added edges  (fillInEdges)
 *
 * The calling syntax is:
 *
 *		[cliqueTreeVertexLabels, cliqueTreeEdges, cliqueTreeEdgeLables, fillInEdges] = cliquetree(edgeMat, orderingAlgorithm, fname)
 *
 * This is a MEX file for MATLAB.
 **/

#include "../graph.hpp"
#include <sstream>
#include <iostream>
#include <string>
#include "mex.hpp"
#include "mexAdapter.hpp"

class MexFunction : public matlab::mex::Function
{
public:
    void operator()(matlab::mex::ArgumentList outputs, matlab::mex::ArgumentList inputs)
    {
        checkArguments(outputs, inputs);
        std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();
        matlab::data::ArrayFactory factory;

        matlab::data::TypedArray<double> edgeMat = std::move(inputs[0]);
        matlab::data::CharArray opt = std::move(inputs[1]);
        bool writeDotOutput = false;
        std::string fname = "";
        if (inputs.size() > 2)
        {
            matlab::data::CharArray fnameorig = std::move(inputs[2]);
            fname = fnameorig.toAscii();
            writeDotOutput = true;
        }
        std::string option = opt.toAscii();

        int numVertices = std::numeric_limits<int>::min();
        for (auto &it : edgeMat)
            numVertices < it ? numVertices = it : numVertices;

        ++numVertices;
        int numEdges = edgeMat.getDimensions()[0];

        graphInterface::graph graphOriginal(numVertices);
        for (int i = 0; i < numEdges; ++i)
            graphOriginal.addUndirectedEdge(edgeMat[i][0], edgeMat[i][1], 1.0);

        graphInterface::graph graphCliqueTree(0);
        std::vector<std::vector<int>> cliques;
        int fEdges;
        if (option.compare("MaximumCardinality") == 0)
            graphOriginal.eliminationGame(cliques, fEdges, graphInterface::graph::eliminationOrderingAlgorithms::MaximumCardinality);
        else if (option.compare("MinimumDegree") == 0)
            graphOriginal.eliminationGame(cliques, fEdges, graphInterface::graph::eliminationOrderingAlgorithms::MinimumDegree);
        else if (option.compare("MinimumFillIn") == 0)
            graphOriginal.eliminationGame(cliques, fEdges, graphInterface::graph::eliminationOrderingAlgorithms::MinimumFillIn);
        else
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Unknown algorithm. Use 'MaximumCardinality', 'MinimumDegree', or 'MinimumFillIn'.")}));
        graphOriginal.cliqueTree(cliques, graphCliqueTree);

        long unsigned int numCliques = graphCliqueTree.getNumVertices();

        matlab::data::CellArray cliqueTreeVertexLabels = factory.createCellArray({1, numCliques});
        matlab::data::TypedArray<double> cliqueTreeEdges = factory.createArray<double>({(long unsigned int)graphCliqueTree.getNumEdges(), 2});
        matlab::data::CellArray cliqueTreeEdgeLabels = factory.createCellArray({1, (long unsigned int)graphCliqueTree.getNumEdges()});
        matlab::data::TypedArray<double> fillInEdges = factory.createArray<double>({1, 1});
        fillInEdges[0][0] = fEdges;

        int i = 0;
        for (const auto &it : graphCliqueTree.getEdges())
        {
            cliqueTreeEdges[i][0] = it->getTailVertex();
            cliqueTreeEdges[i][1] = it->getHeadVertex();
            std::istringstream iss(it->getLabel());
            std::vector<int> numbers;
            std::string tempString;
            while (std::getline(iss, tempString, ','))
            {
                std::istringstream numSS(tempString);
                int num;
                numSS >> num;
                numbers.push_back(num);
            }
            matlab::data::TypedArray<double> edgeLabel = factory.createArray<double>({numbers.size(), 1});
            for (unsigned int j = 0; j < numbers.size(); ++j)
                edgeLabel[j] = numbers[j];
            cliqueTreeEdgeLabels[i] = edgeLabel;
            ++i;
        }

        i = 0;
        for (const auto &it : graphCliqueTree.getVertices())
        {
            std::istringstream iss(it->getLabel());
            std::vector<int> numbers;
            std::string tempString;
            while (std::getline(iss, tempString, ','))
            {
                std::istringstream numSS(tempString);
                int num;
                numSS >> num;
                numbers.push_back(num);
            }
            matlab::data::TypedArray<double> vertexLabel = factory.createArray<double>({numbers.size(), 1});
            for (unsigned int j = 0; j < numbers.size(); ++j)
                vertexLabel[j] = numbers[j];
            cliqueTreeVertexLabels[i] = vertexLabel;
            ++i;
        }

        if (writeDotOutput)
            graphCliqueTree.writeDotFile(fname);

        outputs[0] = std::move(cliqueTreeVertexLabels);
        if (outputs.size() > 1)
            outputs[1] = std::move(cliqueTreeEdges);
        if (outputs.size() > 2)
            outputs[2] = std::move(cliqueTreeEdgeLabels);
        if (outputs.size() > 3)
            outputs[3] = std::move(fillInEdges);
        if (outputs.size() > 4)
        {
            auto filledEdges = graphOriginal.fillInEdges(cliques);
            matlab::data::TypedArray<double> addedEdgesMat = factory.createArray<double>({(long unsigned int)filledEdges.size(), 2});
            auto edgeNum = size_t{0};
            for (const auto &e : filledEdges)
            {
                addedEdgesMat[edgeNum][0] = e.first;
                addedEdgesMat[edgeNum][1] = e.second;
                ++edgeNum;
            }
            outputs[4] = std::move(addedEdgesMat);
        }
    }

    void checkArguments(matlab::mex::ArgumentList outputs, matlab::mex::ArgumentList inputs)
    {
        std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();
        matlab::data::ArrayFactory factory;

        // two arguments required
        if (inputs.size() < 2)
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("At least two inputs required")}));

        // at most four outputs
        if (outputs.size() > 5)
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("At most five outputs can be used")}));

        // check argument types
        if (inputs[0].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[0].getType() == matlab::data::ArrayType::COMPLEX_DOUBLE)
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Input matrix weightMat must be a real matrix")}));

        if (inputs[1].getType() != matlab::data::ArrayType::CHAR)
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("The field orderingAlgorithm must contain a character array")}));

        if (inputs.size() == 3)
            if (inputs[2].getType() != matlab::data::ArrayType::CHAR)
                matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                                 0, std::vector<matlab::data::Array>({factory.createScalar("The field filename must contain a character array")}));

        // matching dimensions
        if (inputs[0].getDimensions().size() != 2)
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Input matrix edgeMat must be m-by-2 dimension")}));
        }

        if (inputs[0].getDimensions()[1] != 2)
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Input matrix edgeMat must be m-by-2 dimension")}));
        }
    }
};
