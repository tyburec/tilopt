/*
Header-only graph class.
Copyright (C) 2018 Marek Tyburec, marek.tyburec@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * ciquetreemex.cpp - create clique tree for a given chordal graph
 *
 * Input:   Nx2 matrix of undirected graph edges    (edgeMat)
 *          Nx1 matrix of edge weights              (weightMat)
 *          String choosing dissection algorithm    (algorithm)
 *
 * Output:  1xM boolean vector of dissection        (dissectVector)
 *          Scalar, cut weight                      (objVal)
 *
 * The calling syntax is:
 *
 *		[dissectVector, objVal] = dissection(edgeMat, weightMat, algorithm)
 *
 * This is a MEX file for MATLAB.
 **/

#include "../graph.hpp"
#include <sstream>
#include <iostream>
#include <string>
#include "mex.hpp"
#include "mexAdapter.hpp"

class MexFunction : public matlab::mex::Function
{
public:
    void operator()(matlab::mex::ArgumentList outputs, matlab::mex::ArgumentList inputs)
    {
        checkArguments(outputs, inputs);
        std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();
        matlab::data::ArrayFactory factory;

        matlab::data::TypedArray<double> edgeMat = std::move(inputs[0]);
        matlab::data::TypedArray<double> edgeWeight = std::move(inputs[1]);
        matlab::data::CharArray opt = std::move(inputs[2]);
        std::string option = opt.toAscii();

        int numVertices = std::numeric_limits<int>::min();
        for (auto it : edgeMat)
        {
            numVertices < it ? numVertices = it : numVertices;
        }
        ++numVertices;
        int numEdges = edgeMat.getDimensions()[0];

        graphInterface::graph graphOriginal(numVertices);
        for (int i = 0; i < numEdges; ++i)
        {
            graphOriginal.addUndirectedEdge(edgeMat[i][0], edgeMat[i][1], edgeWeight[i]);
        }

        std::vector<int> partitionA, partitionB;
        double oVal;
        if (option.compare("KernighanLin") == 0)
        {
            graphOriginal.kernighanLin(oVal, partitionA, partitionB);
        }
        else
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Unknown algorithm. Use 'KernighanLin'.")}));
        }

        matlab::data::TypedArray<bool> dissectVector = factory.createArray<bool>({1, (long unsigned int)numVertices});
        matlab::data::TypedArray<double> objVal = factory.createArray<double>({1, 1});
        objVal[0][0] = oVal;
        for (int i = 0; i < numVertices; ++i)
        {
            dissectVector[0][i] = false;
        }
        for (const auto &it : partitionA)
        {
            dissectVector[0][it] = true;
        }

        outputs[0] = std::move(dissectVector);
        outputs[1] = std::move(objVal);
    }

    void checkArguments(matlab::mex::ArgumentList outputs, matlab::mex::ArgumentList inputs)
    {
        std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();
        matlab::data::ArrayFactory factory;

        // two arguments required
        if (inputs.size() != 3)
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Three inputs required")}));
        }

        // check argument types
        if (inputs[0].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[0].getType() == matlab::data::ArrayType::COMPLEX_DOUBLE)
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Input matrix weightMat must be a real matrix")}));
        }

        if (inputs[1].getType() != matlab::data::ArrayType::DOUBLE ||
            inputs[1].getType() == matlab::data::ArrayType::COMPLEX_DOUBLE)
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Input matrix edgeMat must be a real matrix")}));
        }

        if (inputs[2].getType() != matlab::data::ArrayType::CHAR)
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("The field algorithm must contain a character array")}));
        }

        // matching dimensions
        if (inputs[0].getDimensions().size() != 2)
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Input matrix edgeMat must be m-by-2 dimension")}));
        }

        if (inputs[1].getDimensions().size() != 2)
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Input matrix weightMat must be m-by-1 dimension")}));
        }

        if (inputs[0].getDimensions()[1] != 2)
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Input matrix edgeMat must be m-by-2 dimension")}));
        }

        if (inputs[0].getDimensions()[0] != inputs[1].getDimensions()[0])
        {
            matlabPtr->feval(matlab::engine::convertUTF8StringToUTF16String("error"),
                             0, std::vector<matlab::data::Array>({factory.createScalar("Dimension mismatch")}));
        }
    }
};