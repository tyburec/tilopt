#pragma once

#include <memory>
#include <gurobi_c++.h>
#include "solverModel.hpp"
#include "gurobiVariable.hpp"
#include "linExpression.hpp"
#include "quadExpression.hpp"

namespace optimizerInterface
{
    class gurobiModel : public solverModel
    {
    private:
        std::shared_ptr<GRBEnv> env;
        std::shared_ptr<GRBModel> model;
        int numConstraints;
        int numVariables;

    public:
        gurobiModel();
        gurobiModel(const gurobiModel &obj);
        ~gurobiModel(){};

        void addLinearConstraint(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs);
        void addQuadraticConstraint(optimizerInterface::quadExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs);
        void addSemidefiniteConstraint()
        {
            std::cerr << "ERROR: Gurobi does not support semidefinite constraints!" << std::endl;
            exit(2);
        };
        void constrainTrace(double num, std::shared_ptr<optimizerInterface::variable> &var)
        {
            std::cerr << "ERROR: Gurobi does not support semidefinite constraints!" << std::endl;
            exit(2);
        };

        void addLinearObjective(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::objectiveSenses sense) { addLinearObjective(expr, sense, 0.0); };
        void addLinearObjective(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::objectiveSenses sense, double constant);
        // virtual int addQuadraticObjective() = 0;
        // virtual int addSemidefiniteObjective(){ std::cerr << "ERROR: Gurobi does not support semidefinite objective function!" << std::endl; exit(2); };

        void addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var);
        void addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var, double lb, double ub);
        void addBinaryVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var);
        void addSemidefiniteVariable(std::shared_ptr<optimizerInterface::variable> &var, int size)
        {
            std::cerr << "ERROR: Gurobi does not support semidefinite variables!" << std::endl;
            exit(2);
        };

        void getVarByArrayPosition(std::shared_ptr<optimizerInterface::variable> &var, int varNumber);

        void optimize();
    };

    gurobiModel::gurobiModel(const gurobiModel &obj)
    {
        env = obj.env;
        model = std::make_shared<GRBModel>(*(obj.model));
        numConstraints = obj.getNumberOfConstraints();
        numVariables = obj.getNumberOfConstraints();
        objective = obj.getObjective();
        objectiveSense = obj.getObjectiveSense();
        solverStatus = obj.getSolverStatus();
        obj.getSos(sos);
        sosType = obj.sosType;
        solver = obj.getSolver();
        branchedValues = obj.branchedValues;
    }

    void gurobiModel::getVarByArrayPosition(std::shared_ptr<optimizerInterface::variable> &var, int varNumber)
    {
        std::string name = std::to_string(varNumber);
        std::shared_ptr<GRBVar> gVar = std::make_shared<GRBVar>(model->getVarByName(name));
        var = std::make_shared<optimizerInterface::gurobiVariable>(gVar);
        var = std::dynamic_pointer_cast<gurobiVariable>(var);
    }

    void gurobiModel::optimize()
    {
        model->optimize();
        if ((model->get(GRB_IntAttr_Status)) != 2)
        {
            setSolverStatus(optimizerInterface::solverModel::solversStatuses::INFEASIBLE);
        }
        else
        {
            setSolverStatus(optimizerInterface::solverModel::solversStatuses::OPTIMAL);
            setObjective(model->get(GRB_DoubleAttr_ObjVal));
        }
    }

    gurobiModel::gurobiModel()
    {
        env = std::make_shared<GRBEnv>(true);
        env->set(GRB_IntParam_OutputFlag, 0);
        env->set(GRB_DoubleParam_TimeLimit, 300);
        env->set(GRB_IntParam_Threads, 1);
        model = std::make_shared<GRBModel>(*env);
    }

    void gurobiModel::addLinearConstraint(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs)
    {
        std::vector<double> multipliers;
        std::vector<std::shared_ptr<optimizerInterface::variable>> vars;
        expr.getMultipliers(multipliers);
        expr.getVariables(vars);

        GRBLinExpr constraint;

        for (int nvar = 0; nvar < (int)multipliers.size(); nvar++)
        {
            std::shared_ptr<optimizerInterface::gurobiVariable> vD = std::dynamic_pointer_cast<optimizerInterface::gurobiVariable>(vars[nvar]);
            std::shared_ptr<GRBVar> gvar = vD->getVariable();
            constraint += multipliers[nvar] * (*gvar);
        }

        switch (sense)
        {
        case optimizerInterface::solverModel::constraintSense::EQUAL:
        {
            model->addConstr(constraint, GRB_EQUAL, rhs);
            break;
        }

        case optimizerInterface::solverModel::constraintSense::LESSEQUAL:
        {
            model->addConstr(constraint, GRB_LESS_EQUAL, rhs);
            break;
        }

        case optimizerInterface::solverModel::constraintSense::GREATEREQUAL:
        {
            model->addConstr(constraint, GRB_GREATER_EQUAL, rhs);
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown sense of gurobi linear constraint." << std::endl;
            exit(2);
        }
        }

        numConstraints += 1;
    }

    void gurobiModel::addQuadraticConstraint(optimizerInterface::quadExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs)
    {
        std::vector<double> multipliers;
        std::vector<std::shared_ptr<optimizerInterface::variable>> varsA, varsB;
        expr.getMultipliers(multipliers);
        expr.getVariablesA(varsA);
        expr.getVariablesB(varsB);

        GRBQuadExpr constraint;

        for (int nvar = 0; nvar < (int)multipliers.size(); nvar++)
        {
            std::shared_ptr<optimizerInterface::gurobiVariable> varA = std::dynamic_pointer_cast<optimizerInterface::gurobiVariable>(varsA[nvar]);
            std::shared_ptr<GRBVar> gvarA = varA->getVariable();

            if (varsB[nvar] == NULL)
            {
                constraint += multipliers[nvar] * (*gvarA);
            }
            else
            {
                std::shared_ptr<optimizerInterface::gurobiVariable> varB = std::dynamic_pointer_cast<optimizerInterface::gurobiVariable>(varsB[nvar]);
                std::shared_ptr<GRBVar> gvarB = varB->getVariable();
                constraint += multipliers[nvar] * (*gvarA) * (*gvarB);
            }
        }

        switch (sense)
        {
        case optimizerInterface::solverModel::constraintSense::EQUAL:
        {
            model->addQConstr(constraint, GRB_EQUAL, rhs);
            break;
        }

        case optimizerInterface::solverModel::constraintSense::LESSEQUAL:
        {
            model->addQConstr(constraint, GRB_LESS_EQUAL, rhs);
            break;
        }

        case optimizerInterface::solverModel::constraintSense::GREATEREQUAL:
        {
            model->addQConstr(constraint, GRB_GREATER_EQUAL, rhs);
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown sense of gurobi linear constraint." << std::endl;
            exit(2);
        }
        }

        numConstraints += 1;
    }

    void gurobiModel::addLinearObjective(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::objectiveSenses sense, double constant)
    {
        std::vector<double> multipliers;
        std::vector<std::shared_ptr<optimizerInterface::variable>> vars;
        expr.getMultipliers(multipliers);
        expr.getVariables(vars);

        GRBLinExpr objective;

        for (int nvar = 0; nvar < (int)multipliers.size(); nvar++)
        {
            std::shared_ptr<optimizerInterface::gurobiVariable> vD = std::dynamic_pointer_cast<optimizerInterface::gurobiVariable>(vars[nvar]);
            std::shared_ptr<GRBVar> gvar = vD->getVariable();
            objective += multipliers[nvar] * (*gvar);
        }

        objective += constant;

        switch (sense)
        {
        case optimizerInterface::solverModel::objectiveSenses::MAXIMIZE:
        {
            model->setObjective(objective, GRB_MAXIMIZE);
            break;
        }

        case optimizerInterface::solverModel::objectiveSenses::MINIMIZE:
        {
            model->setObjective(objective, GRB_MINIMIZE);
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown sense of gurobi linear objective." << std::endl;
            exit(2);
        }
        }
    }

    void gurobiModel::addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var)
    {
        var = std::make_shared<optimizerInterface::gurobiVariable>(varNumber, optimizerInterface::variable::variableTypes::CONTINUOUS, model);
        var = std::dynamic_pointer_cast<gurobiVariable>(var);
        numVariables += 1;
    }

    void gurobiModel::addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var, double lb, double ub)
    {
        var = std::make_shared<optimizerInterface::gurobiVariable>(varNumber, optimizerInterface::variable::variableTypes::CONTINUOUS, model, lb, ub);
        var = std::dynamic_pointer_cast<gurobiVariable>(var);
        numVariables += 1;
    }

    void gurobiModel::addBinaryVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var)
    {
        var = std::make_shared<optimizerInterface::gurobiVariable>(varNumber, optimizerInterface::variable::variableTypes::BINARY, model);
        var = std::dynamic_pointer_cast<gurobiVariable>(var);
        numVariables += 1;
    }
}