#pragma once

#include <limits>
#include <gurobi_c++.h>
#include "variable.hpp"
#include "solverModel.hpp"
#include <memory>
#include <vector>
#include <string>

namespace optimizerInterface
{
    class gurobiVariable : public variable
    {
    private:
        std::shared_ptr<GRBVar> var;

    public:
        double getValue() { return var->get(GRB_DoubleAttr_X); };
        std::shared_ptr<GRBVar> getVariable() { return var; };
        gurobiVariable(){};
        gurobiVariable(std::shared_ptr<GRBVar> gVar);
        gurobiVariable(int varNumber, optimizerInterface::variable::variableTypes vtype, std::shared_ptr<GRBModel> &model);
        gurobiVariable(int varNumber, optimizerInterface::variable::variableTypes vtype, std::shared_ptr<GRBModel> &model, double lb, double ub);
        gurobiVariable(optimizerInterface::variable::variableTypes vtype, std::shared_ptr<GRBModel> &model, int size)
        {
            std::cerr << "ERROR: SDP variables are not supported in Gurobi!" << std::endl;
            exit(2);
        };
        void getVarInMatrix(int pos1, int pos2, std::shared_ptr<optimizerInterface::variable> &newVar)
        {
            std::cerr << "ERROR: SDP variables are not supported in Gurobi!" << std::endl;
            exit(2);
        };
        ~gurobiVariable(){};
    };

    gurobiVariable::gurobiVariable(std::shared_ptr<GRBVar> gVar)
    {
        var = gVar;
    }

    gurobiVariable::gurobiVariable(int varNumber, optimizerInterface::variable::variableTypes vtype, std::shared_ptr<GRBModel> &model)
    {
        std::string name = std::to_string(varNumber);

        switch (vtype)
        {
        case optimizerInterface::variable::variableTypes::BINARY:
        {
            var = std::make_shared<GRBVar>(model->addVar(0.0, 1.0, 0.0, GRB_BINARY, name));
            std::vector<double> set = {0.0, 1.0};
            constrainToDoubleSet(set);
            break;
        }

        case optimizerInterface::variable::variableTypes::POWEREQUALONE:
        {
            var = std::make_shared<GRBVar>(model->addVar(-1.0, 1.0, 0.0, GRB_CONTINUOUS, name));
            std::vector<double> set = {-1.0, 1.0};
            constrainToDoubleSet(set);
            break;
        }

        case optimizerInterface::variable::variableTypes::CONTINUOUS:
        {
            var = std::make_shared<GRBVar>(model->addVar(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max(), 0.0, GRB_CONTINUOUS, name));
            break;
        }

        case optimizerInterface::variable::variableTypes::SEMIDEFINITE:
        {
            std::cerr << "ERROR: Gurobi does not support semidefinite variables!" << std::endl;
            exit(2);
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown variable type!" << std::endl;
            exit(2);
            break;
        }
        }
    }

    gurobiVariable::gurobiVariable(int varNumber, optimizerInterface::variable::variableTypes vtype, std::shared_ptr<GRBModel> &model, double lb, double ub)
    {
        std::string name = std::to_string(varNumber);

        switch (vtype)
        {
        case optimizerInterface::variable::variableTypes::BINARY:
        {
            var = std::make_shared<GRBVar>(model->addVar((int)lb, (int)ub, 0.0, GRB_BINARY, name));
            std::vector<double> set = {0.0, 1.0};
            constrainToDoubleSet(set);
            break;
        }

        case optimizerInterface::variable::variableTypes::POWEREQUALONE:
        {
            var = std::make_shared<GRBVar>(model->addVar(-1.0, 1.0, 0.0, GRB_CONTINUOUS, name));
            std::vector<double> set = {-1.0, 1.0};
            constrainToDoubleSet(set);
            break;
        }

        case optimizerInterface::variable::variableTypes::CONTINUOUS:
        {
            var = std::make_shared<GRBVar>(model->addVar(lb, ub, 0.0, GRB_CONTINUOUS, name));
            break;
        }

        case optimizerInterface::variable::variableTypes::SEMIDEFINITE:
        {
            std::cerr << "ERROR: Gurobi does not support semidefinite variables!" << std::endl;
            exit(2);
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown variable type!" << std::endl;
            exit(2);
            break;
        }
        }
    }
}