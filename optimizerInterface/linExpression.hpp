#pragma once

#include <memory>
#include <vector>
#include "variable.hpp"

namespace optimizerInterface
{
    class linExpression
    {
    private:
        std::vector<double> multipliers;
        std::vector<std::shared_ptr<optimizerInterface::variable>> variables;

    public:
        linExpression(){};
        void add(std::shared_ptr<optimizerInterface::variable> var, double multiplier);
        void add(std::shared_ptr<optimizerInterface::variable> var);
        void getMultipliers(std::vector<double> &m) { m = multipliers; };
        void getVariables(std::vector<std::shared_ptr<optimizerInterface::variable>> &v) { v = variables; };
    };

    void linExpression::add(std::shared_ptr<optimizerInterface::variable> var, double multiplier)
    {
        multipliers.push_back(multiplier);
        variables.push_back(var);
    }

    void linExpression::add(std::shared_ptr<optimizerInterface::variable> var)
    {
        multipliers.push_back(1.0);
        variables.push_back(var);
    }
}