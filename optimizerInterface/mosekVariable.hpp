#pragma once

#include <limits>
#include "variable.hpp"
#include "solverModel.hpp"
#include <fusion.h>
#include <monty.h>
#include <memory>

namespace optimizerInterface
{
    class mosekVariable : public variable
    {
    private:
        mosek::fusion::Variable::t *var;

    public:
        double getValue() { return (*(*var)->level())[0]; };
        mosek::fusion::Variable::t getVariable() { return *var; };
        mosekVariable() { *var = NULL; };
        mosekVariable(mosek::fusion::Variable::t *v);
        mosekVariable(int varNumber, optimizerInterface::variable::variableTypes vtype, std::shared_ptr<mosek::fusion::Model::t> &model);
        mosekVariable(int varNumber, optimizerInterface::variable::variableTypes vtype, std::shared_ptr<mosek::fusion::Model::t> &model, double lb, double ub);
        mosekVariable(optimizerInterface::variable::variableTypes vtype, std::shared_ptr<mosek::fusion::Model::t> &model, int size);
        ~mosekVariable() { delete var; };
        void getVarInMatrix(int pos1, int pos2, std::shared_ptr<optimizerInterface::variable> &newVar);
    };

    void mosekVariable::getVarInMatrix(int pos1, int pos2, std::shared_ptr<optimizerInterface::variable> &newVar)
    {
        // mosek::fusion::Variable::t a = (*var)->index(pos1, pos2);
        newVar = std::make_shared<optimizerInterface::mosekVariable>(new mosek::fusion::Variable::t((*var)->index(pos1, pos2)));
        newVar = std::dynamic_pointer_cast<mosekVariable>(newVar);
    }

    mosekVariable::mosekVariable(mosek::fusion::Variable::t *v)
    {
        var = v;
    }

    mosekVariable::mosekVariable(int varNumber, optimizerInterface::variable::variableTypes vtype, std::shared_ptr<mosek::fusion::Model::t> &model)
    {
        std::string name = std::to_string(varNumber);

        switch (vtype)
        {
        case optimizerInterface::variable::variableTypes::BINARY:
        {
            var = new mosek::fusion::Variable::t((*model)->variable(name, 1, mosek::fusion::Domain::binary()));
            std::vector<double> set = {0.0, 1.0};
            constrainToDoubleSet(set);
            break;
        }

        case optimizerInterface::variable::variableTypes::POWEREQUALONE:
        {
            var = new mosek::fusion::Variable::t((*model)->variable(name, 1, mosek::fusion::Domain::inRange(-1.0, 1.0)));
            std::vector<double> set = {-1.0, 1.0};
            constrainToDoubleSet(set);
            break;
        }

        case optimizerInterface::variable::variableTypes::CONTINUOUS:
        {
            var = new mosek::fusion::Variable::t((*model)->variable(name, 1, mosek::fusion::Domain::unbounded()));
            break;
        }

        case optimizerInterface::variable::variableTypes::SEMIDEFINITE:
        {
            std::cerr << "ERROR: SDP variables need to be defined different way!" << std::endl;
            exit(2);
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown variable type!" << std::endl;
            exit(2);
            break;
        }
        }
    }

    mosekVariable::mosekVariable(int varNumber, optimizerInterface::variable::variableTypes vtype, std::shared_ptr<mosek::fusion::Model::t> &model, double lb, double ub)
    {
        std::string name = std::to_string(varNumber);

        switch (vtype)
        {
        case optimizerInterface::variable::variableTypes::BINARY:
        {
            var = new mosek::fusion::Variable::t((*model)->variable(name, 1, mosek::fusion::Domain::binary()));
            std::vector<double> set = {0.0, 1.0};
            constrainToDoubleSet(set);
            break;
        }

        case optimizerInterface::variable::variableTypes::POWEREQUALONE:
        {
            var = new mosek::fusion::Variable::t((*model)->variable(name, 1, mosek::fusion::Domain::inRange(-1.0, 1.0)));
            std::vector<double> set = {-1.0, 1.0};
            constrainToDoubleSet(set);
            break;
        }

        case optimizerInterface::variable::variableTypes::CONTINUOUS:
        {
            var = new mosek::fusion::Variable::t((*model)->variable(name, 1, mosek::fusion::Domain::inRange(lb, ub)));
            break;
        }

        case optimizerInterface::variable::variableTypes::SEMIDEFINITE:
        {
            std::cerr << "ERROR: SDP variables need to be defined different way!" << std::endl;
            exit(2);
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown variable type!" << std::endl;
            exit(2);
            break;
        }
        }
    }

    mosekVariable::mosekVariable(optimizerInterface::variable::variableTypes vtype, std::shared_ptr<mosek::fusion::Model::t> &model, int size)
    {
        if (vtype == optimizerInterface::variable::variableTypes::SEMIDEFINITE)
        {
            try
            {
                var = new mosek::fusion::Variable::t((*model)->variable(size, mosek::fusion::Domain::inPSDCone()));
            }
            catch (mosek::fusion::FusionRuntimeException &e)
            {
                std::cerr << "MOSEK ERROR: " << e.toString() << std::endl;
                exit(2);
            }
        }
        else
        {
            std::cerr << "ERROR: Used mosekVariable constructor is intended to be used only with SDP variables!" << std::endl;
            exit(2);
        }
    }

}