#pragma once

#include <memory>
#include "solverModel.hpp"

#ifdef DMOSEK
#include "mosekModel.hpp"
#endif

#ifdef DGUROBI
#include "gurobiModel.hpp"
#endif

// interface for variables, expressions, model

namespace optimizerInterface
{
    class optimizerEnvironment
    {
    private:
        std::shared_ptr<optimizerInterface::solverModel> model;
        optimizerInterface::solverModel::solvers solver;

    public:
        optimizerEnvironment(){};
        optimizerEnvironment(optimizerInterface::solverModel::solvers solver);
        ~optimizerEnvironment(){};
        void initializeModel(std::shared_ptr<optimizerInterface::solverModel> &m);
        optimizerInterface::solverModel::solvers getSolver() { return solver; };
    };

    optimizerEnvironment::optimizerEnvironment(optimizerInterface::solverModel::solvers s)
    {
        switch (s)
        {
#ifdef DGUROBI
        case optimizerInterface::solverModel::solvers::GUROBI:
        {
            model = std::make_shared<optimizerInterface::gurobiModel>();
            solver = optimizerInterface::solverModel::solvers::GUROBI;
            break;
        }
#endif

#ifdef DMOSEK
        case optimizerInterface::solverModel::solvers::MOSEK:
        {
            model = std::make_shared<optimizerInterface::mosekModel>();
            solver = optimizerInterface::solverModel::solvers::MOSEK;
            break;
        }
#endif

        default:
        {
            std::cerr << "ERROR: Unknown solver in optimizerInterface." << std::endl;
            exit(2);
            break;
        }
        }
    }

    void optimizerEnvironment::initializeModel(std::shared_ptr<optimizerInterface::solverModel> &m)
    {
        switch (solver)
        {
#ifdef DGUROBI
        case optimizerInterface::solverModel::solvers::GUROBI:
        {
            m = std::dynamic_pointer_cast<optimizerInterface::gurobiModel>(model);
            model->setSolver(optimizerInterface::solverModel::solvers::GUROBI);
            break;
        }
#endif

#ifdef DMOSEK
        case optimizerInterface::solverModel::solvers::MOSEK:
        {
            m = std::dynamic_pointer_cast<optimizerInterface::mosekModel>(model);
            model->setSolver(optimizerInterface::solverModel::solvers::MOSEK);
            break;
        }
#endif

        default:
        {
            std::cerr << "ERROR: Unknown solver in optimizerInterface." << std::endl;
            exit(2);
            break;
        }
        }
    }
}