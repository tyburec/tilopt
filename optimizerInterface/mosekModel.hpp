#pragma once

#include <memory>
#include <fusion.h>
#include <monty.h>
#include "solverModel.hpp"
#include "mosekVariable.hpp"
#include "linExpression.hpp"
#include "quadExpression.hpp"

namespace optimizerInterface
{
    class mosekModel : public solverModel
    {
    private:
        std::shared_ptr<mosek::fusion::Model::t> model;
        int numConstraints;
        int numVariables;

    public:
        mosekModel();
        mosekModel(const mosekModel &obj);
        ~mosekModel() { (*model)->dispose(); };

        void addLinearConstraint(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs);
        void addQuadraticConstraint(optimizerInterface::quadExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs);
        void addSemidefiniteConstraint()
        {
            std::cerr << "ERROR: Not implemented yet!" << std::endl;
            exit(2);
        };
        void constrainTrace(double num, std::shared_ptr<optimizerInterface::variable> &var);

        void addLinearObjective(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::objectiveSenses sense) { addLinearObjective(expr, sense, 0.0); };
        void addLinearObjective(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::objectiveSenses sense, double constant);
        // virtual int addQuadraticObjective() = 0;
        // virtual int addSemidefiniteObjective(){ };

        void addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var);
        void addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var, double lb, double ub);
        void addBinaryVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var);
        void addSemidefiniteVariable(std::shared_ptr<optimizerInterface::variable> &var, int size);

        void getVarByArrayPosition(std::shared_ptr<optimizerInterface::variable> &var, int varNumber);

        void optimize();
    };

    void mosekModel::constrainTrace(double num, std::shared_ptr<optimizerInterface::variable> &var)
    {
        std::shared_ptr<optimizerInterface::mosekVariable> nVar = std::dynamic_pointer_cast<optimizerInterface::mosekVariable>(var);
        mosek::fusion::Variable::t mVar = nVar->getVariable();
        (*model)->constraint(mVar->diag(), mosek::fusion::Domain::equalsTo(num));
    }

    mosekModel::mosekModel(const mosekModel &obj)
    {
        numConstraints = obj.getNumberOfConstraints();
        numVariables = obj.getNumberOfConstraints();
        objective = obj.getObjective();
        objectiveSense = obj.getObjectiveSense();
        solverStatus = obj.getSolverStatus();
        obj.getSos(sos);
        sosType = obj.sosType;
        solver = obj.getSolver();
        branchedValues = obj.branchedValues;
        try
        {
            model = std::make_shared<mosek::fusion::Model::t>((*obj.model)->clone());
        }
        catch (mosek::fusion::FusionRuntimeException &e)
        {
            std::cerr << "MOSEK ERROR: " << e.toString() << std::endl;
            exit(2);
        }
    }

    void mosekModel::getVarByArrayPosition(std::shared_ptr<optimizerInterface::variable> &var, int varNumber)
    {
        std::string name = std::to_string(varNumber);
        var = std::make_shared<optimizerInterface::mosekVariable>(new mosek::fusion::Variable::t((*model)->getVariable(name)));
        var = std::dynamic_pointer_cast<mosekVariable>(var);
    }

    mosekModel::mosekModel()
    {
        model = std::make_shared<mosek::fusion::Model::t>(new mosek::fusion::Model());
    }

    void mosekModel::optimize()
    {
        // (*model)->setSolverParam("numThreads",1);
        // (*model)->setSolverParam("optimizer", "freeSimplex");
        // (*model)->setLogHandler([=](const std::string & msg) { std::cout << msg << std::flush; } );

        try
        {
            (*model)->solve();
        }
        catch (mosek::fusion::FusionRuntimeException &e)
        {
            std::cerr << "MOSEK ERROR: " << e.toString() << std::endl;
            exit(2);
        }

        switch ((*model)->getPrimalSolutionStatus())
        {
        case mosek::fusion::SolutionStatus::Undefined:
        case mosek::fusion::SolutionStatus::Unknown:
        case mosek::fusion::SolutionStatus::Certificate:
        case mosek::fusion::SolutionStatus::NearCertificate:
        case mosek::fusion::SolutionStatus::IllposedCert:
        {
            setSolverStatus(optimizerInterface::solverModel::solversStatuses::INFEASIBLE);
            break;
        }

        case mosek::fusion::SolutionStatus::Optimal:
        case mosek::fusion::SolutionStatus::NearOptimal:
        case mosek::fusion::SolutionStatus::Feasible:
        case mosek::fusion::SolutionStatus::NearFeasible:
        {
            setSolverStatus(optimizerInterface::solverModel::solversStatuses::OPTIMAL);
            setObjective((*model)->primalObjValue());
            break;
        }
        }
    }

    void mosekModel::addLinearConstraint(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs)
    {
        std::vector<double> multipliers;
        std::vector<std::shared_ptr<optimizerInterface::variable>> vars;
        expr.getMultipliers(multipliers);
        expr.getVariables(vars);

        mosek::fusion::Expression::t constraint = mosek::fusion::Expr::constTerm(0.0);

        for (int nvar = 0; nvar < (int)multipliers.size(); nvar++)
        {
            std::shared_ptr<optimizerInterface::mosekVariable> vD = std::dynamic_pointer_cast<optimizerInterface::mosekVariable>(vars[nvar]);
            mosek::fusion::Variable::t mvar = vD->getVariable();

            // can be improved?
            constraint = mosek::fusion::Expr::add(constraint, mosek::fusion::Expr::mul(mvar, multipliers[nvar]));
        }

        switch (sense)
        {
        case optimizerInterface::solverModel::constraintSense::EQUAL:
        {
            (*model)->constraint(constraint, mosek::fusion::Domain::equalsTo(rhs));
            break;
        }

        case optimizerInterface::solverModel::constraintSense::LESSEQUAL:
        {
            (*model)->constraint(constraint, mosek::fusion::Domain::lessThan(rhs));
            break;
        }

        case optimizerInterface::solverModel::constraintSense::GREATEREQUAL:
        {
            (*model)->constraint(constraint, mosek::fusion::Domain::greaterThan(rhs));
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown sense of mosek linear constraint." << std::endl;
            exit(2);
        }
        }

        numConstraints += 1;
    }

    void mosekModel::addQuadraticConstraint(optimizerInterface::quadExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs)
    {
        std::cerr << "ERROR: Quadratic constraints are not implemented in MOSEK yet." << std::endl;
        exit(2);
    }

    void mosekModel::addLinearObjective(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::objectiveSenses sense, double constant)
    {
        std::vector<double> multipliers;
        std::vector<std::shared_ptr<optimizerInterface::variable>> vars;
        expr.getMultipliers(multipliers);
        expr.getVariables(vars);

        mosek::fusion::Expression::t objective = mosek::fusion::Expr::constTerm(0.0);

        for (int nvar = 0; nvar < (int)multipliers.size(); nvar++)
        {
            std::shared_ptr<optimizerInterface::mosekVariable> vD = std::dynamic_pointer_cast<optimizerInterface::mosekVariable>(vars[nvar]);
            mosek::fusion::Variable::t mvar = vD->getVariable();

            // can be improved?
            objective = mosek::fusion::Expr::add(objective, mosek::fusion::Expr::mul(mvar, multipliers[nvar]));
        }

        objective = mosek::fusion::Expr::add(objective, constant);

        switch (sense)
        {
        case optimizerInterface::solverModel::objectiveSenses::MAXIMIZE:
        {
            (*model)->objective(mosek::fusion::ObjectiveSense::Maximize, objective);
            break;
        }

        case optimizerInterface::solverModel::objectiveSenses::MINIMIZE:
        {
            (*model)->objective(mosek::fusion::ObjectiveSense::Minimize, objective);
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown sense of mosek linear objective." << std::endl;
            exit(2);
        }
        }
    }

    void mosekModel::addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var)
    {
        var = std::make_shared<optimizerInterface::mosekVariable>(varNumber, optimizerInterface::variable::variableTypes::CONTINUOUS, model);
        var = std::dynamic_pointer_cast<mosekVariable>(var);
        numVariables += 1;
    }

    void mosekModel::addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var, double lb, double ub)
    {
        var = std::make_shared<optimizerInterface::mosekVariable>(varNumber, optimizerInterface::variable::variableTypes::CONTINUOUS, model, lb, ub);
        var = std::dynamic_pointer_cast<mosekVariable>(var);
        numVariables += 1;
    }

    void mosekModel::addBinaryVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var)
    {
        var = std::make_shared<optimizerInterface::mosekVariable>(varNumber, optimizerInterface::variable::variableTypes::BINARY, model);
        var = std::dynamic_pointer_cast<mosekVariable>(var);
        numVariables += 1;
    }

    void mosekModel::addSemidefiniteVariable(std::shared_ptr<optimizerInterface::variable> &var, int size)
    {
        var = std::make_shared<optimizerInterface::mosekVariable>(optimizerInterface::variable::variableTypes::SEMIDEFINITE, model, size);
        var = std::dynamic_pointer_cast<mosekVariable>(var);
        numVariables += 1;
    }
}