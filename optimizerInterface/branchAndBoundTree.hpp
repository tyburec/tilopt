#pragma once
#include <vector>
#include <set>
#include <limits>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "branchAndBoundNode.hpp"

namespace optimizerInterface
{
    class branchAndBoundTree
    {
    private:
        std::multiset<std::shared_ptr<optimizerInterface::branchAndBoundNode>, optimizerInterface::branchAndBoundNode> leafeNodes;
        double timeLimit;
        std::chrono::steady_clock::time_point startTime;
        std::vector<std::shared_ptr<optimizerInterface::branchAndBoundNode>> solution;
        int displayFrequency;
        double lowerBound;
        double upperBound;
        optimizerInterface::solverModel::objectiveSenses objectiveSense;
        unsigned int solCount;

    public:
        branchAndBoundTree(std::shared_ptr<optimizerInterface::solverModel> &model, optimizerInterface::branchAndBoundNode::branchingRules rule, int sCount);
        branchAndBoundTree(std::shared_ptr<optimizerInterface::solverModel> &model, optimizerInterface::branchAndBoundNode::branchingRules rule) { branchAndBoundTree(model, rule, 1); };
        void solve();
        void setCurrentTime() { startTime = std::chrono::steady_clock::now(); };
        std::chrono::steady_clock::time_point getStartTime() const { return startTime; };
        double getElapsedTime() const;
        void setTimeLimit(double time) { timeLimit = time; };
        int getTimeLimit() const { return timeLimit; };
        void setDisplayFrequency(int f) { displayFrequency = f; };
        int getDisplayFrequency() const { return displayFrequency; };

        void setLowerBound(double val) { lowerBound = val; };
        double getLowerBound() { return lowerBound; };
        void setUpperBound(double val) { upperBound = val; };
        double getUpperBound() { return upperBound; };

        optimizerInterface::solverModel::objectiveSenses getObjectiveSense() { return objectiveSense; };
        void setObjectiveSense(optimizerInterface::solverModel::objectiveSenses sense) { objectiveSense = sense; };

        void retrieveSolution(std::shared_ptr<optimizerInterface::solverModel> &smod, int number);
        void retrieveSolution(std::shared_ptr<optimizerInterface::solverModel> &smod) { retrieveSolution(smod, 1); };

        void setSolCount(int sCount) { solCount = sCount; };
        unsigned int getSolCount() { return solCount; };

        unsigned int getFoundSolutionsCount() { return solution.size(); };
    };

    void branchAndBoundTree::retrieveSolution(std::shared_ptr<optimizerInterface::solverModel> &smod, int number)
    {
        if (solution.size() == 0)
        {
            std::cerr << "ERROR: No solution exists!" << std::endl;
            exit(2);
        }
        solution[number]->retrieveOptimizationModel(smod);
    }

    branchAndBoundTree::branchAndBoundTree(std::shared_ptr<optimizerInterface::solverModel> &model, optimizerInterface::branchAndBoundNode::branchingRules rule, int sCount)
    {
        setSolCount(sCount);
        solution.clear();
        setTimeLimit(7200.0);
        setDisplayFrequency(5);
        setLowerBound(-1.0 * std::numeric_limits<double>::infinity());
        setUpperBound(std::numeric_limits<double>::infinity());
        setObjectiveSense(model->getObjectiveSense());
        std::shared_ptr<optimizerInterface::branchAndBoundNode> rootNode = std::make_shared<optimizerInterface::branchAndBoundNode>(model, rule);
        setCurrentTime();

        rootNode->setDepth(0);
        rootNode->solve();
        if (rootNode->isFeasible() == 0)
        {
            std::cerr << "ERROR: The root relaxation is infeasible!" << std::endl;
            exit(2);
        }
        else
        {
            if ((getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE) && (rootNode->getObjective() < getUpperBound()))
            {
                if (rootNode->isModelIntegerFeasible())
                {
                    setUpperBound(rootNode->getObjective());
                    solution.push_back(rootNode);
                }
                else
                {
                    leafeNodes.insert(rootNode);
                }
                setLowerBound(rootNode->getObjective());
            }

            if ((getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MAXIMIZE) && (rootNode->getObjective() > getLowerBound()))
            {
                if (rootNode->isModelIntegerFeasible())
                {
                    setLowerBound(rootNode->getObjective());
                    solution.push_back(rootNode);
                }
                else
                {
                    leafeNodes.insert(rootNode);
                }
                setUpperBound(rootNode->getObjective());
            }
        }

        std::cout << std::setw(10) << "BRANCHED";
        std::cout << " ";
        std::cout << std::setw(10) << "LEAVES";
        std::cout << " ";
        std::cout << std::setw(10) << "DEPTH";
        std::cout << " ";
        std::cout << std::setw(10) << "LOWERBOUND";
        std::cout << " ";
        std::cout << std::setw(10) << "UPPERBOUND";
        std::cout << " ";
        std::cout << std::setw(10) << "GAP";
        std::cout << " ";
        std::cout << std::setw(10) << "OBJECTIVE";
        std::cout << " ";
        std::cout << std::setw(10) << "INTINF";
        std::cout << " ";
        std::cout << std::setw(10) << "TIME" << std::endl;

        std::cout << std::setw(10) << "0";
        std::cout << " ";
        std::cout << std::setw(10) << "1";
        std::cout << " ";
        std::cout << std::setw(10) << "0";
        std::cout << " ";
        std::cout << std::setw(10) << getLowerBound();
        std::cout << " ";
        std::cout << std::setw(10) << getUpperBound();
        std::cout << " ";
        if (getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE)
        {
            std::cout << std::setw(10) << getUpperBound() / getLowerBound();
        }
        else
        {
            std::cout << std::setw(10) << getLowerBound() / getUpperBound();
        }
        std::cout << " ";
        std::cout << std::setw(10) << rootNode->getObjective();
        std::cout << " ";
        std::cout << std::setw(10) << rootNode->getIntInfeasibility();
        std::cout << " ";
        std::cout << std::setw(9) << getElapsedTime() << "s" << std::endl;
    }

    double branchAndBoundTree::getElapsedTime() const
    {
        std::chrono::steady_clock::time_point currentTime = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsedTime = currentTime - getStartTime();
        return elapsedTime.count();
    }

    void branchAndBoundTree::solve()
    {
        int dispMes = 0;
        int iter = 0;
        int branchedNodes = 1; // due to the root relaxation
        int maxIterCount = 100000;

        while ((getElapsedTime() < getTimeLimit()) && (leafeNodes.size() > 0) && (iter < maxIterCount) && 1 - ((std::abs(getUpperBound() - getLowerBound()) <= 1e-6) && (solution.size() >= getSolCount())))
        {

            // increase iteration number
            iter += 1;

            // find the node with highest priority (based on branching rule)
            std::shared_ptr<optimizerInterface::branchAndBoundNode> topNode = *(leafeNodes.begin());

            // remove it from the queue // the object is still stored in topNode!
            leafeNodes.erase(leafeNodes.begin());

            // determine which SOS to branch
            int sosToBranch = topNode->getMaxIntInfeasibilityPosition();

            // get the corresponding sos variables
            std::vector<int> varsToBranch;
            optimizerInterface::solverModel::sosTypes sosType = topNode->readSosNumber(sosToBranch, varsToBranch);

            // branches count (depends on sos)
            int numBranches;
            if (sosType == optimizerInterface::solverModel::sosTypes::EXACTONE)
            {
                numBranches = varsToBranch.size();
            }
            else if (sosType == optimizerInterface::solverModel::sosTypes::ATMOSTONE)
            {
                numBranches = varsToBranch.size() + 1;
            }
            else
            {
                std::cerr << "ERROR: Unknwon sos type!" << std::endl;
                exit(2);
            }

            // copy the models for branching
            std::vector<std::shared_ptr<optimizerInterface::branchAndBoundNode>> newNodes(numBranches);

            // #pragma omp parallel for
            for (int i = 0; i < numBranches - 1; i++)
            {
                newNodes[i] = std::make_shared<optimizerInterface::branchAndBoundNode>(*topNode);
            }
            newNodes[numBranches - 1] = topNode;

            // parallel solution
            // #pragma omp parallel for
            for (int i = 0; i < numBranches; i++)
            {
                newNodes[i]->setDepth(newNodes[i]->getDepth() + 1);
                newNodes[i]->branchSos(sosToBranch, i, sosType);
            }

            // increase the count of branched nodes
            branchedNodes += numBranches;

            // decide what to do with the nodes
            for (int i = 0; i < numBranches; i++)
            {
                // we shall process the node only if it is feasible!
                if (newNodes[i]->isFeasible())
                {
                    // compare bounds
                    if ((getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE) && (newNodes[i]->getObjective() <= getUpperBound()))
                    {
                        if (newNodes[i]->isModelIntegerFeasible())
                        {
                            // if the solution means a strict improvement
                            if (newNodes[i]->getObjective() < getUpperBound())
                            {
                                solution.clear();
                            }
                            setUpperBound(newNodes[i]->getObjective());
                            solution.push_back(newNodes[i]);

                            std::cout << "F" << std::setw(9) << branchedNodes;
                            std::cout << " ";
                            std::cout << std::setw(10) << leafeNodes.size();
                            std::cout << " ";
                            std::cout << std::setw(10) << newNodes[i]->getDepth() + 1;
                            std::cout << " ";
                            std::cout << std::setw(10) << getLowerBound();
                            std::cout << " ";
                            std::cout << std::setw(10) << getUpperBound();
                            std::cout << " ";
                            if (getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE)
                            {
                                std::cout << std::setw(10) << getUpperBound() / getLowerBound();
                            }
                            else
                            {
                                std::cout << std::setw(10) << getLowerBound() / getUpperBound();
                            }
                            std::cout << " ";
                            std::cout << std::setw(10) << newNodes[i]->getObjective();
                            std::cout << " ";
                            double intInf = newNodes[i]->getIntInfeasibility();
                            if (intInf < 0)
                            {
                                std::cout << std::setw(10) << std::numeric_limits<double>::infinity();
                            }
                            else
                            {
                                std::cout << std::setw(10) << intInf;
                            }
                            std::cout << " ";
                            std::cout << std::setw(9) << getElapsedTime() << "s" << std::endl;
                        }
                        else
                        {

                            // rounding heuristics
                            /*std::shared_ptr<optimizerInterface::branchAndBoundNode> roundNode = std::make_shared<optimizerInterface::branchAndBoundNode>(*newNodes[i]);
                            roundNode->round();
                            if ((roundNode->getObjective() < getUpperBound()) && (roundNode->isModelIntegerFeasible()))
                            {
                                solution.push_back(roundNode);

                                // if the solution means a strict improvement
                                if (roundNode->getObjective() < getUpperBound())
                                {
                                    solution.clear();
                                }
                                setUpperBound(roundNode->getObjective());
                                solution.push_back(roundNode);

                                std::cout << "F" << std::setw(9) << branchedNodes;
                                std::cout << " ";
                                std::cout << std::setw(10) << leafeNodes.size();
                                std::cout << " ";
                                std::cout << std::setw(10) << roundNode->getDepth()+1;
                                std::cout << " ";
                                std::cout << std::setw(10) << getLowerBound();
                                std::cout << " ";
                                std::cout << std::setw(10) << getUpperBound();
                                std::cout << " ";
                                if (getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE)
                                {
                                    std::cout << std::setw(10) << getUpperBound()/getLowerBound();
                                } else {
                                    std::cout << std::setw(10) << getLowerBound()/getUpperBound();
                                }
                                std::cout << " ";
                                std::cout << std::setw(10) << roundNode->getObjective();
                                std::cout << " ";
                                double intInf = roundNode->getIntInfeasibility();
                                if (intInf < 0)
                                {
                                    std::cout << std::setw(10) << std::numeric_limits<double>::infinity();
                                } else {
                                    std::cout << std::setw(10) << intInf;
                                }
                                std::cout << " ";
                                std::cout << std::setw(9) << getElapsedTime() << "s" << std::endl;
                            } else {*/
                            leafeNodes.insert(newNodes[i]);
                            //}
                        }
                    }

                    if ((getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MAXIMIZE) && (newNodes[i]->getObjective() >= getLowerBound()))
                    {
                        if (newNodes[i]->isModelIntegerFeasible())
                        {
                            // if the solution means a strict improvement
                            if (newNodes[i]->getObjective() > getLowerBound())
                            {
                                solution.clear();
                            }
                            setLowerBound(newNodes[i]->getObjective());
                            solution.push_back(newNodes[i]);

                            std::cout << "F" << std::setw(9) << branchedNodes;
                            std::cout << " ";
                            std::cout << std::setw(10) << leafeNodes.size();
                            std::cout << " ";
                            std::cout << std::setw(10) << newNodes[i]->getDepth() + 1;
                            std::cout << " ";
                            std::cout << std::setw(10) << getLowerBound();
                            std::cout << " ";
                            std::cout << std::setw(10) << getUpperBound();
                            std::cout << " ";
                            if (getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE)
                            {
                                std::cout << std::setw(10) << getUpperBound() / getLowerBound();
                            }
                            else
                            {
                                std::cout << std::setw(10) << getLowerBound() / getUpperBound();
                            }
                            std::cout << " ";
                            std::cout << std::setw(10) << newNodes[i]->getObjective();
                            std::cout << " ";
                            double intInf = newNodes[i]->getIntInfeasibility();
                            if (intInf < 0)
                            {
                                std::cout << std::setw(10) << std::numeric_limits<double>::infinity();
                            }
                            else
                            {
                                std::cout << std::setw(10) << intInf;
                            }
                            std::cout << " ";
                            std::cout << std::setw(9) << getElapsedTime() << "s" << std::endl;
                        }
                        else
                        {

                            // rounding heuristics
                            /*
                            std::shared_ptr<optimizerInterface::branchAndBoundNode> roundNode = std::make_shared<optimizerInterface::branchAndBoundNode>(*newNodes[i]);
                            roundNode->round();
                            if ((roundNode->getObjective() > getLowerBound()) && (roundNode->isModelIntegerFeasible()))
                            {
                                solution.push_back(roundNode);

                                // if the solution means a strict improvement
                                if (roundNode->getObjective() < getUpperBound())
                                {
                                    solution.clear();
                                }
                                setUpperBound(roundNode->getObjective());
                                solution.push_back(roundNode);

                                std::cout << "F" << std::setw(9) << branchedNodes;
                                std::cout << " ";
                                std::cout << std::setw(10) << leafeNodes.size();
                                std::cout << " ";
                                std::cout << std::setw(10) << roundNode->getDepth()+1;
                                std::cout << " ";
                                std::cout << std::setw(10) << getLowerBound();
                                std::cout << " ";
                                std::cout << std::setw(10) << getUpperBound();
                                std::cout << " ";
                                if (getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE)
                                {
                                    std::cout << std::setw(10) << getUpperBound()/getLowerBound();
                                } else {
                                    std::cout << std::setw(10) << getLowerBound()/getUpperBound();
                                }
                                std::cout << " ";
                                std::cout << std::setw(10) << roundNode->getObjective();
                                std::cout << " ";
                                double intInf = roundNode->getIntInfeasibility();
                                if (intInf < 0)
                                {
                                    std::cout << std::setw(10) << std::numeric_limits<double>::infinity();
                                } else {
                                    std::cout << std::setw(10) << intInf;
                                }
                                std::cout << " ";
                                std::cout << std::setw(9) << getElapsedTime() << "s" << std::endl;
                            } else {*/
                            leafeNodes.insert(newNodes[i]);
                            // }
                        }
                    }
                }
            }

            // display statistics
            div_t t = div((int)getElapsedTime(), getDisplayFrequency());
            if ((t.quot >= dispMes) && (leafeNodes.size() > 0))
            {
                std::shared_ptr<optimizerInterface::branchAndBoundNode> newNode = *(leafeNodes.begin());
                std::cout << std::setw(10) << branchedNodes;
                std::cout << " ";
                std::cout << std::setw(10) << leafeNodes.size();
                std::cout << " ";
                std::cout << std::setw(10) << newNode->getDepth() + 1;
                std::cout << " ";
                std::cout << std::setw(10) << getLowerBound();
                std::cout << " ";
                std::cout << std::setw(10) << getUpperBound();
                std::cout << " ";
                if (getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE)
                {
                    std::cout << std::setw(10) << getUpperBound() / getLowerBound();
                }
                else
                {
                    std::cout << std::setw(10) << getLowerBound() / getUpperBound();
                }
                std::cout << " ";
                std::cout << std::setw(10) << newNode->getObjective();
                std::cout << " ";
                double intInf = newNode->getIntInfeasibility();
                if (intInf < 0)
                {
                    std::cout << std::setw(10) << std::numeric_limits<double>::infinity();
                }
                else
                {
                    std::cout << std::setw(10) << intInf;
                }
                std::cout << " ";
                std::cout << std::setw(9) << getElapsedTime() << "s" << std::endl;
                dispMes += 1;
            }

            // remove all worse nodes from the queue
            if (getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE)
            {
                for (std::multiset<std::shared_ptr<optimizerInterface::branchAndBoundNode>>::const_iterator it = leafeNodes.begin(); it != leafeNodes.end();)
                {
                    if ((*it)->getObjective() < getLowerBound())
                    {
                        it = leafeNodes.erase(it);
                    }
                    else
                    {
                        ++it;
                    }
                }
            }

            if (getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MAXIMIZE)
            {
                for (std::multiset<std::shared_ptr<optimizerInterface::branchAndBoundNode>>::const_iterator it = leafeNodes.begin(); it != leafeNodes.end();)
                {
                    if ((*it)->getObjective() > getUpperBound())
                    {
                        it = leafeNodes.erase(it);
                    }
                    else
                    {
                        ++it;
                    }
                }
            }
        }

        if ((solution.size() == 0) && (leafeNodes.size() == 0))
        {
            std::cerr << "ERROR: No solution exists!" << std::endl;
            exit(2);
        }
        else if (solution.size() == 0)
        {
            std::cerr << "ERROR: Branch and bound failed to find solution in prescribed time and iterations count." << std::endl;
            exit(2);
        }
        else
        {
            std::cout << "Branch and bound found " << solution.size() << " feasible solution(s). Branched " << branchedNodes << " nodes in " << getElapsedTime() << "s. " << std::endl
                      << std::endl;
        }
    }
}