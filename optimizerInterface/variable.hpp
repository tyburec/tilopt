#pragma once

#include <vector>

namespace optimizerInterface
{
    class variable
    {
    private:
        std::vector<double> branchedValues;
        // double constraningSetFeasibility;

    public:
        enum variableTypes
        {
            BINARY,
            CONTINUOUS,
            SEMIDEFINITE,
            POWEREQUALONE
        };
        void constrainToDoubleSet(std::vector<double> set) { branchedValues = set; };
        void getConstrainingSet(std::vector<double> &set) { set = branchedValues; };
        bool hasConstrainingSet() { return branchedValues.size(); };
        virtual double getValue() = 0;
        double getConstrainingSetFeasibility(std::vector<double> &set);
        virtual void getVarInMatrix(int pos1, int pos2, std::shared_ptr<optimizerInterface::variable> &newVar) = 0;
    };

    double variable::getConstrainingSetFeasibility(std::vector<double> &set)
    {
        if (set.size() != 2)
        {
            std::cerr << "ERROR: We currently support only constraining sets of size 2." << std::endl;
            exit(2);
        }

        double inBinRange = ((getValue() - set[0]) * set[1]) / (set[1] - set[0]);
        double val = inBinRange * (1 - inBinRange);

        if (std::abs(val) <= 1e-6)
        {
            val = 0;
        }
        if (std::abs(val - 1.0) <= 1e-6)
        {
            val = 1.0;
        }

        if ((val < 0.0) || (val > 1.0))
        {
            std::cerr << "ERROR: Variable out of bounds!" << std::endl;
            exit(2);
        }
        return val;
    }
}