#pragma once

#include <memory>
#include <vector>
#include "variable.hpp"

namespace optimizerInterface
{
    class quadExpression
    {
    private:
        std::vector<double> multipliers;
        std::vector<std::shared_ptr<optimizerInterface::variable>> variablesA;
        std::vector<std::shared_ptr<optimizerInterface::variable>> variablesB;

    public:
        quadExpression(){};
        void add(std::shared_ptr<optimizerInterface::variable> varA, double multiplier, std::shared_ptr<optimizerInterface::variable> varB);
        void add(std::shared_ptr<optimizerInterface::variable> varA, std::shared_ptr<optimizerInterface::variable> varB);
        void add(std::shared_ptr<optimizerInterface::variable> varA, double multiplier);
        void add(std::shared_ptr<optimizerInterface::variable> varA);

        void getMultipliers(std::vector<double> &m) { m = multipliers; };
        void getVariablesA(std::vector<std::shared_ptr<optimizerInterface::variable>> &v) { v = variablesA; };
        void getVariablesB(std::vector<std::shared_ptr<optimizerInterface::variable>> &v) { v = variablesB; };
    };

    void quadExpression::add(std::shared_ptr<optimizerInterface::variable> varA, double multiplier, std::shared_ptr<optimizerInterface::variable> varB)
    {
        variablesA.push_back(varA);
        variablesB.push_back(varB);
        multipliers.push_back(multiplier);
    }

    void quadExpression::add(std::shared_ptr<optimizerInterface::variable> varA, std::shared_ptr<optimizerInterface::variable> varB)
    {
        variablesA.push_back(varA);
        variablesB.push_back(varB);
        multipliers.push_back(1.0);
    }

    void quadExpression::add(std::shared_ptr<optimizerInterface::variable> varA, double multiplier)
    {
        variablesA.push_back(varA);
        variablesB.push_back(NULL);
        multipliers.push_back(multiplier);
    }

    void quadExpression::add(std::shared_ptr<optimizerInterface::variable> varA)
    {
        variablesA.push_back(varA);
        variablesB.push_back(NULL);
        multipliers.push_back(1.0);
    }
}