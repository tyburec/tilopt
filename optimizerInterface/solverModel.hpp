#pragma once

#include "variable.hpp"
#include "linExpression.hpp"
#include "quadExpression.hpp"
#include "optimizerEnvironment.hpp"

namespace optimizerInterface
{
    class solverModel
    {
    public:
        enum solvers
        {
            GUROBI,
            MOSEK,
            NONE
        };
        enum objectiveSenses
        {
            MINIMIZE,
            MAXIMIZE
        };
        enum solversStatuses
        {
            OPTIMAL,
            INFEASIBLE
        };
        enum sosTypes
        {
            EXACTONE,
            ATMOSTONE
        };

    protected:
        int numConstraints;
        int numVariables;
        double objective;
        objectiveSenses objectiveSense;
        solversStatuses solverStatus;
        std::shared_ptr<std::vector<std::vector<int>>> sos;
        std::shared_ptr<std::vector<sosTypes>> sosType;
        optimizerInterface::solverModel::solvers solver;
        std::vector<double> branchedValues;
        double constraningSetFeasibility;

    public:
        enum constraintSense
        {
            EQUAL,
            LESSEQUAL,
            GREATEREQUAL
        };
        virtual void addLinearConstraint(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs) = 0;
        virtual void addQuadraticConstraint(optimizerInterface::quadExpression &expr, optimizerInterface::solverModel::constraintSense sense, double rhs) = 0;
        virtual void addSemidefiniteConstraint() = 0;
        int getNumberOfConstraints() const { return numConstraints; };
        int getNumberOfVariables() const { return numVariables; };
        virtual void constrainTrace(double num, std::shared_ptr<optimizerInterface::variable> &var) = 0;

        virtual void addLinearObjective(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::objectiveSenses sense) = 0;
        virtual void addLinearObjective(optimizerInterface::linExpression &expr, optimizerInterface::solverModel::objectiveSenses sense, double constant) = 0;
        // virtual int addQuadraticObjective() = 0;
        // virtual int addSemidefiniteObjective() = 0;
        double getObjective() const { return objective; };
        void setObjective(double o) { objective = o; };
        solversStatuses getSolverStatus() const { return solverStatus; };
        void setSolverStatus(solversStatuses s) { solverStatus = s; };
        objectiveSenses getObjectiveSense() const { return objectiveSense; };
        void setObjectiveSense(objectiveSenses o) { objectiveSense = o; };

        void addSos(std::vector<int> sosRow, sosTypes st);
        void getSos(std::shared_ptr<std::vector<std::vector<int>>> &gSos) const { gSos = sos; };
        void getSosTypes(std::shared_ptr<std::vector<sosTypes>> &st) { st = sosType; };

        virtual void addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var) = 0;
        virtual void addContinuousVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var, double lb, double ub) = 0;
        virtual void addBinaryVariable(int varNumber, std::shared_ptr<optimizerInterface::variable> &var) = 0;
        virtual void addSemidefiniteVariable(std::shared_ptr<optimizerInterface::variable> &var, int size) = 0;

        virtual void getVarByArrayPosition(std::shared_ptr<optimizerInterface::variable> &var, int varNumber) = 0;

        virtual void optimize() = 0;

        optimizerInterface::solverModel::solvers getSolver() const { return solver; };
        void setSolver(optimizerInterface::solverModel::solvers s) { solver = s; };

        void constrainToDoubleSet(std::vector<double> set) { branchedValues = set; };
        void getConstrainingSet(std::vector<double> &set) { set = branchedValues; };
        bool hasConstrainingSet() { return branchedValues.size(); };

        virtual ~solverModel(){};
    };

    void solverModel::addSos(std::vector<int> sosRow, sosTypes st)
    {
        if (!sos)
        {
            sos = std::make_shared<std::vector<std::vector<int>>>();
            sosType = std::make_shared<std::vector<sosTypes>>();
        }
        sos->push_back(sosRow);
        sosType->push_back(st);
    }
}