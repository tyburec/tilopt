#pragma once
#include <vector>
#include <limits>
#include "linExpression.hpp"

namespace optimizerInterface
{
    class branchAndBoundNode
    {
    public:
        enum branchingRules
        {
            MIN_INFEASIBILITY,
            MAX_INFEASIBILITY,
            MIN_OBJECTIVE,
            MAX_OBJECTIVE,
            MAX_DEPTH
        };

    private:
        branchingRules branchingRule;
        double objective;
        int depth;
        double intInfeasibility;
        double maxIntInfeasibility;
        int maxIntInfeasibilityPosition;
        bool feasible;
        bool integerFeasible;
        std::shared_ptr<optimizerInterface::solverModel> model;
        std::vector<int> branchedVars;

    public:
        bool isFeasible() { return feasible; };
        void setFeasibility(bool f) { feasible = f; };
        bool operator()(const std::shared_ptr<branchAndBoundNode> node1, const std::shared_ptr<branchAndBoundNode> node2) const;

        double getObjective() const { return objective; };
        optimizerInterface::solverModel::objectiveSenses getObjectiveSense() { return model->getObjectiveSense(); };
        void setObjective(double val) { objective = val; };

        int getDepth() const { return depth; };
        void setDepth(int d) { depth = d; };

        double getIntInfeasibility() const { return intInfeasibility; };
        void setIntInfeasibility(double val) { intInfeasibility = val; };

        bool isModelIntegerFeasible() { return integerFeasible; };
        void setModelIntegerFeasibility(bool b) { integerFeasible = b; };

        double getMaxIntInfeasibility() const { return maxIntInfeasibility; };
        void setMaxIntInfeasibility(double val) { maxIntInfeasibility = val; };
        int getMaxIntInfeasibilityPosition() const { return maxIntInfeasibilityPosition; };
        void setMaxIntInfeasibilityPosition(int val) { maxIntInfeasibilityPosition = val; };

        branchingRules getBranchingRule() const { return branchingRule; };
        void setBranchingRule(optimizerInterface::branchAndBoundNode::branchingRules rule) { branchingRule = rule; };

        void branchSos(int sosToBranch, int varNum, optimizerInterface::solverModel::sosTypes st);

        void solve();

        optimizerInterface::solverModel::sosTypes readSosNumber(int num, std::vector<int> &sos);

        branchAndBoundNode(std::shared_ptr<optimizerInterface::solverModel> &m, optimizerInterface::branchAndBoundNode::branchingRules rule);
        branchAndBoundNode(){};
        branchAndBoundNode(const branchAndBoundNode &obj);

        void retrieveOptimizationModel(std::shared_ptr<optimizerInterface::solverModel> &smod);
        void round();
    };

    void branchAndBoundNode::retrieveOptimizationModel(std::shared_ptr<optimizerInterface::solverModel> &smod)
    {
        smod = model;

        switch (model->getSolver())
        {
#ifdef DGUROBI
        case optimizerInterface::solverModel::solvers::GUROBI:
        {
            smod = std::dynamic_pointer_cast<optimizerInterface::gurobiModel>(smod);
            break;
        }
#endif

#ifdef DMOSEK
        case optimizerInterface::solverModel::solvers::MOSEK:
        {
            smod = std::dynamic_pointer_cast<optimizerInterface::mosekModel>(smod);
            break;
        }
#endif

        default:
        {
            std::cerr << "ERROR: Unknown solver in branchAndBoundNode." << std::endl;
            exit(2);
            break;
        }
        }
    }

    branchAndBoundNode::branchAndBoundNode(const branchAndBoundNode &obj)
    {
        setBranchingRule(obj.getBranchingRule());
        setDepth(obj.getDepth());

        switch (obj.model->getSolver())
        {
#ifdef DGUROBI
        case optimizerInterface::solverModel::solvers::GUROBI:
        {
            std::shared_ptr<optimizerInterface::gurobiModel> gModel = std::dynamic_pointer_cast<optimizerInterface::gurobiModel>(obj.model);
            model = std::make_shared<optimizerInterface::gurobiModel>(*gModel);
            model = std::dynamic_pointer_cast<optimizerInterface::gurobiModel>(model);
            break;
        }
#endif

#ifdef DMOSEK
        case optimizerInterface::solverModel::solvers::MOSEK:
        {
            std::shared_ptr<optimizerInterface::mosekModel> gModel = std::dynamic_pointer_cast<optimizerInterface::mosekModel>(obj.model);
            model = std::make_shared<optimizerInterface::mosekModel>(*gModel);
            model = std::dynamic_pointer_cast<optimizerInterface::mosekModel>(model);
            break;
        }
#endif

        default:
        {
            std::cerr << "ERROR: Unknown solver in branchAndBoundNode." << std::endl;
            exit(2);
            break;
        }
        }
    }

    branchAndBoundNode::branchAndBoundNode(std::shared_ptr<optimizerInterface::solverModel> &m, optimizerInterface::branchAndBoundNode::branchingRules rule)
    {
        setBranchingRule(rule);
        setDepth(0);

        model = m;
        switch (model->getSolver())
        {
#ifdef DGUROBI
        case optimizerInterface::solverModel::solvers::GUROBI:
        {
            model = std::dynamic_pointer_cast<optimizerInterface::gurobiModel>(model);
            break;
        }
#endif

#ifdef DMOSEK
        case optimizerInterface::solverModel::solvers::MOSEK:
        {
            model = std::dynamic_pointer_cast<optimizerInterface::mosekModel>(model);
            break;
        }
#endif

        default:
        {
            std::cerr << "ERROR: Unknown solver in branchAndBoundNode." << std::endl;
            exit(2);
            break;
        }
        }
    }

    optimizerInterface::solverModel::sosTypes branchAndBoundNode::readSosNumber(int num, std::vector<int> &sos)
    {
        std::shared_ptr<std::vector<std::vector<int>>> sosGet;
        std::shared_ptr<std::vector<optimizerInterface::solverModel::sosTypes>> sosT;
        model->getSos(sosGet);
        model->getSosTypes(sosT);

        if ((num >= 0) && ((unsigned int)num < sosGet->size()) && ((unsigned int)num < sosT->size()))
        {
            sos = (*sosGet)[num];
        }
        else
        {
            std::cerr << "ERROR: sos " << num << " out of range " << sosGet->size() << "!" << std::endl;
            exit(2);
        }
        return (*sosT)[num];
    }

    void branchAndBoundNode::branchSos(int sosToBranch, int varNum, optimizerInterface::solverModel::sosTypes st)
    {
        std::shared_ptr<std::vector<std::vector<int>>> sos;
        model->getSos(sos);

        std::vector<double> cSet;
        model->getConstrainingSet(cSet);

        // add constraints
        optimizerInterface::linExpression lin0, lin1;
        int count0 = 0;
        int count1 = 0;

        for (unsigned int i = 0; i < (*sos)[sosToBranch].size(); i++)
        {
            std::shared_ptr<optimizerInterface::variable> var;

            model->getVarByArrayPosition(var, (*sos)[sosToBranch][i]);

            if (i == (unsigned int)varNum)
            {
                lin1.add(var);
                count1 += 1;
            }
            else
            {
                lin0.add(var);
                count0 += 1;
            }
        }

        if (count0 > 0)
        {
            model->addLinearConstraint(lin0, optimizerInterface::solverModel::constraintSense::EQUAL, cSet[0] * count0);
        }
        if (count1 > 0)
        {
            model->addLinearConstraint(lin1, optimizerInterface::solverModel::constraintSense::EQUAL, cSet[1]);
        }

        solve();
    }

    void branchAndBoundNode::round()
    {
        std::shared_ptr<std::vector<std::vector<int>>> sos;
        model->getSos(sos);

        std::vector<double> cSet;
        model->getConstrainingSet(cSet);

        for (unsigned int i = 0; i < (*sos).size(); i++)
        {
            double maxVal = cSet[0];
            unsigned int maxPos = -1;

            for (unsigned int j = 0; j < (*sos)[i].size(); j++)
            {
                // get variable
                std::shared_ptr<optimizerInterface::variable> var;
                model->getVarByArrayPosition(var, (*sos)[i][j]);

                if (var->getValue() > maxVal)
                {
                    maxVal = var->getValue();
                    maxPos = j;
                }
            }

            for (unsigned int j = 0; j < (*sos)[i].size(); j++)
            {
                std::shared_ptr<optimizerInterface::variable> var;
                model->getVarByArrayPosition(var, (*sos)[i][j]);

                optimizerInterface::linExpression linExp;
                linExp.add(var);

                if (j == maxPos)
                {
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, cSet[1]);
                }
                else
                {
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, cSet[0]);
                }
            }
        }

        solve();
    }

    void branchAndBoundNode::solve()
    {
        model->optimize();

        if (model->getSolverStatus() == optimizerInterface::solverModel::solversStatuses::INFEASIBLE)
        {
            setFeasibility(false);
            setModelIntegerFeasibility(false);
            setMaxIntInfeasibility(-1);
            setMaxIntInfeasibilityPosition(-1);
            setIntInfeasibility(-1.0);

            if (model->getObjectiveSense() == optimizerInterface::solverModel::objectiveSenses::MINIMIZE)
            {
                setObjective(std::numeric_limits<double>::infinity());
            }
            else
            {
                setObjective(-1 * std::numeric_limits<double>::infinity());
            }
        }
        else
        {

            setFeasibility(true);
            setObjective(model->getObjective());

            int maxIntInfPos = -1;
            double maxIntInf = 0;
            double sumIntInf = 0;

            std::shared_ptr<std::vector<std::vector<int>>> sos;
            model->getSos(sos);

            // get constraining set, e.g. [0 1] or [-1 1]
            std::vector<double> cSet;
            model->getConstrainingSet(cSet);

            for (unsigned int i = 0; i < (*sos).size(); i++)
            {
                double sosIntInf = 0;

                for (unsigned int j = 0; j < (*sos)[i].size(); j++)
                {
                    // get variable
                    std::shared_ptr<optimizerInterface::variable> var;
                    model->getVarByArrayPosition(var, (*sos)[i][j]);

                    // compute variable integer infeasibility
                    double varIntInf = var->getConstrainingSetFeasibility(cSet);

                    if (varIntInf < 0.0)
                    {
                        std::cerr << "ERROR: Integer infeasibility " << varIntInf << " can not be negative!" << std::endl;
                        exit(2);
                    }

                    // add infeasibility to the problem infeasibility
                    sumIntInf += varIntInf;
                    sosIntInf += varIntInf;
                }

                if ((std::abs(sosIntInf) > 1e-6) && (maxIntInf < sosIntInf))
                {
                    maxIntInf = sosIntInf;
                    maxIntInfPos = i;
                }
            }

            setIntInfeasibility(sumIntInf);
            setMaxIntInfeasibility(maxIntInf);
            setMaxIntInfeasibilityPosition(maxIntInfPos);

            if (std::abs(sumIntInf) < 1e-5)
            {
                setModelIntegerFeasibility(true);
            }
            else
            {
                setModelIntegerFeasibility(false);
            }

            if (sumIntInf < 0)
            {
                std::cerr << "ERROR: Integer infeasibility can not be negative!" << std::endl;
            }
        }
    }

    bool branchAndBoundNode::operator()(const std::shared_ptr<branchAndBoundNode> node1, const std::shared_ptr<branchAndBoundNode> node2) const
    {

        if (node1->getBranchingRule() != node2->getBranchingRule())
        {
            std::cerr << "ERROR: Inconsistent branching rules!" << std::endl;
            exit(2);
        }

        switch (node1->getBranchingRule())
        {
        case branchAndBoundNode::branchingRules::MIN_INFEASIBILITY:
        {
            return node1->getIntInfeasibility() < node2->getIntInfeasibility();
            break;
        }

        case branchAndBoundNode::branchingRules::MAX_INFEASIBILITY:
        {
            return node1->getIntInfeasibility() > node2->getIntInfeasibility();
            break;
        }

        case branchAndBoundNode::branchingRules::MIN_OBJECTIVE:
        {
            return node1->getObjective() < node2->getObjective();
            break;
        }

        case branchAndBoundNode::branchingRules::MAX_OBJECTIVE:
        {
            return node1->getObjective() > node2->getObjective();
            break;
        }

        case branchAndBoundNode::branchingRules::MAX_DEPTH:
        {
            return node1->getDepth() > node2->getDepth();
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown branching rule " << node1->getBranchingRule() << " and " << node2->getBranchingRule() << "." << std::endl;
            exit(2);
            break;
        }
        }
    }
}