#pragma once

#include <string>
#include <fstream>
#include <algorithm>
#include "tile.hpp"
#include "tiling.hpp"
#include "edgetile.hpp"
#include "vertextile.hpp"
#include "graphInterface/graph.hpp"
#include "graphInterface/vertex.hpp"
#include <string>
#include <vector>
#include <sstream>
#include <memory>
#include <set>

class tileset
{
public:
    enum conversionMethod
    {
        DIAGONAL_TRANSLATION,
        HORIZONTAL_TRANSLATION,
        VERTICAL_TRANSLATION,
        ROTATION,
        SUBDIVISION,
        VERTEX_LABELING
    };
    enum setTypes
    {
        EDGE_BASED,
        VERTEX_BASED
    };
    enum edgeTypes
    {
        NORTH,
        WEST,
        SOUTH,
        EAST,
        NORTHWEST,
        SOUTHWEST,
        NORTHEAST,
        SOUTHEAST
    };

private:
    int numTiles;
    int numEdgeColors;
    int numVertexColors;
    std::vector<std::shared_ptr<tile>> tiles;
    std::vector<std::vector<tile::tcode>> north, south, east, west, northeast, northwest, southeast, southwest;
    std::vector<bool> redundantTiles;
    setTypes setType;

public:
    int readTsetFile(std::string fname);
    void generateRandomTileSet(setTypes type, int nTiles, int nColors);
    void setConnectivityConstraints();
    void findStronglyConnectedComponents();
    void printMinDistancesMatrix() const;
    void getTransducerGraph(graphInterface::graph &g);
    void getDualTransducerGraph(graphInterface::graph &g);
    void getMinDistancesMatrixLR(std::vector<std::vector<double>> &dMat) const;
    void getMinDistancesMatrixUD(std::vector<std::vector<double>> &dMat) const;
    void findTilingGroups();
    int getNumVertexColors() const { return numVertexColors; };
    int getNumEdgeColors() const { return numEdgeColors; };
    int getNumTiles() const { return numTiles; };
    void setNumTiles(int nt);
    const std::vector<std::shared_ptr<tile>> &getTiles() const { return tiles; };
    const std::vector<std::vector<tile::tcode>> &getEast() const { return east; };
    const std::vector<std::vector<tile::tcode>> &getWest() const { return west; };
    const std::vector<std::vector<tile::tcode>> &getNorth() const { return north; };
    const std::vector<std::vector<tile::tcode>> &getSouth() const { return south; };
    const std::vector<std::vector<tile::tcode>> &getSouthEast() const { return southeast; };
    const std::vector<std::vector<tile::tcode>> &getSouthWest() const { return southwest; };
    const std::vector<std::vector<tile::tcode>> &getNorthEast() const { return northeast; };
    const std::vector<std::vector<tile::tcode>> &getNorthWest() const { return northwest; };
    bool isRedundantTile(int tile) const;
    void setRedundantTile(int tile);
    void resetRedundantTiles();
    void removeRedundantTiles();
    std::string getSVGTileDefinition(int id) const { return tiles[id]->getSvgData(id); }
    void plotTileSet(int inRow) const;
    void plotTileSetSVG(int inRow, std::string fname) const;
    void plotTiles(std::vector<tile::tcode> tileNums) const;
    void setPlotMode(tile::svgPlotMode mode);
    setTypes getSetType() const { return setType; };
    void edgeToVertex(conversionMethod method, tileset &tset) const;
    int vertexToEdge(tileset &tset) const;
    void groupSet(tileset &tset, std::vector<std::vector<std::vector<tile::tcode>>> &assembly) const;
    void writeTsetFile(std::string fname) const;
    void setTileSet(std::set<std::vector<tile::tcode>> &setOfCodes, setTypes st);
    tile::tcode getEdgeColor(int tileNum, edgeTypes eT);

    std::string base64_encode(unsigned const char *input, unsigned const int len);

    tileset();
    tileset(const tileset &obj);
    tileset(std::string fname);
    ~tileset(){};
};

std::string tileset::base64_encode(unsigned const char *input, unsigned const int len)
{
    std::string ret;
    size_t i = 0;
    unsigned char bytes[3];
    unsigned char sextets[4];

    std::string base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    while (i + 3 <= len)
    {
        bytes[0] = *(input++);
        bytes[1] = *(input++);
        bytes[2] = *(input++);

        sextets[0] = (bytes[0] & 0xfc) >> 2;                              // Cuts last two bits off of first byte
        sextets[1] = ((bytes[0] & 0x03) << 4) + ((bytes[1] & 0xf0) >> 4); // Takes last two bits from first byte and adds it to first 4 bits of 2nd byte
        sextets[2] = ((bytes[1] & 0x0f) << 2) + ((bytes[2] & 0xc0) >> 6); // Takes last 4 bits of 2nd byte and adds it to first 2 bits of third byte
        sextets[3] = bytes[2] & 0x3f;                                     // takes last 6 bits of third byte

        for (size_t j = 0; j < 4; ++j)
        {
            ret += base64_chars[sextets[j]];
        }

        i += 3; // increases to go to third byte
    }

    if (i != len)
    {
        size_t k = 0;
        size_t j = len - i; // Find index of last byte
        while (k < j)
        { // Sets first bytes
            bytes[k] = *(input++);
            ++k;
        }

        while (j < 3)
        { // Set last bytes to 0x00
            bytes[j] = '\0';
            ++j;
        }

        sextets[0] = (bytes[0] & 0xfc) >> 2;                              // Cuts last two bits off of first byte
        sextets[1] = ((bytes[0] & 0x03) << 4) + ((bytes[1] & 0xf0) >> 4); // Takes last two bits from first byte and adds it to first 4 bits of 2nd byte
        sextets[2] = ((bytes[1] & 0x0f) << 2) + ((bytes[2] & 0xc0) >> 6); // Takes last 4 bits of 2nd byte and adds it to first 2 bits of third byte
        // No last one is needed, because if there were 4, then (i == len) == true

        for (j = 0; j < (len - i) + 1; ++j)
        {                                    // Gets sextets that include data
            ret += base64_chars[sextets[j]]; // Appends them to string
        }

        while ((j++) < 4) // Appends remaining ='s
            ret += '=';
    }

    return ret;
}

void tileset::setPlotMode(tile::svgPlotMode mode)
{
    for (int i = 0; i < getNumTiles(); i++)
    {
        tiles[i]->setPrintMode(mode);
    }
}

tileset::tileset(std::string fname)
{
    readTsetFile(fname);
}

tile::tcode tileset::getEdgeColor(int tileNum, edgeTypes eT)
{
    if ((tileNum < 0) | (tileNum >= getNumTiles()))
    {
        std::cerr << "ERROR: Invalid range! " << tileNum << " specified." << std::endl;
        exit(2);
    }

    tile::tcode number = -1;

    switch (eT)
    {
    case NORTH:
    {
        number = tiles[tileNum]->getNorth();
        break;
    }
    case SOUTH:
    {
        number = tiles[tileNum]->getSouth();
        break;
    }
    case EAST:
    {
        number = tiles[tileNum]->getEast();
        break;
    }
    case WEST:
    {
        number = tiles[tileNum]->getWest();
        break;
    }
    case NORTHEAST:
    {
        number = tiles[tileNum]->getNorthEast();
        break;
    }
    case NORTHWEST:
    {
        number = tiles[tileNum]->getNorthWest();
        break;
    }
    case SOUTHEAST:
    {
        number = tiles[tileNum]->getSouthEast();
        break;
    }
    case SOUTHWEST:
    {
        number = tiles[tileNum]->getSouthWest();
        break;
    }
    default:
    {
        std::cerr << "ERROR in tileset::getEdgeColor(): unknown edge type." << std::endl;
        exit(2);
    }
    }

    return number;
}

void tileset::generateRandomTileSet(setTypes type, int nTiles, int nColors)
{
    numTiles = nTiles;
    setType = type;
    std::mt19937_64 rng;
    uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::seed_seq ss{uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed >> 32)};
    rng.seed(ss);
    std::uniform_int_distribution<int> unii(0, nColors - 1);

    switch (type)
    {
    case EDGE_BASED:
    {
        numEdgeColors = nColors;
        numVertexColors = -1;

        for (int i = 0; i < nTiles; ++i)
        {
            tiles.emplace_back(std::shared_ptr<tile>(new edgetile(unii(rng), unii(rng), unii(rng), unii(rng))));
        }
        break;
    }

    case VERTEX_BASED:
    {
        numEdgeColors = nColors ^ 4;
        numVertexColors = nColors;
        for (int i = 0; i < nTiles; ++i)
        {
            tiles.emplace_back(std::shared_ptr<tile>(new vertextile(unii(rng), unii(rng), unii(rng), unii(rng))));
        }
        break;
    }

    default:
    {
        std::cerr << "ERROR in tileset::generateRandomTileSet(): unknown set type" << std::endl;
        exit(2);
        break;
    }
    }

    // tileset::setTilesetConnectivity();

    tileset::setConnectivityConstraints();
}

tileset::tileset(const tileset &obj)
{
    numTiles = obj.numTiles;
    numEdgeColors = obj.numEdgeColors;
    numVertexColors = obj.numVertexColors;
    north = obj.north;
    south = obj.south;
    east = obj.east;
    west = obj.west;
    northwest = obj.northwest;
    southwest = obj.southwest;
    northeast = obj.northeast;
    southeast = obj.southeast;
    redundantTiles = obj.redundantTiles;
    setType = obj.setType;

    for (int i = 0; i < obj.numTiles; i++)
    {
        tiles.emplace_back(obj.tiles[i]->clone());
    }
}

void tileset::setNumTiles(int nt)
{
    numTiles = nt;
    redundantTiles.resize(numTiles);
    for (int i = 0; i < numTiles; i++)
    {
        redundantTiles[i] = false;
    }
}

void tileset::setRedundantTile(int tile)
{
    if (redundantTiles.size() < (unsigned int)getNumTiles())
    {
        redundantTiles.resize(getNumTiles());
        for (int i = 0; i < getNumTiles(); i++)
        {
            redundantTiles[i] = false;
        }
    }
    redundantTiles[tile] = true;
}

void tileset::resetRedundantTiles()
{
    for (unsigned int i = 0; i < redundantTiles.size(); i++)
        redundantTiles[i] = false;
}

void tileset::findTilingGroups()
{
    // horizontal directed graph: left->right
    graphInterface::graph Graph(getNumTiles());
    std::vector<std::vector<int>> List;
    for (int i = 0; i < getNumTiles(); i++)
    {
        for (int j = 0; j < getNumTiles(); j++)
        {
            if (tiles[i]->getEast() == tiles[j]->getWest())
            {
                Graph.addEdge(i, j);
            }
        }
    }

    // vertical directed graph: up->down
    for (int i = 0; i < getNumTiles(); i++)
    {
        for (int j = 0; j < getNumTiles(); j++)
        {
            if (tiles[i]->getSouth() == tiles[j]->getNorth())
            {
                Graph.addEdge(i, j);
            }
        }
    }

    Graph.stronglyConnectedComponents(List);
    std::cout << "Number of tiling groups: " << List.size() << std::endl;
}

void tileset::getTransducerGraph(graphInterface::graph &g)
{
    g = graphInterface::graph(getNumEdgeColors());
    for (int i = 0; i < getNumTiles(); i++)
    {
        auto e = g.addEdge(tiles[i]->getWest(), tiles[i]->getEast(), std::to_string(tiles[i]->getSouth()) + "|" + std::to_string(tiles[i]->getNorth()));
        e->setFlowUpperBound(i);
    }

    std::vector<graphInterface::vertex_t> vertices;
    g.getVertices(vertices);
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
        vertices[i]->setColor(i);
    }
}

void tileset::getDualTransducerGraph(graphInterface::graph &g)
{
    g = graphInterface::graph(getNumEdgeColors());
    for (int i = 0; i < getNumTiles(); i++)
    {
        auto e = g.addEdge(tiles[i]->getNorth(), tiles[i]->getSouth(), std::to_string(tiles[i]->getEast()) + "|" + std::to_string(tiles[i]->getWest()));
        e->setFlowUpperBound(i);
    }

    std::vector<graphInterface::vertex_t> vertices;
    g.getVertices(vertices);
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
        vertices[i]->setColor(i);
    }
}

void tileset::getMinDistancesMatrixLR(std::vector<std::vector<double>> &dMat) const
{
    // horizontal directed graph: left->right
    graphInterface::graph hGraph(getNumTiles());
    for (int i = 0; i < getNumTiles(); i++)
    {
        for (int j = 0; j < getNumTiles(); j++)
        {
            if (tiles[i]->getEast() == tiles[j]->getWest())
            {
                hGraph.addEdge(i, j);
            }
        }
    }
    hGraph.allPairsShortestPaths(dMat, graphInterface::graph::allPairsShortestPathsAlgorithms::BreadthFirstSearch);
}

void tileset::getMinDistancesMatrixUD(std::vector<std::vector<double>> &dMat) const
{
    // horizontal directed graph: left->right
    graphInterface::graph vGraph(getNumTiles());
    for (int i = 0; i < getNumTiles(); i++)
    {
        for (int j = 0; j < getNumTiles(); j++)
        {
            if (tiles[i]->getSouth() == tiles[j]->getNorth())
            {
                vGraph.addEdge(i, j);
            }
        }
    }
    vGraph.allPairsShortestPaths(dMat, graphInterface::graph::allPairsShortestPathsAlgorithms::BreadthFirstSearch);
}

void tileset::printMinDistancesMatrix() const
{
    // horizontal directed graph: left->right
    std::vector<std::vector<double>> hList;
    getMinDistancesMatrixLR(hList);
    double scl = hList[0][0];
    for (unsigned int i = 0; i < hList.size(); i++)
    {
        if (hList[i][i] < scl)
        {
            scl = hList[i][i];
        }
        for (unsigned int j = 0; j < hList[i].size(); j++)
        {
            std::cout << hList[i][j] << "\t";
        }
        std::cout << std::endl;
    }
    std::cout << "Shortest cycle length (horizontal): " << scl << std::endl
              << std::endl;

    // vertical directed graph: up->down
    std::vector<std::vector<double>> vList;
    getMinDistancesMatrixUD(vList);
    scl = vList[0][0];
    for (unsigned int i = 0; i < vList.size(); i++)
    {
        if (vList[i][i] < scl)
        {
            scl = vList[i][i];
        }
        for (unsigned int j = 0; j < vList[i].size(); j++)
        {
            std::cout << vList[i][j] << "\t";
        }
        std::cout << std::endl;
    }
    std::cout << "Shortest cycle length (vertical): " << scl << std::endl
              << std::endl;
}

void tileset::findStronglyConnectedComponents()
{
    // horizontal directed graph: left->right
    graphInterface::graph hGraph(getNumTiles());
    std::vector<std::vector<int>> hList;
    for (int i = 0; i < getNumTiles(); i++)
    {
        for (int j = 0; j < getNumTiles(); j++)
        {
            if (tiles[i]->getEast() == tiles[j]->getWest())
            {
                hGraph.addEdge(i, j);
            }
        }
    }
    hGraph.stronglyConnectedComponents(hList);
    for (unsigned int i = 0; i < hList.size(); i++)
    {
        if (hList[i].size() == 1)
        {
            if (tiles[hList[i][0]]->getEast() != tiles[hList[i][0]]->getWest())
            {
                std::cout << "Warning: The tile " << hList[i][0] << " can not be used for tiling of an infinite row." << std::endl;
                redundantTiles[hList[i][0]] = true;
            }
        }
    }

    // vertical directed graph: up->down
    graphInterface::graph vGraph(getNumTiles());
    std::vector<std::vector<int>> vList;
    for (int i = 0; i < getNumTiles(); i++)
    {
        for (int j = 0; j < getNumTiles(); j++)
        {
            if (tiles[i]->getSouth() == tiles[j]->getNorth())
            {
                vGraph.addEdge(i, j);
            }
        }
    }
    vGraph.stronglyConnectedComponents(vList);
    for (unsigned int i = 0; i < vList.size(); i++)
    {
        if (vList[i].size() == 1)
        {
            if (tiles[vList[i][0]]->getSouth() != tiles[vList[i][0]]->getNorth())
            {
                std::cout << "Warning: The tile " << vList[i][0] << " can not be used for tiling of an infinite column." << std::endl;
                redundantTiles[vList[i][0]] = true;
            }
        }
    }

    if (getSetType() == VERTEX_BASED)
    {
        // SE->NW
        graphInterface::graph nwseGraph(getNumTiles());
        std::vector<std::vector<int>> nwseList;
        for (int i = 0; i < getNumTiles(); i++)
        {
            for (int j = 0; j < getNumTiles(); j++)
            {
                if (tiles[i]->getSouthEast() == tiles[j]->getNorthWest())
                {
                    nwseGraph.addEdge(i, j);
                }
            }
        }
        nwseGraph.stronglyConnectedComponents(nwseList);
        for (unsigned int i = 0; i < nwseList.size(); i++)
        {
            if (nwseList[i].size() == 1)
            {
                if (tiles[nwseList[i][0]]->getSouthEast() != tiles[nwseList[i][0]]->getNorthWest())
                {
                    std::cout << "Warning: The tile " << nwseList[i][0] << " can not be used for tiling of an infinite SE-NW diagonal tiling." << std::endl;
                    redundantTiles[nwseList[i][0]] = true;
                }
            }
        }

        // SW->NE
        graphInterface::graph neswGraph(getNumTiles());
        std::vector<std::vector<int>> neswList;
        for (int i = 0; i < getNumTiles(); i++)
        {
            for (int j = 0; j < getNumTiles(); j++)
            {
                if (tiles[i]->getSouthWest() == tiles[j]->getNorthEast())
                {
                    neswGraph.addEdge(i, j);
                }
            }
        }
        neswGraph.stronglyConnectedComponents(neswList);
        for (unsigned int i = 0; i < neswList.size(); i++)
        {
            if (neswList[i].size() == 1)
            {
                if (tiles[neswList[i][0]]->getSouthWest() != tiles[neswList[i][0]]->getNorthEast())
                {
                    std::cout << "Warning: The tile " << neswList[i][0] << " can not be used for tiling of an infinite SW-NE diagonal tiling." << std::endl;
                    redundantTiles[neswList[i][0]] = true;
                }
            }
        }
    }
    else
    {
        // create feasible combintations - TODO
    }
}

void tileset::groupSet(tileset &tset, std::vector<std::vector<std::vector<tile::tcode>>> &assembly) const
{
    if (assembly.size() == 0)
    {
        std::cerr << "ERROR: Empty solution matrix!" << std::endl;
        exit(2);
    }
    unsigned int newNumTiles = assembly.size();
    unsigned int newHeight = assembly[0].size();
    unsigned int newWidth = assembly[0][0].size();

    // create a set of color codes
    std::set<std::vector<tile::tcode>> cCodes;

    for (unsigned int tile = 0; tile < newNumTiles; tile++)
    {
        // vertical edges
        int westCode = 0;
        int eastCode = 0;
        for (unsigned int row = 0; row < newHeight; row++)
        {
            westCode += std::pow(getNumEdgeColors(), row) * tiles[assembly[tile][row][0]]->getWest();
            eastCode += std::pow(getNumEdgeColors(), row) * tiles[assembly[tile][row][newWidth - 1]]->getEast();
        }

        // horizontal edges
        int northCode = 0;
        int southCode = 0;
        for (unsigned int col = 0; col < newWidth; col++)
        {
            northCode += std::pow(getNumEdgeColors(), col) * tiles[assembly[tile][0][col]]->getNorth();
            southCode += std::pow(getNumEdgeColors(), col) * tiles[assembly[tile][newHeight - 1][col]]->getSouth();
        }

        // append tile
        std::vector<tile::tcode> codes{(tile::tcode)northCode,
                                       (tile::tcode)westCode,
                                       (tile::tcode)southCode,
                                       (tile::tcode)eastCode};
        cCodes.insert(codes);
    }

    tset.setTileSet(cCodes, getSetType());
    std::cout << "The tile set was converted to " << tset.getNumTiles() << " edge-based Wang tiles over " << tset.getNumEdgeColors() << " colors." << std::endl;
}

void tileset::removeRedundantTiles()
{
    // create a set of color codes
    std::set<std::vector<tile::tcode>> cCodes;

    for (int tile = 0; tile < getNumTiles(); tile++)
    {
        if (isRedundantTile(tile) == false)
        {
            switch (getSetType())
            {
            case EDGE_BASED:
            {
                // append tile
                std::vector<tile::tcode> codes{tiles[tile]->getNorth(),
                                               tiles[tile]->getWest(),
                                               tiles[tile]->getSouth(),
                                               tiles[tile]->getEast()};
                cCodes.insert(codes);
                break;
            }

            case VERTEX_BASED:
            {
                // append tile
                std::vector<tile::tcode> codes{tiles[tile]->getNorthWest(),
                                               tiles[tile]->getSouthWest(),
                                               tiles[tile]->getSouthEast(),
                                               tiles[tile]->getNorthEast()};
                cCodes.insert(codes);
                break;
            }

            default:
            {
                std::cerr << "ERROR: Unknown set type!" << std::endl;
                exit(2);
                break;
            }
            }
        }
    }

    setTileSet(cCodes, getSetType());
}

void tileset::setTileSet(std::set<std::vector<tile::tcode>> &setOfCodes, setTypes st)
{
    setNumTiles(setOfCodes.size());
    setType = st;
    tiles.clear();

    std::set<std::vector<tile::tcode>> newCodes;
    int numColors;

    switch (st)
    {
    case EDGE_BASED:
    {
        // for edge-based tiles, the north and south edges are independent of east and west
        std::set<tile::tcode> uniqueNSCodes, uniqueWECodes;

        // iterate through all tiles
        for (std::set<std::vector<tile::tcode>>::iterator tile = setOfCodes.begin(); tile != setOfCodes.end(); tile++)
        {
            // get the actual tile codes
            std::vector<tile::tcode> values = *tile;

            // get uniques
            uniqueNSCodes.insert(values[0]);
            uniqueNSCodes.insert(values[2]);
            uniqueWECodes.insert(values[1]);
            uniqueWECodes.insert(values[3]);
        }

        // assign number of colors
        numColors = std::max(uniqueNSCodes.size(), uniqueWECodes.size());

        // iterate through all tiles
        for (std::set<std::vector<tile::tcode>>::iterator tile = setOfCodes.begin(); tile != setOfCodes.end(); tile++)
        {
            // get the actual tile codes
            std::vector<tile::tcode> values = *tile;

            // replace colors {c1 .. cn} by {0 .. n} to reduce the number of used colors
            // N-S
            tile::tcode iter = 0;
            for (std::set<tile::tcode>::iterator color = uniqueNSCodes.begin(); color != uniqueNSCodes.end(); color++)
            {
                if (*color == values[0])
                    values[0] = iter;
                if (*color == values[2])
                    values[2] = iter;
                iter++;
            }

            // W-E
            iter = 0;
            for (std::set<tile::tcode>::iterator color = uniqueWECodes.begin(); color != uniqueWECodes.end(); color++)
            {
                if (*color == values[1])
                    values[1] = iter;
                if (*color == values[3])
                    values[3] = iter;
                iter++;
            }

            newCodes.insert(values);
        }

        break;
    }

    case VERTEX_BASED:
    {
        std::set<tile::tcode> uniqueCodes;

        // try to reduce the number of colors
        // iterate through all tiles
        for (std::set<std::vector<tile::tcode>>::iterator tile = setOfCodes.begin(); tile != setOfCodes.end(); tile++)
        {
            // get the actual tile codes
            std::vector<tile::tcode> values = *tile;

            for (tile::tcode length = 0; length < (tile::tcode)values.size(); length++)
            {
                uniqueCodes.insert(values[length]);
            }
        }

        // assign number of colors
        numColors = uniqueCodes.size();

        // iterate through all tiles
        for (std::set<std::vector<tile::tcode>>::iterator tile = setOfCodes.begin(); tile != setOfCodes.end(); tile++)
        {
            // get the actual tile codes
            std::vector<tile::tcode> values = *tile;

            // replace colors {c1 .. cn} by {0 .. n} to reduce the number of used colors
            tile::tcode iter = 0;
            for (std::set<tile::tcode>::iterator color = uniqueCodes.begin(); color != uniqueCodes.end(); color++)
            {
                std::replace(values.begin(), values.end(), *color, iter);
                iter++;
            }
            newCodes.insert(values);
        }

        break;
    }

    default:
    {
        std::cerr << "ERROR: Unknown tile type!" << std::endl;
        exit(2);
    }
    }

    switch (st)
    {
    case EDGE_BASED:
    {
        numEdgeColors = numColors;
        break;
    }

    case VERTEX_BASED:
    {
        numVertexColors = numColors;
        numEdgeColors = numVertexColors * numVertexColors;
        break;
    }

    default:
    {
        std::cerr << "ERROR: Unknown tile type!" << std::endl;
        exit(2);
    }
    }

    for (std::set<std::vector<tile::tcode>>::iterator it = newCodes.begin(); it != newCodes.end(); it++)
    {
        std::vector<tile::tcode> values = *it;

        switch (st)
        {
        case EDGE_BASED:
        {
            tiles.emplace_back(std::shared_ptr<tile>(new edgetile(values[0], values[1], values[2], values[3])));
            break;
        }

        case VERTEX_BASED:
        {
            tiles.emplace_back(std::shared_ptr<tile>(new vertextile(values[0], values[1], values[2], values[3], numVertexColors)));
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown tile type!" << std::endl;
            exit(2);
        }
        }
    }

    // tileset::setTilesetConnectivity();

    tileset::setConnectivityConstraints();
}

void tileset::writeTsetFile(std::string fname) const
{
    fname = fname + ".tset";
    std::ofstream myfile(fname.c_str());

    switch (getSetType())
    {
    case EDGE_BASED:
    {
        myfile << "# type of tiling" << std::endl;
        myfile << "EDGE_BASED" << std::endl
               << std::endl;

        myfile << "# numTiles numColors" << std::endl;
        myfile << getNumTiles() << " " << getNumEdgeColors() << std::endl
               << std::endl;

        myfile << "# north west south east colors of all tiles" << std::endl;
        for (int i = 0; i < getNumTiles(); i++)
        {
            myfile << tiles[i]->getNorth() << " " << tiles[i]->getWest() << " " << tiles[i]->getSouth() << " " << tiles[i]->getEast() << std::endl;
        }
        myfile.close();
        break;
    }

    case VERTEX_BASED:
    {
        myfile << "# type of tiling" << std::endl;
        myfile << "VERTEX_BASED" << std::endl
               << std::endl;

        myfile << "# numTiles numColors" << std::endl;
        myfile << getNumTiles() << " " << getNumVertexColors() << std::endl
               << std::endl;

        myfile << "# northwest southwest southeast northeast colors of all tiles" << std::endl;
        for (int i = 0; i < getNumTiles(); i++)
        {
            myfile << tiles[i]->getNorthWest() << " " << tiles[i]->getSouthWest() << " " << tiles[i]->getSouthEast() << " " << tiles[i]->getNorthEast() << std::endl;
        }
        myfile.close();
        break;
    }

    default:
    {
        std::cerr << "ERROR: Can not save tiling. Unknown tiling type (" << getSetType() << ")." << std::endl;
        exit(2);
        break;
    }
    }
}

int tileset::vertexToEdge(tileset &tset) const
{
    if (getSetType() == VERTEX_BASED)
    {
        // create a set of color codes
        std::set<std::vector<tile::tcode>> cCodes;

        for (int tile = 0; tile < getNumTiles(); tile++)
        {
            // append tile
            std::vector<tile::tcode> codes{tiles[tile]->getNorth(),
                                           tiles[tile]->getWest(),
                                           tiles[tile]->getSouth(),
                                           tiles[tile]->getEast()};
            cCodes.insert(codes);
        }

        // create the actual tile set
        tset.setTileSet(cCodes, EDGE_BASED);
        std::cout << "The tile set was converted to " << tset.getNumTiles() << " edge-based Wang tiles over " << tset.getNumEdgeColors() << " colors." << std::endl;
        return 0;
    }
    else
    {
        std::cerr << "ERROR: Supplied tile set is not vertex-based! Conversion to edge-based set aborted." << std::endl;
        exit(2);
        return 1;
    }
}

void tileset::edgeToVertex(conversionMethod method, tileset &tset) const
{
    switch (method)
    {
    case DIAGONAL_TRANSLATION:
    {
        // based on http://graphics.cs.kuleuven.be/publications/LKD06ASSTCC/LKD06ASSTCC_paper.pdf

        // create a set of color codes
        std::set<std::vector<tile::tcode>> cCodes;

        // northwest tile
        for (int i = 0; i < getNumTiles(); i++)
        {
            int northEdge = tiles[i]->getEast();
            int westEdge = tiles[i]->getSouth();

            // southwest tile
            for (int j = 0; j < getNumTiles(); j++)
            {
                if (westEdge == tiles[j]->getNorth())
                {
                    int southEdge = tiles[j]->getEast();

                    // southeast tile
                    for (int k = 0; k < getNumTiles(); k++)
                    {
                        if (southEdge == tiles[k]->getWest())
                        {
                            int eastEdge = tiles[k]->getNorth();

                            // northeast tile
                            for (int l = 0; l < getNumTiles(); l++)
                            {
                                if ((eastEdge == tiles[l]->getSouth()) && (northEdge == tiles[l]->getWest()))
                                {
                                    std::vector<tile::tcode> codes{(tile::tcode)i, (tile::tcode)j, (tile::tcode)k, (tile::tcode)l};
                                    cCodes.insert(codes);
                                }
                            }
                        }
                    }
                }
            }
        }

        tset.setTileSet(cCodes, VERTEX_BASED);
        std::cout << "The tile set was converted using diagonal translation to " << tset.getNumTiles() << " vertex-based Wang tiles over " << tset.getNumVertexColors() << " colors." << std::endl;
        break;
    }

    case HORIZONTAL_TRANSLATION:
    {
        // based on http://graphics.cs.kuleuven.be/publications/LKD06ASSTCC/LKD06ASSTCC_paper.pdf

        // check if there the conversion is bijective
        // the horizontal edges have to uniquely determine the (explicitly removed) vertical edges
        graphInterface::graph transducer(getNumEdgeColors());
        for (int i = 0; i < getNumTiles(); i++)
        {
            transducer.addEdge(tiles[i]->getNorth(), tiles[i]->getSouth(), std::to_string(tiles[i]->getEast()) + "|" + std::to_string(tiles[i]->getWest()));
        }
        if (transducer.isMultiGraph())
        {
            std::cout << "Warning: the horizontal translation method is not bijective!" << std::endl;
        }

        // create a set of color codes
        std::set<std::vector<tile::tcode>> cCodes;

        // west tile
        for (int i = 0; i < getNumTiles(); i++)
        {
            int edge = tiles[i]->getEast();

            // east tile
            for (int j = 0; j < getNumTiles(); j++)
            {
                if (edge == tiles[j]->getWest())
                {
                    std::vector<tile::tcode> codes{tiles[i]->getNorth(),
                                                   tiles[i]->getSouth(),
                                                   tiles[j]->getSouth(),
                                                   tiles[j]->getNorth()};
                    cCodes.insert(codes);
                }
            }
        }

        tset.setTileSet(cCodes, VERTEX_BASED);
        std::cout << "The tile set was converted using horizontal translation to " << tset.getNumTiles() << " vertex-based Wang tiles over " << tset.getNumVertexColors() << " colors." << std::endl;
        break;
    }

    case VERTICAL_TRANSLATION:
    {
        // based on http://graphics.cs.kuleuven.be/publications/LKD06ASSTCC/LKD06ASSTCC_paper.pdf

        // check if there the conversion is bijective
        // the vertical edges have to uniquely determine the (explicitly removed) horizontal edges
        graphInterface::graph transducer(getNumEdgeColors());
        for (int i = 0; i < getNumTiles(); i++)
        {
            transducer.addEdge(tiles[i]->getWest(), tiles[i]->getEast(), std::to_string(tiles[i]->getNorth()) + "|" + std::to_string(tiles[i]->getSouth()));
        }
        if (transducer.isMultiGraph())
        {
            std::cout << "Warning: the vertical translation method is not bijective!" << std::endl;
        }

        // create a set of color codes
        std::set<std::vector<tile::tcode>> cCodes;

        // north tile
        for (int i = 0; i < getNumTiles(); i++)
        {
            int edge = tiles[i]->getSouth();

            // south tile
            for (int j = 0; j < getNumTiles(); j++)
            {
                if (edge == tiles[j]->getNorth())
                {
                    std::vector<tile::tcode> codes{tiles[i]->getWest(),
                                                   tiles[j]->getWest(),
                                                   tiles[j]->getEast(),
                                                   tiles[i]->getEast()};
                    cCodes.insert(codes);
                }
            }
        }

        tset.setTileSet(cCodes, VERTEX_BASED);
        std::cout << "The tile set was converted using vertical translation to " << tset.getNumTiles() << " vertex-based Wang tiles over " << tset.getNumVertexColors() << " colors." << std::endl;
        break;
    }

    case ROTATION:
    {
        // based on http://graphics.cs.kuleuven.be/publications/LKD06ASSTCC/LKD06ASSTCC_paper.pdf

        // create a set of color codes
        std::set<std::vector<tile::tcode>> cCodes;

        // northwest tile
        for (int i = 0; i < getNumTiles(); i++)
        {
            int northEdge = tiles[i]->getEast();
            int westEdge = tiles[i]->getSouth();

            // the white tiles (inside individual tiles), superimposed with the [1 0 1 0] tile
            std::vector<tile::tcode> codesW{tile::tcode(tiles[i]->getNorth() + getNumEdgeColors()),
                                            tile::tcode(tiles[i]->getWest()),
                                            tile::tcode(tiles[i]->getSouth() + getNumEdgeColors()),
                                            tile::tcode(tiles[i]->getEast())};
            cCodes.insert(codesW);

            // southwest tile
            for (int j = 0; j < getNumTiles(); j++)
            {
                if (westEdge == tiles[j]->getNorth())
                {
                    int southEdge = tiles[j]->getEast();

                    // southeast tile
                    for (int k = 0; k < getNumTiles(); k++)
                    {
                        if (southEdge == tiles[k]->getWest())
                        {
                            int eastEdge = tiles[k]->getNorth();

                            // northeast tile
                            for (int l = 0; l < getNumTiles(); l++)
                            {
                                if ((eastEdge == tiles[l]->getSouth()) && (northEdge == tiles[l]->getWest()))
                                {
                                    // the black tiles, superimposed with the [0 1 0 1] tile
                                    std::vector<tile::tcode> codesB{tile::tcode(northEdge),
                                                                    tile::tcode(westEdge + getNumEdgeColors()),
                                                                    tile::tcode(southEdge),
                                                                    tile::tcode(eastEdge + getNumEdgeColors())};
                                    cCodes.insert(codesB);
                                }
                            }
                        }
                    }
                }
            }
        }

        tset.setTileSet(cCodes, VERTEX_BASED);
        std::cout << "The tile set was converted using rotation to " << tset.getNumTiles() << " vertex-based Wang tiles over " << tset.getNumVertexColors() << " colors." << std::endl;
        break;
    }

    case SUBDIVISION:
    {
        // based on http://graphics.cs.kuleuven.be/publications/LKD06ASSTCC/LKD06ASSTCC_paper.pdf

        // create a set of color codes
        std::set<std::vector<tile::tcode>> cCodes;

        tile::tcode uniNumber = getNumEdgeColors() + getNumTiles();
        // tile
        for (int i = 0; i < getNumTiles(); i++)
        {
            tile::tcode tileNumber = getNumEdgeColors() + i;

            // northwest tile
            std::vector<tile::tcode> codesNW{uniNumber,
                                             tiles[i]->getWest(),
                                             tileNumber,
                                             tiles[i]->getNorth()};
            cCodes.insert(codesNW);

            // southwest tile
            std::vector<tile::tcode> codesSW{tiles[i]->getWest(),
                                             uniNumber,
                                             tiles[i]->getSouth(),
                                             tileNumber};
            cCodes.insert(codesSW);

            // southeast tile
            std::vector<tile::tcode> codesSE{tileNumber,
                                             tiles[i]->getSouth(),
                                             uniNumber,
                                             tiles[i]->getEast()};
            cCodes.insert(codesSE);

            // northeast tile
            std::vector<tile::tcode> codesNE{tiles[i]->getNorth(),
                                             tileNumber,
                                             tiles[i]->getEast(),
                                             uniNumber};
            cCodes.insert(codesNE);
        }

        tset.setTileSet(cCodes, VERTEX_BASED);
        std::cout << "The tile set was converted using subdivision to " << tset.getNumTiles() << " vertex-based Wang tiles over " << tset.getNumVertexColors() << " colors." << std::endl;
        break;
    }

    case VERTEX_LABELING:
    {
        // based on https://mathoverflow.net/a/164405

        // create a set of color codes
        std::set<std::vector<tile::tcode>> cCodes;

        /* 3x3 grid: [i j k
                      l m n
                      o p q] */

        // currently implemented very inefficiently
        for (int i = 0; i < getNumTiles(); i++)
        {
            for (int j = 0; j < getNumTiles(); j++)
            {
                if (tiles[i]->getEast() == tiles[j]->getWest())
                {
                    for (int k = 0; k < getNumTiles(); k++)
                    {
                        if (tiles[j]->getEast() == tiles[k]->getWest())
                        {
                            for (int l = 0; l < getNumTiles(); l++)
                            {
                                if (tiles[i]->getSouth() == tiles[l]->getNorth())
                                {
                                    for (int m = 0; m < getNumTiles(); m++)
                                    {
                                        if ((tiles[j]->getSouth() == tiles[m]->getNorth()) && (tiles[l]->getEast() == tiles[m]->getWest()))
                                        {
                                            for (int n = 0; n < getNumTiles(); n++)
                                            {
                                                if ((tiles[k]->getSouth() == tiles[n]->getNorth()) && (tiles[m]->getEast() == tiles[n]->getWest()))
                                                {
                                                    for (int o = 0; o < getNumTiles(); o++)
                                                    {
                                                        if (tiles[l]->getSouth() == tiles[o]->getNorth())
                                                        {
                                                            for (int p = 0; p < getNumTiles(); p++)
                                                            {
                                                                if ((tiles[m]->getSouth() == tiles[p]->getNorth()) && (tiles[o]->getEast() == tiles[p]->getWest()))
                                                                {
                                                                    for (int q = 0; q < getNumTiles(); q++)
                                                                    {
                                                                        if ((tiles[n]->getSouth() == tiles[q]->getNorth()) && (tiles[p]->getEast() == tiles[q]->getWest()))
                                                                        {
                                                                            tile::tcode code1 = tiles[i]->getEast() +
                                                                                                tiles[i]->getSouth() * getNumEdgeColors() +
                                                                                                tiles[m]->getWest() * getNumEdgeColors() * getNumEdgeColors() +
                                                                                                tiles[m]->getNorth() * getNumEdgeColors() * getNumEdgeColors() * getNumEdgeColors();
                                                                            tile::tcode code2 = tiles[l]->getEast() +
                                                                                                tiles[l]->getSouth() * getNumEdgeColors() +
                                                                                                tiles[p]->getWest() * getNumEdgeColors() * getNumEdgeColors() +
                                                                                                tiles[p]->getNorth() * getNumEdgeColors() * getNumEdgeColors() * getNumEdgeColors();
                                                                            tile::tcode code3 = tiles[m]->getEast() +
                                                                                                tiles[m]->getSouth() * getNumEdgeColors() +
                                                                                                tiles[q]->getWest() * getNumEdgeColors() * getNumEdgeColors() +
                                                                                                tiles[q]->getNorth() * getNumEdgeColors() * getNumEdgeColors() * getNumEdgeColors();
                                                                            tile::tcode code4 = tiles[j]->getEast() +
                                                                                                tiles[j]->getSouth() * getNumEdgeColors() +
                                                                                                tiles[n]->getWest() * getNumEdgeColors() * getNumEdgeColors() +
                                                                                                tiles[n]->getNorth() * getNumEdgeColors() * getNumEdgeColors() * getNumEdgeColors();
                                                                            std::vector<tile::tcode> codes{code1, code2, code3, code4};
                                                                            cCodes.insert(codes);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        tset.setTileSet(cCodes, VERTEX_BASED);
        std::cout << "The tile set was converted using vertex labeling to " << tset.getNumTiles() << " vertex-based Wang tiles over " << tset.getNumVertexColors() << " colors." << std::endl;
        break;
    }

    default:
    {
        std::cerr << "ERROR: Unknown conversion method!" << std::endl;
        exit(1);
    }
    }
}

bool tileset::isRedundantTile(int tile) const
{
    if ((tile >= (tile::tcode)getNumTiles()) || (tile < 0) || ((tile::tcode)redundantTiles.size() <= tile))
    {
        std::cerr << "ERROR: Can not check tile " << tile << " redundancy!" << std::endl;
        exit(2);
        return 0;
    }
    else
    {
        return redundantTiles[tile];
    }
}

void tileset::plotTiles(std::vector<tile::tcode> tileNums) const
{
    for (int line = 0; line < 4; line++)
    {
        for (int tile = 0; tile < (tile::tcode)tileNums.size(); tile++)
        {
            if (tileNums[tile] < 0)
            {
                std::cout << "      ";
            }
            else
            {
                tiles[tileNums[tile]]->printTile(line);
            }
            std::cout << " ";
        }
        std::cout << std::endl;
    }
}

void tileset::setConnectivityConstraints()
{
    north.clear();
    south.clear();
    east.clear();
    west.clear();
    for (int color = 0; color < getNumEdgeColors(); color++)
    {
        std::vector<tile::tcode> nc, sc, ec, wc;

        for (int tile = 0; tile < getNumTiles(); tile++)
        {
            if (tiles[tile]->getNorth() == color)
            {
                nc.push_back(tile);
            }

            if (tiles[tile]->getSouth() == color)
            {
                sc.push_back(tile);
            }

            if (tiles[tile]->getEast() == color)
            {
                ec.push_back(tile);
            }

            if (tiles[tile]->getWest() == color)
            {
                wc.push_back(tile);
            }
        }

        north.push_back(nc);
        south.push_back(sc);
        east.push_back(ec);
        west.push_back(wc);
    }

    if (getSetType() == VERTEX_BASED)
    {
        for (int color = 0; color < getNumVertexColors(); color++)
        {
            std::vector<tile::tcode> nwc, nec, sec, swc;

            for (int tile = 0; tile < numTiles; tile++)
            {
                if (tiles[tile]->getNorthWest() == color)
                {
                    nwc.push_back(tile);
                }

                if (tiles[tile]->getSouthWest() == color)
                {
                    swc.push_back(tile);
                }

                if (tiles[tile]->getNorthEast() == color)
                {
                    nec.push_back(tile);
                }

                if (tiles[tile]->getSouthEast() == color)
                {
                    sec.push_back(tile);
                }
            }

            northwest.push_back(nwc);
            southwest.push_back(swc);
            northeast.push_back(nec);
            southeast.push_back(sec);
        }
    }
}

int tileset::readTsetFile(std::string fname)
{
    std::ifstream infile(fname.c_str());
    std::string line;
    int counter = 0;
    std::string path = "./";

    if (!infile.is_open())
    {
        path = "./tilesets/";
        std::string newName = path + fname;
        infile.open(newName.c_str());
    }

    if (!infile.is_open())
    {
        std::string folder = fname;
        folder.erase(folder.length() - 5);
        path = "./tilesets/" + folder + "/";
        std::string newName = path + fname;
        infile.open(newName.c_str());
    }

    if (!infile.is_open())
    {
        std::cerr << "ERROR in tileset::readTsetFile(): The file " << fname << " failed to open." << std::endl;
        exit(2);
    }

    while (std::getline(infile, line))
    {
        std::stringstream ss(line);

        if ((line.find("#") == line.npos) &&
            (line.find("/") == line.npos) &&
            (line.find("%") == line.npos) &&
            (line.find_first_not_of("\t\n ") != line.npos) &&
            (line.length() != 0))
        {
            std::string tilingType;
            switch (counter)
            {
            case 0:
                ss >> tilingType;
                if (!tilingType.compare("EDGE_BASED"))
                {
                    setType = EDGE_BASED;
                }
                else
                {
                    if (!tilingType.compare("VERTEX_BASED"))
                    {
                        setType = VERTEX_BASED;
                    }
                    else
                    {
                        std::cerr << "ERROR in tileset::readTsetFile(): The file " << fname << " should define either EDGE_BASED or VERTEX_BASED tiles." << std::endl;
                        exit(2);
                    }
                }
                break;

            case 1:
                if (getSetType() == EDGE_BASED)
                {
                    ss >> numTiles >> numEdgeColors;
                    setNumTiles(numTiles);
                }
                else
                {
                    if (getSetType() == VERTEX_BASED)
                    {
                        ss >> numTiles >> numVertexColors;
                        setNumTiles(numTiles);
                        numEdgeColors = numVertexColors * numVertexColors;
                    }
                    else
                    {
                        std::cerr << "ERROR in tileset::readTsetFile(): Is it vertex or edge based? This should not happen!" << std::endl;
                        exit(2);
                    }
                }
                break;

            default:
                // try to read tile texture from a file
                std::vector<std::string> extensions{".svg", ".png", ".jpg", ".jpeg", ".tiff", ".tif", ".gif", ".bmp"};
                std::vector<std::string> mimetypes{"svg+xml", "png", "jpeg", "jpeg", "tiff", "tiff", "gif", "bmp"};

                std::string svgData = "";
                auto extId = -1;
                for (const auto &ext : extensions)
                {
                    extId++;
                    std::string tileName = path + std::to_string(counter - 2) + ext;
                    std::ifstream f;
                    f.open(tileName.c_str(), std::ios::binary);

                    if (f.good())
                    {
                        std::vector<char> tmp;
                        while (!f.eof())
                        {
                            char c = (char)f.get();
                            tmp.push_back(c);
                        }
                        std::string newStr(tmp.begin(), tmp.end() - 1);
                        newStr = base64_encode((unsigned const char *)newStr.c_str(), newStr.size());

                        svgData += "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n";
                        svgData += "<image width=\"100\" height=\"100\" xlink:href=\"data:image/" + mimetypes[extId] + ";base64," + newStr + "\"/>\n";
                        svgData += "</svg>\n";

                        f.close();
                        break;
                    }
                    f.close();
                }

                if (getSetType() == EDGE_BASED)
                {
                    tile::tcode north, south, east, west;
                    ss >> north >> west >> south >> east;

                    if ((north >= numEdgeColors) || (south >= numEdgeColors) || (east >= numEdgeColors) || (west >= numEdgeColors) || (north < 0) || (south < 0) || (east < 0) || (west < 0))
                    {
                        std::cerr << "ERROR in tileset::readTsetFile(): The tile " << counter - 2 << " from the tile set " << fname << " contains color code(s) out of the range <0, " << numEdgeColors - 1 << ">." << std::endl;
                        exit(2);
                    }

                    tiles.emplace_back(std::shared_ptr<tile>(new edgetile(north, west, south, east)));
                    if (!svgData.empty())
                    {
                        tiles[counter - 2]->setSvgData(svgData);
                        tiles[counter - 2]->setPrintMode(tile::svgPlotMode::SVGCOLOR);
                    }
                }
                else
                {
                    if (getSetType() == VERTEX_BASED)
                    {
                        tile::tcode northwest, southwest, southeast, northeast;
                        ss >> northwest >> southwest >> southeast >> northeast;

                        if ((northwest >= numVertexColors) || (northeast >= numVertexColors) || (southwest >= numVertexColors) || (southeast >= numVertexColors) || (northwest < 0) || (southwest < 0) || (southeast < 0) || (northeast < 0))
                        {
                            std::cerr << "ERROR in tileset::readTsetFile(): The tile " << counter - 2 << " from the tile set " << fname << " contains color code(s) out of the range <0, " << numVertexColors - 1 << ">." << std::endl;
                            exit(2);
                        }

                        tiles.emplace_back(std::shared_ptr<tile>(new vertextile(northwest, southwest, southeast, northeast, numVertexColors)));
                        if (!svgData.empty())
                        {
                            tiles[counter - 2]->setSvgData(svgData);
                            tiles[counter - 2]->setPrintMode(tile::svgPlotMode::SVGCOLOR);
                        }
                    }
                    else
                    {
                        std::cerr << "ERROR in tileset::readTsetFile(): Is it vertex or edge based? This should not happen!" << std::endl;
                        exit(2);
                    }
                }
                break;
            }

            if (counter == numTiles + 1)
            {
                break;
            }

            counter += 1;
        }
    }

    if (infile.fail() && !infile.eof())
    {
        std::cerr << "ERROR in tileset::readTsetFile(): The file " << fname << " is not a valid tile set format." << std::endl;
        exit(2);
    }

    if ((tile::tcode)tiles.size() != numTiles)
    {
        std::cerr << "ERROR in tileset::readTsetFile(): The file " << fname << " suggests " << numTiles << " tiles, but only " << tiles.size() << " tiles found." << std::endl;
        exit(2);
    }

    // tileset::setTilesetConnectivity();

    tileset::setConnectivityConstraints();

    infile.close();

    return 0;
}

void tileset::plotTileSet(int inRow) const
{
    std::cout << std::endl
              << "### TILE SET:" << std::endl;

    int numRows = numTiles / inRow + ((numTiles % inRow) > 0);

    int curTile = 0;
    for (int i = 0; i < numRows; i++)
    {
        int maxTile = std::min(curTile + inRow, numTiles);

        for (int line = 0; line < 4; line++)
        {
            for (int j = curTile; j < maxTile; j++)
            {
                tiles[j]->printTile(line);
                std::cout << "  ";
            }
            std::cout << std::endl;
        }
        curTile = maxTile;
    }
    std::cout << std::endl;
}

void tileset::plotTileSetSVG(int inRow, std::string fname) const
{
    int numRows = numTiles / inRow + ((numTiles % inRow) > 0);

    fname = fname + ".svg";
    std::ofstream myfile(fname.c_str());

    double delimeter = 25.0;

    myfile << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" << std::endl;
    myfile << "<svg width=\"" << (inRow * 100.0 + (inRow - 1) * delimeter) << "\" height=\"" << (numRows * 100.0 + (numRows - 1) * delimeter) << "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">" << std::endl;
    myfile << "<desc>Wang tiling exported by tiling_optimizer</desc>" << std::endl;

    // define tiles
    myfile << "<defs>" << std::endl;
    for (int i = 0; i < getNumTiles(); i++)
    {
        std::string svgTile = getSVGTileDefinition(i);
        myfile << svgTile << std::endl;
    }
    myfile << "</defs>" << std::endl;

    int curTile = 0;
    for (int i = 0; i < numRows; i++)
    {
        int maxTile = std::min(curTile + inRow, numTiles);

        for (int j = curTile; j < maxTile; j++)
        {
            double row = i + i * delimeter / 100.0;
            double col = (j - curTile) + (j - curTile) * delimeter / 100.0;
            std::string usage = "<use x=\"" + std::to_string(col * 100.0) + "\" y=\"" + std::to_string(row * 100.0) + "\" xlink:href=\"#T" + std::to_string(j) + "\"/>";
            myfile << usage << std::endl;
        }
        curTile = maxTile;
    }

    myfile << "</svg>" << std::endl;
    myfile.close();
}

tileset::tileset()
{
    numTiles = 0;
    numEdgeColors = 0;
    numVertexColors = 0;
    redundantTiles.clear();
}