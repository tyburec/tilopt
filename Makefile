# (c) 2017 Marek Tyburec

ifdef OS
	GUROBI_HOME = C:/Gurobi810/win64
	GUROBI_LIB  = -L$(GUROBI_HOME)/lib -lgurobi_c++ -lgurobi81
	MOSEK_HOME 	= $(HOME)/Matlab/mosek/8.1/tools/platform/linux64x86
else
	GUROBI_LIB  = -L$(GUROBI_HOME)/lib -lgurobi_c++ -lgurobi95
	MOSEK_HOME 	= $(HOME)/Matlab/mosek/10.0/tools/platform/linux64x86
endif

MOSEK_LIB   = -L$(MOSEK_HOME)/bin/ -lfusion64 -lmosek64 '-Wl,-rpath-link,$(MOSEK_HOME)/bin/ -Wl,-rpath=$$ORIGIN$(MOSEK_HOME)/bin/'
MOSEK_INC 	= -isystem$(MOSEK_HOME)/h -I$(MOSEK_HOME)/include
GUROBI_INC  = -I$(GUROBI_HOME)/include
CPP         = g++ -Wall -pthread -std=c++17 -ftree-vectorize
DEBUG_ARGS  = -fopenmp -D_GLIBCXX_USE_CXX11_ABI=0 -ggdb3 -g -O2
RELEASE_ARGS= -fopenmp -D_GLIBCXX_USE_CXX11_ABI=0 -O3

tiling_optimizer:
	release_gurobi

release_mosek:
	$(CPP) $(RELEASE_ARGS) $(MOSEK_INC) -o tiling_optimizer ./tiling_optimizer.cpp $(MOSEK_LIB) -DDMOSEK -lstdc++ -lpthread -lm

release_gurobi:
	$(CPP) $(RELEASE_ARGS) $(GUROBI_INC) -o tiling_optimizer ./tiling_optimizer.cpp $(GUROBI_LIB) -DDGUROBI -lstdc++ -lpthread -lm

release_none:
	$(CPP) $(RELEASE_ARGS) -o tiling_optimizer ./tiling_optimizer.cpp -lstdc++ -lpthread -lm

debug_mosek:
	$(CPP) $(DEBUG_ARGS) $(MOSEK_INC) -o tiling_optimizer ./tiling_optimizer.cpp $(MOSEK_LIB) -DDMOSEK -lstdc++ -lpthread -lm

debug_gurobi:
	$(CPP) $(DEBUG_ARGS) $(GUROBI_INC) -o tiling_optimizer ./tiling_optimizer.cpp $(GUROBI_LIB) -DDGUROBI -lstdc++ -lpthread -lm

debug_none:
	$(CPP) $(DEBUG_ARGS) -o tiling_optimizer ./tiling_optimizer.cpp -lstdc++ -lpthread -lm
