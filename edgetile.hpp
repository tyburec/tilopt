#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <memory>

class edgetile : public tile
{
public:
	void printTile(int line) const;
	std::string getSvgData(int id, tile::svgPlotMode spm) const;

	tcode getNorth() const { return code1; };
	tcode getSouth() const { return code3; };
	tcode getEast() const { return code4; };
	tcode getWest() const { return code2; };
	tcode getNorthEast() const { return -1; };
	tcode getNorthWest() const { return -1; };
	tcode getSouthEast() const { return -1; };
	tcode getSouthWest() const { return -1; };

	using tile::tile;

	~edgetile(){};

	std::shared_ptr<tile> clone() const;
};

std::shared_ptr<tile> edgetile::clone() const
{
	return std::make_shared<edgetile>(getNorth(), getWest(), getSouth(), getEast());
}

std::string edgetile::getSvgData(int id, tile::svgPlotMode spm) const
{
	std::string out = "";

	switch (spm)
	{
	case tile::svgPlotMode::SVGDATA:
	{
		if (svgData.empty())
		{
			std::cerr << "ERROR in edgetile::getSvgData(): No svgData present!" << std::endl;
			exit(2);
		}
		out += "<g id=\"T" + std::to_string(id) + "\">\n";
		out += svgData;
		out += "</g>";
		break;
	}

	case tile::svgPlotMode::COLOR:
	case tile::svgPlotMode::SVGCOLOR:
	{
		double d1 = 15.0;
		double d2 = 20.0;
		double opacity = 1.0;

		out += "<g id=\"T" + std::to_string(id) + "\">\n";
		if (spm == tile::svgPlotMode::SVGCOLOR)
		{
			if (svgData.empty())
			{
				std::cerr << "ERROR in edgetile::getSvgData(): No svgData present!" << std::endl;
				exit(2);
			}
			out += svgData;
			opacity = 0.4;
		}
		// north
		out += "<rect x=\"" + std::to_string(d2) + "\" y=\"0.0\" width=\"" + std::to_string(100.0 - 2 * d2) + "\" height=\"" + std::to_string(d1) + "\" fill=\"" + printColorRGB(getNorth()) + "\" stroke=\"" + printColorRGB(getNorth()) + "\" opacity=\"" + std::to_string(opacity) + "\"/>\n";
		// south
		out += "<rect x=\"" + std::to_string(d2) + "\" y=\"" + std::to_string(100.0 - d1) + "\" width=\"" + std::to_string(100.0 - 2 * d2) + "\" height=\"" + std::to_string(d1) + "\" fill=\"" + printColorRGB(getSouth()) + "\" stroke=\"" + printColorRGB(getSouth()) + "\" opacity=\"" + std::to_string(opacity) + "\"/>\n";
		// west
		out += "<rect x=\"0.0\" y=\"" + std::to_string(d2) + "\" width=\"" + std::to_string(d1) + "\" height=\"" + std::to_string(100.0 - 2 * d2) + "\" fill=\"" + printColorRGB(getWest()) + "\" stroke=\"" + printColorRGB(getWest()) + "\" opacity=\"" + std::to_string(opacity) + "\"/>\n";
		// east
		out += "<rect x=\"" + std::to_string(100.0 - d1) + "\" y=\"" + std::to_string(d2) + "\" width=\"" + std::to_string(d1) + "\" height=\"" + std::to_string(100.0 - 2 * d2) + "\" fill=\"" + printColorRGB(getEast()) + "\" stroke=\"" + printColorRGB(getEast()) + "\" opacity=\"" + std::to_string(opacity) + "\"/>\n";

		if (spm == tile::svgPlotMode::COLOR)
		{
			// outer shape
			out += "<rect x=\"0.0\" y=\"0.0\" width=\"1.0\" height=\"100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
			out += "<rect x=\"0.0\" y=\"0.0\" width=\"100.0\" height=\"1.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
			out += "<rect x=\"99.0\" y=\"0.0\" width=\"1.0\" height=\"100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
			out += "<rect x=\"0.0\" y=\"99.0\" width=\"100.0\" height=\"1.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
		}
		out += "</g>";
		break;
	}

	case tile::svgPlotMode::TEXT:
	{
		out += "<g id=\"T" + std::to_string(id) + "\">\n";
		out += "<rect x=\"0.0\" y=\"0.0\" width=\"1.0\" height=\"100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
		out += "<rect x=\"0.0\" y=\"0.0\" width=\"100.0\" height=\"1.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
		out += "<rect x=\"99.0\" y=\"0.0\" width=\"1.0\" height=\"100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
		out += "<rect x=\"0.0\" y=\"99.0\" width=\"100.0\" height=\"1.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
		out += "<polygon points=\"0.0,0.0 0.7,0.0 100.0,99.3 100.0,100.0 99.3,100.0 0.0,0.7\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
		out += "<polygon points=\"0.0,100.0 0.0,99.3 99.3,0.0 100.0,0.0 100.0,0.7 0.7,100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
		out += "<text x=\"50.0\" y=\"78.0\" text-anchor=\"middle\" style=\"font-size: 35px;font-family:Times,serif;\" dominant-baseline=\"middle\">" + std::to_string(getSouth()) + "</text>\n";
		out += "<text x=\"22.0\" y=\"50.0\" text-anchor=\"middle\" style=\"font-size: 35px;font-family:Times,serif;\" dominant-baseline=\"middle\">" + std::to_string(getWest()) + "</text>\n";
		out += "<text x=\"50.0\" y=\"22.0\" text-anchor=\"middle\" style=\"font-size: 35px;font-family:Times,serif;\" dominant-baseline=\"middle\">" + std::to_string(getNorth()) + "</text>\n";
		out += "<text x=\"78.0\" y=\"50.0\" text-anchor=\"middle\" style=\"font-size: 35px;font-family:Times,serif;\" dominant-baseline=\"middle\">" + std::to_string(getEast()) + "</text>\n";
		out += "</g>";
		break;
	}

	default:
	{
		std::cerr << "ERROR in edgetile::getSvgData(): Unknown svgPlotMode!" << std::endl;
		exit(2);
		break;
	}
	}
	return out;
}

void edgetile::printTile(int line) const
{
	if (line == 0)
		std::cout << "╔" << printColor(getNorth()) << "━━━━" << printColor(-1) << "╗";

	if ((line == 1) || (line == 2))
		std::cout << printColor(getWest()) << "┃" << printColor(-1) << "░░░░" << printColor(getEast()) << "┃" << printColor(-1);

	if (line == 3)
		std::cout << "╚" << printColor(getSouth()) << "━━━━" << printColor(-1) << "╝";
}