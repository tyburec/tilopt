#pragma once

#include "tileset.hpp"
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <random>
#include <set>
#include <cmath>
#include "optimizerInterface/optimizerEnvironment.hpp"
#include "optimizerInterface/linExpression.hpp"
#include "optimizerInterface/quadExpression.hpp"
#include "optimizerInterface/branchAndBoundTree.hpp"
#include "graphInterface/graph.hpp"

class tiling
{
public:
    enum formulationTypes
    {
        LP_TILES,
        LP_BNB_TILES,
        QP_TILES,
        SDP_TILES,
        SDP_TILESBIN
    };
    enum objTypes
    {
        NONE,
        MAXCONAREA,
        MAXCONSQUARE,
        MINPERIODIC,
        MAXCOVER,
        REDUNDANCY,
        NONE_TWEAKED,
        MAXCSP
    };

private:
    int width;
    int height;
    tileset tileSet;
    std::vector<std::vector<std::vector<tile::tcode>>> assembly;
    formulationTypes formulation;
    std::vector<std::vector<int>> boundaryConditions;
    bool feasible;
    bool periodic;
    bool allSolutions;
    int solutionsCount;
    bool packing;
    objTypes objType;

public:
    void addPeriodicBoundaryCondition() { periodic = true; };
    bool isPeriodic() const { return periodic; };
    int getHeight() const { return height; };
    int getWidth() const { return width; };
    int getNumberOfPlacedTiles(int order) const;
    bool getFeasibility() const { return feasible; };
    void setHeight(int height) { tiling::height = height; };
    void setWidth(int width) { tiling::width = width; };
    void setFeasibility(bool f) { feasible = f; };
    const tileset &getTileSet() const { return tileSet; };
    void plotTiling(int num) const;
    void plotTilings() const;
    void plotTilingSVG(std::string fname, int num) const;
    void plotTilingsSVG(std::string fname) const;
    void printAssemblyMatrix(int num) const;
    tiling();
    tiling(std::string fname);
    tiling(std::string fname, int width, int height);
    tiling(tileset tset, int width, int height);
    ~tiling(){};
    int readTilingFile(std::string fname);
    void optimize(formulationTypes formulation, optimizerInterface::solverModel::solvers solver);
    void callSolver(optimizerInterface::solverModel::solvers solver);
    void setFormulationType(formulationTypes formulation);
    formulationTypes getFormulationType() const { return formulation; };
    void addBoundaryCondition(std::vector<int> boundaryCondition);
    void addRandomBoundaryCondition();
    void removeBoundaryCondition(int position);
    void removeBoundaryConditions();
    void depthInspectTileSet(int depth, optimizerInterface::solverModel::solvers solver);
    void findNecessaryTiles(int width, int height, optimizerInterface::solverModel::solvers solver);
    void findNecessaryTile(int w, int h, optimizerInterface::solverModel::solvers solver, tile::tcode tile);
    bool getAllSolutions() { return allSolutions; };
    void setAllSolutions(bool as) { allSolutions = as; };
    void getAssemblyMatrix(std::vector<std::vector<std::vector<tile::tcode>>> &assMat) { assMat = assembly; };
    void setSolutionsCount(int s) { solutionsCount = s; };
    int getSolutionsCount() { return solutionsCount; }
    void infiniteRowColumnReduce();
    void addPackingConstraint() { packing = true; };
    bool getPacking() { return packing; };
    objTypes getObjType() { return objType; };
    void setObjType(objTypes o) { objType = o; };
    int heuristics(bool randomized, bool improved, int algNum);
    int heuristicsSimple(bool randomized);
    int heuristics050(bool randomized);
    int heuristics067(bool randomized);
    void heuristicsImproveColumn(int col, std::vector<std::vector<tile::tcode>> &assMat, bool random, std::vector<std::vector<bool>> &penal, int penalDist, graphInterface::graph &transducer, bool noeven);
    void heuristicsImproveRow(int row, std::vector<std::vector<tile::tcode>> &assMat, bool random, std::vector<std::vector<bool>> &penal, int penalDist, graphInterface::graph &dualTransducer, bool noeven);

protected:
    void initializeTileVariables(std::shared_ptr<optimizerInterface::solverModel> &model, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsTiles);
    void preventOverlaps(std::shared_ptr<optimizerInterface::solverModel> &model, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsTiles);
    void adjacencyVerticalEdges(std::shared_ptr<optimizerInterface::solverModel> &model, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsTiles, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsEdgesV);
    void adjacencyHorizontalEdges(std::shared_ptr<optimizerInterface::solverModel> &model, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsTiles, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsEdgesH);
    int matCoorToVec(int row, int col, int til) { return til + col * tileSet.getNumTiles() + row * tileSet.getNumTiles() * getWidth(); };
};

int tiling::getNumberOfPlacedTiles(int order) const
{
    if (order >= (int)assembly.size())
    {
        std::cerr << "ERROR in tiling::getNumberOfPlacedTiles(): out of the range of computed tilings." << std::endl;
        exit(2);
    }

    int out = 0;

    for (int i = 0; i < getWidth(); ++i)
        for (int j = 0; j < getHeight(); ++j)
            if (assembly[order][j][i] != -1)
                out += 1;

    return out;
}

void tiling::heuristicsImproveRow(int row, std::vector<std::vector<tile::tcode>> &assMat, bool random, std::vector<std::vector<bool>> &penal, int penalDist, graphInterface::graph &dualTransducer, bool noeven)
{
    // useful variables
    auto numEdgeColors = tileSet.getNumEdgeColors() + 1;
    auto numTilesInRow = getWidth();
    auto numVertices = numEdgeColors * (numTilesInRow + 1) + 1;
    double eps = 0.5 / (1 + numTilesInRow);

    // create empty graph
    graphInterface::graph rowGraph(numVertices);

    // source to west edges
    for (int i = 0; i < numEdgeColors - 1; ++i)
        rowGraph.addEdge(0, i + 1, "-1", 0.0);

    // east edges to terminal
    for (int i = 0; i < numEdgeColors - 1; ++i)
        rowGraph.addEdge(numEdgeColors * numTilesInRow + i + 1, numVertices - 1, "-1", 0.0);

    // intermediate edges -- tiles
    auto tiles = tileSet.getTiles();
    for (unsigned int j = 0; j < tiles.size(); ++j)
    {
        for (int i = 0; i < numTilesInRow; ++i)
        {
            // initialize cost
            double cost = 0.0;

            // even position
            if (noeven)
                if (i % 2 == 1)
                    cost = 1.01;

            // matching north tile
            if (cost < 1.0)
                if (row > 0)
                {
                    auto tileToNorth = assMat[row - 1][i];
                    if (tileToNorth != -1)
                    {
                        if (tiles[j]->getNorth() != tiles[tileToNorth]->getSouth())
                            cost = 1.01;
                    }
                    else
                    {
                        // penalize enforced voids
                        if (dualTransducer.getVertex(tiles[j]->getNorth())->getIndegree() == 0)
                        {
                            cost += eps;
                            // penalize based on dual transducer graph
                        }
                        else
                        {
                            if (penalDist > 0)
                                if (row >= penalDist)
                                {
                                    auto pTile = assMat[row - penalDist][i];
                                    if (pTile < 0)
                                    {
                                        if (penal.back()[j] == false)
                                            cost += eps;
                                    }
                                    else
                                    {
                                        if (penal[pTile][j] == false)
                                            cost += eps;
                                    }
                                }
                        }
                    }
                }

            // matching south tile + penalize enforced voids
            if (cost < 1.0)
                if (row < getHeight() - 1)
                {
                    auto tileToSouth = assMat[row + 1][i];
                    if (tileToSouth != -1)
                    {
                        if (tiles[j]->getSouth() != tiles[tileToSouth]->getNorth())
                            cost = 1.01;
                    }
                    else
                    {
                        // penalize enforced voids
                        if (dualTransducer.getVertex(tiles[j]->getSouth())->getOutdegree() == 0)
                            cost += eps;
                    }
                }

            // add tile if it can be placed
            if (cost < 1.0)
                rowGraph.addEdge(i * numEdgeColors + 1 + tiles[j]->getWest(),
                                 (i + 1) * numEdgeColors + 1 + tiles[j]->getEast(),
                                 std::to_string(j), cost);
        }
    }

    // add void intermediate edges
    for (int i = 0; i < numTilesInRow; ++i)
        for (int j = 0; j < numEdgeColors - 1; ++j)
            rowGraph.addEdge(i * numEdgeColors + 1 + j,
                             (i + 1) * numEdgeColors, "0",
                             1.0);
    for (int i = 0; i < numTilesInRow; ++i)
        for (int j = 0; j < numEdgeColors - 1; ++j)
            rowGraph.addEdge((i + 1) * numEdgeColors,
                             (i + 1) * numEdgeColors + 1 + j, "-3",
                             0.0);

    // find shortest path//maximum row cover
    std::vector<double> len;
    std::vector<graphInterface::edge_t> shortestPath;
    if (random)
    {
        rowGraph.randomizeEdgeOrder();
    }
    rowGraph.DAGShortestPath(0, len, shortestPath, false);

    // recover the shortest path
    int lastVertex = numVertices - 1;
    int position = getWidth() - 1;
    while (lastVertex != 0)
    {
        auto curEdge = shortestPath[lastVertex];
        // curEdge->setThickness(true);
        auto iLabel = std::stoi(curEdge->getLabel());
        if (iLabel >= 0)
        {
            if (curEdge->getWeight() < 1.0)
            {
                assMat[row][position] = iLabel;
            }
            else
            {
                assMat[row][position] = -1;
            }
            --position;
        }
        lastVertex = curEdge->getTailVertex();
    }

    /*// visualize
    rowGraph.setSourceVertex(0);
    rowGraph.setTerminalVertex(numVertices-1);
    std::vector<graphInterface::vertex_t> vertices;
    rowGraph.getVertices(vertices);
    for (int i=0; i<numTilesInRow+1; ++i)
    {
        for (int j=0; j<numEdgeColors-1; ++j)
        {
            vertices[1 + i*numEdgeColors + j]->setColor(j);
        }
    }
    rowGraph.writeDotFile("rowGraph");*/
}

void tiling::heuristicsImproveColumn(int col, std::vector<std::vector<tile::tcode>> &assMat, bool random, std::vector<std::vector<bool>> &penal, int penalDist, graphInterface::graph &transducer, bool noeven)
{
    // useful variables
    auto numEdgeColors = tileSet.getNumEdgeColors() + 1;
    auto numTilesInColumn = getHeight();
    auto numVertices = numEdgeColors * (numTilesInColumn + 1) + 1;
    double eps = 0.5 / (1 + numTilesInColumn);

    // create empty graph
    graphInterface::graph colGraph(numVertices);

    // source to north edges
    for (int i = 0; i < numEdgeColors - 1; ++i)
        colGraph.addEdge(0, i + 1, "-1", 0.0);

    // south to terminal edges
    for (int i = 0; i < numEdgeColors - 1; ++i)
        colGraph.addEdge(numEdgeColors * numTilesInColumn + i + 1, numVertices - 1, "-1", 0.0);

    // intermediate edges -- tiles
    auto tiles = tileSet.getTiles();
    for (unsigned int j = 0; j < tiles.size(); ++j)
    {
        for (int i = 0; i < numTilesInColumn; ++i)
        {
            // initialize cost
            double cost = 0.0;

            // even position
            if (noeven)
                if (i % 2 == 1)
                    cost = 1.01;

            // matching west tile
            if (cost < 1.0)
                if (col > 0)
                {
                    auto tileToWest = assMat[i][col - 1];
                    if (tileToWest != -1)
                    {
                        if (tiles[j]->getWest() != tiles[tileToWest]->getEast())
                            cost = 1.01;
                    }
                    else
                    {
                        // penalize enforced voids
                        if (transducer.getVertex(tiles[j]->getWest())->getIndegree() == 0)
                        {
                            cost += eps;
                            // penalize based on dual transducer graph
                        }
                        else
                        {
                            if (penalDist > 0)
                                if (col >= penalDist)
                                {
                                    auto pTile = assMat[col - penalDist][i];
                                    if (pTile < 0)
                                    {
                                        if (penal.back()[j] == false)
                                            cost += eps;
                                    }
                                    else
                                    {
                                        if (penal[pTile][j] == false)
                                            cost += eps;
                                    }
                                }
                        }
                    }
                }

            // matching east tile + penalize enforced voids
            if (cost < 1.0)
                if (col < getWidth() - 1)
                {
                    auto tileToEast = assMat[i][col + 1];
                    if (tileToEast != -1)
                    {
                        if (tiles[j]->getEast() != tiles[tileToEast]->getWest())
                            cost = 1.01;
                    }
                    else
                    {
                        // penalize enforced voids
                        if (transducer.getVertex(tiles[j]->getEast())->getOutdegree() == 0)
                            cost += eps;
                    }
                }

            // add tile if it can be placed
            if (cost < 1.0)
                colGraph.addEdge(i * numEdgeColors + 1 + tiles[j]->getNorth(),
                                 (i + 1) * numEdgeColors + 1 + tiles[j]->getSouth(),
                                 std::to_string(j), cost);
        }
    }

    // intermediate edges -- empty
    for (int i = 0; i < numTilesInColumn; ++i)
        for (int j = 0; j < numEdgeColors - 1; ++j)
            colGraph.addEdge(i * numEdgeColors + 1 + j,
                             (i + 1) * numEdgeColors, "0",
                             1.0);
    for (int i = 0; i < numTilesInColumn; ++i)
        for (int j = 0; j < numEdgeColors - 1; ++j)
            colGraph.addEdge((i + 1) * numEdgeColors,
                             (i + 1) * numEdgeColors + 1 + j, "-3",
                             0.0);

    // find shortest path
    std::vector<double> len;
    std::vector<graphInterface::edge_t> shortestPath;
    if (random)
        colGraph.randomizeEdgeOrder();
    colGraph.DAGShortestPath(0, len, shortestPath, false);

    // recover the shortest path
    int lastVertex = numVertices - 1;
    int position = getHeight() - 1;
    while (lastVertex != 0)
    {
        auto curEdge = shortestPath[lastVertex];
        // curEdge->setThickness(true);
        auto iLabel = std::stoi(curEdge->getLabel());
        if (iLabel >= 0)
        {
            if (curEdge->getWeight() < 1.0)
            {
                assMat[position][col] = iLabel;
            }
            else
            {
                assMat[position][col] = -1;
            }
            --position;
        }
        lastVertex = curEdge->getTailVertex();
    }

    /*// visualize
    colGraph.setSourceVertex(0);
    colGraph.setTerminalVertex(numVertices-1);
    std::vector<graphInterface::vertex_t> vertices;
    colGraph.getVertices(vertices);
    for (int i=0; i<numTilesInColumn+1; ++i)
    {
        for (int j=0; j<numEdgeColors-1; ++j)
        {
            vertices[1 + i*numEdgeColors + j]->setColor(j);
        }
    }
    colGraph.writeDotFile("colGraph");*/
}

int tiling::heuristicsSimple(bool randomized)
{
    // construct transducer graph
    graphInterface::graph dualTransducerGraph(0);
    tileSet.getDualTransducerGraph(dualTransducerGraph);

    // initialize assembly plan
    assembly.resize(1);
    std::vector<std::vector<tile::tcode>> &assMat = assembly[0];
    assMat.resize(getHeight());
    for (int i = 0; i < getHeight(); i++)
        assMat[i].resize(getWidth(), -1);

    // max row-cover heuristics
    std::vector<std::vector<bool>> emptySet;
    int maxDist = 0;
    for (int row = 0; row < getHeight(); row += 1)
        heuristicsImproveRow(row, assMat, randomized, emptySet, maxDist, dualTransducerGraph, false);

    int now = 0;
    for (int i = 0; i < getHeight(); i++)
        for (int j = 0; j < getWidth(); j++)
            if (assMat[i][j] == -1)
                now += 1;

    feasible = true;
    return getWidth() * getHeight() - now;
}

int tiling::heuristics050(bool randomized)
{
    // construct dual transducer graph
    graphInterface::graph dualTransducerGraph(0);
    tileSet.getDualTransducerGraph(dualTransducerGraph);

    // find tile-long compatible colors in dual transducer
    std::vector<std::vector<bool>> compat2(dualTransducerGraph.getNumVertices() + 1, std::vector<bool>(dualTransducerGraph.getNumVertices(), false));
    for (const auto &e : dualTransducerGraph.getEdges())
    {
        compat2[e->getTailVertex()][e->getTailVertex()] = true;
        compat2[dualTransducerGraph.getNumVertices()][e->getTailVertex()] = true;
    }

    // initialize assembly plan
    assembly.resize(1);
    std::vector<std::vector<tile::tcode>> &assMat = assembly[0];
    assMat.resize(getHeight());
    for (int i = 0; i < getHeight(); i++)
        assMat[i].resize(getWidth(), -1);

    // max row-cover heuristics
    for (int row = 0; row < getHeight(); row += 2)
        heuristicsImproveRow(row, assMat, randomized, compat2, 1, dualTransducerGraph, false);
    for (int row = 1; row < getHeight(); row += 2)
        heuristicsImproveRow(row, assMat, randomized, compat2, 0, dualTransducerGraph, false);

    int now = 0;
    for (int i = 0; i < getHeight(); i++)
        for (int j = 0; j < getWidth(); j++)
            if (assMat[i][j] == -1)
                now += 1;

    feasible = true;
    return getWidth() * getHeight() - now;
}

int tiling::heuristics067(bool randomized)
{
    // construct transducer and dual transducer graph
    graphInterface::graph dualTransducerGraph(0);
    tileSet.getDualTransducerGraph(dualTransducerGraph);

    // find tile-long compatible colors in dual transducer
    std::vector<std::vector<bool>> compat2(dualTransducerGraph.getNumVertices() + 1, std::vector<bool>(dualTransducerGraph.getNumVertices(), false));
    for (const auto &e : dualTransducerGraph.getEdges())
    {
        compat2[e->getTailVertex()][e->getTailVertex()] = true;
        compat2[dualTransducerGraph.getNumVertices()][e->getTailVertex()] = true;
    }

    // find two-tile long compatible colors in dual transducer
    std::vector<std::vector<bool>> compat3(dualTransducerGraph.getNumVertices() + 1, std::vector<bool>(dualTransducerGraph.getNumVertices(), false));
    for (int i = 0; i < dualTransducerGraph.getNumVertices(); ++i)
    {
        for (auto it = compat2[i].begin(); it != compat2[i].end(); ++it)
        {
            auto j = *it;
            for (auto ij = compat2[j].begin(); ij != compat2[j].end(); ++ij)
            {
                compat3[i][*ij] = true;
                compat3[dualTransducerGraph.getNumVertices()][*ij] = true;
            }
        }
    }

    // initialize assembly plan
    assembly.resize(1);
    std::vector<std::vector<tile::tcode>> &assMat = assembly[0];
    assMat.resize(getHeight());
    for (int i = 0; i < getHeight(); i++)
        assMat[i].resize(getWidth(), -1);

    // max row-cover heuristics
    for (int row = 0; row < getHeight(); row += 3)
        heuristicsImproveRow(row, assMat, randomized, compat3, 2, dualTransducerGraph, false);
    for (int row = 2; row < getHeight(); row += 3)
        heuristicsImproveRow(row, assMat, randomized, compat2, 1, dualTransducerGraph, true);
    for (int row = 1; row < getHeight(); row += 3)
        heuristicsImproveRow(row, assMat, randomized, compat2, 0, dualTransducerGraph, false);
    for (int row = 2; row < getHeight(); row += 3)
        heuristicsImproveRow(row, assMat, randomized, compat2, 0, dualTransducerGraph, false);

    int now = 0;
    for (int i = 0; i < getHeight(); i++)
        for (int j = 0; j < getWidth(); j++)
            if (assMat[i][j] == -1)
                now += 1;

    feasible = true;
    return getWidth() * getHeight() - now;
}

int tiling::heuristics(bool randomized, bool improved, int algNum)
{
    // initial cover
    int out = 0;
    switch (algNum)
    {
    case 0:
    {
        out = heuristicsSimple(randomized);
        break;
    }

    case 1:
    {
        out = heuristics050(randomized);
        break;
    }

    case 2:
    {
        out = heuristics067(randomized);
        break;
    }

    default:
    {
        std::cerr << "ERROR in tiling::heuristics(): Unknown algorithm.";
        exit(2);
    }
    }

    bool rowWise = false;
    int improvement = std::numeric_limits<int>::max();
    int prev;
    int now = getWidth() * getHeight() - out;
    int it = 0;
    std::vector<std::vector<bool>> emptySet;
    auto &assMat = this->assembly[0];
    graphInterface::graph dualTransducerGraph(0), transducerGraph(0);
    tileSet.getDualTransducerGraph(dualTransducerGraph);
    tileSet.getTransducerGraph(transducerGraph);

    std::vector<bool> rowToChange(getHeight(), false);
    std::vector<bool> colToChange(getWidth(), false);
    for (int i = 0; i < getHeight(); i++)
        for (int j = 0; j < getWidth(); j++)
            if (this->assembly[0][i][j] == -1)
            {
                colToChange[j] = true;
                rowToChange[i] = true;
                break;
            }

    std::cout << "Tiling heuristics: it 0(" << 0 << ")"
              << ": " << now << " empty slots." << std::endl;
    rowWise = false;

    // final maxcover heristics
    if (improved)
    {
        while (true)
        {
            it += 1;

            if ((improvement == 0) | (now == 0))
                break;

            if (rowWise)
            {
                for (int row = 0; row < getHeight(); row++)
                    if (rowToChange[row])
                        heuristicsImproveRow(row, assMat, randomized, emptySet, 0, dualTransducerGraph, false);

                rowWise = false;
            }
            else
            {
                for (int col = 0; col < getWidth(); col++)
                    if (colToChange[col])
                        heuristicsImproveColumn(col, assMat, randomized, emptySet, 0, transducerGraph, false);

                rowWise = true;
            }

            for (int row = 0; row < getHeight(); row++)
                rowToChange[row] = false;
            for (int col = 0; col < getWidth(); col++)
                colToChange[col] = false;

            prev = now;
            now = 0;
            for (int i = 0; i < getHeight(); i++)
                for (int j = 0; j < getWidth(); j++)
                    if (assMat[i][j] == -1)
                    {
                        now += 1;
                        colToChange[j] = true;
                        rowToChange[i] = true;
                    }

            improvement = prev - now;
            if (improvement < 0)
            {
                std::cout << "ERROR in tiling::heuristics(): negative improvement!" << std::endl;
                break;
            }

            if (!improved)
                break;

            std::cout << "Tiling heuristics: it " << it << "(" << 1 - rowWise << ")"
                      << ": " << now << " empty slots." << std::endl;
        }
    }

    feasible = true;

    return getWidth() * getHeight() - now;
}

tiling::tiling(tileset tset, int w, int h)
{
    allSolutions = 0;
    setWidth(w);
    setHeight(h);
    feasible = 0;
    periodic = 0;
    tileSet = tset;
    setFormulationType(LP_TILES);
    packing = false;
    setObjType(objTypes::NONE);
    setSolutionsCount(1);
}

tiling::tiling()
{
    allSolutions = 0;
    width = 0;
    height = 0;
    feasible = 0;
    periodic = 0;
    packing = 0;
    setFormulationType(LP_TILES);
    setObjType(objTypes::NONE);
    setSolutionsCount(1);
}

void tiling::infiniteRowColumnReduce()
{
    tileSet.resetRedundantTiles();
    tileSet.findStronglyConnectedComponents();
    tileSet.removeRedundantTiles();

    int numColors;
    switch (tileSet.getSetType())
    {
    case tileset::setTypes::EDGE_BASED:
    {
        numColors = tileSet.getNumEdgeColors();
        break;
    }

    case tileset::setTypes::VERTEX_BASED:
    {
        numColors = tileSet.getNumVertexColors();
        break;
    }

    default:
    {
        std::cerr << "ERROR in tiling::infiniteRowColumnReduce(): Unknown set type!" << std::endl;
        exit(2);
        break;
    }
    }
    std::cout << "The tile set was reduced to " << tileSet.getNumTiles() << " tiles over " << numColors << " colors." << std::endl;

    tileSet.findTilingGroups();
}

void tiling::depthInspectTileSet(int depth, optimizerInterface::solverModel::solvers solver)
{
    for (int i = 0; i < tileSet.getNumTiles(); i++)
    {
        std::cout << "Processing tile " << i << "/" << tileSet.getNumTiles() - 1 << "... ";
        tiling newTiling;
        newTiling.tileSet = tileSet;
        newTiling.width = 2 * depth + 1;
        newTiling.height = 2 * depth + 1;
        newTiling.periodic = false;
        newTiling.formulation = formulation;
        newTiling.addBoundaryCondition(std::vector<int>{depth, depth, i});
        newTiling.callSolver(solver);
        if (newTiling.getFeasibility() == false)
        {
            std::cout << "Redundant!" << std::endl;
            tileSet.setRedundantTile(i);
        }
        else
        {
            std::cout << "Ok" << std::endl;
        }
    }
    tileSet.removeRedundantTiles();

    int numColors;
    switch (tileSet.getSetType())
    {
    case tileset::setTypes::EDGE_BASED:
    {
        numColors = tileSet.getNumEdgeColors();
        break;
    }

    case tileset::setTypes::VERTEX_BASED:
    {
        numColors = tileSet.getNumVertexColors();
        break;
    }

    default:
    {
        std::cerr << "ERROR in tiling::depthInspectTileSet: Unknown set type!" << std::endl;
        exit(2);
        break;
    }
    }
    std::cout << "The tile set was reduced to " << tileSet.getNumTiles() << " tiles over " << numColors << " colors." << std::endl;
}

void tiling::findNecessaryTile(int w, int h, optimizerInterface::solverModel::solvers solver, tile::tcode tile)
{
    tiling newTiling;
    tileset newSet = tileSet;
    newSet.resetRedundantTiles();
    newSet.setRedundantTile(tile);
    newSet.removeRedundantTiles();
    newTiling.tileSet = newSet;
    newTiling.width = w;
    newTiling.height = h;
    newTiling.periodic = false;
    newTiling.formulation = formulation;
    newTiling.callSolver(solver);
    if (newTiling.getFeasibility() == false)
    {
        std::cout << "Necessary!" << std::endl;
    }
    else
    {
        std::cout << "\033[1;31mTile " << tile << " may be unnecessary.\033[0m" << std::endl;
    }
}

void tiling::findNecessaryTiles(int w, int h, optimizerInterface::solverModel::solvers solver)
{
    for (int i = 0; i < tileSet.getNumTiles(); i++)
    {
        std::cout << "Processing tile " << i << "/" << tileSet.getNumTiles() - 1 << "... ";
        findNecessaryTile(w, h, solver, i);
    }
}

void tiling::addBoundaryCondition(std::vector<int> boundaryCondition)
{
    if (boundaryCondition.size() == 3)
    {
        if ((boundaryCondition[0] < 0) | (boundaryCondition[0] >= getHeight()))
        {
            std::cerr << "ERROR in tiling::addBoundaryCondition(): Boundary condition does not correspond with the tiling height." << std::endl;
            exit(2);
        }
        else if ((boundaryCondition[1] < 0) | (boundaryCondition[1] >= getWidth()))
        {
            std::cerr << "ERROR in tiling::addBoundaryCondition(): Boundary condition does not correspond with the tiling width." << std::endl;
            exit(2);
        }
        else if ((boundaryCondition[2] < 0) | (boundaryCondition[2] >= tileSet.getNumTiles()))
        {
            std::cerr << "ERROR in tiling::addBoundaryCondition(): Boundary condition does not correspond with the number of tiles in the tile set." << std::endl;
            exit(2);
        }
        boundaryConditions.push_back(boundaryCondition);
    }
    else
    {
        std::cerr << "ERROR in tiling::addBoundaryCondition(): Boundary condition is of bad size." << std::endl;
        exit(2);
    }
}

void tiling::addRandomBoundaryCondition()
{
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<int> randW(0, getWidth() - 1), randH(0, getHeight() - 1);
    auto rW = randW(rng);
    auto rH = randH(rng);

    // get not redundant tiles
    std::vector<int> notRedundant;
    for (tile::tcode tile = 0; tile < (tile::tcode)tileSet.getNumTiles(); tile++)
    {
        if (tileSet.isRedundantTile(tile) == false)
        {
            notRedundant.push_back(tile);
        }
    }

    // randomly pick one non-redundant tile
    if (notRedundant.size() > 0)
    {
        std::uniform_int_distribution<int> randT(0, notRedundant.size() - 1);
        auto rT = randT(rng);
        std::vector<int> boundaryCondition = {rH, rW, notRedundant[rT]};
        addBoundaryCondition(boundaryCondition);
        std::cout << "Tile " << rT << " randomly placed at (" << rH << ", " << rW << ")." << std::endl;
    }
    else
    {
        std::cerr << "Warning: All tiles are redundant. Can not generate a random boundary condition." << std::endl;
    }
}

void tiling::removeBoundaryCondition(int position)
{
    if (position < (int)boundaryConditions.size())
    {
        boundaryConditions.erase(boundaryConditions.begin() + position);
    }
    else
    {
        std::cerr << "ERROR in tiling::removeBoundaryCondition(): No such boundary condition exists!" << std::endl;
        exit(2);
    }
}

void tiling::removeBoundaryConditions()
{
    boundaryConditions.clear();
}

void tiling::setFormulationType(tiling::formulationTypes formulation)
{
    tiling::formulation = formulation;
}

void tiling::printAssemblyMatrix(int num) const
{
    for (int row = 0; row < getHeight(); row++)
    {
        for (int col = 0; col < getWidth(); col++)
        {
            std::cout << assembly[num][row][col] << "\t";
        }
        std::cout << std::endl;
    }
}

void tiling::initializeTileVariables(std::shared_ptr<optimizerInterface::solverModel> &model, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsTiles)
{
    varsTiles.resize(getHeight() * getWidth() * tileSet.getNumTiles());

    for (int row = 0; row < getHeight(); row++)
    {
        for (int col = 0; col < getWidth(); col++)
        {
            std::vector<int> sos;
            for (tile::tcode tile = 0; tile < (tile::tcode)tileSet.getNumTiles(); tile++)
            {
                int varNumber = matCoorToVec(row, col, tile);
                sos.push_back(varNumber);
                if (getFormulationType() == SDP_TILES)
                {
                    model->addContinuousVariable(varNumber, varsTiles[varNumber], -1.0, 1.0);
                }
                else if ((getFormulationType() == LP_BNB_TILES) | (getFormulationType() == SDP_TILESBIN))
                {
                    model->addContinuousVariable(varNumber, varsTiles[varNumber], 0.0, 1.0);
                }
                else
                {
                    model->addBinaryVariable(varNumber, varsTiles[varNumber]);
                }
            }
            if ((getObjType() == objTypes::MAXCONAREA) | (getObjType() == objTypes::MAXCONSQUARE) | (getObjType() == objTypes::MINPERIODIC) | (getObjType() == objTypes::MAXCOVER))
            {
                model->addSos(sos, optimizerInterface::solverModel::sosTypes::ATMOSTONE);
            }
            else
            {
                model->addSos(sos, optimizerInterface::solverModel::sosTypes::EXACTONE);
            }
        }
    }

    // define integer variables
    if (getFormulationType() == SDP_TILES)
    {
        std::vector<double> set = {-1.0, 1.0};
        model->constrainToDoubleSet(set);
    }
    else
    {
        std::vector<double> set = {0.0, 1.0};
        model->constrainToDoubleSet(set);
    }

    // non-convex integrality constraints for QP
    if (getFormulationType() == QP_TILES)
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                {
                    optimizerInterface::quadExpression quadExp;
                    int varNumber = matCoorToVec(row, col, tile);
                    quadExp.add(varsTiles[varNumber], varsTiles[varNumber]);
                    quadExp.add(varsTiles[varNumber], -1.0);
                    model->addQuadraticConstraint(quadExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                }
            }
        }
    }
}

void tiling::preventOverlaps(std::shared_ptr<optimizerInterface::solverModel> &model, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsTiles)
{
    switch (getObjType())
    {
    case objTypes::NONE:
    case objTypes::NONE_TWEAKED:
    case objTypes::REDUNDANCY:
    case objTypes::MAXCSP:
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                optimizerInterface::linExpression linExp;

                for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                {
                    linExp.add(varsTiles[matCoorToVec(row, col, tile)]);
                }

                switch (getFormulationType())
                {
                case LP_TILES:
                case LP_BNB_TILES:
                case QP_TILES:
                case SDP_TILESBIN:
                {
                    if ((row == 0) | (col == 0) | (row == getHeight() - 1) | (col == getWidth() - 1))
                    {
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 1.0);
                    }
                    break;
                }

                case SDP_TILES:
                {
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 2.0 - tileSet.getNumTiles());
                    break;
                }

                default:
                {
                    std::cerr << "ERROR in tiling::preventOverlaps(): Unknown formulation type!" << std::endl;
                    exit(2);
                    break;
                }
                }
            }
        }
        break;
    }

    case objTypes::MAXCONAREA:
    case objTypes::MAXCONSQUARE:
    case objTypes::MINPERIODIC:
    case objTypes::MAXCOVER:
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                optimizerInterface::linExpression linExp;

                for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                {
                    linExp.add(varsTiles[matCoorToVec(row, col, tile)]);
                }

                switch (getFormulationType())
                {
                case LP_TILES:
                case LP_BNB_TILES:
                case QP_TILES:
                case SDP_TILESBIN:
                {
                    if (((col == 0) & (row == 0)) & (getObjType() != objTypes::MAXCOVER))
                    {
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 1.0);
                    }
                    else
                    {
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 1.0);
                    }
                    break;
                }

                case SDP_TILES:
                {
                    if (((col == 0) & (row == 0)) & (getObjType() != objTypes::MAXCOVER))
                    {
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 2.0 - tileSet.getNumTiles());
                    }
                    else
                    {
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 2.0 - tileSet.getNumTiles());
                    }
                    break;
                }

                default:
                {
                    std::cerr << "ERROR in tiling::preventOverlaps(): Unknown formulation type!" << std::endl;
                    exit(2);
                    break;
                }
                }
            }
        }
        break;
    }

    default:
    {
        std::cerr << "ERROR in tiling::preventOverlaps(): Unknown objective!" << std::endl;
        exit(2);
        break;
    }
    }
}

void tiling::adjacencyVerticalEdges(std::shared_ptr<optimizerInterface::solverModel> &model, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsTiles, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsEdgesV)
{
    // edges colors
    const std::vector<std::vector<tile::tcode>> &east = tileSet.getEast();
    const std::vector<std::vector<tile::tcode>> &west = tileSet.getWest();

    switch (getObjType())
    {
    case objTypes::NONE:
    case objTypes::REDUNDANCY:
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth() - 1; col++)
            {
                for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
                {
                    if ((east[edgc].size() > 0) || (west[edgc].size() > 0))
                    {
                        optimizerInterface::linExpression linExp;

                        double rhs = 0.0;
                        for (tile::tcode tile = 0; tile < (tile::tcode)east[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col, east[edgc][tile])]);
                            rhs += 1.0;
                        }

                        for (tile::tcode tile = 0; tile < (tile::tcode)west[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col + 1, west[edgc][tile])], -1.0);
                            rhs -= 1.0;
                        }

                        switch (getFormulationType())
                        {
                        case LP_TILES:
                        case LP_BNB_TILES:
                        case QP_TILES:
                        case SDP_TILESBIN:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                            break;
                        }

                        case SDP_TILES:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                            break;
                        }

                        default:
                        {
                            std::cerr << "ERROR in tiling::adjacencyVerticalEdges(): Unknown formulation type!" << std::endl;
                            exit(2);
                            break;
                        }
                        }
                    }
                }
            }
        }
        break;
    }

    case objTypes::MAXCSP:
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth() - 1; col++)
            {
                for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
                {
                    if ((east[edgc].size() > 0) || (west[edgc].size() > 0))
                    {
                        optimizerInterface::linExpression linExpA, linExpB;

                        for (tile::tcode tile = 0; tile < (tile::tcode)east[edgc].size(); tile++)
                        {
                            linExpA.add(varsTiles[matCoorToVec(row, col, east[edgc][tile])]);
                            linExpB.add(varsTiles[matCoorToVec(row, col, east[edgc][tile])], -1.0);
                        }

                        for (tile::tcode tile = 0; tile < (tile::tcode)west[edgc].size(); tile++)
                        {
                            linExpA.add(varsTiles[matCoorToVec(row, col + 1, west[edgc][tile])], -1.0);
                            linExpB.add(varsTiles[matCoorToVec(row, col + 1, west[edgc][tile])]);
                        }

                        linExpA.add(varsEdgesV[row + col * getHeight()], -1.0);
                        linExpB.add(varsEdgesV[row + col * getHeight()], -1.0);

                        switch (getFormulationType())
                        {
                        case LP_TILES:
                        case LP_BNB_TILES:
                        case QP_TILES:
                        case SDP_TILESBIN:
                        {
                            model->addLinearConstraint(linExpA, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 0.0);
                            model->addLinearConstraint(linExpB, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 0.0);
                            break;
                        }

                        case SDP_TILES:
                        {
                            std::cerr << "ERROR: Not supported yet!" << std::endl;
                            exit(2);
                            // model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                            break;
                        }

                        default:
                        {
                            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
                            exit(2);
                            break;
                        }
                        }
                    }
                }
            }
        }
        break;
    }

    case objTypes::MAXCONAREA:
    case objTypes::MAXCONSQUARE:
    case objTypes::MINPERIODIC:
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth() - 1; col++)
            {
                for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
                {
                    if ((east[edgc].size() > 0) || (west[edgc].size() > 0))
                    {
                        optimizerInterface::linExpression linExp;

                        double rhs = 0.0;
                        for (tile::tcode tile = 0; tile < (tile::tcode)east[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col, east[edgc][tile])]);
                            rhs += 1.0;
                        }

                        for (tile::tcode tile = 0; tile < (tile::tcode)west[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col + 1, west[edgc][tile])], -1.0);
                            rhs -= 1.0;
                        }

                        switch (getFormulationType())
                        {
                        case LP_TILES:
                        case LP_BNB_TILES:
                        case QP_TILES:
                        case SDP_TILESBIN:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::GREATEREQUAL, 0.0);
                            break;
                        }

                        case SDP_TILES:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::GREATEREQUAL, -rhs);
                            break;
                        }

                        default:
                        {
                            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
                            exit(2);
                            break;
                        }
                        }
                    }
                }
            }
        }
        break;
    }

    case objTypes::MAXCOVER:
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth() - 1; col++)
            {
                for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
                {
                    if ((east[edgc].size() > 0) || (west[edgc].size() > 0))
                    {
                        optimizerInterface::linExpression linExp;

                        double rhs = 0.0;
                        for (tile::tcode tile = 0; tile < (tile::tcode)east[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col, east[edgc][tile])]);
                            rhs += 1.0;
                        }

                        for (tile::tcode tile = 0; tile < tileSet.getNumTiles(); tile++)
                        {
                            if (!(std::find(west[edgc].begin(), west[edgc].end(), tile) != west[edgc].end()))
                            {
                                linExp.add(varsTiles[matCoorToVec(row, col + 1, tile)]);
                                rhs += 1.0;
                            }
                        }

                        switch (getFormulationType())
                        {
                        case LP_TILES:
                        case LP_BNB_TILES:
                        case QP_TILES:
                        case SDP_TILESBIN:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 1.0);
                            break;
                        }

                        case SDP_TILES:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, -rhs + 2.0);
                            break;
                        }

                        default:
                        {
                            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
                            exit(2);
                            break;
                        }
                        }
                    }
                }
            }
        }
        break;
    }

    case objTypes::NONE_TWEAKED:
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth() - 1; col++)
            {
                optimizerInterface::linExpression linExp;
                double rhs = 0.0;

                for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                {
                    linExp.add(varsTiles[matCoorToVec(row, col, tile)], tileSet.getEdgeColor(tile, tileset::edgeTypes::EAST) + 100000 + 1.0);
                    rhs += (tileSet.getEdgeColor(tile, tileset::edgeTypes::EAST) + 100000 + 1);
                }

                for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                {
                    linExp.add(varsTiles[matCoorToVec(row, col + 1, tile)], (1 + 100000 + tileSet.getEdgeColor(tile, tileset::edgeTypes::WEST)) * (-1));
                    rhs -= (tileSet.getEdgeColor(tile, tileset::edgeTypes::WEST) + 100000 + 1);
                }

                switch (getFormulationType())
                {
                case LP_TILES:
                case LP_BNB_TILES:
                case QP_TILES:
                case SDP_TILESBIN:
                {
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                    break;
                }

                case SDP_TILES:
                {
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                    break;
                }

                default:
                {
                    std::cerr << "ERROR: Unknown formulation type!" << std::endl;
                    exit(2);
                    break;
                }
                }
            }
        }
        break;
        break;
    }

    default:
    {
        std::cerr << "ERROR: Unknown objective!" << std::endl;
        exit(2);
        break;
    }
    }
}

void tiling::adjacencyHorizontalEdges(std::shared_ptr<optimizerInterface::solverModel> &model, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsTiles, std::vector<std::shared_ptr<optimizerInterface::variable>> &varsEdgesH)
{
    // edges colors
    const std::vector<std::vector<tile::tcode>> &north = tileSet.getNorth();
    const std::vector<std::vector<tile::tcode>> &south = tileSet.getSouth();

    switch (getObjType())
    {
    case objTypes::NONE:
    case objTypes::REDUNDANCY:
    {
        for (int row = 0; row < getHeight() - 1; row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
                {
                    if ((north[edgc].size() > 0) || (south[edgc].size() > 0))
                    {
                        optimizerInterface::linExpression linExp;

                        double rhs = 0.0;
                        for (tile::tcode tile = 0; tile < (tile::tcode)south[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col, south[edgc][tile])]);
                            rhs += 1.0;
                        }

                        for (tile::tcode tile = 0; tile < (tile::tcode)north[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row + 1, col, north[edgc][tile])], -1.0);
                            rhs -= 1.0;
                        }

                        switch (getFormulationType())
                        {
                        case LP_TILES:
                        case LP_BNB_TILES:
                        case QP_TILES:
                        case SDP_TILESBIN:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                            break;
                        }

                        case SDP_TILES:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                            break;
                        }

                        default:
                        {
                            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
                            exit(2);
                            break;
                        }
                        }
                    }
                }
            }
        }
        break;
    }

    case objTypes::MAXCSP:
    {
        for (int row = 0; row < getHeight() - 1; row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
                {
                    if ((north[edgc].size() > 0) || (south[edgc].size() > 0))
                    {
                        optimizerInterface::linExpression linExpA, linExpB;

                        for (tile::tcode tile = 0; tile < (tile::tcode)south[edgc].size(); tile++)
                        {
                            linExpA.add(varsTiles[matCoorToVec(row, col, south[edgc][tile])]);
                            linExpB.add(varsTiles[matCoorToVec(row, col, south[edgc][tile])], -1.0);
                        }

                        for (tile::tcode tile = 0; tile < (tile::tcode)north[edgc].size(); tile++)
                        {
                            linExpA.add(varsTiles[matCoorToVec(row + 1, col, north[edgc][tile])], -1.0);
                            linExpB.add(varsTiles[matCoorToVec(row + 1, col, north[edgc][tile])]);
                        }

                        linExpA.add(varsEdgesH[row + col * (getHeight() - 1)], -1.0);
                        linExpB.add(varsEdgesH[row + col * (getHeight() - 1)], -1.0);

                        switch (getFormulationType())
                        {
                        case LP_TILES:
                        case LP_BNB_TILES:
                        case QP_TILES:
                        case SDP_TILESBIN:
                        {
                            model->addLinearConstraint(linExpA, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 0.0);
                            model->addLinearConstraint(linExpB, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 0.0);
                            break;
                        }

                        case SDP_TILES:
                        {
                            std::cerr << "ERROR: Not supported yet!" << std::endl;
                            exit(2);
                            // model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                            break;
                        }

                        default:
                        {
                            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
                            exit(2);
                            break;
                        }
                        }
                    }
                }
            }
        }
        break;
    }

    case objTypes::MAXCONAREA:
    case objTypes::MAXCONSQUARE:
    case objTypes::MINPERIODIC:
    {
        for (int row = 0; row < getHeight() - 1; row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
                {
                    if ((north[edgc].size() > 0) || (south[edgc].size() > 0))
                    {
                        optimizerInterface::linExpression linExp;

                        double rhs = 0.0;
                        for (tile::tcode tile = 0; tile < (tile::tcode)south[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col, south[edgc][tile])]);
                            rhs += 1.0;
                        }

                        for (tile::tcode tile = 0; tile < (tile::tcode)north[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row + 1, col, north[edgc][tile])], -1.0);
                            rhs -= 1.0;
                        }

                        switch (getFormulationType())
                        {
                        case LP_TILES:
                        case LP_BNB_TILES:
                        case QP_TILES:
                        case SDP_TILESBIN:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::GREATEREQUAL, 0.0);
                            break;
                        }

                        case SDP_TILES:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::GREATEREQUAL, -rhs);
                            break;
                        }

                        default:
                        {
                            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
                            exit(2);
                            break;
                        }
                        }
                    }
                }
            }
        }
        break;
    }

    case objTypes::MAXCOVER:
    {
        for (int row = 0; row < getHeight() - 1; row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
                {
                    if ((north[edgc].size() > 0) || (south[edgc].size() > 0))
                    {
                        optimizerInterface::linExpression linExp;

                        double rhs = 0.0;
                        for (tile::tcode tile = 0; tile < (tile::tcode)south[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col, south[edgc][tile])]);
                            rhs += 1.0;
                        }

                        for (tile::tcode tile = 0; tile < tileSet.getNumTiles(); tile++)
                        {
                            if (!(std::find(north[edgc].begin(), north[edgc].end(), tile) != north[edgc].end()))
                            {
                                linExp.add(varsTiles[matCoorToVec(row + 1, col, tile)]);
                                rhs += 1.0;
                            }
                        }

                        switch (getFormulationType())
                        {
                        case LP_TILES:
                        case LP_BNB_TILES:
                        case QP_TILES:
                        case SDP_TILESBIN:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 1.0);
                            break;
                        }

                        case SDP_TILES:
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, -rhs + 2.0);
                            break;
                        }

                        default:
                        {
                            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
                            exit(2);
                            break;
                        }
                        }
                    }
                }
            }
        }
        break;
    }

    case objTypes::NONE_TWEAKED:
    {
        for (int row = 0; row < getHeight() - 1; row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                optimizerInterface::linExpression linExp;
                double rhs = 0.0;

                for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                {
                    linExp.add(varsTiles[matCoorToVec(row, col, tile)], tileSet.getEdgeColor(tile, tileset::edgeTypes::SOUTH) + 100000 + 1);
                    rhs += (tileSet.getEdgeColor(tile, tileset::edgeTypes::SOUTH) + 100000 + 1);
                }

                for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                {
                    linExp.add(varsTiles[matCoorToVec(row + 1, col, tile)], -1 - 100000 - tileSet.getEdgeColor(tile, tileset::edgeTypes::NORTH));
                    rhs -= (tileSet.getEdgeColor(tile, tileset::edgeTypes::NORTH) + 100000 + 1);
                }

                switch (getFormulationType())
                {
                case LP_TILES:
                case LP_BNB_TILES:
                case QP_TILES:
                case SDP_TILESBIN:
                {
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                    break;
                }

                case SDP_TILES:
                {
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                    break;
                }

                default:
                {
                    std::cerr << "ERROR: Unknown formulation type!" << std::endl;
                    exit(2);
                    break;
                }
                }
            }
        }
        break;
        break;
    }

    default:
    {
        std::cerr << "ERROR: Unknown objective!" << std::endl;
        exit(2);
        break;
    }
    }
}

void tiling::callSolver(optimizerInterface::solverModel::solvers solver)
{
    // create optimization environment
    optimizerInterface::optimizerEnvironment environment(solver);

    // create optimization model
    std::shared_ptr<optimizerInterface::solverModel> model;
    environment.initializeModel(model);

    // initialize 3d array of pointers to design variables
    std::vector<std::shared_ptr<optimizerInterface::variable>> varsTiles(getHeight() * getWidth() * tileSet.getNumTiles());

    // create variables belonging to tiles
    initializeTileVariables(model, varsTiles);

    // initialize matrix variable that is used in SDP
    std::shared_ptr<optimizerInterface::variable> lmi;
    if ((getFormulationType() == SDP_TILES) | (getFormulationType() == SDP_TILESBIN))
    {
        model->addSemidefiniteVariable(lmi, getHeight() * getWidth() * tileSet.getNumTiles() + 1);
    }

    // initialize variables for edges in max-CSP formulation
    std::vector<std::shared_ptr<optimizerInterface::variable>> varsEdgesV(getHeight() * (getWidth() - 1));
    std::vector<std::shared_ptr<optimizerInterface::variable>> varsEdgesH((getHeight() - 1) * getWidth());
    if (getObjType() == objTypes::MAXCSP)
    {
        for (unsigned int i = 0; i < varsEdgesV.size(); i++)
        {
            model->addContinuousVariable(-1 - i, varsEdgesV[i]);
        }

        for (unsigned int i = 0; i < varsEdgesH.size(); i++)
        {
            model->addContinuousVariable(-1 - i - varsEdgesV.size(), varsEdgesH[i]);
        }
    }

    // at most one tile per position
    preventOverlaps(model, varsTiles);

    // adjacency constraint - east-west edges
    adjacencyVerticalEdges(model, varsTiles, varsEdgesV);

    // adjacency constraint - east-west edges
    adjacencyHorizontalEdges(model, varsTiles, varsEdgesH);

    // edges colors
    const std::vector<std::vector<tile::tcode>> &north = tileSet.getNorth();
    const std::vector<std::vector<tile::tcode>> &south = tileSet.getSouth();
    const std::vector<std::vector<tile::tcode>> &east = tileSet.getEast();
    const std::vector<std::vector<tile::tcode>> &west = tileSet.getWest();

    // add vertical periodic constraint
    if (isPeriodic())
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
            {
                if ((east[edgc].size() > 0) || (west[edgc].size() > 0))
                {
                    optimizerInterface::linExpression linExp;

                    double rhs = 0.0;
                    for (tile::tcode tile = 0; tile < (tile::tcode)east[edgc].size(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(row, getWidth() - 1, east[edgc][tile])]);
                        rhs += 1.0;
                    }

                    for (tile::tcode tile = 0; tile < (tile::tcode)west[edgc].size(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(row, 0, west[edgc][tile])], -1.0);
                        rhs -= 1.0;
                    }

                    if (getFormulationType() == SDP_TILES)
                    {
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                    }
                    else
                    {
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                    }
                }
            }
        }
    }

    // add horizontal periodic constraint
    if (isPeriodic())
    {
        for (int col = 0; col < getWidth(); col++)
        {
            for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
            {
                if ((north[edgc].size() > 0) || (south[edgc].size() > 0))
                {
                    optimizerInterface::linExpression linExp;

                    double rhs = 0.0;
                    for (tile::tcode tile = 0; tile < (tile::tcode)south[edgc].size(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(getHeight() - 1, col, south[edgc][tile])]);
                        rhs += 1.0;
                    }

                    for (tile::tcode tile = 0; tile < (tile::tcode)north[edgc].size(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(0, col, north[edgc][tile])], -1.0);
                        rhs -= 1.0;
                    }

                    if (getFormulationType() == SDP_TILES)
                    {
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                    }
                    else
                    {
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                    }
                }
            }
        }
    }

    /*
    // additional diagonal constraints for vertex-based tiles
    if (tileSet.getSetType() == tileset::setTypes::VERTEX_BASED)
    {
        // colors of vertices
        const std::vector<std::vector<tile::tcode> > & northwest = tileSet.getNorthWest();
        const std::vector<std::vector<tile::tcode> > & northeast = tileSet.getNorthEast();
        const std::vector<std::vector<tile::tcode> > & southwest = tileSet.getSouthWest();
        const std::vector<std::vector<tile::tcode> > & southeast = tileSet.getSouthEast();

        // diagonal connectivity right down
        for (int col=0; col<getWidth()-1; col++)
        {
            for (int row=0; row<getHeight()-1; row++)
            {
                for (int verc=0; verc<tileSet.getNumVertexColors(); verc++)
                {
                    if ((northwest[verc].size()>0) || (southeast[verc].size()>0))
                    {
                        optimizerInterface::linExpression linExp;

                        double rhs = 0.0;
                        for (tile::tcode tile=0; tile<(tile::tcode)southeast[verc].size(); tile++)
                        {
                            linExp.add(varsTiles[ matCoorToVec(row, col, southeast[verc][tile]) ]);
                            rhs += 1.0;
                        }

                        if (getObjType()==objTypes::MAXCOVER)
                        {
                            for (tile::tcode tile=0; tile<tileSet.getNumTiles(); tile++)
                            {
                                if (!(std::find(northwest[verc].begin(), northwest[verc].end(), tile) != northwest[verc].end()))
                                {
                                    linExp.add(varsTiles[ matCoorToVec(row+1, col+1, tile) ]);
                                }
                            }
                        } else {
                            for (tile::tcode tile=0; tile<(tile::tcode)northwest[verc].size(); tile++)
                            {
                                linExp.add(varsTiles[ matCoorToVec(row+1, col+1, northwest[verc][tile]) ], -1.0);
                                rhs -= 1.0;
                            }
                        }

                        if (getFormulationType()==SDP_TILES)
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                        } else {
                            switch (getObjType())
                            {
                                case objTypes::NONE:
                                case objTypes::REDUNDANCY:
                                {
                                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                                    break;
                                }

                                case objTypes::MAXCOVER:
                                {
                                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 1.0);
                                    break;
                                }

                                case objTypes::MAXCONAREA:
                                case objTypes::MAXCONSQUARE:
                                case objTypes::MINPERIODIC:
                                {
                                    break;
                                }

                                default:
                                {
                                    std::cerr << "ERROR: Unknown objective!" << std::endl;
                                    exit(2);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        // diagonal connectivity left down
        for (int col=1; col<getWidth(); col++)
        {
            for (int row=0; row<getHeight()-1; row++)
            {
                for (int verc=0; verc<tileSet.getNumVertexColors(); verc++)
                {
                    if ((northeast[verc].size()>0) || (southwest[verc].size()>0))
                    {
                        optimizerInterface::linExpression linExp;

                        double rhs = 0.0;
                        for (tile::tcode tile=0; tile<(tile::tcode)southwest[verc].size(); tile++)
                        {
                            linExp.add(varsTiles[ matCoorToVec(row, col, southwest[verc][tile]) ]);
                            rhs += 1.0;
                        }

                        if (getObjType()==objTypes::MAXCOVER)
                        {
                            for (tile::tcode tile=0; tile<tileSet.getNumTiles(); tile++)
                            {
                                if (!(std::find(northeast[verc].begin(), northeast[verc].end(), tile) != northeast[verc].end()))
                                {
                                    linExp.add(varsTiles[ matCoorToVec(row+1, col-1, tile) ]);
                                }
                            }
                        } else {
                            for (tile::tcode tile=0; tile<(tile::tcode)northeast[verc].size(); tile++)
                            {
                                linExp.add(varsTiles[ matCoorToVec(row+1, col-1, northeast[verc][tile]) ], -1.0);
                                rhs -= 1.0;
                            }
                        }

                        if (getFormulationType()==SDP_TILES)
                        {
                            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, -rhs);
                        } else {
                            switch (getObjType())
                            {
                                case objTypes::NONE:
                                case objTypes::REDUNDANCY:
                                {
                                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                                    break;
                                }

                                case objTypes::MAXCOVER:
                                {
                                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 1.0);
                                    break;
                                }

                                case objTypes::MAXCONAREA:
                                case objTypes::MAXCONSQUARE:
                                case objTypes::MINPERIODIC:
                                {
                                    break;
                                }

                                default:
                                {
                                    std::cerr << "ERROR: Unknown objective!" << std::endl;
                                    exit(2);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }*/

    // add packing constraint
    if (getPacking())
    {
        for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
        {
            optimizerInterface::linExpression linExp;
            for (int row = 0; row < getHeight(); row++)
            {
                for (int col = 0; col < getWidth(); col++)
                {
                    linExp.add(varsTiles[matCoorToVec(row, col, tile)]);
                }
            }
            if (getFormulationType() == SDP_TILES)
            {
                model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, getWidth() * getHeight() * (-1) + 1);
            }
            else
            {
                model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 1.0);
            }
        }
    }

    // compact area constraint (rectangular region)
    switch (getObjType())
    {
    case objTypes::NONE:
    case objTypes::MAXCOVER:
    case objTypes::REDUNDANCY:
    case objTypes::NONE_TWEAKED:
    case objTypes::MAXCSP:
    {
        break;
    }

    case objTypes::MAXCONAREA:
    case objTypes::MAXCONSQUARE:
    case objTypes::MINPERIODIC:
    {
        if (getFormulationType() == SDP_TILES)
        {
            for (int row = 0; row < getHeight() - 1; row++)
            {
                for (int col = 0; col < getWidth() - 1; col++)
                {
                    optimizerInterface::linExpression linExp;
                    for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(row + 1, col, tile)]);
                        linExp.add(varsTiles[matCoorToVec(row, col + 1, tile)]);
                        linExp.add(varsTiles[matCoorToVec(row + 1, col + 1, tile)], -1.0);
                    }
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 2.0 - tileSet.getNumTiles());
                }
            }

            if (getObjType() == objTypes::MAXCONSQUARE)
            {
                for (int diag = 0; diag < std::min(getHeight(), getWidth()) - 1; diag++)
                {
                    optimizerInterface::linExpression linExp;
                    for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(diag + 1, diag, tile)]);
                        linExp.add(varsTiles[matCoorToVec(diag, diag + 1, tile)], -1.0);
                    }
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                }
            }
        }
        else
        {
            for (int row = 0; row < getHeight() - 1; row++)
            {
                for (int col = 0; col < getWidth() - 1; col++)
                {
                    optimizerInterface::linExpression linExp;
                    for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(row + 1, col, tile)]);
                        linExp.add(varsTiles[matCoorToVec(row, col + 1, tile)]);
                        linExp.add(varsTiles[matCoorToVec(row + 1, col + 1, tile)], -1.0);
                    }
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 1.0);
                }
            }

            if (getObjType() == objTypes::MAXCONSQUARE)
            {
                for (int diag = 0; diag < std::min(getHeight(), getWidth()) - 1; diag++)
                {
                    optimizerInterface::linExpression linExp;
                    for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(diag + 1, diag, tile)]);
                        linExp.add(varsTiles[matCoorToVec(diag, diag + 1, tile)], -1.0);
                    }
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                }
            }
        }

        break;
    }

    default:
    {
        std::cerr << "ERROR: Unknown objective!" << std::endl;
        exit(2);
        break;
    }
    }

    // arbitrary periodicity constraint (horizontal and vertical)
    if (getObjType() == objTypes::MINPERIODIC)
    {
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {

                for (int edgc = 0; edgc < tileSet.getNumEdgeColors(); edgc++)
                {

                    if ((north[edgc].size() > 0) || (south[edgc].size() > 0))
                    {

                        optimizerInterface::linExpression linExp;

                        for (tile::tcode tile = 0; tile < tileSet.getNumTiles(); tile++)
                        {
                            if (!(std::find(south[edgc].begin(), south[edgc].end(), tile) != south[edgc].end()))
                            {
                                linExp.add(varsTiles[matCoorToVec(row, col, tile)]);
                            }
                        }

                        for (tile::tcode tile = 0; tile < (tile::tcode)north[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(0, col, north[edgc][tile])]);
                        }

                        if (row + 1 < getHeight())
                        {
                            for (tile::tcode tile = 0; tile < tileSet.getNumTiles(); tile++)
                            {
                                linExp.add(varsTiles[matCoorToVec(row + 1, col, tile)], -1.0);
                            }
                        }

                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 1.0);
                    }

                    if ((east[edgc].size() > 0) || (west[edgc].size() > 0))
                    {
                        optimizerInterface::linExpression linExp;

                        for (tile::tcode tile = 0; tile < tileSet.getNumTiles(); tile++)
                        {
                            if (!(std::find(east[edgc].begin(), east[edgc].end(), tile) != east[edgc].end()))
                            {
                                linExp.add(varsTiles[matCoorToVec(row, col, tile)]);
                            }
                        }

                        for (tile::tcode tile = 0; tile < (tile::tcode)west[edgc].size(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, 0, west[edgc][tile])]);
                        }

                        if (col + 1 < getWidth())
                        {
                            for (tile::tcode tile = 0; tile < tileSet.getNumTiles(); tile++)
                            {
                                linExp.add(varsTiles[matCoorToVec(row, col + 1, tile)], -1.0);
                            }
                        }

                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, 1.0);
                    }
                }
            }
        }
    }

    // constrain matrix variable
    if ((getFormulationType() == SDP_TILES) | (getFormulationType() == SDP_TILESBIN))
    {
        // diagonal equal to one
        if (getFormulationType() == SDP_TILES)
        {
            for (int i = 0; i < getHeight() * getWidth() * tileSet.getNumTiles() + 1; i++)
            {
                std::shared_ptr<optimizerInterface::variable> var;
                lmi->getVarInMatrix(i, i, var);

                optimizerInterface::linExpression linExp;
                linExp.add(var);
                model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 1.0);
            }
        }
        else
        {
            std::shared_ptr<optimizerInterface::variable> var;
            lmi->getVarInMatrix(0, 0, var);
            optimizerInterface::linExpression linExp;
            linExp.add(var);
            model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 1.0);

            // integrality constraint
            for (int i = 0; i < getHeight() * getWidth() * tileSet.getNumTiles(); i++)
            {
                std::shared_ptr<optimizerInterface::variable> var;
                lmi->getVarInMatrix(i + 1, i + 1, var);

                optimizerInterface::linExpression linExp;
                linExp.add(var);
                linExp.add(varsTiles[i], -1.0);
                model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
            }
        }

        // first row/column equal to varTiles
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                for (tile::tcode tile = 0; tile < (tile::tcode)tileSet.getNumTiles(); tile++)
                {
                    std::shared_ptr<optimizerInterface::variable> var;
                    int varNumber = matCoorToVec(row, col, tile);
                    lmi->getVarInMatrix(0, varNumber + 1, var);

                    optimizerInterface::linExpression linExp;
                    linExp.add(var);
                    linExp.add(varsTiles[matCoorToVec(row, col, tile)], -1.0);
                    model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                }
            }
        }

        // enforce shortest-paths sparsity pattern
        if (getFormulationType() == SDP_TILESBIN)
        {
            // bool matrix of sparsity pattern
            std::vector<std::vector<bool>> sparsityPattern(getWidth() * getHeight() * tileSet.getNumTiles() + 1);
            for (unsigned int i = 0; i < sparsityPattern.size(); i++)
            {
                sparsityPattern[i].resize(sparsityPattern.size());
                for (unsigned int j = 0; j < sparsityPattern.size(); j++)
                {
                    sparsityPattern[i][j] = 0;
                }
            }

            // horizontal constraints
            std::vector<std::vector<double>> hDistanceMatrix;
            tileSet.getMinDistancesMatrixLR(hDistanceMatrix);

            for (int tileA = 0; tileA < tileSet.getNumTiles(); tileA++)
            {
                for (int tileB = 0; tileB < tileSet.getNumTiles(); tileB++)
                {
                    for (int row = 0; row < getHeight(); row++)
                    {
                        for (int col = 0; col < getWidth(); col++)
                        {
                            int tileAPos = matCoorToVec(row, col, tileA);
                            double distToTileB = hDistanceMatrix[tileA][tileB];
                            double negDistToTileB = hDistanceMatrix[tileB][tileA];
                            for (int moveDist = 0; moveDist < (int)distToTileB; moveDist++)
                            {
                                if ((col + moveDist) < getWidth())
                                {
                                    int tileBPos = matCoorToVec(row, col + moveDist, tileB);
                                    if (tileAPos != tileBPos)
                                    {
                                        sparsityPattern[tileAPos + 1][tileBPos + 1] = true;
                                        sparsityPattern[tileBPos + 1][tileAPos + 1] = true;
                                    }
                                }
                            }
                            for (int moveDist = 0; moveDist < (int)negDistToTileB; moveDist++)
                            {
                                if ((col - moveDist) >= 0)
                                {
                                    int tileBPos = matCoorToVec(row, col - moveDist, tileB);
                                    if (tileAPos != tileBPos)
                                    {
                                        sparsityPattern[tileAPos + 1][tileBPos + 1] = true;
                                        sparsityPattern[tileBPos + 1][tileAPos + 1] = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // vertical constraints
            std::vector<std::vector<double>> vDistanceMatrix;
            tileSet.getMinDistancesMatrixUD(vDistanceMatrix);
            for (int tileA = 0; tileA < tileSet.getNumTiles(); tileA++)
            {
                for (int tileB = 0; tileB < tileSet.getNumTiles(); tileB++)
                {
                    for (int row = 0; row < getHeight(); row++)
                    {
                        for (int col = 0; col < getWidth(); col++)
                        {
                            int tileAPos = matCoorToVec(row, col, tileA);
                            double distToTileB = vDistanceMatrix[tileA][tileB];
                            double negDistToTileB = hDistanceMatrix[tileB][tileA];
                            for (int moveDist = 0; moveDist < (int)distToTileB; moveDist++)
                            {
                                if ((row + moveDist) < getHeight())
                                {
                                    int tileBPos = matCoorToVec(row + moveDist, col, tileB);
                                    if (tileAPos != tileBPos)
                                    {
                                        sparsityPattern[tileAPos + 1][tileBPos + 1] = true;
                                        sparsityPattern[tileBPos + 1][tileAPos + 1] = true;
                                    }
                                }
                            }
                            for (int moveDist = 0; moveDist < (int)negDistToTileB; moveDist++)
                            {
                                if ((row - moveDist) >= 0)
                                {
                                    int tileBPos = matCoorToVec(row - moveDist, col, tileB);
                                    if (tileAPos != tileBPos)
                                    {
                                        sparsityPattern[tileAPos + 1][tileBPos + 1] = true;
                                        sparsityPattern[tileBPos + 1][tileAPos + 1] = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (unsigned int i = 0; i < sparsityPattern.size(); i++)
            {
                for (unsigned int j = i; j < sparsityPattern[i].size(); j++)
                {
                    if (sparsityPattern[i][j] == true)
                    {
                        optimizerInterface::linExpression linExp;
                        std::shared_ptr<optimizerInterface::variable> var;
                        lmi->getVarInMatrix(i, j, var);
                        linExp.add(var);
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);
                    }
                }
            }

            std::ofstream myfile("sparsity_pattern.txt");
            for (unsigned int i = 0; i < sparsityPattern.size(); i++)
            {
                for (unsigned int j = 0; j < sparsityPattern[i].size(); j++)
                {
                    myfile << sparsityPattern[i][j] << "\t";
                }
                myfile << std::endl;
            }
            myfile.close();
        }
    }

    // boundary conditions
    for (int bc = 0; bc < (int)boundaryConditions.size(); bc++)
    {
        std::vector<int> &boundaryCondition = boundaryConditions[bc];
        optimizerInterface::linExpression linExp;
        linExp.add(varsTiles[matCoorToVec(boundaryCondition[0], boundaryCondition[1], boundaryCondition[2])]);
        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::EQUAL, 1.0);
    }

    // objective
    switch (getObjType())
    {
    case objTypes::NONE:
    case objTypes::REDUNDANCY:
    case objTypes::NONE_TWEAKED:
    {
        model->setObjectiveSense(optimizerInterface::solverModel::objectiveSenses::MINIMIZE);
        break;
    }

    case objTypes::MAXCSP:
    {
        optimizerInterface::linExpression linExp;
        for (unsigned int i = 0; i < varsEdgesV.size(); i++)
        {
            linExp.add(varsEdgesV[i], -1.0);
        }

        for (unsigned int i = 0; i < varsEdgesH.size(); i++)
        {
            linExp.add(varsEdgesH[i], -1.0);
        }

        double constTerm = getWidth() * (getHeight() - 1) + (getWidth() - 1) * getHeight();

        model->addLinearObjective(linExp, optimizerInterface::solverModel::objectiveSenses::MAXIMIZE, constTerm);
        break;
    }

    case objTypes::MAXCONAREA:
    case objTypes::MAXCOVER:
    {
        double constTerm = 0.0;
        optimizerInterface::linExpression linExp;

        switch (getFormulationType())
        {
        case formulationTypes::LP_TILES:
        case formulationTypes::LP_BNB_TILES:
        case formulationTypes::QP_TILES:
        case formulationTypes::SDP_TILESBIN:
        {
            for (int row = 0; row < getHeight(); row++)
            {
                for (int col = 0; col < getWidth(); col++)
                {
                    for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(row, col, tile)]);
                    }
                }
            }
            break;
        }

        case formulationTypes::SDP_TILES:
        {
            for (int row = 0; row < getHeight(); row++)
            {
                for (int col = 0; col < getWidth(); col++)
                {
                    for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                    {
                        linExp.add(varsTiles[matCoorToVec(row, col, tile)], 0.5);
                    }
                }
            }
            constTerm = 0.5 * tileSet.getNumTiles() * getWidth() * getHeight();
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
            exit(2);
            break;
        }
        }
        model->addLinearObjective(linExp, optimizerInterface::solverModel::objectiveSenses::MAXIMIZE, constTerm);
        model->setObjectiveSense(optimizerInterface::solverModel::objectiveSenses::MAXIMIZE);

        break;
    }

    case objTypes::MAXCONSQUARE:
    {
        double constTerm = 0.0;
        optimizerInterface::linExpression linExp;

        switch (getFormulationType())
        {
        case formulationTypes::LP_TILES:
        case formulationTypes::LP_BNB_TILES:
        case formulationTypes::QP_TILES:
        case formulationTypes::SDP_TILESBIN:
        {
            for (int row = 0; row < getHeight(); row++)
            {
                for (int col = 0; col < getWidth(); col++)
                {
                    if (row == col)
                    {
                        for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col, tile)]);
                        }
                    }
                }
            }
        }

        case formulationTypes::SDP_TILES:
        {
            for (int row = 0; row < getHeight(); row++)
            {
                for (int col = 0; col < getWidth(); col++)
                {
                    if (row == col)
                    {
                        for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                        {
                            linExp.add(varsTiles[matCoorToVec(row, col, tile)], 0.5);
                            constTerm += 0.5;
                        }
                    }
                }
            }
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
            exit(2);
            break;
        }
        }

        model->addLinearObjective(linExp, optimizerInterface::solverModel::objectiveSenses::MAXIMIZE, constTerm);
        model->setObjectiveSense(optimizerInterface::solverModel::objectiveSenses::MAXIMIZE);
        break;
    }

    case objTypes::MINPERIODIC:
    {
        optimizerInterface::linExpression linExp;
        for (int row = 0; row < getHeight(); row++)
        {
            for (int col = 0; col < getWidth(); col++)
            {
                for (int tile = 0; tile < tileSet.getNumTiles(); tile++)
                {
                    linExp.add(varsTiles[matCoorToVec(row, col, tile)]);
                }
            }
        }
        model->addLinearObjective(linExp, optimizerInterface::solverModel::objectiveSenses::MINIMIZE);
        model->setObjectiveSense(optimizerInterface::solverModel::objectiveSenses::MINIMIZE);
        break;
    }

    default:
    {
        std::cerr << "ERROR: Unknown objective!" << std::endl;
        exit(2);
        break;
    }
    }

    // optimize
    std::vector<bool> unusableTiles(tileSet.getNumTiles(), true); // let us assume that all the tiles are unusable; feasible solution will make the tile usable again
    bool modelFeasible = true;
    int nsol = -1;

    while (modelFeasible)
    {
        switch (getFormulationType())
        {
        case LP_TILES:
        case QP_TILES:
        {
            model->optimize();

            // check solution status
            if (model->getSolverStatus() == optimizerInterface::solverModel::solversStatuses::INFEASIBLE)
            {
                modelFeasible = false;
            }
            else
            {
                setFeasibility(true);

                if ((getAllSolutions() == false) && (getSolutionsCount() <= nsol + 2))
                {
                    modelFeasible = false;
                }

                nsol += 1;

                if (nsol + 1 < getSolutionsCount())
                {
                    std::cout << "Found feasible solution " << nsol + 1 << "." << std::endl;
                }

                // recover solution
                std::vector<std::vector<tile::tcode>> assMat;
                assMat.resize(getHeight());
                for (int row = 0; row < getHeight(); row++)
                {
                    assMat[row].resize(getWidth());
                }

                optimizerInterface::linExpression linExp;
                double rhs = 0.0;
                for (int row = 0; row < getHeight(); row++)
                {
                    for (int col = 0; col < getWidth(); col++)
                    {
                        tile::tcode curTile = -1;
                        for (tile::tcode tile = 0; tile < (tile::tcode)tileSet.getNumTiles(); tile++)
                        {
                            std::shared_ptr<optimizerInterface::variable> var;
                            int varNumber = matCoorToVec(row, col, tile);
                            model->getVarByArrayPosition(var, varNumber);
                            if (std::abs((var->getValue() - 1.0)) <= 1e-6)
                            {
                                curTile = tile;
                                if (nsol + 1 < getSolutionsCount())
                                {
                                    linExp.add(var);
                                    rhs += 1.0;
                                }
                                break;
                            }
                        }
                        assMat[row][col] = curTile;
                    }
                }

                // save solution
                if (getObjType() != objTypes::REDUNDANCY)
                {
                    assembly.push_back(assMat);
                }

                // plotTilingSVG(std::to_string(nsol), nsol);

                // avoid obtaining equal solution
                if (getObjType() == objTypes::REDUNDANCY)
                {
                    optimizerInterface::linExpression centerTile;
                    centerTile.add(varsTiles[matCoorToVec((getHeight() - 1) / 2, (getWidth() - 1) / 2, assMat[(getHeight() - 1) / 2][(getWidth() - 1) / 2])]);
                    model->addLinearConstraint(centerTile, optimizerInterface::solverModel::constraintSense::EQUAL, 0.0);

                    unusableTiles[assMat[(getHeight() - 1) / 2][(getWidth() - 1) / 2]] = false;

                    std::cout << "Tile " << assMat[(getHeight() - 1) / 2][(getWidth() - 1) / 2] << " is usable." << std::endl;
                }
                else
                {
                    if (nsol + 1 < getSolutionsCount())
                    {
                        std::cout << rhs << std::endl;
                        model->addLinearConstraint(linExp, optimizerInterface::solverModel::constraintSense::LESSEQUAL, rhs - 1.0);
                    }
                }
            }

            if ((model->getSolverStatus() == optimizerInterface::solverModel::solversStatuses::INFEASIBLE) & (assembly.size() == 0))
            {
                setFeasibility(false);
            }
            break;
        }

        case LP_BNB_TILES:
        case SDP_TILESBIN:
        case SDP_TILES:
        {
            optimizerInterface::branchAndBoundNode::branchingRules bRule;
            int solC = getSolutionsCount();

            switch (getObjType())
            {
            case objTypes::NONE:
            case objTypes::REDUNDANCY:
            case objTypes::NONE_TWEAKED:
            {
                bRule = optimizerInterface::branchAndBoundNode::branchingRules::MIN_INFEASIBILITY;
                break;
            }

            case objTypes::MAXCONAREA:
            case objTypes::MAXCOVER:
            case objTypes::MAXCONSQUARE:
            case objTypes::MAXCSP:
            {
                bRule = optimizerInterface::branchAndBoundNode::branchingRules::MAX_OBJECTIVE;
                break;
            }

            case objTypes::MINPERIODIC:
            {
                bRule = optimizerInterface::branchAndBoundNode::branchingRules::MIN_OBJECTIVE;
                break;
            }

            default:
            {
                std::cerr << "ERROR: Unknown objective!" << std::endl;
                exit(2);
                break;
            }
            }

            optimizerInterface::branchAndBoundTree bTree = optimizerInterface::branchAndBoundTree(model, bRule, solC);

            bTree.solve();

            // recover solution
            unsigned int solCount = bTree.getFoundSolutionsCount();
            if (solCount == 0)
            {
                modelFeasible = false;
                setFeasibility(false);
            }
            else
            {
                for (unsigned int sol = 0; sol < solCount; sol++)
                {
                    bTree.retrieveSolution(model, sol);
                    std::vector<std::vector<tile::tcode>> assMat;
                    assMat.resize(getHeight());
                    for (int row = 0; row < getHeight(); row++)
                    {
                        assMat[row].resize(getWidth());
                    }

                    for (int row = 0; row < getHeight(); row++)
                    {
                        for (int col = 0; col < getWidth(); col++)
                        {
                            tile::tcode curTile = -1;
                            for (tile::tcode tile = 0; tile < (tile::tcode)tileSet.getNumTiles(); tile++)
                            {
                                std::shared_ptr<optimizerInterface::variable> var;
                                int varNumber = matCoorToVec(row, col, tile);
                                model->getVarByArrayPosition(var, varNumber);
                                if (std::abs((var->getValue() - 1.0)) <= 1e-6)
                                {
                                    curTile = tile;
                                    break;
                                }
                            }
                            assMat[row][col] = curTile;
                        }
                    }

                    assembly.push_back(assMat);
                }
            }
            modelFeasible = false;
            setFeasibility(true);
            break;
        }

        default:
        {
            std::cerr << "ERROR: Unknown formulation type!" << std::endl;
            exit(2);
            break;
        }
        }
    }

    if (getObjType() == objTypes::REDUNDANCY)
    {
        int numRedundant = 0;
        for (tile::tcode til = 0; til < tileSet.getNumTiles(); til++)
        {
            if (unusableTiles[til] == true)
            {
                numRedundant += 1;
                tileSet.setRedundantTile(til);
                std::cout << "Tile " << til << " is redundant!" << std::endl;
            }
        }
        tileSet.removeRedundantTiles();
        std::cout << "Removed " << numRedundant << " redundant tiles." << std::endl;
    }
}

void tiling::optimize(formulationTypes formulation, optimizerInterface::solverModel::solvers solver)
{
    tiling::setFormulationType(formulation);
    std::cout << std::endl
              << "### OPTIMIZATION:" << std::endl;
    tiling::callSolver(solver);
}

tiling::tiling(std::string fname)
{
    readTilingFile(fname);
    tiling::setFormulationType(LP_TILES);
    periodic = false;
    allSolutions = 0;
    feasible = 0;
    packing = false;
    setObjType(objTypes::NONE);
    setSolutionsCount(1);
}

int tiling::readTilingFile(std::string fname)
{
    std::ifstream infile(fname.c_str());
    std::string line;
    int counter = 0;
    std::string path = "./";

    if (!infile.is_open())
    {
        path = "./tilings/";
        std::string newName = path + fname;
        infile.open(newName.c_str());
    }

    if (!infile.is_open())
    {
        std::string folder = fname;
        folder.erase(folder.length() - 5);
        path = "./tilings/" + folder + "/";
        std::string newName = path + fname;
        infile.open(newName.c_str());
    }

    if (!infile.is_open())
    {
        std::cerr << "ERROR in tiling::readTilingFile(): The file " << fname << " failed to open." << std::endl;
        exit(2);
    }

    std::vector<std::vector<tile::tcode>> assMat;
    while (std::getline(infile, line))
    {
        std::stringstream ss(line);

        if ((line.find("#") == line.npos) &&
            (line.find("/") == line.npos) &&
            (line.find("%") == line.npos) &&
            (line.find_first_not_of("\t\n ") != line.npos) &&
            (line.length() != 0))
        {
            std::string tSetFileName;
            int tW, tH;
            std::string opt;

            switch (counter)
            {
            // tile set name
            case 0:
                ss >> tSetFileName;
                tileSet.readTsetFile(tSetFileName + ".tset");
                break;

            // dimensions
            case 1:
                ss >> tH;
                setHeight(tH);
                ss >> tW;
                setWidth(tW);

                assMat.resize(getHeight());
                for (int row = 0; row < getHeight(); row++)
                {
                    assMat[row].resize(getWidth());
                }
                break;

            default:
                // tile numbers
                if (counter < 2 + getHeight())
                {
                    int row = counter - 2;
                    for (int col = 0; col < getWidth(); col++)
                    {
                        tile::tcode tileNum;
                        ss >> tileNum;
                        assMat[row][col] = tileNum;
                    }
                    if (counter == (1 + getHeight()))
                    {
                        assembly.push_back(assMat);
                    }
                }
                else
                {
                    // options
                    ss >> opt;
                    std::string delimiter = "=";
                    std::string key = opt.substr(0, opt.find(delimiter));
                    std::string value = opt.substr(opt.find(delimiter) + 1, opt.length());
                    if (!key.compare("TILE_FORMAT"))
                    {
                        if (!value.compare("COLOR"))
                        {
                            tileSet.setPlotMode(tile::svgPlotMode::COLOR);
                        }
                        else if (!value.compare("TEXT"))
                        {
                            tileSet.setPlotMode(tile::svgPlotMode::TEXT);
                        }
                        else if (!value.compare("IMAGE"))
                        {
                            tileSet.setPlotMode(tile::svgPlotMode::SVGDATA);
                        }
                        else if (!value.compare("IMAGECOLOR"))
                        {
                            tileSet.setPlotMode(tile::svgPlotMode::SVGCOLOR);
                        }
                        else
                        {
                            std::cerr << "ERROR in tiling::readTilingFile(): Unknown TILE_FORMAT option." << std::endl;
                            exit(2);
                        }
                    }

                    break;
                }
            }
            counter += 1;
        }
    }

    if (infile.fail() && !infile.eof())
    {
        std::cerr << "ERROR: The file " << fname << " is not a valid tiling format." << std::endl;
        exit(2);
    }

    infile.close();

    return 0;
}

tiling::tiling(std::string fname, int width, int height)
{
    tileSet.readTsetFile(fname);
    tiling::setWidth(width);
    tiling::setHeight(height);
    tiling::setFormulationType(LP_TILES);
    periodic = false;
    allSolutions = 0;
    feasible = 0;
    packing = false;
    setObjType(objTypes::NONE);
    setSolutionsCount(1);
}

void tiling::plotTilings() const
{
    for (unsigned int num = 0; num < assembly.size(); num++)
    {
        plotTiling((int)num);
    }
}

void tiling::plotTiling(int num) const
{
    std::cout << std::endl
              << "### TILING:" << std::endl;

    for (int row = 0; row < getHeight(); row++)
    {
        tileSet.plotTiles(assembly[num][row]);
    }
    std::cout << std::endl;
}

void tiling::plotTilingsSVG(std::string fname) const
{
    for (unsigned int num = 0; num < assembly.size(); num++)
    {
        plotTilingSVG(fname + "_" + std::to_string(num), (int)num);
    }
}

void tiling::plotTilingSVG(std::string fname, int num) const
{
    fname = fname + ".svg";
    std::ofstream myfile(fname.c_str());

    myfile << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" << std::endl;
    myfile << "<svg width=\"" << getWidth() * 100 << "\" height=\"" << getHeight() * 100
           << "\" style=\"shape-rendering:crispEdges\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">" << std::endl;
    myfile << "<desc>Wang tiling exported by tiling_optimizer</desc>" << std::endl;

    // define tiles
    myfile << "<defs>" << std::endl;
    for (int i = 0; i < tileSet.getNumTiles(); i++)
    {
        std::string svgTile = tileSet.getSVGTileDefinition(i);
        myfile << svgTile << std::endl;
    }
    myfile << "</defs>" << std::endl;

    for (int row = 0; row < getHeight(); row++)
    {
        for (int col = 0; col < getWidth(); col++)
        {
            if (assembly[num][row][col] > -1)
            {
                std::string usage = "<use x=\"" + std::to_string(col * 100.0) + "\" y=\"" + std::to_string(row * 100.0) + "\" xlink:href=\"#T" + std::to_string(assembly[num][row][col]) + "\"/>";
                myfile << usage << std::endl;
            }
        }
    }

    myfile << "</svg>" << std::endl;
    myfile.close();
    std::cout << "File " << fname.c_str() << " written successfully." << std::endl;
}