#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <memory>

class vertextile : public tile
{
private:
    int numVertexColors;

public:
    void printTile(int line) const;
    std::string getSvgData(int id, tile::svgPlotMode spm) const;

    tcode getNorth() const { return getNorthWest() + getNorthEast() * numVertexColors; };
    tcode getSouth() const { return getSouthWest() + getSouthEast() * numVertexColors; };
    tcode getEast() const { return getNorthEast() + getSouthEast() * numVertexColors; };
    tcode getWest() const { return getNorthWest() + getSouthWest() * numVertexColors; };
    tcode getNorthEast() const { return code4; };
    tcode getNorthWest() const { return code1; };
    tcode getSouthEast() const { return code3; };
    tcode getSouthWest() const { return code2; };

    using tile::tile;

    vertextile(tcode northwest, tcode southwest, tcode southeast, tcode northeast, int numVertexColors);

    ~vertextile(){};

    std::shared_ptr<tile> clone() const;
};

std::shared_ptr<tile> vertextile::clone() const
{
    return std::make_shared<vertextile>(getNorthWest(), getSouthWest(), getSouthEast(), getNorthEast(), numVertexColors);
}

std::string vertextile::getSvgData(int id, tile::svgPlotMode spm) const
{
    std::string out = "";

    switch (spm)
    {
    case tile::svgPlotMode::SVGDATA:
    {
        if (svgData.empty())
        {
            std::cerr << "ERROR in vertextile::getSvgData(): No svgData present!" << std::endl;
            exit(2);
        }
        out += svgData;
        break;
    }

    case tile::svgPlotMode::COLOR:
    case tile::svgPlotMode::SVGCOLOR:
    {
        double d = 33.0;
        double opacity = 1.0;
        out += "<g id=\"T" + std::to_string(id) + "\">\n";
        if (spm == tile::svgPlotMode::SVGCOLOR)
        {
            if (svgData.empty())
            {
                std::cerr << "ERROR in edgetile::getSvgData(): No svgData present!" << std::endl;
                exit(2);
            }
            out += svgData;
            opacity = 0.4;
        }

        // northwest
        // northwest
        out += "<polygon points=\"0.0,0.0 " + std::to_string(d) + ",0.0 0.0," + std::to_string(d) + "\" style=\"fill:" + printColorRGB(getNorthWest()) + ";stroke:none;stroke-width:0\" fill-opacity=\"" + std::to_string(opacity) + "\"/>\n";
        // northeast
        out += "<polygon points=\"" + std::to_string(100.0) + ",0.0 " + std::to_string(100.0 - d) + ",0.0 100.0," + std::to_string(d) + "\" style=\"fill:" + printColorRGB(getNorthEast()) + ";stroke:none;stroke-width:0\" fill-opacity=\"" + std::to_string(opacity) + "\"/>\n";
        // southwest
        out += "<polygon points=\"0.0,100.0 " + std::to_string(d) + ",100.0 0.0," + std::to_string(100.0 - d) + "\" style=\"fill:" + printColorRGB(getSouthWest()) + ";stroke:none;stroke-width:0\" fill-opacity=\"" + std::to_string(opacity) + "\"/>\n";
        // southeast
        out += "<polygon points=\"100.0,100.0 " + std::to_string(100.0 - d) + ",100.0 100.0," + std::to_string(100.0 - d) + "\" style=\"fill:" + printColorRGB(getSouthEast()) + ";stroke:none;stroke-width:0\" fill-opacity=\"" + std::to_string(opacity) + "\"/>\n";
        // outer shape
        if (spm == tile::svgPlotMode::COLOR)
        {
            // outer shape
            out += "<rect x=\"0.0\" y=\"0.0\" width=\"1.0\" height=\"100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
            out += "<rect x=\"0.0\" y=\"0.0\" width=\"100.0\" height=\"1.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
            out += "<rect x=\"99.0\" y=\"0.0\" width=\"1.0\" height=\"100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
            out += "<rect x=\"0.0\" y=\"99.0\" width=\"100.0\" height=\"1.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
        }
        out += "</g>";
        break;
    }

    case tile::svgPlotMode::TEXT:
    {
        out += "<g id=\"T" + std::to_string(id) + "\">\n";
        out += "<rect x=\"0.0\" y=\"0.0\" width=\"1.0\" height=\"100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
        out += "<rect x=\"0.0\" y=\"0.0\" width=\"100.0\" height=\"1.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
        out += "<rect x=\"99.0\" y=\"0.0\" width=\"1.0\" height=\"100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
        out += "<rect x=\"0.0\" y=\"99.0\" width=\"100.0\" height=\"1.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
        out += "<rect x=\"49.5\" y=\"0.0\" width=\"1.0\" height=\"100.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
        out += "<rect x=\"0.0\" y=\"49.5\" width=\"100.0\" height=\"1.0\" style=\"fill:rgb(0,0,0);stroke-width:1;stroke:none\" />\n";
        out += "<text x=\"25.0\" y=\"25.0\" text-anchor=\"middle\" style=\"font-size: 35px;font-family:Times,serif;\" dominant-baseline=\"middle\">" + std::to_string(getNorthWest()) + "</text>\n";
        out += "<text x=\"25.0\" y=\"75.0\" text-anchor=\"middle\" style=\"font-size: 35px;font-family:Times,serif;\" dominant-baseline=\"middle\">" + std::to_string(getSouthWest()) + "</text>\n";
        out += "<text x=\"75.0\" y=\"25.0\" text-anchor=\"middle\" style=\"font-size: 35px;font-family:Times,serif;\" dominant-baseline=\"middle\">" + std::to_string(getNorthEast()) + "</text>\n";
        out += "<text x=\"75.0\" y=\"75.0\" text-anchor=\"middle\" style=\"font-size: 35px;font-family:Times,serif;\" dominant-baseline=\"middle\">" + std::to_string(getSouthEast()) + "</text>\n";
        out += "</g>";
        break;
    }

    default:
    {
        std::cerr << "ERROR in vertextile::getSvgData(): Unknown svgPlotMode!" << std::endl;
        exit(2);
        break;
    }
    }
    return out;
}

vertextile::vertextile(tcode northwest, tcode southwest, tcode southeast, tcode northeast, int numVertexColors)
{
    setValues(northwest, southwest, southeast, northeast);
    vertextile::numVertexColors = numVertexColors;
}

void vertextile::printTile(int line) const
{
    if (line == 0)
        std::cout << printColor(getNorthWest()) << "┏" << printColor(-1) << "════" << printColor(getNorthEast()) << "┓" << printColor(-1);

    if ((line == 1) || (line == 2))
        std::cout << "║"
                  << "░░░░"
                  << "║";

    if (line == 3)
        std::cout << printColor(getSouthWest()) << "┗" << printColor(-1) << "════" << printColor(getSouthEast()) << "┛" << printColor(-1);
}